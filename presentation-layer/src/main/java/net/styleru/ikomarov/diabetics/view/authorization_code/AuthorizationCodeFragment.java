package net.styleru.ikomarov.diabetics.view.authorization_code;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.transition.AutoTransition;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.view.ViperFragment;
import net.styleru.ikomarov.diabetics.contracts.screens.ScreenContract;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IResourceManagersProvider;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IValidationsProvider;
import net.styleru.ikomarov.diabetics.presenter.authorization_code.AuthorizationCodePresenter;
import net.styleru.ikomarov.diabetics.routers.authorization.AuthorizationRouter;
import net.styleru.ikomarov.diabetics.ui.transitions.SequentialFadeTransition;
import net.styleru.ikomarov.diabetics.view.authorization.IAuthorizationView;
import net.styleru.ikomarov.domain_layer.di.providers.IAuthenticationModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IExecutionEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.use_cases.session.CodeAuthenticationUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.session.PhoneAuthenticationUseCase;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by i_komarov on 02.06.17.
 */

public class AuthorizationCodeFragment extends ViperFragment<IAuthorizationCodeView, AuthorizationRouter, AuthorizationCodePresenter> implements IAuthorizationCodeView {

    private AppCompatTextView codeRecipientHolder;
    private AppCompatTextView wrongPhoneButton;
    private TextInputEditText codeInputHolder;
    private AppCompatButton continueButton;
    private MaterialProgressBar codeAuthenticationProgressBar;
    private AppCompatButton resendCodeButton;
    private MaterialProgressBar phoneAuthenticationProgressBar;

    private AuthorizationCodeFragmentState state;

    @NonNull
    public static Fragment newInstance(@NonNull String phone) {
        Fragment fragment = new AuthorizationCodeFragment();
        fragment.setArguments(new AuthorizationCodeFragmentState(phone).asBundle());

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        state = new AuthorizationCodeFragmentState(getArguments(), savedInstanceState);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementEnterTransition(new AutoTransition().setDuration(250));
            setSharedElementReturnTransition(new AutoTransition().setDuration(250));
            setEnterTransition(new SequentialFadeTransition().setDuration(1000));
            setReturnTransition(new SequentialFadeTransition().setDuration(1000));
        }
    }

    @Override
    public void onPhoneAuthenticationStarted() {
        TransitionManager.beginDelayedTransition((ViewGroup) getView());

        phoneAuthenticationProgressBar.setVisibility(View.VISIBLE);
        resendCodeButton.setText("");
        resendCodeButton.setEnabled(false);
        continueButton.setEnabled(false);
    }

    @Override
    public void onPhoneAuthenticationSuccess() {

    }

    @Override
    public void onPhoneAuthenticationFinished() {
        TransitionManager.beginDelayedTransition((ViewGroup) getView());

        continueButton.setEnabled(true);
        resendCodeButton.setEnabled(true);
        resendCodeButton.setText(R.string.action_code_resend);
        phoneAuthenticationProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void setCodeError(@NonNull String message) {
        codeInputHolder.setError(message);
    }

    @Override
    public void onCodeAuthenticationStarted() {
        TransitionManager.beginDelayedTransition((ViewGroup) getView());

        codeAuthenticationProgressBar.setVisibility(View.VISIBLE);
        continueButton.setText("");
        continueButton.setEnabled(false);
        resendCodeButton.setEnabled(false);
    }

    @Override
    public void onCodeAuthenticationFinished() {
        TransitionManager.beginDelayedTransition((ViewGroup) getView());

        resendCodeButton.setEnabled(true);
        continueButton.setEnabled(true);
        continueButton.setText(R.string.action_continue);
        codeAuthenticationProgressBar.setVisibility(View.GONE);
    }

    @Override
    protected void bindUserInterface(View root) {
        codeRecipientHolder = (AppCompatTextView) root.findViewById(R.id.fragment_authorization_code_phone_label);
        wrongPhoneButton = (AppCompatTextView) root.findViewById(R.id.fragment_authorization_code_wrong_phone);
        codeInputHolder = (TextInputEditText) root.findViewById(R.id.fragment_authorization_code_enter_code_field);
        continueButton = (AppCompatButton) root.findViewById(R.id.fragment_authorization_code_continue_button);
        codeAuthenticationProgressBar = (MaterialProgressBar) root.findViewById(R.id.fragment_authorization_code_code_authentication_progressbar);
        resendCodeButton = (AppCompatButton) root.findViewById(R.id.fragment_authorization_code_resend_code_button);
        phoneAuthenticationProgressBar = (MaterialProgressBar) root.findViewById(R.id.fragment_authorization_code_phone_authentication_progressbar);

        codeRecipientHolder.setText(String.format(getResources().getString(R.string.label_code_recipient), state.getPhone()));

        wrongPhoneButton.setOnClickListener(this::onWrongPhoneButtonClicks);
        continueButton.setOnClickListener(this::onContinueButtonClicks);
        resendCodeButton.setOnClickListener(this::onResendCodeButtonClicks);

    }

    @Override
    public void onActivityResultDelivered(int requestCode, int resultCode, Intent data) {
        //just ignore, nothing interesting lays here
    }

    @Override
    protected void onViewStateDelivered(Bundle savedInstanceState) {
        //just ignore, nothing interesting lays here
    }

    private void onWrongPhoneButtonClicks(View view) {
        getPresenter().onWrongPhoneButtonClicks();
    }

    private void onContinueButtonClicks(View view) {
        Editable code = codeInputHolder.getText();
        getPresenter().onContinueButtonClicks(state.getPhone(), code != null ? code.toString() : null);
    }

    private void onResendCodeButtonClicks(View view) {
        getPresenter().onResendCodeButtonClicks(state.getPhone());
    }

    @NonNull
    @Override
    protected IAuthorizationCodeView provideView() {
        return this;
    }

    @NonNull
    @Override
    protected AuthorizationRouter provideRouter() {
        return ((IAuthorizationView) getActivity()).getRouter();
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.fragment_authroization_code;
    }

    @Override
    protected int provideScreenId() {
        return ScreenContract.AUTHORIZATION_CODE.getCode();
    }

    @NonNull
    @Override
    protected ViperPresenter.Factory<IAuthorizationCodeView, AuthorizationRouter, AuthorizationCodePresenter> provideFactory() {
        IAuthenticationModelProvider modelProvider = (IAuthenticationModelProvider) getActivity().getApplication();
        IExecutionEnvironmentProvider executionEnvironmentProvider = (IExecutionEnvironmentProvider) getActivity().getApplication();
        IResourceManagersProvider resourceManagersProvider = (IResourceManagersProvider) getActivity();
        IValidationsProvider validationsProvider = (IValidationsProvider) getActivity();

        return new AuthorizationCodePresenter.Factory(
                resourceManagersProvider.provideNotificationsManager(),
                new CodeAuthenticationUseCase(
                        modelProvider.provideAuthenticationRepository(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler(),
                        validationsProvider.provideCodeValidationStrategy()
                ),
                new PhoneAuthenticationUseCase(
                        modelProvider.provideAuthenticationRepository(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler(),
                        validationsProvider.providerPhoneValidationStrategy()
                )
        );
    }
}
