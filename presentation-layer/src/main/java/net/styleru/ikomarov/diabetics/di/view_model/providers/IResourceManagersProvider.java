package net.styleru.ikomarov.diabetics.di.view_model.providers;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.managers.resources.labels.ILabelsManager;
import net.styleru.ikomarov.diabetics.managers.resources.notifications.INotificationsManager;

/**
 * Created by i_komarov on 12.05.17.
 */

public interface IResourceManagersProvider {

    @NonNull
    ILabelsManager provideLabelsManager();

    @NonNull
    INotificationsManager provideNotificationsManager();
}
