package net.styleru.ikomarov.diabetics.view_model.toolbar;

import android.support.annotation.StringRes;

/**
 * Created by i_komarov on 11.05.17.
 */

public interface IToolbarModelMutator {

    void setTitle(String title);

    void setSubtitle(String subtitle);
}
