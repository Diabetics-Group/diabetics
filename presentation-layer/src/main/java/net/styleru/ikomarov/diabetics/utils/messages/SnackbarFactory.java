package net.styleru.ikomarov.diabetics.utils.messages;

import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by i_komarov on 14.05.17.
 */

public class SnackbarFactory {

    private final View root;

    public SnackbarFactory(View root) {
        this.root = root;
    }

    public Snackbar create(CharSequence text, int duration) {
        return Snackbar.make(root, text, duration);
    }

    public Snackbar create(@StringRes int text, int duration) {
        return Snackbar.make(root, text, duration);
    }
}
