package net.styleru.ikomarov.diabetics.adapter.categories;

import android.os.Parcelable;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.utils.serialization.ParcelableHashMap;
import net.styleru.ikomarov.diabetics.utils.serialization.ParcelableStringHashMap;
import net.styleru.ikomarov.diabetics.view_holder.categories.CategoryViewHolder;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by i_komarov on 03.05.17.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    private ParcelableStringHashMap<CategoryViewObject> itemMap;

    private SortedList<CategoryViewObject> items;

    private CategoryViewHolder.InteractionCallbacks callbacks;

    public CategoriesAdapter() {
        itemMap = new ParcelableStringHashMap<>(CategoryViewObject.class);
        init(itemMap);
    }

    private void init(Map<String, CategoryViewObject> itemMap) {
        items = new SortedList<>(CategoryViewObject.class, new CategoriesAdapterCallbacks(this));
        items.addAll(itemMap.values());
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_category, parent, false));
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        holder.bind(position, items.get(position), callbacks);
    }

    @Override
    public void onViewAttachedToWindow(CategoryViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bindCallbacks();
    }

    @Override
    public void onViewDetachedFromWindow(CategoryViewHolder holder) {
        holder.unbindCallbacks();
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setCallbacks(CategoryViewHolder.InteractionCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void addItems(List<CategoryViewObject> items) {
        boolean allExists = true;

        for(int i = 0; i < items.size(); i++) {
            CategoryViewObject newItem = items.get(i);
            CategoryViewObject oldItem = itemMap.get(newItem.getId());

            if(oldItem == null) {
                allExists = false;
                break;
            }
        }

        if(allExists) {
            performBatchUpdateWithoutNewItems(items);
        } else {
            performBatchUpdateWithNewItems(items);
        }
    }

    private void performBatchUpdateWithoutNewItems(List<CategoryViewObject> items) {
        for(int i = 0; i < items.size(); i++) {
            CategoryViewObject newItem = items.get(i);
            CategoryViewObject oldItem = itemMap.get(newItem.getId());

            this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
            this.itemMap.put(newItem.getId(), newItem);
        }
    }

    private void performBatchUpdateWithNewItems(List<CategoryViewObject> items) {
        try {
            this.items.beginBatchedUpdates();
            for (int i = 0; i < items.size(); i++) {
                CategoryViewObject newItem = items.get(i);
                CategoryViewObject oldItem = itemMap.get(newItem.getId());

                if (oldItem != null) {
                    this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
                } else {
                    this.items.add(newItem);
                }

                this.itemMap.put(newItem.getId(), newItem);
            }

        } finally {
            this.items.endBatchedUpdates();
        }
    }

    public Parcelable onSaveInstanceState() {
        return this.itemMap;
    }

    public void onRestoreInstanceState(Parcelable savedState) {
        this.itemMap = (ParcelableStringHashMap<CategoryViewObject>) savedState;
        this.items.addAll(itemMap.values());
    }
}
