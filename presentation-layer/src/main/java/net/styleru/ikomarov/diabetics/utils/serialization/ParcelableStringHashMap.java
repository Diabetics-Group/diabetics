package net.styleru.ikomarov.diabetics.utils.serialization;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by i_komarov on 04.05.17.
 */

public class ParcelableStringHashMap<V extends Parcelable> extends HashMap<String, V> implements Parcelable {

    @NonNull
    private final Class<V> valueClass;

    public ParcelableStringHashMap(@NonNull Class<V> valueClass) {
        this.valueClass = valueClass;
    }

    ParcelableStringHashMap(Parcel in) {
        int size = in.readInt();
        valueClass = (Class<V>) in.readSerializable();
        for(int i = 0; i < size; i++){
            this.put(in.readString(), in.readParcelable(valueClass.getClassLoader()));
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.size());
        dest.writeSerializable(valueClass);
        for(Map.Entry<String, V> e : this.entrySet()){
            dest.writeString(e.getKey());
            dest.writeParcelable(e.getValue(), flags);
        }
    }

    public static final Parcelable.Creator<ParcelableHashMap> CREATOR = new Creator<ParcelableHashMap>() {
        @Override
        public ParcelableHashMap createFromParcel(Parcel in) {
            return new ParcelableHashMap(in);
        }

        @Override
        public ParcelableHashMap[] newArray(int size) {
            return new ParcelableHashMap[0];
        }
    };
}
