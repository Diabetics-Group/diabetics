package net.styleru.ikomarov.diabetics.presenter.preferences;

import android.support.annotation.NonNull;
import android.util.Log;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.diabetics.BuildConfig;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.exceptions.ExceptionHandler;
import net.styleru.ikomarov.diabetics.managers.resources.labels.ILabelsManager;
import net.styleru.ikomarov.diabetics.managers.resources.notifications.INotificationsManager;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.diabetics.view.preferences.IPreferencesView;
import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelMutator;
import net.styleru.ikomarov.domain_layer.use_cases.cache.InvalidateCacheUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.cache.TotalSpaceUsedGetUseCase;

/**
 * Created by i_komarov on 13.05.17.
 */

public class PreferencesPresenter extends ViperPresenter<IPreferencesView, MainRouter> implements TotalSpaceUsedGetUseCase.Callbacks, InvalidateCacheUseCase.Callbacks {

    @NonNull
    private final TotalSpaceUsedGetUseCase totalSpaceUsedGet;

    @NonNull
    private final InvalidateCacheUseCase invalidateCache;

    @NonNull
    private final INotificationsManager notificationsManager;

    @NonNull
    private final ILabelsManager labelsManager;

    @NonNull
    private final IToolbarModelMutator toolbarModelMutator;

    @NonNull
    private final ExceptionHandler handler;

    private PreferencesPresenter(@NonNull TotalSpaceUsedGetUseCase totalSpaceUsedGet,
                                 @NonNull InvalidateCacheUseCase invalidateCache,
                                 @NonNull INotificationsManager notificationsManager,
                                 @NonNull ILabelsManager labelsManager,
                                 @NonNull IToolbarModelMutator toolbarModelMutator) {

        this.totalSpaceUsedGet = totalSpaceUsedGet;
        this.invalidateCache = invalidateCache;
        this.notificationsManager = notificationsManager;
        this.labelsManager = labelsManager;
        this.toolbarModelMutator = toolbarModelMutator;
        this.handler = new ExceptionHandler();
    }

    @Override
    protected void onViewAttach() {
        toolbarModelMutator.setTitle(labelsManager.getPreferencesLabel());
        toolbarModelMutator.setSubtitle("");
        totalSpaceUsedGet.execute(this);
        //TODO: refactor below two lines to use IAppPreferences
        getView().setVersionCode(BuildConfig.VERSION_CODE);
        getView().setVersionName(BuildConfig.VERSION_NAME);
    }

    @Override
    protected void onViewDetach() {
        totalSpaceUsedGet.dispose();
        invalidateCache.dispose();
    }

    @Override
    protected void onDestroy() {
        totalSpaceUsedGet.dispose();
        invalidateCache.dispose();
    }

    public void onClearCachePreferenceClicks() {
        invalidateCache.execute(this);
    }

    public void onClearSessionPreferenceClicks() {
        Log.d("PreferencesPresenter", "clear session action was requested from user");
        //TODO: clear all session data
    }

    public void onReportBugButtonClicks() {
        Log.d("PreferencesPresenter", "report bug action was requested from user");
    }

    public void onRateButtonClicks() {
        getRouter().navigateToRate();
    }

    @Override
    public void onTotalSpaceUsedLoaded(long totalSpace) {
        getView().setTotalSpaceUsed(
                totalSpace / 1024
        );
    }

    @Override
    public void onTotalSpaceUsedLoadingFailure(ExceptionBundle error) {
        handler.handleDebug(error);
    }

    @Override
    public void onCacheInvalidated() {
        getRouter().newSnackBarNotification(notificationsManager.getCacheClearedNotification());
        totalSpaceUsedGet.execute(this);
    }

    @Override
    public void onCacheInvalidationFailure(ExceptionBundle error) {
        handler.handleDebug(error);
    }

    public static final class Factory implements ViperPresenter.Factory<IPreferencesView, MainRouter, PreferencesPresenter> {

        @NonNull
        private final TotalSpaceUsedGetUseCase totalSpaceUsedGet;

        @NonNull
        private final InvalidateCacheUseCase invalidateCache;

        @NonNull
        private final INotificationsManager notificationsManager;

        @NonNull
        private final ILabelsManager labelsManager;

        @NonNull
        private final IToolbarModelMutator toolbarModelMutator;

        public Factory(@NonNull TotalSpaceUsedGetUseCase totalSpaceUsedGet,
                       @NonNull InvalidateCacheUseCase invalidateCache,
                       @NonNull INotificationsManager notificationsManager,
                       @NonNull ILabelsManager labelsManager,
                       @NonNull IToolbarModelMutator toolbarModelMutator) {
            this.totalSpaceUsedGet = totalSpaceUsedGet;
            this.invalidateCache = invalidateCache;
            this.notificationsManager = notificationsManager;
            this.labelsManager = labelsManager;
            this.toolbarModelMutator = toolbarModelMutator;
        }

        @Override
        public PreferencesPresenter create() {
            return new PreferencesPresenter(totalSpaceUsedGet, invalidateCache, notificationsManager, labelsManager, toolbarModelMutator);
        }
    }
}
