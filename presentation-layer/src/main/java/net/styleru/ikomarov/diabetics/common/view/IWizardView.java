package net.styleru.ikomarov.diabetics.common.view;

import android.content.Intent;

/**
 * Created by i_komarov on 05.05.17.
 */

public interface IWizardView extends IView {

    void setResult(int code);

    void setResult(int code, Intent data);

    void finish();
}
