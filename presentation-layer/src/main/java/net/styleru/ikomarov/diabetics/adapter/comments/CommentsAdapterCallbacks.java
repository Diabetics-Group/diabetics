package net.styleru.ikomarov.diabetics.adapter.comments;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;

import net.styleru.ikomarov.diabetics.contracts.locale.DateTimeContract;
import net.styleru.ikomarov.diabetics.view_object.comments.CommentViewObject;

import java.text.ParseException;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsAdapterCallbacks extends SortedListAdapterCallback<CommentViewObject> {

    public CommentsAdapterCallbacks(RecyclerView.Adapter adapter) {
        super(adapter);
    }

    @Override
    public int compare(CommentViewObject o1, CommentViewObject o2) {
        try {
            return DateTimeContract.parseDateFormat.parse(o1.getDate()).compareTo(DateTimeContract.parseDateFormat.parse(o2.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public boolean areContentsTheSame(CommentViewObject oldItem, CommentViewObject newItem) {
        return oldItem.getFirstName().equals(newItem.getFirstName()) &&
                oldItem.getLastName().equals(newItem.getLastName()) &&
                ((oldItem.getImageUrl() == null && newItem.getImageUrl() == null) || (oldItem.getImageUrl() != null && newItem.getImageUrl() != null && oldItem.getImageUrl().equals(newItem.getImageUrl()))) &&
                oldItem.getRole().equals(newItem.getRole()) &&
                oldItem.getContent().equals(newItem.getContent()) &&
                oldItem.getDate().equals(newItem.getDate()) &&
                oldItem.isSynchronized().equals(newItem.isSynchronized());
    }

    @Override
    public boolean areItemsTheSame(CommentViewObject item1, CommentViewObject item2) {
        return item1.getId().equals(item2.getId());
    }
}
