package net.styleru.ikomarov.diabetics.validation.authentication;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import net.styleru.ikomarov.data_layer.contracts.exception.Reason;
import net.styleru.ikomarov.data_layer.contracts.exception.ValidationExceptionContract;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.diabetics.managers.resources.notifications.INotificationsManager;
import net.styleru.ikomarov.domain_layer.validation.base.IValidationStrategy;

/**
 * Created by i_komarov on 05.06.17.
 */

public class CodeValidationStrategy implements IValidationStrategy<String> {

    @NonNull
    private final INotificationsManager notificationsManager;

    public CodeValidationStrategy(@NonNull INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    @Override
    public Boolean apply(String code) throws ExceptionBundle {
        State state = null;

        if(TextUtils.isEmpty(code) || code.length() < 4) {
            state = State.LENGTH_FAILURE;
        }

        if(state != null) {
            throw prepareVerificationException(state);
        }

        return true;
    }

    private ExceptionBundle prepareVerificationException(State state) {
        ExceptionBundle error = new ExceptionBundle(Reason.VALIDATION);
        ValidationExceptionContract contract = error.getReason().getContract();

        switch(state) {
            case LENGTH_FAILURE: {
                contract.putMessage(error, notificationsManager.getBadCodeLengthNotification());
                break;
            }
        }

        return error;
    }

    private enum State {
        LENGTH_FAILURE
    }
}
