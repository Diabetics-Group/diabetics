package net.styleru.ikomarov.diabetics.mapping.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.view_object.users.UserViewObject;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;
import net.styleru.ikomarov.domain_layer.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 31.05.17.
 */

public class UsersMapping implements Function<List<UserDTO>, List<UserViewObject>> {

    @NonNull
    private final Function<UserDTO, UserViewObject> singleMapper;

    public UsersMapping(@NonNull Function<UserDTO, UserViewObject> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<UserViewObject> apply(List<UserDTO> userDTOs) throws Exception {
        return Observable.fromIterable(userDTOs)
                .map(singleMapper)
                .compose(RxUtils.accumulate())
                .blockingFirst();
    }
}
