package net.styleru.ikomarov.diabetics.utils.pagination;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by i_komarov on 04.05.17.
 */

public class PaginationListener extends RecyclerView.OnScrollListener {

    private static final int LIMIT_DEFAULT = 50;

    private final Utils utils;

    private final Callback callback;

    private int limit = LIMIT_DEFAULT;

    public PaginationListener(Callback callback) {
        this.utils = new Utils();
        this.callback = callback;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getLimit() {
        return this.limit;
    }

    @Override
    public void onScrolled(RecyclerView list, int dx, int dy) {
        super.onScrolled(list, dx, dy);
        int position = getLastVisibleItemPosition(list);
        int limit = getLimit();
        int updatePosition = list.getAdapter().getItemCount() - 1 - (limit / 2);
        if (position >= updatePosition) {
            int offset = list.getAdapter().getItemCount() - 1;
            State state = new State(offset, limit);
            if(utils.isDistinct(state)) {
                utils.setState(state);
                callback.onListStateChanged(state);
            }
        }
    }

    public interface Callback {

        void onListStateChanged(State state);
    }

    private int getLastVisibleItemPosition(RecyclerView list) {
        Class recyclerViewLMClass = list.getLayoutManager().getClass();
        if (recyclerViewLMClass == LinearLayoutManager.class || LinearLayoutManager.class.isAssignableFrom(recyclerViewLMClass)) {
            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) list.getLayoutManager();
            return linearLayoutManager.findLastVisibleItemPosition();
        } else if (recyclerViewLMClass == StaggeredGridLayoutManager.class || StaggeredGridLayoutManager.class.isAssignableFrom(recyclerViewLMClass)) {
            StaggeredGridLayoutManager staggeredGridLayoutManager = (StaggeredGridLayoutManager) list.getLayoutManager();
            int[] into = staggeredGridLayoutManager.findLastVisibleItemPositions(null);
            List<Integer> intoList = new ArrayList<>();
            for (int i : into) {
                intoList.add(i);
            }
            return Collections.max(intoList);
        }
        throw new IllegalStateException("Unknown LayoutManager class: " + recyclerViewLMClass.toString());
    }

    private static final class Utils {

        private State state;

        public Utils() {

        }

        public boolean isDistinct(State state) {
            return this.state == null || this.state.getOffset() != state.getOffset();
        }

        private void setState(State state) {
            this.state = state;
        }
    }
}

