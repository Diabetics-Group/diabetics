package net.styleru.ikomarov.diabetics.di.view_model.providers;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.validation.base.IValidationStrategy;

/**
 * Created by i_komarov on 05.06.17.
 */

public interface IValidationsProvider {

    @NonNull
    IValidationStrategy<String> providerPhoneValidationStrategy();

    @NonNull
    IValidationStrategy<String> provideCodeValidationStrategy();
}
