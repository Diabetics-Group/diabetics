package net.styleru.ikomarov.diabetics.view.topic;

import android.support.annotation.StringRes;

import net.styleru.ikomarov.diabetics.common.view.IView;
import net.styleru.ikomarov.diabetics.common.view.IWizardView;

/**
 * Created by i_komarov on 05.05.17.
 */

public interface ITopicCreateView extends IWizardView {

    void showTitleError(@StringRes int message);

    void showTitleError(String message);

    void showContentError(@StringRes int message);

    void showContentError(String message);
}
