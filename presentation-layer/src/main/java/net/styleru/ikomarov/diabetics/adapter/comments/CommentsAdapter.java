package net.styleru.ikomarov.diabetics.adapter.comments;

import android.os.Parcelable;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.utils.serialization.ParcelableStringHashMap;
import net.styleru.ikomarov.diabetics.view_holder.comments.CommentViewHolder;
import net.styleru.ikomarov.diabetics.view_holder.common.BindableViewHolder;
import net.styleru.ikomarov.diabetics.view_holder.common.LoadPreviousHeader;
import net.styleru.ikomarov.diabetics.view_object.comments.CommentViewObject;

import java.util.List;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsAdapter extends RecyclerView.Adapter<BindableViewHolder> {

    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ITEM = 1;

    private ParcelableStringHashMap<CommentViewObject> itemMap;

    private SortedList<CommentViewObject> items;

    private RequestManager glide;

    private View.OnClickListener headerClicksCallback;

    private CommentViewHolder.InteractionCallbacks callbacks;

    private boolean showHeader = true;

    public CommentsAdapter(RequestManager glide, View.OnClickListener headerClicksCallback) {
        this.glide = glide;
        this.headerClicksCallback = headerClicksCallback;
        this.itemMap = new ParcelableStringHashMap<>(CommentViewObject.class);
        init(itemMap);
    }

    private void init(ParcelableStringHashMap<CommentViewObject> itemMap) {
        items = new SortedList<>(CommentViewObject.class, new CommentsAdapterCallbacks(this));
        items.addAll(itemMap.values());
    }

    @Override
    public BindableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewType == VIEW_TYPE_ITEM ?
                new CommentViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_comment, parent, false)) :
                new LoadPreviousHeader(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_load_previous, parent, false));
    }

    @Override
    public void onBindViewHolder(BindableViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        Object item;
        Object callbacks;
        if(viewType == VIEW_TYPE_HEADER) {
            item = showHeader;
            callbacks = headerClicksCallback;
        } else {
            item = items.get(position - 1);
            callbacks = this.callbacks;
        }

        holder.bind(
                glide,
                position,
                item,
                callbacks
        );
    }

    @Override
    public int getItemCount() {
        return items.size() + 1;
    }

    public int getRealItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? VIEW_TYPE_HEADER :
                VIEW_TYPE_ITEM;
    }

    @Override
    public void onViewAttachedToWindow(BindableViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bindCallbacks();
    }

    @Override
    public void onViewDetachedFromWindow(BindableViewHolder holder) {
        holder.unbindCallbacks();
        super.onViewDetachedFromWindow(holder);
    }

    public void removeHeader() {
        showHeader = false;
        notifyItemChanged(0);
    }

    public void setCallbacks(CommentViewHolder.InteractionCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void addItems(List<CommentViewObject> items) {
        boolean allExists = true;

        for(int i = 0; i < items.size(); i++) {
            CommentViewObject newItem = items.get(i);
            CommentViewObject oldItem = itemMap.get(newItem.getId());

            if(oldItem == null) {
                allExists = false;
                break;
            }
        }

        if(allExists) {
            performBatchUpdateWithoutNewItems(items);
        } else {
            performBatchUpdateWithNewItems(items);
        }
    }

    private void performBatchUpdateWithoutNewItems(List<CommentViewObject> items) {
        for(int i = 0; i < items.size(); i++) {
            CommentViewObject newItem = items.get(i);
            CommentViewObject oldItem = itemMap.get(newItem.getId());

            this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
            this.itemMap.put(newItem.getId(), newItem);
        }
    }

    private void performBatchUpdateWithNewItems(List<CommentViewObject> items) {
        try {
            this.items.beginBatchedUpdates();
            for (int i = 0; i < items.size(); i++) {
                CommentViewObject newItem = items.get(i);
                CommentViewObject oldItem = itemMap.get(newItem.getId());

                if (oldItem != null) {
                    this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
                } else {
                    this.items.add(newItem);
                }

                this.itemMap.put(newItem.getId(), newItem);
            }

        } finally {
            this.items.endBatchedUpdates();
        }
    }

    public Parcelable onSaveInstanceState() {
        return this.itemMap;
    }

    public void onRestoreInstanceState(Parcelable savedState) {
        this.itemMap = (ParcelableStringHashMap<CommentViewObject>) savedState;
        this.items.addAll(itemMap.values());
    }
}
