package net.styleru.ikomarov.diabetics.routers.main;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.View;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.common.routers.BaseRouter;
import net.styleru.ikomarov.diabetics.utils.intent_factories.GooglePlayIntentFactory;
import net.styleru.ikomarov.diabetics.utils.messages.SnackbarFactory;
import net.styleru.ikomarov.diabetics.view.categories.CategoriesFragment;
import net.styleru.ikomarov.diabetics.view.comments.CommentsFragment;
import net.styleru.ikomarov.diabetics.view.main.IMainView;
import net.styleru.ikomarov.diabetics.view.main.MainActivity;
import net.styleru.ikomarov.diabetics.view.preferences.PreferencesFragment;
import net.styleru.ikomarov.diabetics.view.topic.TopicCreateActivity;
import net.styleru.ikomarov.diabetics.view.topics.TopicsFragment;
import net.styleru.ikomarov.diabetics.view.users.UsersFragment;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;
import net.styleru.ikomarov.diabetics.view_object.chats.ChatViewObject;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;

/**
 * Created by i_komarov on 02.05.17.
 */

public class MainRouter extends BaseRouter<IMainView> implements IMainRouter {

    public static final int CODE_TOPIC_CREATE = 9981;

    @NonNull
    private final SnackbarFactory factory;

    @NonNull
    private final GooglePlayIntentFactory gPIntentFactory;

    public MainRouter(@NonNull MainActivity view,
                      @NonNull SnackbarFactory factory,
                      @NonNull GooglePlayIntentFactory gPIntentFactory) {
        super(view);
        this.factory = factory;
        this.gPIntentFactory = gPIntentFactory;
        getView().setDrawerEnabled(getView().getSupportFragmentManager().getBackStackEntryCount() == 0);
    }

    @Override
    public void navigateUp() {
        getView().setDrawerEnabled(getView().getSupportFragmentManager().getBackStackEntryCount() <= 1);
        getView().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void instantiateDefaultFragment() {
        getView().setDrawerEnabled(true);
        getView().setCheckedDrawerItem(R.id.navigation_menu_item_categories);
        getView().closeDrawer();
        getView().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_fragment_container, CategoriesFragment.newInstance())
                .commit();
    }

    @Override
    public void navigateToCategories() {
        getView().setDrawerEnabled(true);
        getView().setCheckedDrawerItem(R.id.navigation_menu_item_categories);
        getView().closeDrawer();
        getView().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_fragment_container, CategoriesFragment.newInstance())
                .commit();
    }

    @Override
    public void navigateToTopics(CategoryViewObject model) {
        getView().setDrawerEnabled(false);
        getView().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_fragment_container, TopicsFragment.newInstance(model))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void navigateToTopicCreate(CategoryViewObject model) {
        getView().startActivityForResult(TopicCreateActivity.createIntent((Activity) getView(), model), CODE_TOPIC_CREATE);
    }

    @Override
    public void navigateToComments(TopicViewObject model) {
        getView().setDrawerEnabled(false);
        getView().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_fragment_container, CommentsFragment.newInstance(model))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void navigateToChats() {
        getView().setDrawerEnabled(true);
        getView().setCheckedDrawerItem(R.id.navigation_menu_item_chats);
        getView().closeDrawer();
    }

    @Override
    public void navigateToChat(ChatViewObject model) {
        getView().setDrawerIconEnabled(false);
    }

    @Override
    public void navigateToParticipants() {
        getView().setDrawerEnabled(true);
        getView().setCheckedDrawerItem(R.id.navigation_menu_item_participants);
        getView().closeDrawer();
        getView().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_fragment_container, UsersFragment.newInstance())
                .commit();
    }

    @Override
    public void navigateToProfile() {
        getView().setDrawerEnabled(true);
        getView().setCheckedDrawerItem(R.id.navigation_menu_item_profile);
        getView().closeDrawer();
    }

    @Override
    public void navigateToPreferences() {
        getView().setDrawerEnabled(true);
        getView().setCheckedDrawerItem(R.id.navigation_menu_item_preferences);
        getView().closeDrawer();
        getView().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_fragment_container, PreferencesFragment.newInstance())
                .commit();
    }

    @Override
    public void navigateToRate() {
        getView().startActivity(
                gPIntentFactory.create()
        );
    }

    @Override
    public void newSnackBarNotification(@StringRes int content) {
        Snackbar snack = factory.create(content, Snackbar.LENGTH_INDEFINITE);
        snack.setAction(R.string.action_ok, (view) -> snack.dismiss());
        snack.show();
    }

    @Override
    public void newSnackBarNotification(String content) {
        Snackbar snack = factory.create(content, Snackbar.LENGTH_INDEFINITE);
        snack.setAction(R.string.action_ok, (view) -> snack.dismiss());
        snack.show();
    }

    @Override
    public void newSnackBarNotification(@StringRes int content, View.OnClickListener callback) {
        Snackbar snack = factory.create(content, Snackbar.LENGTH_INDEFINITE);
        snack.setAction(R.string.action_ok, (view) -> {
            callback.onClick(view);
            snack.dismiss();
        });
        snack.show();
    }

    @Override
    public void newSnackBarNotification(String content, View.OnClickListener callback) {
        Snackbar snack = factory.create(content, Snackbar.LENGTH_INDEFINITE);
        snack.setAction(R.string.action_ok, (view) -> {
            callback.onClick(view);
            snack.dismiss();
        });
        snack.show();
    }
}
