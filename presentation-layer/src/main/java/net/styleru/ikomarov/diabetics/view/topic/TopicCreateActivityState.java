package net.styleru.ikomarov.diabetics.view.topic;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;

/**
 * Created by i_komarov on 05.05.17.
 */

public class TopicCreateActivityState {

    @NonNull
    private static final String PREFIX = TopicCreateActivity.class.getCanonicalName() + ".";

    @NonNull
    private static final String KEY_CATEGORY_MODEL = PREFIX + "CATEGORY_MODEL";

    private final CategoryViewObject model;

    public static Bundle asBundle(CategoryViewObject model) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_CATEGORY_MODEL, model);
        return bundle;
    }

    public TopicCreateActivityState(Intent intent, Bundle saved) {
        model = (intent != null && intent.getExtras() != null ? intent.getExtras() : saved).getParcelable(KEY_CATEGORY_MODEL);
    }

    public void save(Bundle outState) {
        outState.putParcelable(KEY_CATEGORY_MODEL, model);
    }

    public CategoryViewObject getModel() {
        return this.model;
    }
}
