package net.styleru.ikomarov.diabetics.view.users;

import net.styleru.ikomarov.diabetics.common.view.IView;
import net.styleru.ikomarov.diabetics.view_object.users.UserViewObject;

import java.util.List;

/**
 * Created by i_komarov on 15.05.17.
 */

public interface IUsersView extends IView {

    void addUsers(List<UserViewObject> items);
}
