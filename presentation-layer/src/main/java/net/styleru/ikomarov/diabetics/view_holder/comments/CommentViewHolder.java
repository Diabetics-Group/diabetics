package net.styleru.ikomarov.diabetics.view_holder.comments;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.bumptech.glide.RequestManager;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.contracts.locale.DateTimeContract;
import net.styleru.ikomarov.diabetics.view_holder.common.BindableViewHolder;
import net.styleru.ikomarov.diabetics.view_object.comments.CommentViewObject;

import java.text.ParseException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentViewHolder extends BindableViewHolder<CommentViewObject, CommentViewHolder.InteractionCallbacks> {

    private AppCompatTextView dateHolder;
    private CircleImageView imageHolder;
    private AppCompatTextView nameHolder;
    private AppCompatTextView badgeHolder;
    private AppCompatTextView contentHolder;

    private int position;
    private CommentViewObject item;
    private InteractionCallbacks callbacks;

    public CommentViewHolder(View itemView) {
        super(itemView);

        dateHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_comment_time);
        imageHolder = (CircleImageView) itemView.findViewById(R.id.list_item_comment_author_image);
        nameHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_comment_author_name);
        badgeHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_comment_author_badge);
        contentHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_comment_content);
    }

    public void bindCallbacks() {
        itemView.setOnClickListener((view) -> {
            if(callbacks != null) {
                callbacks.clicks(position, item);
            }
        });
    }

    public void unbindCallbacks() {
        itemView.setOnClickListener(null);
    }

    public void bind(RequestManager glide, int position, CommentViewObject item, InteractionCallbacks callbacks) {
        this.position = position;
        this.item = item;
        this.callbacks = callbacks;

        if(imageHolder.getDrawable() != null) {
            imageHolder.setImageDrawable(null);
        }

        try {
            dateHolder.setText(DateTimeContract.displayDateFormat.format(DateTimeContract.parseDateFormat.parse(item.getDate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(!TextUtils.isEmpty(item.getImageUrl())) {
            glide.load(item.getImageUrl())
                    .into(imageHolder);
        }

        nameHolder.setText(item.getFirstName() + " " + item.getLastName());
        badgeHolder.setVisibility(item.getRole().equalsIgnoreCase(itemView.getResources().getString(R.string.label_expert)) ? View.VISIBLE : View.GONE);
        contentHolder.setText(item.getContent());
    }

    public interface InteractionCallbacks {

        void clicks(int position, CommentViewObject item);
    }
}
