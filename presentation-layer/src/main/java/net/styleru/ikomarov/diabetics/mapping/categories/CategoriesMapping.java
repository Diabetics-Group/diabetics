package net.styleru.ikomarov.diabetics.mapping.categories;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;
import net.styleru.ikomarov.domain_layer.dto.categories.CategoryDTO;
import net.styleru.ikomarov.domain_layer.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 04.05.17.
 */

public class CategoriesMapping implements Function<List<CategoryDTO>, List<CategoryViewObject>> {

    @NonNull
    private final Function<CategoryDTO, CategoryViewObject> singleMapper;

    public CategoriesMapping(@NonNull Function<CategoryDTO, CategoryViewObject> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<CategoryViewObject> apply(List<CategoryDTO> categoryDTOs) throws Exception {
        return Observable.fromIterable(categoryDTOs)
                .map(singleMapper)
                .compose(RxUtils.accumulate())
                .blockingFirst();
    }
}
