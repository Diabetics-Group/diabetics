package net.styleru.ikomarov.diabetics.view.authorization;

import android.content.Intent;
import android.support.v4.app.FragmentManager;

import net.styleru.ikomarov.diabetics.common.view.IView;
import net.styleru.ikomarov.diabetics.routers.authorization.AuthorizationRouter;

/**
 * Created by i_komarov on 02.06.17.
 */

public interface IAuthorizationView extends IView {

    AuthorizationRouter getRouter();

    FragmentManager getSupportFragmentManager();

    void setResult(int code);

    void setResult(int code, Intent data);

    void finish();
}
