package net.styleru.ikomarov.diabetics.contracts.screens;

import android.util.SparseArray;

/**
 * Created by i_komarov on 02.05.17.
 */

public enum ScreenContract {
    MAIN                (0x0000000),
    CATEGORIES          (0x0000001),
    TOPICS              (0x0000002),
    TOPICS_CREATE       (0x0000003),
    COMMENTS            (0x0000004),
    CHATS               (0x0000005),
    CHAT                (0x0000006),
    CHAT_PREFERENCES    (0x0000007),
    PREFERENCES         (0x0000008),
    PROFILE             (0x0000009),
    USERS               (0x0000010),
    AUTHORIZATION       (0x0000011),
    AUTHORIZATION_PHONE (0x0000012),
    AUTHORIZATION_CODE  (0x0000013);

    private static final SparseArray<ScreenContract> contractsMap = new SparseArray<>(ScreenContract.values().length);

    static {
        for(ScreenContract contract : ScreenContract.values()) {
            contractsMap.put(contract.code, contract);
        }
    }

    private final int code;

    public static ScreenContract getContract(int code) {
        return contractsMap.get(code);
    }

    ScreenContract(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}
