package net.styleru.ikomarov.diabetics.common.routers;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.common.view.IView;

/**
 * Created by i_komarov on 02.05.17.
 */

public class BaseRouter<V extends IView> {

    @NonNull
    private final V view;

    public BaseRouter(@NonNull V view) {
        this.view = view;
    }

    @NonNull
    protected final V getView() {
        return this.view;
    }
}
