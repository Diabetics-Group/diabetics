package net.styleru.ikomarov.diabetics.common.routers;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;

import net.styleru.ikomarov.diabetics.common.view.IWizardView;

/**
 * Created by i_komarov on 05.05.17.
 */

public class WizardRouter<R extends Parcelable, V extends IWizardView> extends BaseRouter<V> implements IWizardRouter<R> {

    public static final String KEY_RESULT = WizardRouter.class.getCanonicalName() + "." + "RESULT";

    public WizardRouter(V view) {
        super(view);
    }

    @Override
    public void returnOnSuccess(R result) {
        Intent intent = new Intent();
        intent.putExtra(KEY_RESULT, result);
        getView().setResult(Activity.RESULT_OK, intent);
        getView().finish();
    }

    @Override
    public void returnOnCancel() {
        getView().setResult(Activity.RESULT_CANCELED);
        getView().finish();
    }
}
