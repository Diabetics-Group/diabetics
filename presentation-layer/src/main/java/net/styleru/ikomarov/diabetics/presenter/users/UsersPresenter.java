package net.styleru.ikomarov.diabetics.presenter.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.exceptions.ExceptionHandler;
import net.styleru.ikomarov.diabetics.managers.resources.labels.ILabelsManager;
import net.styleru.ikomarov.diabetics.mapping.users.UserMapping;
import net.styleru.ikomarov.diabetics.mapping.users.UsersMapping;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.diabetics.view.users.IUsersView;
import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelMutator;
import net.styleru.ikomarov.diabetics.view_object.users.UserViewObject;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;
import net.styleru.ikomarov.domain_layer.use_cases.users.UsersGetUseCase;

import java.util.List;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UsersPresenter extends ViperPresenter<IUsersView, MainRouter> implements UsersGetUseCase.Callbacks<UserViewObject> {

    @NonNull
    private final ILabelsManager labelsManager;

    @NonNull
    private final IToolbarModelMutator toolbarModelMutator;

    @NonNull
    private final UsersGetUseCase usersGet;

    @NonNull
    private final Function<UserDTO, UserViewObject> singleMapper;

    @NonNull
    private final Function<List<UserDTO>, List<UserViewObject>> listMapper;

    @NonNull
    private final ExceptionHandler handler;

    private UsersPresenter(@NonNull ILabelsManager labelsManager,
                           @NonNull IToolbarModelMutator toolbarModelMutator,
                           @NonNull UsersGetUseCase usersGet) {

        this.labelsManager = labelsManager;
        this.toolbarModelMutator = toolbarModelMutator;
        this.usersGet = usersGet;
        this.singleMapper = new UserMapping();
        this.listMapper = new UsersMapping(singleMapper);
        this.handler = new ExceptionHandler();
    }

    @Override
    protected void onViewAttach() {
        toolbarModelMutator.setTitle(labelsManager.getParticipantsLabel());
        toolbarModelMutator.setSubtitle("");
    }

    @Override
    protected void onViewDetach() {
        usersGet.dispose();
    }

    @Override
    protected void onDestroy() {
        usersGet.dispose();
    }

    public void loadUsers(int offset, int limit) {
        usersGet.execute(
                listMapper,
                this,
                offset,
                limit
        );
    }

    @Override
    public void onUserLoaded(@NonNull List<UserViewObject> users) {
        getView().addUsers(users);
    }

    @Override
    public void onUserLoadingFailed(@NonNull ExceptionBundle error) {
        handler.handleDebug(error);
    }

    public static final class Factory implements ViperPresenter.Factory<IUsersView, MainRouter, UsersPresenter> {

        @NonNull
        private final ILabelsManager labelsManager;

        @NonNull
        private final IToolbarModelMutator toolbarModelMutator;

        @NonNull
        private final UsersGetUseCase usersGet;

        public Factory(@NonNull ILabelsManager labelsManager,
                       @NonNull IToolbarModelMutator toolbarModelMutator,
                       @NonNull UsersGetUseCase usersGet) {

            this.labelsManager = labelsManager;
            this.toolbarModelMutator = toolbarModelMutator;
            this.usersGet = usersGet;
        }

        @Override
        public UsersPresenter create() {
            return new UsersPresenter(labelsManager, toolbarModelMutator, usersGet);
        }
    }
}
