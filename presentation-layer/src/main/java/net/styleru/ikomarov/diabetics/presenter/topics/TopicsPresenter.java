package net.styleru.ikomarov.diabetics.presenter.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.exceptions.ExceptionHandler;
import net.styleru.ikomarov.diabetics.managers.resources.labels.ILabelsManager;
import net.styleru.ikomarov.diabetics.mapping.topics.TopicMapping;
import net.styleru.ikomarov.diabetics.mapping.topics.TopicsMapping;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.diabetics.view.topics.ITopicsView;
import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelMutator;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;
import net.styleru.ikomarov.domain_layer.use_cases.topics.TopicGetUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.topics.TopicsGetUseCase;

import java.util.Collections;
import java.util.List;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsPresenter extends ViperPresenter<ITopicsView, MainRouter> implements TopicsGetUseCase.Callbacks<TopicViewObject>, TopicGetUseCase.Callbacks<TopicViewObject> {

    @NonNull
    private final CategoryViewObject model;

    @NonNull
    private final ILabelsManager labelsManager;

    @NonNull
    private final IToolbarModelMutator toolbarModelMutator;

    @NonNull
    private final TopicsGetUseCase topicsGet;

    @NonNull
    private final TopicGetUseCase topicGet;

    @NonNull
    private final Function<TopicDTO, TopicViewObject> singleMapper;

    @NonNull
    private final Function<List<TopicDTO>, List<TopicViewObject>> listMapper;

    @NonNull
    private final ExceptionHandler handler;

    private TopicsPresenter(@NonNull CategoryViewObject model,
                            @NonNull ILabelsManager labelsManager,
                            @NonNull IToolbarModelMutator toolbarModelMutator,
                            @NonNull TopicsGetUseCase topicsGet,
                            @NonNull TopicGetUseCase topicGet) {

        this.model = model;
        this.labelsManager = labelsManager;
        this.toolbarModelMutator = toolbarModelMutator;
        this.topicsGet = topicsGet;
        this.topicGet = topicGet;
        this.singleMapper = new TopicMapping();
        this.listMapper = new TopicsMapping(singleMapper);
        this.handler = new ExceptionHandler();
    }

    @Override
    protected void onViewAttach() {
        toolbarModelMutator.setTitle(labelsManager.getTopicsLabel());
        toolbarModelMutator.setSubtitle(model.getTitle());
    }

    @Override
    protected void onViewDetach() {
        topicsGet.dispose();
        topicGet.dispose();
    }

    @Override
    protected void onDestroy() {
        topicsGet.dispose();
        topicGet.dispose();
    }

    public void onTopicClicks(TopicViewObject item) {
        getRouter().navigateToComments(item);
    }

    public void loadTopics(int offset, int limit) {
        topicsGet.execute(
                listMapper,
                this,
                model.getId(),
                offset,
                limit
        );
    }

    public void createTopic() {
        getRouter().navigateToTopicCreate(model);
    }

    public void loadTopic(String id) {
        topicGet.execute(singleMapper, this, id);
    }

    @Override
    public void onTopicsLoaded(@NonNull List<TopicViewObject> viewObjectList) {
        getView().addTopics(viewObjectList);
    }

    @Override
    public void onTopicsLoadingFailed(@NonNull ExceptionBundle error) {
        handler.handleDebug(error);
    }

    @Override
    public void onTopicLoaded(TopicViewObject item) {
        getView().addTopics(Collections.singletonList(item));
    }

    @Override
    public void onTopicLoadingFailed(ExceptionBundle error) {
        handler.handleDebug(error);
    }

    public static final class Factory implements ViperPresenter.Factory<ITopicsView, MainRouter, TopicsPresenter> {

        @NonNull
        private final CategoryViewObject model;

        @NonNull
        private final ILabelsManager labelsManager;

        @NonNull
        private final IToolbarModelMutator toolbarModelMutator;

        @NonNull
        private final TopicsGetUseCase topicsGet;

        @NonNull
        private final TopicGetUseCase topicGet;

        public Factory(@NonNull CategoryViewObject model,
                       @NonNull ILabelsManager labelsManager,
                       @NonNull IToolbarModelMutator toolbarModelMutator,
                       @NonNull TopicsGetUseCase topicsGet,
                       @NonNull TopicGetUseCase topicGet) {

            this.model = model;
            this.labelsManager = labelsManager;
            this.toolbarModelMutator = toolbarModelMutator;
            this.topicsGet = topicsGet;
            this.topicGet = topicGet;
        }

        @Override
        public TopicsPresenter create() {
            return new TopicsPresenter(model, labelsManager, toolbarModelMutator, topicsGet, topicGet);
        }
    }
}
