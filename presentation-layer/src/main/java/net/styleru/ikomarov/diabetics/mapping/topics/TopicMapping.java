package net.styleru.ikomarov.diabetics.mapping.topics;

import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicMapping implements Function<TopicDTO, TopicViewObject> {

    @Override
    public TopicViewObject apply(TopicDTO dto) throws Exception {
        return new TopicViewObject(
                dto.getId(),
                dto.getFirstName(),
                dto.getLastName(),
                dto.getImageUrl(),
                dto.getRole(),
                dto.getTitle(),
                dto.getContent(),
                dto.getCommentsCount().intValue(),
                dto.getLastUpdate()
        );
    }
}
