package net.styleru.ikomarov.diabetics.view.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.common.container.RetainContainer;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.view.ViperActivity;
import net.styleru.ikomarov.diabetics.common.view.ViperFragment;
import net.styleru.ikomarov.diabetics.contracts.screens.ScreenContract;
import net.styleru.ikomarov.diabetics.di.view_model.component.view_model.IViewModelComponent;
import net.styleru.ikomarov.diabetics.di.view_model.component.view_model.ViewModelComponentFactory;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IResourceManagersProvider;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IToolbarViewModelProvider;
import net.styleru.ikomarov.diabetics.managers.resources.labels.ILabelsManager;
import net.styleru.ikomarov.diabetics.managers.resources.notifications.INotificationsManager;
import net.styleru.ikomarov.diabetics.utils.intent_factories.GooglePlayIntentFactory;
import net.styleru.ikomarov.diabetics.utils.messages.SnackbarFactory;
import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelMutator;
import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelAccessor;
import net.styleru.ikomarov.diabetics.presenter.main.MainPresenter;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.domain_layer.di.providers.IApplicationEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IGraphProvider;

/**
 * Created by i_komarov on 28.04.17.
 */

public class MainActivity extends ViperActivity<IMainView, MainRouter, MainPresenter> implements IMainView, IToolbarViewModelProvider, IResourceManagersProvider {

    @NonNull
    private static final String TAG_VIEW_MODEL_CONTAINER = MainActivity.class.getCanonicalName() + "." + "VMC";

    private MainRouter router;

    private Toolbar toolbar;
    private CoordinatorLayout rootView;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private ActionBarDrawerToggle drawerToggle;

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getPresenter().onNavigateUp();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Fragment fragment;
        if((fragment = getSupportFragmentManager().findFragmentById(R.id.activity_main_fragment_container)) == null || fragment.isDetached()) {
            getPresenter().onInstantiateDefaultFragment();
        }
    }

    @Override
    public void setDrawerIconEnabled(boolean enabled) {
        drawerToggle.setDrawerIndicatorEnabled(enabled);
    }

    @Override
    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void setSubtitle(String subtitle) {
        getSupportActionBar().setSubtitle(subtitle);
    }

    @Override
    public void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void setDrawerEnabled(boolean enabled) {
        drawerLayout.setDrawerLockMode(enabled ? DrawerLayout.LOCK_MODE_UNLOCKED : DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        setDrawerIconEnabled(enabled);
    }

    @Override
    public void setCheckedDrawerItem(@IdRes int id) {
        navigationView.setCheckedItem(id);
    }

    @Override
    public MainRouter getRouter() {
        return router;
    }

    @Override
    protected void bindUserInterface() {
        rootView = (CoordinatorLayout) findViewById(R.id.activity_main_root);
        drawerLayout = (DrawerLayout) findViewById(R.id.activity_main_drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.activity_main_navigation_view);
        toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);

        setupToolbar(toolbar);
        setupDrawer();

        if(router == null) {
            router = new MainRouter(
                    this,
                    new SnackbarFactory(rootView),
                    new GooglePlayIntentFactory(this)
            );
        }
    }

    @Override
    protected void onActivityResultDelivered(int requestCode, int resultCode, Intent data) {
        Fragment fragment;
        if(null != (fragment = getSupportFragmentManager().findFragmentById(R.id.activity_main_fragment_container))) {
            ((ViperFragment) fragment).onActivityResultDelivered(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onViewStateDelivered(Bundle savedInstanceState) {

    }

    private void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupDrawer() {
        //configure toggle and attach to layout
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_opened, R.string.drawer_closed);
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();
        drawerToggle.setDrawerIndicatorEnabled(getSupportFragmentManager().getBackStackEntryCount() == 0);
        //set listeners
        drawerLayout.addDrawerListener(drawerToggle);
        navigationView.setNavigationItemSelectedListener(this::onNavigationViewItemSelected);
        toolbar.setNavigationOnClickListener(this::onNavigationClicked);
    }

    private void onNavigationClicked(View view) {
        //handle clicks on the either hamburger or back icon
        if(getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getPresenter().onNavigateUp();
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    private boolean onNavigationViewItemSelected(@NonNull MenuItem item) {
        //handle clicks on the navigation drawer's items
        return getPresenter().onDrawerItemSelected(item.getItemId());
    }

    @NonNull
    @Override
    public IToolbarModelMutator provideToolbarModelMutator() {
        verifyComponentProvidersExist();
        return ((RetainContainer<IViewModelComponent>) getSupportFragmentManager()
                .findFragmentByTag(TAG_VIEW_MODEL_CONTAINER))
                .provideComponent()
                .toolbarViewModels()
                .provideToolbarModelMutator();
    }

    @NonNull
    @Override
    public IToolbarModelAccessor provideToolbarModelObserver() {
        verifyComponentProvidersExist();
        return ((RetainContainer<IViewModelComponent>) getSupportFragmentManager()
                .findFragmentByTag(TAG_VIEW_MODEL_CONTAINER))
                .provideComponent()
                .toolbarViewModels()
                .provideToolbarModelObserver();
    }

    @NonNull
    @Override
    public ILabelsManager provideLabelsManager() {
        verifyComponentProvidersExist();
        return ((RetainContainer<IViewModelComponent>) getSupportFragmentManager()
                .findFragmentByTag(TAG_VIEW_MODEL_CONTAINER))
                .provideComponent()
                .labels()
                .provideLabelsManager();
    }

    @NonNull
    @Override
    public INotificationsManager provideNotificationsManager() {
        verifyComponentProvidersExist();
        return ((RetainContainer<IViewModelComponent>) getSupportFragmentManager()
                .findFragmentByTag(TAG_VIEW_MODEL_CONTAINER))
                .provideComponent()
                .notifications()
                .provideNotificationsManager();
    }

    @NonNull
    @Override
    protected IMainView provideView() {
        return this;
    }

    @NonNull
    @Override
    protected MainRouter provideRouter() {
        return router;
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected int provideScreenId() {
        return ScreenContract.MAIN.getCode();
    }

    @NonNull
    @Override
    protected ViperPresenter.Factory<IMainView, MainRouter, MainPresenter> provideFactory() {
        return new MainPresenter.Factory(
                provideToolbarModelObserver()
        );
    }

    private void verifyComponentProvidersExist() {
        if(getSupportFragmentManager().findFragmentByTag(TAG_VIEW_MODEL_CONTAINER) == null) {
            RetainContainer<IViewModelComponent> container = new RetainContainer<>();
            IGraphProvider graphProvider = (IGraphProvider) getApplication();
            container.setFactory(new ViewModelComponentFactory(graphProvider.provideGraphRoot()));
            getSupportFragmentManager().beginTransaction()
                    .add(container, TAG_VIEW_MODEL_CONTAINER)
                    .commitNow();
        }
    }
}
