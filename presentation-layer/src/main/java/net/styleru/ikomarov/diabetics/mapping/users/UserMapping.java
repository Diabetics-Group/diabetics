package net.styleru.ikomarov.diabetics.mapping.users;

import net.styleru.ikomarov.diabetics.contracts.user_role.UserRole;
import net.styleru.ikomarov.diabetics.view_object.users.UserViewObject;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 31.05.17.
 */

public class UserMapping implements Function<UserDTO, UserViewObject> {

    @Override
    public UserViewObject apply(UserDTO dto) throws Exception {
        return new UserViewObject(
                dto.getId(),
                dto.getFirstName(),
                dto.getLastName(),
                dto.getImageUrl(),
                UserRole.forValue(dto.getRole())
        );
    }
}
