package net.styleru.ikomarov.diabetics.di.view_model.providers;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelMutator;
import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelAccessor;

/**
 * Created by i_komarov on 12.05.17.
 */

public interface IToolbarViewModelProvider {

    @NonNull
    IToolbarModelMutator provideToolbarModelMutator();

    @NonNull
    IToolbarModelAccessor provideToolbarModelObserver();
}
