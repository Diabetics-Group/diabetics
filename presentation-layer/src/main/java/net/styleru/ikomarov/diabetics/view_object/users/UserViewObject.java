package net.styleru.ikomarov.diabetics.view_object.users;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.diabetics.contracts.user_role.UserRole;

/**
 * Created by i_komarov on 02.05.17.
 */

public class UserViewObject implements Parcelable {

    @NonNull
    private final String id;

    @NonNull
    private final String firstName;

    @NonNull
    private final String lastName;

    @Nullable
    private final String imageUrl;

    @NonNull
    private final UserRole role;

    protected UserViewObject(Parcel in) {
        this.id = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        String imageUrl = in.readString();
        this.imageUrl = imageUrl.equals("") ? null : imageUrl;
        this.role = UserRole.forValue(in.readString());
    }

    public UserViewObject(@NonNull String id,
                          @NonNull String firstName,
                          @NonNull String lastName,
                          @Nullable String imageUrl,
                          @NonNull UserRole role) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.role = role;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public UserRole getRole() {
        return role;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(imageUrl == null ? "" : imageUrl);
        dest.writeString(role.getValue());
    }

    public static final Creator<UserViewObject> CREATOR = new Creator<UserViewObject>() {
        @Override
        public UserViewObject createFromParcel(Parcel in) {
            return new UserViewObject(in);
        }

        @Override
        public UserViewObject[] newArray(int size) {
            return new UserViewObject[size];
        }
    };
}
