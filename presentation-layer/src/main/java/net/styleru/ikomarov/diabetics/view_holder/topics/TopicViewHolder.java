package net.styleru.ikomarov.diabetics.view_holder.topics;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.bumptech.glide.RequestManager;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.contracts.locale.DateTimeContract;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;

import java.text.ParseException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicViewHolder extends RecyclerView.ViewHolder {

    private AppCompatTextView commentsCountHolder;
    private AppCompatTextView lastUpdateHolder;
    private AppCompatTextView titleHolder;
    private CircleImageView imageHolder;
    private AppCompatTextView nameHolder;

    private int position;
    private TopicViewObject item;
    private InteractionCallbacks callbacks;

    public TopicViewHolder(View itemView) {
        super(itemView);

        commentsCountHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_topic_answers_count);
        lastUpdateHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_topic_last_update);
        titleHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_topic_title);
        imageHolder = (CircleImageView) itemView.findViewById(R.id.list_item_topic_author_image);
        nameHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_topic_author_name);
    }

    public void bindCallbacks() {
        itemView.setOnClickListener((view) -> {
            if(callbacks != null) {
                callbacks.clicks(position, item);
            }
        });
    }

    public void unbindCallbacks() {
        itemView.setOnClickListener(null);
    }

    public void bind(RequestManager glide, int position, TopicViewObject item, InteractionCallbacks listener) {
        this.position = position;
        this.item = item;
        this.callbacks = listener;

        if(item.getCommentsCount() != null && item.getCommentsCount() != 0) {
            commentsCountHolder.setText(itemView.getResources().getQuantityString(R.plurals.comments_count, item.getCommentsCount(), item.getCommentsCount()));
        } else {
            commentsCountHolder.setText(itemView.getResources().getString(R.string.comments_count_zero));
        }

        try {
            lastUpdateHolder.setText(DateTimeContract.displayDateFormat.format(DateTimeContract.parseDateFormat.parse(item.getLastUpdate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        titleHolder.setText(item.getTitle());
        if(!TextUtils.isEmpty(item.getImageUrl())) {
            glide.load(item.getImageUrl()).into(imageHolder);
        }

        nameHolder.setText(item.getFirstName() + " " + item.getLastName());
    }

    public interface InteractionCallbacks {

        void clicks(int position, TopicViewObject item);
    }
}
