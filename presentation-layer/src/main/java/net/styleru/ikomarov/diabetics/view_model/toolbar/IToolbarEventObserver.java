package net.styleru.ikomarov.diabetics.view_model.toolbar;

/**
 * Created by i_komarov on 11.05.17.
 */

public interface IToolbarEventObserver {

    void onNewTitle(String title);

    void onNewSubtitle(String subtitle);
}
