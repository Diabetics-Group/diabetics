package net.styleru.ikomarov.diabetics.mapping.categories;

import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;
import net.styleru.ikomarov.domain_layer.dto.categories.CategoryDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 04.05.17.
 */

public class CategoryMapping implements Function<CategoryDTO, CategoryViewObject> {

    @Override
    public CategoryViewObject apply(CategoryDTO dto) throws Exception {
        return new CategoryViewObject(
                dto.getId(),
                dto.getTitle(),
                dto.getLastUpdate(),
                dto.getTopicsCount().intValue(),
                dto.isSynchronized()
        );
    }
}
