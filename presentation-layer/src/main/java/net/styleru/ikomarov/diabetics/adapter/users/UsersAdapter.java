package net.styleru.ikomarov.diabetics.adapter.users;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.utils.serialization.ParcelableStringHashMap;
import net.styleru.ikomarov.diabetics.view_holder.users.UserViewHolder;
import net.styleru.ikomarov.diabetics.view_object.users.UserViewObject;

import java.util.List;
import java.util.Map;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UsersAdapter extends RecyclerView.Adapter<UserViewHolder> {

    private RequestManager glide;

    private ParcelableStringHashMap<UserViewObject> itemMap;

    private SortedList<UserViewObject> items;

    private UserViewHolder.InteractionCallbacks callbacks;

    public UsersAdapter(@NonNull RequestManager glide) {
        this.glide = glide;
        itemMap = new ParcelableStringHashMap<>(UserViewObject.class);
        init(itemMap);
    }

    private void init(Map<String, UserViewObject> itemMap) {
        items = new SortedList<>(UserViewObject.class, new UsersAdapterCallbacks(this));
        items.addAll(itemMap.values());
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_participant, parent, false));
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.bind(glide, position, items.get(position), callbacks);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onViewAttachedToWindow(UserViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bindCallbacks();
    }

    @Override
    public void onViewDetachedFromWindow(UserViewHolder holder) {
        holder.unbindCallbacks();
        super.onViewDetachedFromWindow(holder);
    }

    public void setCallbacks(UserViewHolder.InteractionCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void addItems(List<UserViewObject> items) {
        boolean allExists = true;

        for(int i = 0; i < items.size(); i++) {
            UserViewObject newItem = items.get(i);
            UserViewObject oldItem = itemMap.get(newItem.getId());

            if(oldItem == null) {
                allExists = false;
                break;
            }
        }

        if(allExists) {
            performBatchUpdateWithoutNewItems(items);
        } else {
            performBatchUpdateWithNewItems(items);
        }
    }

    private void performBatchUpdateWithoutNewItems(List<UserViewObject> items) {
        for(int i = 0; i < items.size(); i++) {
            UserViewObject newItem = items.get(i);
            UserViewObject oldItem = itemMap.get(newItem.getId());

            this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
            this.itemMap.put(newItem.getId(), newItem);
        }
    }

    private void performBatchUpdateWithNewItems(List<UserViewObject> items) {
        try {
            this.items.beginBatchedUpdates();
            for (int i = 0; i < items.size(); i++) {
                UserViewObject newItem = items.get(i);
                UserViewObject oldItem = itemMap.get(newItem.getId());

                if (oldItem != null) {
                    this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
                } else {
                    this.items.add(newItem);
                }

                this.itemMap.put(newItem.getId(), newItem);
            }

        } finally {
            this.items.endBatchedUpdates();
        }
    }

    public Parcelable onSaveInstanceState() {
        return this.itemMap;
    }

    public void onRestoreInstanceState(Parcelable savedState) {
        this.itemMap = (ParcelableStringHashMap<UserViewObject>) savedState;
        this.items.beginBatchedUpdates();
        this.items.addAll(itemMap.values());
        this.items.endBatchedUpdates();
    }
}
