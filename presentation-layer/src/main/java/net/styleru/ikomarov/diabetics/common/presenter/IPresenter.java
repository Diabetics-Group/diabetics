package net.styleru.ikomarov.diabetics.common.presenter;

import net.styleru.ikomarov.diabetics.common.view.IView;
import net.styleru.ikomarov.diabetics.common.routers.BaseRouter;

/**
 * Created by i_komarov on 28.04.17.
 */

public interface IPresenter<V extends IView, R extends BaseRouter> {

    void attachView(V view, R router);

    void detachView();
}
