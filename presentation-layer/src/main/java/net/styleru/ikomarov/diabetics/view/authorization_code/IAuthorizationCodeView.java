package net.styleru.ikomarov.diabetics.view.authorization_code;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.common.view.IView;

/**
 * Created by i_komarov on 02.06.17.
 */

public interface IAuthorizationCodeView extends IView {

    void onPhoneAuthenticationStarted();

    void onPhoneAuthenticationSuccess();

    void onPhoneAuthenticationFinished();

    void setCodeError(@NonNull String message);

    void onCodeAuthenticationStarted();

    void onCodeAuthenticationFinished();
}
