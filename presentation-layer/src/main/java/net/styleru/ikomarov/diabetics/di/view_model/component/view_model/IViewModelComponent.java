package net.styleru.ikomarov.diabetics.di.view_model.component.view_model;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.di.view_model.component.validation.IValidationComponent;
import net.styleru.ikomarov.diabetics.di.view_model.resource.LabelsModule;
import net.styleru.ikomarov.diabetics.di.view_model.resource.NotificationsModule;
import net.styleru.ikomarov.diabetics.di.view_model.toolbar.ToolbarModule;

/**
 * Created by i_komarov on 12.05.17.
 */

public interface IViewModelComponent {

    @NonNull
    IValidationComponent plusValidationComponent();

    @NonNull
    ToolbarModule toolbarViewModels();

    @NonNull
    LabelsModule labels();

    @NonNull
    NotificationsModule notifications();
}
