package net.styleru.ikomarov.diabetics.view_model.user;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.diabetics.contracts.user_role.UserRole;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by i_komarov on 05.06.17.
 */

public class UserViewModel implements Parcelable, IUserModelAccessor, IUserModelMutator {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private String id;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @Nullable
    private String imageUrl;

    @NonNull
    private UserRole role;

    @NonNull
    private String token;

    private final Set<IUserEventObserver> observers;

    public UserViewModel(@NonNull String id,
                         @NonNull String firstName,
                         @NonNull String lastName,
                         @Nullable String imageUrl,
                         @NonNull UserRole role,
                         @NonNull String token) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.role = role;
        this.token = token;
        this.observers = new HashSet<>();
    }


    protected UserViewModel(Parcel in) {
        id = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        String temp = in.readString();
        imageUrl = temp.equals("") ? null : temp;
        role = UserRole.forValue(in.readString());
        token = in.readString();
        this.observers = new HashSet<>();
    }

    @NonNull
    public String getId() {
        return id;
    }

    @Override
    public void setFirstName(@NonNull String firstName) {
        synchronized (lock) {
            this.firstName = firstName;
            for(IUserEventObserver observer : observers) {
                observer.onFirstNameChange(this.firstName);
            }
        }
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @Override
    public void setLastName(@NonNull String lastName) {
        synchronized (lock) {
            this.lastName = lastName;
            for(IUserEventObserver observer : observers) {
                observer.onLastNameChange(this.lastName);
            }
        }
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @Override
    public void setImageUrl(@NonNull String imageUrl) {
        synchronized (lock) {
            this.imageUrl = imageUrl;
            for(IUserEventObserver observer : observers) {
                observer.onImageUrlChange(this.imageUrl);
            }
        }
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public void setRole(@NonNull UserRole role) {
        synchronized (lock) {
            this.role = role;
            for(IUserEventObserver observer : observers) {
                observer.onRoleChange(this.role);
            }
        }
    }

    @NonNull
    public UserRole getRole() {
        return role;
    }

    @NonNull
    public String getToken() {
        return token;
    }

    @Override
    public void registerObserver(@NonNull IUserEventObserver observer) {
        if(!observers.contains(observer)) {
            synchronized (lock) {
                if(!observers.contains(observer)) {
                    observers.add(observer);
                }
            }
        }
    }

    @Override
    public void unregisterObserver(@NonNull IUserEventObserver observer) {
        if(observers.contains(observer)) {
            synchronized (lock) {
                if(observers.contains(observer)) {
                    observers.remove(observer);
                }
            }
        }
    }

    @Override
    public void closeSession() {
        synchronized (lock) {
            for(IUserEventObserver observer : observers) {
                observer.onSessionClosing();
            }
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(imageUrl != null ? imageUrl : "");
        dest.writeString(role.getValue());
        dest.writeString(token);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserViewModel> CREATOR = new Creator<UserViewModel>() {
        @Override
        public UserViewModel createFromParcel(Parcel in) {
            return new UserViewModel(in);
        }

        @Override
        public UserViewModel[] newArray(int size) {
            return new UserViewModel[size];
        }
    };
}
