package net.styleru.ikomarov.diabetics.adapter.topics;

import android.os.Parcelable;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.utils.serialization.ParcelableStringHashMap;
import net.styleru.ikomarov.diabetics.view_holder.topics.TopicViewHolder;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;

import java.util.List;
import java.util.Map;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsAdapter extends RecyclerView.Adapter<TopicViewHolder> {

    private RequestManager glide;

    private ParcelableStringHashMap<TopicViewObject> itemMap;

    private SortedList<TopicViewObject> items;

    private TopicViewHolder.InteractionCallbacks callbacks;

    public TopicsAdapter(RequestManager glide) {
        this.glide = glide;
        itemMap = new ParcelableStringHashMap<>(TopicViewObject.class);
        init(itemMap);
    }

    private void init(Map<String, TopicViewObject> itemMap) {
        items = new SortedList<>(TopicViewObject.class, new TopicsAdapterCallbacks(this));
        items.addAll(itemMap.values());
    }

    @Override
    public TopicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TopicViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_topic, parent, false));
    }

    @Override
    public void onBindViewHolder(TopicViewHolder holder, int position) {
        holder.bind(glide, position, items.get(position), callbacks);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onViewAttachedToWindow(TopicViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bindCallbacks();
    }

    @Override
    public void onViewDetachedFromWindow(TopicViewHolder holder) {
        holder.unbindCallbacks();
        super.onViewDetachedFromWindow(holder);
    }

    public void setCallbacks(TopicViewHolder.InteractionCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void addItems(List<TopicViewObject> items) {
        boolean allExists = true;

        for(int i = 0; i < items.size(); i++) {
            TopicViewObject newItem = items.get(i);
            TopicViewObject oldItem = itemMap.get(newItem.getId());

            if(oldItem == null) {
                allExists = false;
                break;
            }
        }

        if(allExists) {
            performBatchUpdateWithoutNewItems(items);
        } else {
            performBatchUpdateWithNewItems(items);
        }
    }

    private void performBatchUpdateWithoutNewItems(List<TopicViewObject> items) {
        for(int i = 0; i < items.size(); i++) {
            TopicViewObject newItem = items.get(i);
            TopicViewObject oldItem = itemMap.get(newItem.getId());

            this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
            this.itemMap.put(newItem.getId(), newItem);
        }
    }

    private void performBatchUpdateWithNewItems(List<TopicViewObject> items) {
        try {
            this.items.beginBatchedUpdates();
            for (int i = 0; i < items.size(); i++) {
                TopicViewObject newItem = items.get(i);
                TopicViewObject oldItem = itemMap.get(newItem.getId());

                if (oldItem != null) {
                    this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
                } else {
                    this.items.add(newItem);
                }

                this.itemMap.put(newItem.getId(), newItem);
            }

        } finally {
            this.items.endBatchedUpdates();
        }
    }

    public Parcelable onSaveInstanceState() {
        return this.itemMap;
    }

    public void onRestoreInstanceState(Parcelable savedState) {
        this.itemMap = (ParcelableStringHashMap<TopicViewObject>) savedState;
        this.items.beginBatchedUpdates();
        this.items.addAll(itemMap.values());
        this.items.endBatchedUpdates();
    }
}
