package net.styleru.ikomarov.diabetics.view.users;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.adapter.users.UsersAdapter;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.view.ViperFragment;
import net.styleru.ikomarov.diabetics.contracts.screens.ScreenContract;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IResourceManagersProvider;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IToolbarViewModelProvider;
import net.styleru.ikomarov.diabetics.presenter.users.UsersPresenter;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.diabetics.utils.pagination.PaginationListener;
import net.styleru.ikomarov.diabetics.utils.pagination.State;
import net.styleru.ikomarov.diabetics.view.main.IMainView;
import net.styleru.ikomarov.diabetics.view_holder.users.UserViewHolder;
import net.styleru.ikomarov.diabetics.view_object.users.UserViewObject;
import net.styleru.ikomarov.domain_layer.di.providers.IExecutionEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IUsersModelProvider;
import net.styleru.ikomarov.domain_layer.use_cases.users.UsersGetUseCase;

import java.util.List;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UsersFragment extends ViperFragment<IUsersView, MainRouter, UsersPresenter> implements IUsersView, UserViewHolder.InteractionCallbacks, PaginationListener.Callback {

    private UsersFragmentState state;

    private RecyclerView list;

    private UsersAdapter adapter;

    private PaginationListener listener;

    public static Fragment newInstance() {
        return new UsersFragment();
    }

    @Override
    public void addUsers(List<UserViewObject> items) {
        adapter.addItems(items);
    }

    @Override
    public void onListStateChanged(State state) {
        getPresenter().loadUsers(state.getOffset(), state.getLimit());
    }

    @Override
    public void clicks(int position, UserViewObject item) {

    }

    @Override
    public void onActivityResultDelivered(int requestCode, int resultCode, Intent data) {

    }

    @Override
    protected void bindUserInterface(View root) {
        list = (RecyclerView) root.findViewById(R.id.fragment_participants_list_recycler_view);
        list.setLayoutManager(new LinearLayoutManager(root.getContext()));
        list.setAdapter(adapter == null ? adapter = new UsersAdapter(Glide.with(getActivity())) : adapter);
        listener = new PaginationListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        list.addOnScrollListener(listener);
        adapter.setCallbacks(this);
        if(adapter.getItemCount() == 0) {
            getPresenter().loadUsers(0, listener.getLimit());
        }
    }

    @Override
    public void onPause() {
        adapter.setCallbacks(null);
        list.removeOnScrollListener(listener);
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        state.saveListState(outState, list);
        state.saveAdapterState(outState, adapter);
    }

    @Override
    protected void onViewStateDelivered(Bundle savedInstanceState) {
        if(state.getListState() != null) {
            list.getLayoutManager().onRestoreInstanceState(state.getListState());
        }
        if(state.getAdapterState() != null) {
            adapter.onRestoreInstanceState(state.getAdapterState());
        }
    }

    @NonNull
    @Override
    protected IUsersView provideView() {
        return this;
    }

    @NonNull
    @Override
    protected MainRouter provideRouter() {
        return ((IMainView) getActivity()).getRouter();
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.fragment_participants_list;
    }

    @Override
    protected int provideScreenId() {
        return ScreenContract.USERS.getCode();
    }

    @NonNull
    @Override
    protected ViperPresenter.Factory<IUsersView, MainRouter, UsersPresenter> provideFactory() {
        IUsersModelProvider modelProvider = (IUsersModelProvider) getActivity().getApplication();
        IExecutionEnvironmentProvider executionEnvironmentProvider = (IExecutionEnvironmentProvider) getActivity().getApplication();
        IResourceManagersProvider resourceManagersProvider = (IResourceManagersProvider) getActivity();
        IToolbarViewModelProvider toolbarViewModelProvider = (IToolbarViewModelProvider) getActivity();

        return new UsersPresenter.Factory(
                resourceManagersProvider.provideLabelsManager(),
                toolbarViewModelProvider.provideToolbarModelMutator(),
                new UsersGetUseCase(
                        modelProvider.provideUsersRepository(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler()
                )
        );
    }
}
