package net.styleru.ikomarov.diabetics.mapping.users;

import net.styleru.ikomarov.diabetics.contracts.user_role.UserRole;
import net.styleru.ikomarov.diabetics.view_model.user.UserViewModel;
import net.styleru.ikomarov.domain_layer.dto.users.UserExtendedDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 05.06.17.
 */

public class UserModelMapping implements Function<UserExtendedDTO, UserViewModel> {

    @Override
    public UserViewModel apply(UserExtendedDTO dto) throws Exception {
        return new UserViewModel(
                dto.getId(),
                dto.getFirstName(),
                dto.getLastName(),
                dto.getImageUrl(),
                UserRole.forValue(dto.getRole()),
                dto.getToken()
        );
    }
}
