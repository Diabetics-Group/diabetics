package net.styleru.ikomarov.diabetics.adapter.categories;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;

import net.styleru.ikomarov.diabetics.contracts.locale.DateTimeContract;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;

import java.text.ParseException;


/**
 * Created by i_komarov on 03.05.17.
 */

public class CategoriesAdapterCallbacks extends SortedListAdapterCallback<CategoryViewObject> {

    public CategoriesAdapterCallbacks(RecyclerView.Adapter adapter) {
        super(adapter);
    }

    @Override
    public int compare(CategoryViewObject o1, CategoryViewObject o2) {
        try {
            return DateTimeContract.parseDateFormat.parse(o2.getLastUpdate()).compareTo(DateTimeContract.parseDateFormat.parse(o1.getLastUpdate()));
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public boolean areContentsTheSame(CategoryViewObject oldItem, CategoryViewObject newItem) {
        return oldItem.getLastUpdate().equals(newItem.getLastUpdate()) &&
                oldItem.getTitle().equals(newItem.getTitle()) &&
                oldItem.getTopicsCount().equals(newItem.getTopicsCount()) &&
                oldItem.getSynchronized().equals(newItem.getSynchronized());
    }

    @Override
    public boolean areItemsTheSame(CategoryViewObject item1, CategoryViewObject item2) {
        return item1.getId().equals(item2.getId());
    }
}
