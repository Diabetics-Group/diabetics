package net.styleru.ikomarov.diabetics.routers.authorization;

import net.styleru.ikomarov.diabetics.common.routers.IWizardRouter;
import net.styleru.ikomarov.diabetics.common.view.ITransactionFunction;
import net.styleru.ikomarov.diabetics.view_model.user.UserViewModel;

/**
 * Created by i_komarov on 02.06.17.
 */

public interface IAuthorizationRouter extends IWizardRouter<UserViewModel> {

    void navigateUp();

    void instantiateDefaultFragment();

    void navigateToVerification(String phone, ITransactionFunction function);
}
