package net.styleru.ikomarov.diabetics.di.view_model.component.view_model;

import android.content.Context;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.di.view_model.component.validation.IValidationComponent;
import net.styleru.ikomarov.diabetics.di.view_model.component.validation.ValidationComponent;
import net.styleru.ikomarov.diabetics.di.view_model.resource.LabelsModule;
import net.styleru.ikomarov.diabetics.di.view_model.resource.NotificationsModule;
import net.styleru.ikomarov.diabetics.di.view_model.toolbar.ToolbarModule;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 12.05.17.
 */

public class ViewModelComponent implements IViewModelComponent {

    @NonNull
    private final IAppComponent component;

    @NonNull
    private final ToolbarModule toolbar;

    @NonNull
    private final LabelsModule labels;

    @NonNull
    private final NotificationsModule notifications;

    public ViewModelComponent(@NonNull IAppComponent component) {
        this.component = component;
        this.toolbar = new ToolbarModule();
        this.labels = new LabelsModule(component.app().provideContext().getResources());
        this.notifications = new NotificationsModule(component.app().provideContext().getResources());
    }

    @NonNull
    @Override
    public IValidationComponent plusValidationComponent() {
        return new ValidationComponent(this);
    }

    @NonNull
    @Override
    public ToolbarModule toolbarViewModels() {
        return toolbar;
    }

    @NonNull
    @Override
    public LabelsModule labels() {
        return labels;
    }

    @NonNull
    @Override
    public NotificationsModule notifications() {
        return notifications;
    }
}
