package net.styleru.ikomarov.diabetics.presenter.authorization_code;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.exception.ValidationExceptionContract;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.exceptions.ExceptionHandler;
import net.styleru.ikomarov.diabetics.managers.resources.notifications.INotificationsManager;
import net.styleru.ikomarov.diabetics.mapping.users.UserModelMapping;
import net.styleru.ikomarov.diabetics.routers.authorization.AuthorizationRouter;
import net.styleru.ikomarov.diabetics.view.authorization_code.IAuthorizationCodeView;
import net.styleru.ikomarov.diabetics.view_model.user.UserViewModel;
import net.styleru.ikomarov.domain_layer.dto.users.UserExtendedDTO;
import net.styleru.ikomarov.domain_layer.use_cases.session.CodeAuthenticationUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.session.PhoneAuthenticationUseCase;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 02.06.17.
 */

public class AuthorizationCodePresenter extends ViperPresenter<IAuthorizationCodeView, AuthorizationRouter> implements CodeAuthenticationUseCase.Callbacks<UserViewModel>, PhoneAuthenticationUseCase.Callbacks {

    @NonNull
    private final INotificationsManager notificationsManager;

    @NonNull
    private final CodeAuthenticationUseCase codeAuthentication;

    @NonNull
    private final PhoneAuthenticationUseCase phoneAuthentication;

    @NonNull
    private final Function<UserExtendedDTO, UserViewModel> userModelMapper;

    @NonNull
    private final ExceptionHandler handler;

    private AuthorizationCodePresenter(@NonNull INotificationsManager notificationsManager,
                                       @NonNull CodeAuthenticationUseCase codeAuthentication,
                                       @NonNull PhoneAuthenticationUseCase phoneAuthentication) {

        this.notificationsManager = notificationsManager;
        this.codeAuthentication = codeAuthentication;
        this.phoneAuthentication = phoneAuthentication;
        this.userModelMapper = new UserModelMapping();
        this.handler = new ExceptionHandler();
    }

    @Override
    protected void onViewAttach() {

    }

    @Override
    protected void onViewDetach() {
        codeAuthentication.dispose();
    }

    @Override
    protected void onDestroy() {
        codeAuthentication.dispose();
    }

    public void onWrongPhoneButtonClicks() {
        getRouter().navigateUp();
    }

    public void onContinueButtonClicks(String phone, String code) {
        codeAuthentication.execute(
                this,
                userModelMapper,
                phone,
                code
        );
    }

    public void onResendCodeButtonClicks(String phone) {
        phoneAuthentication.execute(
                this,
                phone,
                false
        );
    }

    @Override
    public void onStartCodeAuthentication() {
        getView().onCodeAuthenticationStarted();
    }

    @Override
    public void onCodeAuthenticationSuccess(@NonNull UserViewModel model) {
        getRouter().returnOnSuccess(model);
    }

    @Override
    public void onCodeVerificationFailure(ExceptionBundle error) {
        handler.handleDebug(error);
        switch(error.getReason()) {
            case VALIDATION: {
                ValidationExceptionContract contract = error.getReason().getContract();
                getView().setCodeError(contract.extractMessage(error));
                break;
            }
        }
    }

    @Override
    public void onFinishCodeAuthentication() {
        getView().onCodeAuthenticationFinished();
    }

    @Override
    public void onPhoneAuthenticationStarted() {
        getView().onPhoneAuthenticationStarted();
    }

    @Override
    public void onPhoneAuthenticationSuccess(@NonNull String phone) {
        getView().onPhoneAuthenticationSuccess();
    }

    @Override
    public void onPhoneAuthenticationFailure(ExceptionBundle error) {
        //TODO: what should I do here ???
    }

    @Override
    public void onPhoneAuthenticationFinished() {
        getView().onPhoneAuthenticationFinished();
    }

    public static final class Factory implements ViperPresenter.Factory<IAuthorizationCodeView, AuthorizationRouter, AuthorizationCodePresenter> {

        @NonNull
        private final INotificationsManager notificationsManager;

        @NonNull
        private final CodeAuthenticationUseCase codeAuthentication;

        @NonNull
        private final PhoneAuthenticationUseCase phoneAuthentication;

        public Factory(@NonNull INotificationsManager notificationsManager,
                       @NonNull CodeAuthenticationUseCase codeAuthentication,
                       @NonNull PhoneAuthenticationUseCase phoneAuthentication) {

            this.notificationsManager = notificationsManager;
            this.codeAuthentication = codeAuthentication;
            this.phoneAuthentication = phoneAuthentication;
        }

        @Override
        public AuthorizationCodePresenter create() {
            return new AuthorizationCodePresenter(notificationsManager, codeAuthentication, phoneAuthentication);
        }
    }
}
