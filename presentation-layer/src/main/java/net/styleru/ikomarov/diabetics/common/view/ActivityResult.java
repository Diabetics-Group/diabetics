package net.styleru.ikomarov.diabetics.common.view;

import android.content.Intent;

/**
 * Created by i_komarov on 02.05.17.
 */

public class ActivityResult {

    private final int requestCode;

    private final int resultCode;

    private final Intent data;

    public ActivityResult(int requestCode, int resultCode, Intent data) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public int getResultCode() {
        return resultCode;
    }

    public Intent getData() {
        return data;
    }
}
