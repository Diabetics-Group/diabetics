package net.styleru.ikomarov.diabetics.common.container;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

/**
 * Created by i_komarov on 12.05.17.
 */

public class RetainContainer<Component> extends Fragment {

    @NonNull
    private final Object lock = new Object();

    @Nullable
    private volatile Component component;

    private Factory<Component> factory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @NonNull
    public Component provideComponent() {
        Component localInstance = component;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = component;
                if(localInstance == null) {
                    localInstance = component = factory.create();
                }
            }
        }

        return localInstance;
    }

    public void setFactory(Factory<Component> factory) {
        this.factory = factory;
    }

    public interface Factory<T> {

        T create();
    }
}
