package net.styleru.ikomarov.diabetics.view.authorization_code;

import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 02.06.17.
 */

public class AuthorizationCodeFragmentState {

    @NonNull
    private static final String PREFIX = AuthorizationCodeFragmentState.class.getCanonicalName() + ".";

    @NonNull
    private static final String KEY_STATE_PHONE = PREFIX + "STATE_PHONE";

    @NonNull
    private final String phone;

    public AuthorizationCodeFragmentState(@NonNull String phone) {
        this.phone = phone;
    }

    public AuthorizationCodeFragmentState(Bundle args, Bundle saved) {
        phone = (args != null ? args : saved).getString(KEY_STATE_PHONE);
    }

    public Bundle asBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_STATE_PHONE, phone);
        return bundle;
    }

    public void saveState(Bundle outState) {
        outState.putString(KEY_STATE_PHONE, phone);
    }

    @NonNull
    public String getPhone() {
        return this.phone;
    }
}
