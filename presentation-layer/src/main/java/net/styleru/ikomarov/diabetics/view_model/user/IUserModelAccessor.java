package net.styleru.ikomarov.diabetics.view_model.user;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.diabetics.contracts.user_role.UserRole;

/**
 * Created by i_komarov on 05.06.17.
 */

public interface IUserModelAccessor {

    @NonNull
    String getId();

    @NonNull
    String getFirstName();

    @NonNull
    String getLastName();

    @Nullable
    String getImageUrl();

    @NonNull
    UserRole getRole();

    @NonNull
    String getToken();

    void registerObserver(@NonNull IUserEventObserver observer);

    void unregisterObserver(@NonNull IUserEventObserver observer);
}
