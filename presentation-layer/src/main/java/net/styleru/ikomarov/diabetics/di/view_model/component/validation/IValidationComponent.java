package net.styleru.ikomarov.diabetics.di.view_model.component.validation;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.di.view_model.validation.ValidationModule;

/**
 * Created by i_komarov on 05.06.17.
 */

public interface IValidationComponent {

    @NonNull
    ValidationModule validation();
}
