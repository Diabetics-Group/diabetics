package net.styleru.ikomarov.diabetics.view_holder.categories;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.contracts.locale.DateTimeContract;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by i_komarov on 03.05.17.
 */

public class CategoryViewHolder extends RecyclerView.ViewHolder {

    private AppCompatTextView topicsCountHolder;
    private AppCompatTextView lastUpdateHolder;
    private AppCompatTextView titleHolder;

    private int position;
    private CategoryViewObject item;
    private InteractionCallbacks callbacks;

    public CategoryViewHolder(View itemView) {
        super(itemView);

        topicsCountHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_category_topics_count);
        lastUpdateHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_category_last_update);
        titleHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_category_title);
    }

    public void bindCallbacks() {
        itemView.setOnClickListener((view) -> {
            if(callbacks != null) {
                callbacks.clicks(position, item);
            }
        });
    }

    public void unbindCallbacks() {
        itemView.setOnClickListener(null);
    }

    public void bind(int position, CategoryViewObject item, InteractionCallbacks callbacks) {
        this.position = position;
        this.item = item;
        this.callbacks = callbacks;

        titleHolder.setText(item.getTitle());
        if(item.getTopicsCount() != 0) {
            topicsCountHolder.setText(itemView.getResources().getQuantityString(R.plurals.topics_count, item.getTopicsCount() != null ? item.getTopicsCount() : 0, item.getTopicsCount() != null ? item.getTopicsCount() : 0));
        } else {
            topicsCountHolder.setText(itemView.getResources().getString(R.string.topics_count_zero));
        }

        try {
            lastUpdateHolder.setText(DateTimeContract.displayDateFormat.format(DateTimeContract.parseDateFormat.parse(item.getLastUpdate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public interface InteractionCallbacks {

        void clicks(int position, CategoryViewObject item);
    }
}
