package net.styleru.ikomarov.diabetics.utils.pagination;

/**
 * Created by i_komarov on 04.05.17.
 */

public class State {

    private final int offset;
    private final int limit;

    public State(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }
}
