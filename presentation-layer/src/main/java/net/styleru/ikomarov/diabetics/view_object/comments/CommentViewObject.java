package net.styleru.ikomarov.diabetics.view_object.comments;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by i_komarov on 02.05.17.
 */

public class CommentViewObject implements Parcelable {

    @NonNull
    private String id;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @Nullable
    private String imageUrl;

    @NonNull
    private String role;

    @NonNull
    private String content;

    @NonNull
    private String date;

    @NonNull
    private Boolean isSynchronized;

    public CommentViewObject(@NonNull String id,
                             @NonNull String firstName,
                             @NonNull String lastName,
                             @Nullable String imageUrl,
                             @NonNull String role,
                             @NonNull String content,
                             @NonNull String date,
                             @NonNull Boolean isSynchronized) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.role = role;
        this.content = content;
        this.date = date;
        this.isSynchronized = isSynchronized;
    }

    protected CommentViewObject(Parcel in) {
        String temp;
        id = (temp = in.readString()).equals("") ? null : temp;
        firstName = (temp = in.readString()).equals("") ? null : temp;
        lastName = (temp = in.readString()).equals("") ? null : temp;
        imageUrl = (temp = in.readString()).equals("") ? null : temp;
        role = (temp = in.readString()).equals("") ? null : temp;
        content = (temp = in.readString()).equals("") ? null : temp;
        date = (temp = in.readString()).equals("") ? null : temp;
        isSynchronized = in.readByte() == (byte) 1;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public String getRole() {
        return role;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    @NonNull
    public Boolean isSynchronized() {
        return isSynchronized;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(firstName != null ? firstName : "");
        dest.writeString(lastName != null ? lastName : "");
        dest.writeString(imageUrl != null ? imageUrl : "");
        dest.writeString(role != null ? role : "");
        dest.writeString(content != null ? content : "");
        dest.writeString(date != null ? date : "");
        dest.writeByte(isSynchronized != null && isSynchronized ? (byte) 1 : (byte) 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CommentViewObject> CREATOR = new Creator<CommentViewObject>() {
        @Override
        public CommentViewObject createFromParcel(Parcel in) {
            return new CommentViewObject(in);
        }

        @Override
        public CommentViewObject[] newArray(int size) {
            return new CommentViewObject[size];
        }
    };
}
