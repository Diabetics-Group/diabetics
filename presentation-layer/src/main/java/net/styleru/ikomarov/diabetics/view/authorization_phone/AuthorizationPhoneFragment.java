package net.styleru.ikomarov.diabetics.view.authorization_phone;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.view.ViperFragment;
import net.styleru.ikomarov.diabetics.contracts.screens.ScreenContract;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IResourceManagersProvider;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IValidationsProvider;
import net.styleru.ikomarov.diabetics.managers.view.ITransitionManager;
import net.styleru.ikomarov.diabetics.presenter.authorization_code.AuthorizationCodePresenter;
import net.styleru.ikomarov.diabetics.presenter.authorization_phone.AuthorizationPhonePresenter;
import net.styleru.ikomarov.diabetics.routers.authorization.AuthorizationRouter;
import net.styleru.ikomarov.diabetics.view.authorization.IAuthorizationView;
import net.styleru.ikomarov.domain_layer.di.providers.IAuthenticationModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IExecutionEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.use_cases.session.PhoneAuthenticationUseCase;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by i_komarov on 02.06.17.
 */

public class AuthorizationPhoneFragment extends ViperFragment<IAuthorizationPhoneView, AuthorizationRouter, AuthorizationPhonePresenter> implements IAuthorizationPhoneView {

    private AppCompatTextView phoneLabelHolder;
    private MaskedEditText phoneInputHolder;
    private AppCompatButton continueButton;
    private MaterialProgressBar phoneAuthenticationProgressBar;

    @NonNull
    public static Fragment newInstance() {
        Fragment fragment = new AuthorizationPhoneFragment();

        return fragment;
    }

    @Override
    public void setPhoneError(@NonNull String message) {
        phoneInputHolder.setError(message);
    }

    @Override
    public void onPhoneAuthenticationStarted() {
        TransitionManager.beginDelayedTransition((ViewGroup) getView());

        continueButton.setText("");
        continueButton.setEnabled(false);
        phoneInputHolder.setEnabled(false);
        phoneAuthenticationProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPhoneAuthenticationFinished() {
        TransitionManager.beginDelayedTransition((ViewGroup) getView());

        phoneAuthenticationProgressBar.setVisibility(View.GONE);
        phoneInputHolder.setEnabled(true);
        continueButton.setEnabled(true);
        continueButton.setText(R.string.action_continue);
    }

    @Override
    public ITransitionManager<String> getTransitionManager() {
        return new AuthorizationPhoneTransitionManager(getView());
    }

    @Override
    protected void bindUserInterface(View root) {
        phoneLabelHolder = (AppCompatTextView) root.findViewById(R.id.fragment_authorization_phone_enter_phone_label);
        phoneInputHolder = (MaskedEditText) root.findViewById(R.id.fragment_authorization_phone_enter_phone_field);
        continueButton = (AppCompatButton) root.findViewById(R.id.fragment_authorization_phone_continue_button);
        phoneAuthenticationProgressBar = (MaterialProgressBar) root.findViewById(R.id.fragment_authorization_phone_phone_authentication_progressbar);

        phoneInputHolder.setKeepHint(true);

        continueButton.setOnClickListener(this::onContinueButtonClicks);
    }

    @Override
    public void onActivityResultDelivered(int requestCode, int resultCode, Intent data) {
        //just ignore, nothing interesting lays here
    }

    @Override
    protected void onViewStateDelivered(Bundle savedInstanceState) {
        //just ignore, nothing interesting lays here
    }

    private void onContinueButtonClicks(View view) {
        String phone = "+7" + phoneInputHolder.getRawText();
        getPresenter().onContinueButtonClicks(phone);

        //provideRouter().navigateToVerification("79161687611", getTransitionManager().provideFunction(AuthorizationCodePresenter.class.getSimpleName()));
    }

    @NonNull
    @Override
    protected IAuthorizationPhoneView provideView() {
        return this;
    }

    @NonNull
    @Override
    protected AuthorizationRouter provideRouter() {
        return ((IAuthorizationView) getActivity()).getRouter();
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.fragment_authorization_phone;
    }

    @Override
    protected int provideScreenId() {
        return ScreenContract.AUTHORIZATION_PHONE.getCode();
    }

    @NonNull
    @Override
    protected ViperPresenter.Factory<IAuthorizationPhoneView, AuthorizationRouter, AuthorizationPhonePresenter> provideFactory() {
        IAuthenticationModelProvider modelProvider = (IAuthenticationModelProvider) getActivity().getApplication();
        IExecutionEnvironmentProvider executionEnvironmentProvider = (IExecutionEnvironmentProvider) getActivity().getApplication();
        IResourceManagersProvider resourceManagersProvider = (IResourceManagersProvider) getActivity();
        IValidationsProvider validationsProvider = (IValidationsProvider) getActivity();

        return new AuthorizationPhonePresenter.Factory(
                resourceManagersProvider.provideNotificationsManager(),
                new PhoneAuthenticationUseCase(
                        modelProvider.provideAuthenticationRepository(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler(),
                        validationsProvider.providerPhoneValidationStrategy()
                )
        );
    }
}
