package net.styleru.ikomarov.diabetics.common.view;

import android.support.annotation.NonNull;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by i_komarov on 02.05.17.
 */

public class ActivityResultBundle {

    @NonNull
    private final Queue<ActivityResult> results;

    public ActivityResultBundle() {
        this.results = new LinkedList<>();
    }

    public void offer(ActivityResult result) {
        results.offer(result);
    }

    public ActivityResult poll() {
        return results.poll();
    }

    public int size() {
        return results.size();
    }
}
