package net.styleru.ikomarov.diabetics.view_model.user;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.contracts.user_role.UserRole;

/**
 * Created by i_komarov on 05.06.17.
 */

public interface IUserEventObserver {

    void onFirstNameChange(@NonNull String firstName);

    void onLastNameChange(@NonNull String lastName);

    void onImageUrlChange(@NonNull String imageUrl);

    void onRoleChange(@NonNull UserRole role);

    void onSessionClosing();
}
