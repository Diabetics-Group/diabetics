package net.styleru.ikomarov.diabetics.presenter.authorization;

import net.styleru.ikomarov.diabetics.common.container.RetainContainer;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.routers.authorization.AuthorizationRouter;
import net.styleru.ikomarov.diabetics.view.authorization.IAuthorizationView;

/**
 * Created by i_komarov on 02.06.17.
 */

public class AuthorizationPresenter extends ViperPresenter<IAuthorizationView, AuthorizationRouter> {

    private AuthorizationPresenter() {

    }

    @Override
    protected void onViewAttach() {

    }

    @Override
    protected void onViewDetach() {

    }

    @Override
    protected void onDestroy() {

    }

    public void onInstantiateDefaultFragment() {
        getRouter().instantiateDefaultFragment();
    }

    public void onNavigateUp() {
        getRouter().navigateUp();
    }

    public static final class Factory implements ViperPresenter.Factory<IAuthorizationView, AuthorizationRouter, AuthorizationPresenter> {

        public Factory() {

        }

        @Override
        public AuthorizationPresenter create() {
            return new AuthorizationPresenter();
        }
    }
}
