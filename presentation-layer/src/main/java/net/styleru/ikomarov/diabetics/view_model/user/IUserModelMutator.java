package net.styleru.ikomarov.diabetics.view_model.user;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.contracts.user_role.UserRole;

/**
 * Created by i_komarov on 05.06.17.
 */

public interface IUserModelMutator {

    void setFirstName(@NonNull String firstName);

    void setLastName(@NonNull String lastName);

    void setImageUrl(@NonNull String imageUrl);

    void setRole(@NonNull UserRole role);

    void closeSession();
}
