package net.styleru.ikomarov.diabetics.validation.authentication;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.exception.Reason;
import net.styleru.ikomarov.data_layer.contracts.exception.ValidationExceptionContract;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.diabetics.managers.resources.notifications.INotificationsManager;
import net.styleru.ikomarov.domain_layer.validation.base.IValidationStrategy;

/**
 * Created by i_komarov on 04.06.17.
 */

public class PhoneValidationStrategy implements IValidationStrategy<String> {

    @NonNull
    private final INotificationsManager notificationsManager;

    public PhoneValidationStrategy(@NonNull INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    @Override
    public Boolean apply(String phone) throws ExceptionBundle {
        String universalPhoneContent = phone.replaceAll("\\+", "")
                .replaceAll("\\(", "")
                .replaceAll("\\)", "")
                .replaceAll("-", "");

        State state = null;
        if(universalPhoneContent.length() < 11) {
            state = State.LENGTH_FAILURE;
        } else {
            String countryCode;
            if(universalPhoneContent.length() == 11) {
                countryCode = universalPhoneContent.substring(0, 1);
            } else {
                countryCode = universalPhoneContent.substring(0, 2);
            }

            if(!ensureCounryCodeValidity(countryCode)) {
                state = State.COUNTRY_CODE_FAILURE;
            }
        }

        if(state != null) {
            throw prepareVerificationException(state);
        }

        return true;
    }

    private boolean ensureCounryCodeValidity(String countryCode) {
        return countryCode.equals("7");
    }

    private ExceptionBundle prepareVerificationException(State state) {
        ExceptionBundle error = new ExceptionBundle(Reason.VALIDATION);
        ValidationExceptionContract contract = error.getReason().getContract();

        switch(state) {
            case LENGTH_FAILURE: {
                contract.putMessage(error, notificationsManager.getBadPhoneLengthNotification());
                break;
            }
            case COUNTRY_CODE_FAILURE: {
                contract.putMessage(error, notificationsManager.getBadPhoneCountryCodeNotification());
                break;
            }
        }

        return error;
    }

    private enum State {
        LENGTH_FAILURE,
        COUNTRY_CODE_FAILURE
    }
}
