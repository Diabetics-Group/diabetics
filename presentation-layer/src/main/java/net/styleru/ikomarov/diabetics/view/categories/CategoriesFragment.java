package net.styleru.ikomarov.diabetics.view.categories;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.adapter.categories.CategoriesAdapter;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.view.ViperFragment;
import net.styleru.ikomarov.diabetics.contracts.screens.ScreenContract;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IResourceManagersProvider;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IToolbarViewModelProvider;
import net.styleru.ikomarov.diabetics.presenter.categories.CategoriesPresenter;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.diabetics.utils.pagination.PaginationListener;
import net.styleru.ikomarov.diabetics.utils.pagination.State;
import net.styleru.ikomarov.diabetics.view.main.IMainView;
import net.styleru.ikomarov.diabetics.view_holder.categories.CategoryViewHolder;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;
import net.styleru.ikomarov.domain_layer.di.providers.ICategoriesModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IExecutionEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.use_cases.categories.CategoriesGetUseCase;

import java.util.List;

/**
 * Created by i_komarov on 02.05.17.
 */

public class CategoriesFragment extends ViperFragment<ICategoriesView, MainRouter, CategoriesPresenter> implements ICategoriesView, PaginationListener.Callback, CategoryViewHolder.InteractionCallbacks {

    private RecyclerView list;

    private CategoriesAdapter adapter;

    private PaginationListener listener;

    private CategoriesFragmentState state;

    public static Fragment newInstance() {
        return new CategoriesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        state = new CategoriesFragmentState(getArguments(), savedInstanceState);
    }

    @Override
    public void addCategories(List<CategoryViewObject> categories) {
        adapter.addItems(categories);
    }

    @Override
    public void clicks(int position, CategoryViewObject item) {
        getPresenter().onCategoryClicks(item);
    }

    @Override
    public void onListStateChanged(State state) {
        getPresenter().loadCategories(state.getOffset(), state.getLimit());
    }

    @Override
    public void onActivityResultDelivered(int requestCode, int resultCode, Intent data) {

    }

    @Override
    protected void bindUserInterface(View root) {
        list = (RecyclerView) root.findViewById(R.id.fragment_categories_list_recycler_view);
        list.setLayoutManager(new LinearLayoutManager(root.getContext()));
        list.setAdapter(adapter == null ? adapter = new CategoriesAdapter() : adapter);
        listener = new PaginationListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        list.addOnScrollListener(listener);
        adapter.setCallbacks(this);
        if(adapter.getItemCount() == 0) {
            getPresenter().loadCategories(0, listener.getLimit());
        }
    }

    @Override
    public void onPause() {
        adapter.setCallbacks(null);
        list.removeOnScrollListener(listener);
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        list.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        state.saveAdapterState(adapter, outState);
        state.saveListState(list, outState);

    }

    @Override
    protected void onViewStateDelivered(Bundle savedInstanceState) {
        state.applySavedState(savedInstanceState);
        if(state.getAdapterState() != null) {
            adapter.onRestoreInstanceState(state.getAdapterState());
        }
        if(state.getListState() != null) {
            list.getLayoutManager().onRestoreInstanceState(state.getListState());
        }
    }

    @NonNull
    @Override
    protected ICategoriesView provideView() {
        return this;
    }

    @NonNull
    @Override
    protected MainRouter provideRouter() {
        return ((IMainView) getActivity()).getRouter();
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.fragment_categories_list;
    }

    @Override
    protected int provideScreenId() {
        return ScreenContract.CATEGORIES.getCode();
    }

    @NonNull
    @Override
    protected ViperPresenter.Factory<ICategoriesView, MainRouter, CategoriesPresenter> provideFactory() {
        //obtain links to providers needed from the current Application context
        ICategoriesModelProvider modelProvider = (ICategoriesModelProvider) getActivity().getApplication();
        IExecutionEnvironmentProvider executionEnvironmentProvider = (IExecutionEnvironmentProvider) getActivity().getApplication();
        IResourceManagersProvider resourceManagersProvider = (IResourceManagersProvider) getActivity();
        IToolbarViewModelProvider toolbarViewModelProvider = (IToolbarViewModelProvider) getActivity();
        //create new instance of presenter factory to be used with retain-presenter framework
        return new CategoriesPresenter.Factory(
                resourceManagersProvider.provideLabelsManager(),
                toolbarViewModelProvider.provideToolbarModelMutator(),
                new CategoriesGetUseCase(
                        modelProvider.provideCategoriesRepository(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler()
                )
        );
    }
}
