package net.styleru.ikomarov.diabetics.presenter.main;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarEventObserver;
import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelAccessor;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.diabetics.view.main.IMainView;

/**
 * Created by i_komarov on 02.05.17.
 */

public class MainPresenter extends ViperPresenter<IMainView, MainRouter> implements IToolbarEventObserver {

    @NonNull
    private final IToolbarModelAccessor toolbarModelObserver;

    public MainPresenter(@NonNull IToolbarModelAccessor toolbarModelObserver) {
        this.toolbarModelObserver = toolbarModelObserver;
    }

    @Override
    protected void onViewAttach() {
        getView().setTitle(toolbarModelObserver.getTitle());
        getView().setSubtitle(toolbarModelObserver.getSubtitle());
        toolbarModelObserver.registerObserver(this);
    }

    @Override
    protected void onViewDetach() {
        toolbarModelObserver.registerObserver(null);
    }

    @Override
    protected void onDestroy() {

    }

    @Override
    public void onNewTitle(String title) {
        getView().setTitle(title);
    }

    @Override
    public void onNewSubtitle(String subtitle) {
        getView().setSubtitle(subtitle);
    }

    public void onNavigateUp() {
        getRouter().navigateUp();
    }

    public boolean onDrawerItemSelected(@IdRes int id) {
        switch(id) {
            case R.id.navigation_menu_item_categories: {
                getRouter().navigateToCategories();
                break;
            }
            case R.id.navigation_menu_item_participants: {
                getRouter().navigateToParticipants();
                break;
            }
            case R.id.navigation_menu_item_chats: {
                getRouter().navigateToChats();
                break;
            }
            case R.id.navigation_menu_item_profile: {
                getRouter().navigateToProfile();
                break;
            }
            case R.id.navigation_menu_item_preferences: {
                getRouter().navigateToPreferences();
                break;
            }
            default: {
                return false;
            }
        }

        return true;
    }

    public void onInstantiateDefaultFragment() {
        getRouter().instantiateDefaultFragment();
    }

    public static final class Factory implements ViperPresenter.Factory<IMainView, MainRouter, MainPresenter> {

        @NonNull
        private final IToolbarModelAccessor toolbarModelObserver;

        public Factory(@NonNull IToolbarModelAccessor toolbarModelObserver) {
            this.toolbarModelObserver = toolbarModelObserver;
        }

        @Override
        public MainPresenter create() {
            return new MainPresenter(toolbarModelObserver);
        }
    }
}
