package net.styleru.ikomarov.diabetics.utils.serialization;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by i_komarov on 04.05.17.
 */

public class ParcelableHashMap<K extends Parcelable, V extends Parcelable> extends HashMap<K, V> implements Parcelable {

    @NonNull
    private final Class<K> keyClass;

    @NonNull
    private final Class<V> valueClass;

    public ParcelableHashMap(@NonNull Class<K> keyClass, @NonNull Class<V> valueClass) {
        this.keyClass = keyClass;
        this.valueClass = valueClass;
    }

    ParcelableHashMap(Parcel in) {
        int size = in.readInt();
        keyClass = (Class<K>) in.readSerializable();
        valueClass = (Class<V>) in.readSerializable();
        for(int i = 0; i < size; i++){
            this.put(in.readParcelable(keyClass.getClassLoader()), in.readParcelable(valueClass.getClassLoader()));
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.size());
        dest.writeSerializable(keyClass);
        dest.writeSerializable(valueClass);
        for(Map.Entry<K, V> e : this.entrySet()){
            dest.writeParcelable(e.getKey(), flags);
            dest.writeParcelable(e.getValue(), flags);
        }
    }

    public static final Parcelable.Creator<ParcelableHashMap> CREATOR = new Creator<ParcelableHashMap>() {
        @Override
        public ParcelableHashMap createFromParcel(Parcel in) {
            return new ParcelableHashMap(in);
        }

        @Override
        public ParcelableHashMap[] newArray(int size) {
            return new ParcelableHashMap[0];
        }
    };
}
