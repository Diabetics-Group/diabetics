package net.styleru.ikomarov.diabetics.view.topics;

import net.styleru.ikomarov.diabetics.common.view.IView;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;

import java.util.List;

/**
 * Created by i_komarov on 04.05.17.
 */

public interface ITopicsView extends IView {

    void addTopics(List<TopicViewObject> items);
}
