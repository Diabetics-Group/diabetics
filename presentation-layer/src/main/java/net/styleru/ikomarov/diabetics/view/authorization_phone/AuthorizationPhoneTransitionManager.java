package net.styleru.ikomarov.diabetics.view.authorization_phone;

import android.support.annotation.NonNull;
import android.view.View;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.common.view.ITransactionFunction;
import net.styleru.ikomarov.diabetics.managers.view.ITransitionManager;

/**
 * Created by i_komarov on 03.06.17.
 */

public class AuthorizationPhoneTransitionManager implements ITransitionManager<String> {

    @NonNull
    private final View root;

    public AuthorizationPhoneTransitionManager(@NonNull View root) {
        this.root = root;
    }

    @NonNull
    @Override
    public ITransactionFunction provideFunction(@NonNull String s) {
        return transaction -> transaction.addSharedElement(
                root.findViewById(R.id.fragment_authorization_phone_continue_button),
                root.getResources().getString(R.string.transition_continue_button)
        );
    }
}
