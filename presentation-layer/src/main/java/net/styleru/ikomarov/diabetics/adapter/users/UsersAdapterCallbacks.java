package net.styleru.ikomarov.diabetics.adapter.users;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;

import net.styleru.ikomarov.diabetics.view_object.users.UserViewObject;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UsersAdapterCallbacks extends SortedListAdapterCallback<UserViewObject> {

    public UsersAdapterCallbacks(RecyclerView.Adapter adapter) {
        super(adapter);
    }

    @Override
    public int compare(UserViewObject o1, UserViewObject o2) {
        return (o1.getFirstName() + " " + o1.getLastName()).compareTo(o2.getFirstName() + " " + o2.getLastName());
    }

    @Override
    public boolean areContentsTheSame(UserViewObject oldItem, UserViewObject newItem) {
        return oldItem.getFirstName().equals(newItem.getFirstName()) &&
                oldItem.getLastName().equals(newItem.getLastName()) &&
                ((oldItem.getImageUrl() == null && newItem.getImageUrl() == null) || (oldItem.getImageUrl() != null && newItem.getImageUrl() != null && oldItem.getImageUrl().equals(newItem.getImageUrl()))) &&
                oldItem.getRole().equals(newItem.getRole());
    }

    @Override
    public boolean areItemsTheSame(UserViewObject item1, UserViewObject item2) {
        return item1.getId().equals(item2.getId());
    }
}
