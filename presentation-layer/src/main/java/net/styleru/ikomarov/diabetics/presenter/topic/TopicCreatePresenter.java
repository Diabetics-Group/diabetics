package net.styleru.ikomarov.diabetics.presenter.topic;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.routers.WizardRouter;
import net.styleru.ikomarov.diabetics.exceptions.ExceptionHandler;
import net.styleru.ikomarov.diabetics.mapping.topics.TopicMapping;
import net.styleru.ikomarov.diabetics.mapping.topics.TopicsMapping;
import net.styleru.ikomarov.diabetics.view.topic.ITopicCreateView;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;
import net.styleru.ikomarov.domain_layer.use_cases.topics.TopicCreateUseCase;

import java.util.List;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 05.05.17.
 */

public class TopicCreatePresenter extends ViperPresenter<ITopicCreateView, WizardRouter<TopicViewObject, ITopicCreateView>> implements TopicCreateUseCase.Callbacks<TopicViewObject> {

    @NonNull
    private final CategoryViewObject model;

    @NonNull
    private final TopicCreateUseCase topicCreate;

    @NonNull
    private final Function<TopicDTO, TopicViewObject> singleMapper;

    @NonNull
    private final Function<List<TopicDTO>, List<TopicViewObject>> listMapper;

    @NonNull
    private final ExceptionHandler handler;

    private TopicCreatePresenter(@NonNull CategoryViewObject model,
                                 @NonNull TopicCreateUseCase topicCreate) {

        this.model = model;
        this.topicCreate = topicCreate;
        this.singleMapper = new TopicMapping();
        this.listMapper = new TopicsMapping(singleMapper);
        this.handler = new ExceptionHandler();
    }

    @Override
    protected void onViewAttach() {

    }

    @Override
    protected void onViewDetach() {
        topicCreate.dispose();
    }

    @Override
    protected void onDestroy() {
        topicCreate.dispose();
    }

    public void createTopic(String title, String content) {
        boolean predicatesCompleted = true;

        if(TextUtils.isEmpty(title)) {
            getView().showTitleError(R.string.error_enter_title);
            predicatesCompleted = false;
        }

        if(TextUtils.isEmpty(content)) {
            getView().showContentError(R.string.error_enter_content);
            predicatesCompleted = false;
        }

        if(predicatesCompleted) {
            //TODO: inject real userId
            topicCreate.execute(singleMapper, this, model.getId(), "82", new TopicDTO.Builder(title, content));
        }
    }

    public void cancel() {
        getRouter().returnOnCancel();
    }

    @Override
    public void onTopicCreated(TopicViewObject item) {
        getRouter().returnOnSuccess(item);
    }

    @Override
    public void onTopicCreationFailed(ExceptionBundle error) {
        handler.handleDebug(error);
    }

    @Override
    public void onTopicCreationDelayed() {

    }

    public static final class Factory implements ViperPresenter.Factory<ITopicCreateView, WizardRouter<TopicViewObject, ITopicCreateView>, TopicCreatePresenter> {

        @NonNull
        private final CategoryViewObject model;

        @NonNull
        private final TopicCreateUseCase topicCreate;

        public Factory(@NonNull CategoryViewObject model, @NonNull TopicCreateUseCase topicCreate) {
            this.model = model;
            this.topicCreate = topicCreate;
        }

        @Override
        public TopicCreatePresenter create() {
            return new TopicCreatePresenter(model, topicCreate);
        }
    }
}
