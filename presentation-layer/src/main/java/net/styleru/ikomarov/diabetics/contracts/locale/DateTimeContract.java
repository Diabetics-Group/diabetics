package net.styleru.ikomarov.diabetics.contracts.locale;

import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by i_komarov on 03.05.17.
 */

public class DateTimeContract {

    @NonNull
    public static final SimpleDateFormat parseDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());

    @NonNull
    public static final SimpleDateFormat displayDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
}
