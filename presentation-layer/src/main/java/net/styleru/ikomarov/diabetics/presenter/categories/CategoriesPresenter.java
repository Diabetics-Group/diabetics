package net.styleru.ikomarov.diabetics.presenter.categories;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.exceptions.ExceptionHandler;
import net.styleru.ikomarov.diabetics.managers.resources.labels.ILabelsManager;
import net.styleru.ikomarov.diabetics.mapping.categories.CategoriesMapping;
import net.styleru.ikomarov.diabetics.mapping.categories.CategoryMapping;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.diabetics.view.categories.ICategoriesView;
import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelMutator;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;
import net.styleru.ikomarov.domain_layer.dto.categories.CategoryDTO;
import net.styleru.ikomarov.domain_layer.use_cases.categories.CategoriesGetUseCase;

import java.util.List;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 02.05.17.
 */

public class CategoriesPresenter extends ViperPresenter<ICategoriesView, MainRouter> implements CategoriesGetUseCase.Callbacks<CategoryViewObject> {

    @NonNull
    private final ILabelsManager labelsManager;

    @NonNull
    private final IToolbarModelMutator toolbarModelMutator;

    @NonNull
    private final CategoriesGetUseCase categoriesGet;

    @NonNull
    private final Function<CategoryDTO, CategoryViewObject> singleMapper;

    @NonNull
    private final Function<List<CategoryDTO>, List<CategoryViewObject>> listMapper;

    @NonNull
    private final ExceptionHandler handler;

    private CategoriesPresenter(@NonNull ILabelsManager labelsManager,
                                @NonNull IToolbarModelMutator toolbarModelMutator,
                                @NonNull CategoriesGetUseCase categoriesGet) {

        this.labelsManager = labelsManager;
        this.toolbarModelMutator = toolbarModelMutator;
        this.categoriesGet = categoriesGet;
        this.singleMapper = new CategoryMapping();
        this.listMapper = new CategoriesMapping(singleMapper);
        this.handler = new ExceptionHandler();
    }

    @Override
    protected void onViewAttach() {
        toolbarModelMutator.setTitle(labelsManager.getCategoriesLabel());
        toolbarModelMutator.setSubtitle("");
    }

    @Override
    protected void onViewDetach() {
        categoriesGet.dispose();
    }

    @Override
    protected void onDestroy() {
        categoriesGet.dispose();
    }

    public void loadCategories(int offset, int limit) {
        categoriesGet.execute(listMapper, this, offset, limit);
    }

    public void onCategoryClicks(CategoryViewObject item) {
        getRouter().navigateToTopics(item);
    }

    @Override
    public void onCategoriesLoaded(@NonNull List<CategoryViewObject> viewObjectList) {
        getView().addCategories(viewObjectList);
    }

    @Override
    public void onCategoriesLoadingFailed(@NonNull ExceptionBundle error) {
        handler.handleDebug(error);
    }

    public static final class Factory implements ViperPresenter.Factory<ICategoriesView, MainRouter, CategoriesPresenter> {

        @NonNull
        private final ILabelsManager labelsManager;

        @NonNull
        private final IToolbarModelMutator toolbarModelMutator;

        @NonNull
        private final CategoriesGetUseCase categoriesGet;

        public Factory(@NonNull ILabelsManager labelsManager,
                       @NonNull IToolbarModelMutator toolbarModelMutator,
                       @NonNull CategoriesGetUseCase categoriesGet) {

            this.labelsManager = labelsManager;
            this.toolbarModelMutator = toolbarModelMutator;
            this.categoriesGet = categoriesGet;
        }

        @Override
        public CategoriesPresenter create() {
            return new CategoriesPresenter(labelsManager, toolbarModelMutator, categoriesGet);
        }
    }
}
