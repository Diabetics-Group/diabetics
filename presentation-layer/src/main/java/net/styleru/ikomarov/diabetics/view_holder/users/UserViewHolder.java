package net.styleru.ikomarov.diabetics.view_holder.users;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.bumptech.glide.RequestManager;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.contracts.user_role.UserRole;
import net.styleru.ikomarov.diabetics.view_object.users.UserViewObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UserViewHolder extends RecyclerView.ViewHolder {

    private CircleImageView imageHolder;
    private AppCompatTextView nameHolder;
    private AppCompatImageView expertBadge;

    private int position;
    private UserViewObject item;
    private InteractionCallbacks callbacks;

    public UserViewHolder(View itemView) {
        super(itemView);

        imageHolder = (CircleImageView) itemView.findViewById(R.id.list_item_participants_image_holder);
        nameHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_participants_name_holder);
        expertBadge = (AppCompatImageView) itemView.findViewById(R.id.list_item_participants_expert_toggle);
    }

    public void bindCallbacks() {
        itemView.setOnClickListener((view) -> {
            if(callbacks != null) {
                callbacks.clicks(position, item);
            }
        });
    }

    public void unbindCallbacks() {
        itemView.setOnClickListener(null);
    }

    public void bind(RequestManager glide, int position, UserViewObject item, InteractionCallbacks callbacks) {
        this.position = position;
        this.item = item;
        this.callbacks = callbacks;

        if(imageHolder.getDrawable() != null) {
            imageHolder.setImageDrawable(null);
        }

        if(TextUtils.isEmpty(item.getImageUrl())) {
            glide.load(R.drawable.img_profile_default).into(imageHolder);
        } else {
            glide.load(item.getImageUrl()).into(imageHolder);
        }

        nameHolder.setText(item.getFirstName() + " " + item.getLastName());
        expertBadge.setVisibility(item.getRole() == UserRole.EXPERT ? View.VISIBLE : View.INVISIBLE);
    }

    public interface InteractionCallbacks {

        void clicks(int position, UserViewObject item);
    }
}
