package net.styleru.ikomarov.diabetics.di.view_model.resource;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.diabetics.managers.resources.labels.ILabelsManager;
import net.styleru.ikomarov.diabetics.managers.resources.labels.LabelsManager;

/**
 * Created by i_komarov on 12.05.17.
 */

public class LabelsModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final Resources resources;

    @Nullable
    private volatile ILabelsManager labelsManager;

    public LabelsModule(@NonNull Resources resources) {
        this.resources = resources;
    }

    @NonNull
    public ILabelsManager provideLabelsManager() {
        ILabelsManager localInstance = labelsManager;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = labelsManager;
                if(localInstance == null) {
                    localInstance = labelsManager = new LabelsManager(resources);
                }
            }
        }

        return localInstance;
    }
}
