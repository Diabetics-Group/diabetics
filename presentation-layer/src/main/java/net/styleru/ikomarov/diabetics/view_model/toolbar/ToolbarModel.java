package net.styleru.ikomarov.diabetics.view_model.toolbar;

/**
 * Created by i_komarov on 11.05.17.
 */

public class ToolbarModel implements IToolbarModelAccessor, IToolbarModelMutator {

    private String title;

    private String subtitle;

    private IToolbarEventObserver observer;

    public ToolbarModel() {

    }

    @Override
    public void setTitle(String title) {
        this.title = title;
        observer.onNewTitle(this.title);
    }

    @Override
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
        observer.onNewSubtitle(this.subtitle);
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public String getSubtitle() {
        return this.subtitle;
    }

    @Override
    public void registerObserver(IToolbarEventObserver listener) {
        this.observer = listener;
    }
}
