package net.styleru.ikomarov.diabetics.managers.resources.notifications;

import android.content.res.Resources;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.R;

/**
 * Created by i_komarov on 14.05.17.
 */

public class NotificationsManager implements INotificationsManager {

    @NonNull
    private final Resources resources;

    public NotificationsManager(@NonNull Resources resources) {
        this.resources = resources;
    }

    @NonNull
    @Override
    public String getOperationDelayedNotification() {
        return resources.getString(R.string.notification_operation_delayed);
    }

    @NonNull
    @Override
    public String getCacheClearedNotification() {
        return resources.getString(R.string.notification_cache_cleared);
    }

    @NonNull
    @Override
    public String getSessionClearedNotification() {
        return resources.getString(R.string.notification_session_cleared);
    }

    @NonNull
    @Override
    public String getEmptyFieldNotification() {
        return resources.getString(R.string.notification_field_empty);
    }

    @NonNull
    @Override
    public String getBadPhoneCountryCodeNotification() {
        return resources.getString(R.string.notification_bad_phone_country_code);
    }

    @NonNull
    @Override
    public String getBadPhoneLengthNotification() {
        return resources.getString(R.string.notification_bad_phone_length);
    }

    @NonNull
    @Override
    public String getBadCodeLengthNotification() {
        return resources.getString(R.string.notification_bad_code_length);
    }
}
