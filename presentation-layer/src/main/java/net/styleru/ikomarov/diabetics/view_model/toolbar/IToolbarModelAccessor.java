package net.styleru.ikomarov.diabetics.view_model.toolbar;

/**
 * Created by i_komarov on 11.05.17.
 */

public interface IToolbarModelAccessor {

    String getTitle();

    String getSubtitle();

    void registerObserver(IToolbarEventObserver listener);
}
