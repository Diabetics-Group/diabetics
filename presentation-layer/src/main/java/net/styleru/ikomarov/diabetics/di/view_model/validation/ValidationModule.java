package net.styleru.ikomarov.diabetics.di.view_model.validation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.diabetics.di.view_model.component.view_model.IViewModelComponent;
import net.styleru.ikomarov.diabetics.validation.authentication.CodeValidationStrategy;
import net.styleru.ikomarov.diabetics.validation.authentication.PhoneValidationStrategy;
import net.styleru.ikomarov.domain_layer.validation.base.IValidationStrategy;

/**
 * Created by i_komarov on 05.06.17.
 */

public class ValidationModule {

    @NonNull
    private final Object lock1 = new Object();

    @NonNull
    private final Object lock2 = new Object();

    @NonNull
    private final IViewModelComponent component;

    @Nullable
    private volatile IValidationStrategy<String> phoneValidationStrategy;

    @Nullable
    private volatile IValidationStrategy<String> codeValidationStrategy;

    public ValidationModule(@NonNull IViewModelComponent component) {
        this.component = component;
    }

    @NonNull
    public IValidationStrategy<String> providePhoneValidationStrategy() {
        IValidationStrategy<String> localInstance = phoneValidationStrategy;
        if(localInstance == null) {
            synchronized (lock1) {
                localInstance = phoneValidationStrategy;
                if(localInstance == null) {
                    localInstance = phoneValidationStrategy = new PhoneValidationStrategy(component.notifications().provideNotificationsManager());
                }
            }
        }

        return localInstance;
    }

    @NonNull
    public IValidationStrategy<String> provideCodeValidationStrategy() {
        IValidationStrategy<String> localInstance = codeValidationStrategy;
        if(localInstance == null) {
            synchronized (lock2) {
                localInstance = codeValidationStrategy;
                if(localInstance == null) {
                    localInstance = codeValidationStrategy = new CodeValidationStrategy(component.notifications().provideNotificationsManager());
                }
            }
        }

        return localInstance;
    }
}
