package net.styleru.ikomarov.diabetics.common.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.util.Log;

import net.styleru.ikomarov.diabetics.common.view.IView;
import net.styleru.ikomarov.diabetics.common.routers.BaseRouter;

/**
 * Created by i_komarov on 02.05.17.
 */

public class PresenterLoader<V extends IView, R extends BaseRouter<V>, P extends ViperPresenter<V, R>> extends Loader<P> {

    @NonNull
    private static final String TAG = PresenterLoader.class.getSimpleName();

    @NonNull
    private final ViperPresenter.Factory<V, R, P> factory;

    @Nullable
    private P presenter;

    public PresenterLoader(Context context, @NonNull ViperPresenter.Factory<V, R, P> factory) {
        super(context);
        this.factory = factory;
    }

    @Override
    public void onStartLoading() {
        if(presenter != null) {
            deliverResult(presenter);
        } else {
            forceLoad();
        }
    }

    @Override
    public void forceLoad() {
        presenter = factory.create();
        deliverResult(presenter);
    }

    @Override
    protected void onReset() {
        if(presenter.isViewAttached()) {
            presenter.detachView();
        }
        presenter.onDestroy();
        presenter = null;
    }
}
