package net.styleru.ikomarov.diabetics.view_object.topics;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 02.05.17.
 */

public class TopicViewObject implements Parcelable {

    @NonNull
    private final String id;

    @NonNull
    private final String firstName;

    @NonNull
    private final String lastName;

    @NonNull
    private final String imageUrl;

    @NonNull
    private final String role;

    @NonNull
    private final String title;

    @NonNull
    private final String content;

    @NonNull
    private final Integer commentsCount;

    @NonNull
    private final String lastUpdate;

    public TopicViewObject(@NonNull String id,
                           @NonNull String firstName,
                           @NonNull String lastName,
                           @NonNull String imageUrl,
                           @NonNull String role,
                           @NonNull String title,
                           @NonNull String content,
                           @NonNull Integer commentsCount,
                           @NonNull String lastUpdate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.role = role;
        this.title = title;
        this.content = content;
        this.commentsCount = commentsCount;
        this.lastUpdate = lastUpdate;
    }

    protected TopicViewObject(Parcel in) {
        String safe = null;
        id = (safe = in.readString()).equals("") ? null : safe;
        firstName = (safe = in.readString()).equals("") ? null : safe;
        lastName = (safe = in.readString()).equals("") ? null : safe;
        imageUrl = (safe = in.readString()).equals("") ? null : safe;
        role = (safe = in.readString()).equals("") ? null : safe;
        title = (safe = in.readString()).equals("") ? null : safe;
        content = (safe = in.readString()).equals("") ? null : safe;
        commentsCount = in.readInt();
        lastUpdate = (safe = in.readString()).equals("") ? null : safe;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public String getRole() {
        return role;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    @NonNull
    public Integer getCommentsCount() {
        return commentsCount;
    }

    @NonNull
    public String getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id != null ? id : "");
        dest.writeString(firstName != null ? firstName : "");
        dest.writeString(lastName != null ? lastName : "");
        dest.writeString(imageUrl != null ? imageUrl : "");
        dest.writeString(role != null ? role : "");
        dest.writeString(title != null ? title : "");
        dest.writeString(content != null ? content : "");
        dest.writeInt(commentsCount != null ? commentsCount : 0);
        dest.writeString(lastUpdate != null ? lastUpdate : "");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TopicViewObject> CREATOR = new Creator<TopicViewObject>() {
        @Override
        public TopicViewObject createFromParcel(Parcel in) {
            return new TopicViewObject(in);
        }

        @Override
        public TopicViewObject[] newArray(int size) {
            return new TopicViewObject[size];
        }
    };
}
