package net.styleru.ikomarov.diabetics.view_holder.common;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.RequestManager;

/**
 * Created by i_komarov on 27.05.17.
 */

public abstract class BindableViewHolder<Item, Callbacks> extends RecyclerView.ViewHolder {

    public BindableViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bind(RequestManager glide, int position, Item item, Callbacks callbacks);

    public abstract void bindCallbacks();

    public abstract void unbindCallbacks();
}
