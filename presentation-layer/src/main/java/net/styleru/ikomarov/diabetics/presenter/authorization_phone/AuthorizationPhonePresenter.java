package net.styleru.ikomarov.diabetics.presenter.authorization_phone;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import net.styleru.ikomarov.data_layer.contracts.exception.Reason;
import net.styleru.ikomarov.data_layer.contracts.exception.ValidationExceptionContract;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.exceptions.ExceptionHandler;
import net.styleru.ikomarov.diabetics.managers.resources.notifications.INotificationsManager;
import net.styleru.ikomarov.diabetics.presenter.authorization_code.AuthorizationCodePresenter;
import net.styleru.ikomarov.diabetics.routers.authorization.AuthorizationRouter;
import net.styleru.ikomarov.diabetics.view.authorization_phone.IAuthorizationPhoneView;
import net.styleru.ikomarov.domain_layer.use_cases.session.PhoneAuthenticationUseCase;

/**
 * Created by i_komarov on 02.06.17.
 */

public class AuthorizationPhonePresenter extends ViperPresenter<IAuthorizationPhoneView, AuthorizationRouter> implements PhoneAuthenticationUseCase.Callbacks {

    @NonNull
    private final INotificationsManager notificationsManager;

    @NonNull
    private final PhoneAuthenticationUseCase phoneAuthentication;

    @NonNull
    private final ExceptionHandler handler;

    private AuthorizationPhonePresenter(@NonNull INotificationsManager notificationsManager,
                                        @NonNull PhoneAuthenticationUseCase phoneAuthentication) {

        this.notificationsManager = notificationsManager;
        this.phoneAuthentication = phoneAuthentication;
        this.handler = new ExceptionHandler();
    }

    @Override
    protected void onViewAttach() {

    }

    @Override
    protected void onViewDetach() {
        phoneAuthentication.dispose();
    }

    @Override
    protected void onDestroy() {
        phoneAuthentication.dispose();
    }

    public void onContinueButtonClicks(@Nullable String phone) {
        phoneAuthentication.execute(
                this,
                phone,
                true
        );
    }

    @Override
    public void onPhoneAuthenticationStarted() {
        getView().onPhoneAuthenticationStarted();
    }

    @Override
    public void onPhoneAuthenticationSuccess(@NonNull String phone) {
        getRouter().navigateToVerification(
                phone,
                getView().getTransitionManager().provideFunction(AuthorizationCodePresenter.class.getSimpleName())
        );
    }

    @Override
    public void onPhoneAuthenticationFailure(ExceptionBundle error) {
        handler.handleDebug(error);
        switch(error.getReason()) {
            case VALIDATION: {
                ValidationExceptionContract contract = error.getReason().getContract();
                getView().setPhoneError(contract.extractMessage(error));
                break;
            }
        }
    }

    @Override
    public void onPhoneAuthenticationFinished() {
        getView().onPhoneAuthenticationFinished();
    }

    public static final class Factory implements ViperPresenter.Factory<IAuthorizationPhoneView, AuthorizationRouter, AuthorizationPhonePresenter> {

        @NonNull
        private final INotificationsManager notificationsManager;

        @NonNull
        private final PhoneAuthenticationUseCase phoneAuthentication;

        public Factory(@NonNull INotificationsManager notificationsManager,
                       @NonNull PhoneAuthenticationUseCase phoneAuthentication) {

            this.notificationsManager = notificationsManager;
            this.phoneAuthentication = phoneAuthentication;
        }

        @Override
        public AuthorizationPhonePresenter create() {
            return new AuthorizationPhonePresenter(notificationsManager, phoneAuthentication);
        }
    }
}
