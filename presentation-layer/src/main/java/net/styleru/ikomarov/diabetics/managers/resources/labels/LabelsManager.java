package net.styleru.ikomarov.diabetics.managers.resources.labels;

import android.content.res.Resources;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.R;

/**
 * Created by i_komarov on 12.05.17.
 */

public class LabelsManager implements ILabelsManager {

    @NonNull
    private final Resources resources;

    public LabelsManager(@NonNull Resources resources) {
        this.resources = resources;
    }

    @NonNull
    @Override
    public String getCategoriesLabel() {
        return resources.getString(R.string.label_categories);
    }

    @NonNull
    @Override
    public String getTopicsLabel() {
        return resources.getString(R.string.label_topics);
    }

    @NonNull
    @Override
    public String getCommentsLabel() {
        return resources.getString(R.string.label_comments);
    }

    @NonNull
    @Override
    public String getCreateTopicLabel() {
        return resources.getString(R.string.label_topic_create);
    }

    @NonNull
    @Override
    public String getPreferencesLabel() {
        return resources.getString(R.string.title_preferences);
    }

    @NonNull
    @Override
    public String getParticipantsLabel() {
        return resources.getString(R.string.label_participants);
    }
}
