package net.styleru.ikomarov.diabetics.di.view_model.resource;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.diabetics.managers.resources.notifications.INotificationsManager;
import net.styleru.ikomarov.diabetics.managers.resources.notifications.NotificationsManager;

/**
 * Created by i_komarov on 14.05.17.
 */

public class NotificationsModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final Resources resources;

    @Nullable
    private volatile INotificationsManager notificationsManager;

    public NotificationsModule(@NonNull Resources resources) {
        this.resources = resources;
    }

    @NonNull
    public INotificationsManager provideNotificationsManager() {
        INotificationsManager localInstance = notificationsManager;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = notificationsManager;
                if(localInstance == null) {
                    localInstance = notificationsManager = new NotificationsManager(resources);
                }
            }
        }

        return localInstance;
    }
}
