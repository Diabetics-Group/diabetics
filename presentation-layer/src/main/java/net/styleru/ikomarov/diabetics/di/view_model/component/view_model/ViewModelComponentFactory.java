package net.styleru.ikomarov.diabetics.di.view_model.component.view_model;

import android.content.Context;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.common.container.RetainContainer;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 12.05.17.
 */

public class ViewModelComponentFactory implements RetainContainer.Factory<IViewModelComponent> {

    @NonNull
    private final IAppComponent component;

    public ViewModelComponentFactory(@NonNull IAppComponent component) {
        this.component = component;
    }

    @Override
    public IViewModelComponent create() {
        return new ViewModelComponent(component);
    }
}
