package net.styleru.ikomarov.diabetics.exceptions;

import android.support.annotation.NonNull;
import android.util.Log;

import net.styleru.ikomarov.data_layer.BuildConfig;
import net.styleru.ikomarov.data_layer.contracts.exception.DatabaseExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.EnvironmentExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.HttpExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.InternalExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.LayerExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.NetworkExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.Reason;
import net.styleru.ikomarov.data_layer.contracts.exception.ValidationExceptionContract;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 04.05.17.
 */

public class ExceptionHandler {

    public ExceptionHandler() {

    }

    public void handleDebug(@NonNull ExceptionBundle exception) {
        if(BuildConfig.IS_DEBUG_BUILD) {
            switch (exception.getReason()) {
                case NETWORK: {
                    NetworkExceptionContract contract = exception.getReason().getContract();
                    Log.e("ExceptionHandler", "exception raised\nreason: " + Reason.NETWORK + ",\ntype: " + contract.extractType(exception), exception);
                    break;
                }
                case HTTP: {
                    HttpExceptionContract contract = exception.getReason().getContract();
                    Log.e("ExceptionHandler", "exception raised\nreason: " + Reason.HTTP + ",\ncode: " + contract.extractCode(exception) + ",\nmessage: " + contract.extractMessage(exception) + ",\njson: " + contract.extractJson(exception), exception);
                    break;
                }
                case LAYER: {
                    LayerExceptionContract contract = exception.getReason().getContract();
                    Log.e("ExceptionHandler", "exception raised\nreason: " + Reason.LAYER, exception);
                    break;
                }
                case INTERNAL: {
                    InternalExceptionContract contract = exception.getReason().getContract();
                    Log.e("ExceptionHandler", "exception raised\nreason: " + Reason.INTERNAL + ",\nmessage: " + contract.extractMessage(exception) + ",\nthrowable: " + contract.extractThrowable(exception), contract.extractThrowable(exception));
                    break;
                }
                case DATABASE: {
                    DatabaseExceptionContract contract = exception.getReason().getContract();
                    Log.e("ExceptionHandler", "exception raised\nreason: " + Reason.DATABASE + ",\nmessage: " + contract.extractMessage(exception) + ",\nthrowable: " + contract.extractThrowable(exception) + ",\ncode: " + contract.extractCode(exception) + ",\nis sqlite: " + contract.extractIsSQLite(exception), contract.extractThrowable(exception));
                    break;
                }
                case ENVIRONMENT: {
                    EnvironmentExceptionContract contract = exception.getReason().getContract();
                    Log.e("ExceptionHandler", "exception raised\nreason: " + Reason.ENVIRONMENT + ",\ntype: " + contract.extractType(exception), exception);
                    break;
                }
                case VALIDATION: {
                    ValidationExceptionContract contract = exception.getReason().getContract();
                    Log.e("ExceptionHandler", "exception raised\nreason: " + Reason.VALIDATION + ",\nmessage: " + contract.extractMessage(exception));
                    break;
                }
            }
        }
    }
}
