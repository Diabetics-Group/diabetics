package net.styleru.ikomarov.diabetics.view.comments;

import net.styleru.ikomarov.diabetics.common.view.IView;
import net.styleru.ikomarov.diabetics.view_object.comments.CommentViewObject;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;

import java.util.List;

/**
 * Created by i_komarov on 08.05.17.
 */

public interface ICommentsView extends IView {

    void hideLoadPreviousCommentsButton();

    void addComment(CommentViewObject item);

    void addComments(List<CommentViewObject> items);

    void displayTopic(TopicViewObject item);
}
