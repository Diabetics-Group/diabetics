package net.styleru.ikomarov.diabetics.common.presenter;

import net.styleru.ikomarov.diabetics.common.view.IView;
import net.styleru.ikomarov.diabetics.common.routers.BaseRouter;

/**
 * Created by i_komarov on 02.05.17.
 */

public abstract class ViperPresenter<V extends IView, R extends BaseRouter> implements IPresenter<V, R> {

    private V view;

    private R router;

    @Override
    public final void attachView(V view, R router) {
        this.view = view;
        this.router = router;
        this.onViewAttach();
    }

    protected abstract void onViewAttach();

    protected abstract void onViewDetach();

    protected abstract void onDestroy();

    public final boolean isViewAttached() {
        return view != null || router != null;
    }

    @Override
    public final void detachView() {
        this.onViewDetach();
        this.view = null;
        this.router = null;
    }

    protected final V getView() {
        return view;
    }

    protected final R getRouter() {
        return router;
    }

    public interface Factory<V extends IView, R extends BaseRouter, P extends ViperPresenter<V, R>> {

        P create();
    }
}
