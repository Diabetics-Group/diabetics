package net.styleru.ikomarov.diabetics.managers.resources.labels;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 12.05.17.
 */

public interface ILabelsManager {

    @NonNull
    String getCategoriesLabel();

    @NonNull
    String getTopicsLabel();

    @NonNull
    String getCommentsLabel();

    @NonNull
    String getCreateTopicLabel();

    @NonNull
    String getPreferencesLabel();

    @NonNull
    String getParticipantsLabel();
}
