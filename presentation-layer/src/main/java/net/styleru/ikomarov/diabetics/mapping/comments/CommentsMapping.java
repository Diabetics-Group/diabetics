package net.styleru.ikomarov.diabetics.mapping.comments;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.view_object.comments.CommentViewObject;
import net.styleru.ikomarov.domain_layer.dto.comments.CommentDTO;
import net.styleru.ikomarov.domain_layer.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsMapping implements Function<List<CommentDTO>, List<CommentViewObject>> {

    @NonNull
    private final Function<CommentDTO, CommentViewObject> singleMapper;

    public CommentsMapping(@NonNull Function<CommentDTO, CommentViewObject> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<CommentViewObject> apply(List<CommentDTO> commentDTOs) throws Exception {
        return Observable.fromIterable(commentDTOs)
                .map(singleMapper)
                .compose(RxUtils.accumulate())
                .blockingFirst();
    }
}
