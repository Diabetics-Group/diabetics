package net.styleru.ikomarov.diabetics.common.routers;

import android.os.Parcelable;

/**
 * Created by i_komarov on 02.06.17.
 */

public interface IWizardRouter<R extends Parcelable> {

    void returnOnSuccess(R result);

    void returnOnCancel();
}
