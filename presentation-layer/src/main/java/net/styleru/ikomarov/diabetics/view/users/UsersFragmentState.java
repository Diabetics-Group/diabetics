package net.styleru.ikomarov.diabetics.view.users;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;

import net.styleru.ikomarov.diabetics.adapter.users.UsersAdapter;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UsersFragmentState {

    private static final String PREFIX = UsersFragmentState.class.getCanonicalName() + ".";

    private static final String KEY_LIST_STATE = PREFIX + "LIST";

    private static final String KEY_ADAPTER_STATE = PREFIX + "ADAPTER";

    private Parcelable adapterState;

    private Parcelable listState;

    public UsersFragmentState(Bundle args, Bundle saved) {
        adapterState = (saved != null) ? saved.getParcelable(KEY_ADAPTER_STATE) : null;
        listState = (saved != null) ? saved.getParcelable(KEY_LIST_STATE) : null;
    }

    public void saveListState(Bundle outState, RecyclerView list) {
        outState.putParcelable(KEY_LIST_STATE, list.getLayoutManager().onSaveInstanceState());
    }

    public Parcelable getListState() {
        return listState;
    }

    public void saveAdapterState(Bundle outState, UsersAdapter adapter) {
        outState.putParcelable(KEY_ADAPTER_STATE, adapter.onSaveInstanceState());
    }

    public Parcelable getAdapterState() {
        return adapterState;
    }
}
