package net.styleru.ikomarov.diabetics.mapping.comments;

import net.styleru.ikomarov.diabetics.view_object.comments.CommentViewObject;
import net.styleru.ikomarov.domain_layer.dto.comments.CommentDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentMapping implements Function<CommentDTO, CommentViewObject> {

    @Override
    public CommentViewObject apply(CommentDTO dto) throws Exception {
        return new CommentViewObject(
                dto.getId(),
                dto.getFirstName(),
                dto.getLastName(),
                dto.getImageUrl(),
                dto.getRole(),
                dto.getContent(),
                dto.getDate(),
                dto.isSynchronized()
        );
    }
}
