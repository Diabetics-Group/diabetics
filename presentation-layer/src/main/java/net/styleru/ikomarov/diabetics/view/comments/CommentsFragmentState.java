package net.styleru.ikomarov.diabetics.view.comments;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import net.styleru.ikomarov.diabetics.adapter.comments.CommentsAdapter;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsFragmentState {

    @NonNull
    private static final String PREFIX = CommentsFragmentState.class.getCanonicalName() + ".";

    @NonNull
    private static final String KEY_TOPIC_MODEL = PREFIX + "TOPIC_ID";

    @NonNull
    private static final String KEY_TOPIC_NAME = PREFIX + "TOPIC_NAME";

    @NonNull
    private static final String KEY_ADAPTER_STATE = PREFIX + "ADAPTER_STATE";

    @NonNull
    private static final String KEY_LIST_STATE = PREFIX + "LIST_STATE";

    @NonNull
    private static final String KEY_INIT_LOADING = PREFIX + "INIT_LOADING";

    @NonNull
    private final TopicViewObject model;

    private boolean shouldInitLoading;

    private Parcelable adapterState;

    private Parcelable listState;

    public static Bundle createArguments(@NonNull TopicViewObject model) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_TOPIC_MODEL, model);
        args.putBoolean(KEY_INIT_LOADING, true);
        return args;
    }

    public CommentsFragmentState(Bundle args, Bundle saved) {
        this.model = (args != null ? args : saved).getParcelable(KEY_TOPIC_MODEL);
        this.shouldInitLoading = (args != null && args.containsKey(KEY_INIT_LOADING)) || (saved != null && saved.containsKey(KEY_INIT_LOADING));
        adapterState = (saved != null) ? saved.getParcelable(KEY_ADAPTER_STATE) : null;
        listState = (saved != null) ? saved.getParcelable(KEY_LIST_STATE) : null;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_TOPIC_MODEL, model);
    }

    @NonNull
    public TopicViewObject getModel() {
        return this.model;
    }

    public boolean shouldInitLoading() {
        return this.shouldInitLoading;
    }

    public void saveListState(Bundle outState, RecyclerView list) {
        outState.putParcelable(KEY_LIST_STATE, list.getLayoutManager().onSaveInstanceState());
    }

    public Parcelable getListState() {
        return this.listState;
    }

    public void saveAdapterState(Bundle outState, CommentsAdapter adapter) {
        outState.putParcelable(KEY_ADAPTER_STATE, adapter.onSaveInstanceState());
    }

    public Parcelable getAdapterState() {
        return this.adapterState;
    }
}
