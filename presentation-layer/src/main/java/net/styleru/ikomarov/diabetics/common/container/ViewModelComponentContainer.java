package net.styleru.ikomarov.diabetics.common.container;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.diabetics.di.view_model.component.validation.IValidationComponent;
import net.styleru.ikomarov.diabetics.di.view_model.component.view_model.IViewModelComponent;

/**
 * Created by i_komarov on 05.06.17.
 */

public class ViewModelComponentContainer extends RetainContainer<IViewModelComponent> {

    @NonNull
    private final Object lock = new Object();

    @Nullable
    private volatile IValidationComponent component;

    public IValidationComponent provideValidationComponent() {
        IValidationComponent localInstance = component;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = component;
                if(localInstance == null) {
                    localInstance = component = provideComponent().plusValidationComponent();
                }
            }
        }

        return localInstance;
    }
}
