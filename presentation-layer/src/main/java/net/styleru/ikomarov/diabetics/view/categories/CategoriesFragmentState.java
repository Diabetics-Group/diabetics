package net.styleru.ikomarov.diabetics.view.categories;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import net.styleru.ikomarov.diabetics.adapter.categories.CategoriesAdapter;

/**
 * Created by i_komarov on 05.05.17.
 */

public class CategoriesFragmentState {

    @NonNull
    private static final String PREFIX = CategoriesFragmentState.class.getCanonicalName() + ".";

    @NonNull
    private static final String KEY_ADAPTER_STATE = PREFIX + "ADAPTER";

    @NonNull
    private static final String KEY_LIST_STATE = PREFIX + "LIST";

    private Parcelable adapterState;

    private Parcelable listState;

    public CategoriesFragmentState(Bundle args, Bundle saved) {
        if(saved != null) {
            adapterState = saved.getParcelable(KEY_ADAPTER_STATE);
            listState = saved.getParcelable(KEY_LIST_STATE);
        }
    }

    public void applySavedState(Bundle saved) {
        adapterState = (saved != null) ? saved.getParcelable(KEY_ADAPTER_STATE) : null;
        listState = (saved != null) ? saved.getParcelable(KEY_LIST_STATE) : null;
    }

    public void saveAdapterState(CategoriesAdapter adapter, Bundle outState) {
        outState.putParcelable(KEY_ADAPTER_STATE, adapter.onSaveInstanceState());
    }

    public Parcelable getAdapterState() {
        return this.adapterState;
    }

    public void saveListState(RecyclerView list, Bundle outState) {
        outState.putParcelable(KEY_LIST_STATE, list.getLayoutManager().onSaveInstanceState());
    }

    public Parcelable getListState() {
        return this.listState;
    }
}
