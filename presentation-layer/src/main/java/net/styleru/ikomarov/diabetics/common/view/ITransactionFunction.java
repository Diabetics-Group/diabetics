package net.styleru.ikomarov.diabetics.common.view;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by i_komarov on 03.06.17.
 */

public interface ITransactionFunction {

    @NonNull
    FragmentTransaction apply(@NonNull FragmentTransaction transaction);
}
