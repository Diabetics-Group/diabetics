package net.styleru.ikomarov.diabetics.mapping.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;
import net.styleru.ikomarov.domain_layer.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsMapping implements Function<List<TopicDTO>, List<TopicViewObject>> {

    @NonNull
    private final Function<TopicDTO, TopicViewObject> singleMapper;

    public TopicsMapping(@NonNull Function<TopicDTO, TopicViewObject> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<TopicViewObject> apply(List<TopicDTO> topicDTOs) throws Exception {
        return Observable.fromIterable(topicDTOs)
                .map(singleMapper)
                .compose(RxUtils.accumulate())
                .blockingFirst();
    }
}
