package net.styleru.ikomarov.diabetics.common.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.ikomarov.diabetics.common.presenter.PresenterLoader;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.routers.BaseRouter;

/**
 * Created by i_komarov on 02.05.17.
 */

public abstract class ViperFragment<V extends IView, R extends BaseRouter, P extends ViperPresenter<V, R>> extends Fragment implements LoaderManager.LoaderCallbacks<P> {

    private static final String TAG = ViperFragment.class.getSimpleName();

    private P presenter;

    private ActivityResultBundle resultBundle = new ActivityResultBundle();

    private Bundle savedInstanceState;

    @Override
    public final void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(provideScreenId(), null, this);
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(provideLayoutId(), parent, false);
    }

    @Override
    public final void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindUserInterface(view);
    }

    @Override
    public final Loader<P> onCreateLoader(int id, Bundle args) {
        return new PresenterLoader<>(getActivity(), provideFactory());
    }

    @Override
    public final void onLoadFinished(Loader<P> loader, P presenter) {
        this.presenter = presenter;
    }

    @Override
    public final void onLoaderReset(Loader<P> loader) {
        this.presenter = null;
    }

    @CallSuper
    @Override
    public void onResume() {
        super.onResume();
        //first deliver the activity results
        while(resultBundle.size() > 0) {
            ActivityResult result = resultBundle.poll();
            onActivityResultDelivered(result.getRequestCode(), result.getResultCode(), result.getData());
        }

        if(savedInstanceState != null) {
            //second deliver the previously saved view state
            onViewStateDelivered(savedInstanceState);
        }

        presenter.attachView(provideView(), provideRouter());
    }

    @CallSuper
    @Override
    public void onPause() {
        presenter.detachView();
        super.onPause();
    }

    @Override
    public final void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
    }

    protected final P getPresenter() {
        return this.presenter;
    }

    public abstract void onActivityResultDelivered(int requestCode, int resultCode, Intent data);

    protected abstract void bindUserInterface(View root);

    protected abstract void onViewStateDelivered(Bundle savedInstanceState);

    @NonNull
    protected abstract V provideView();

    @NonNull
    protected abstract R provideRouter();

    @LayoutRes
    protected abstract int provideLayoutId();

    protected abstract int provideScreenId();

    @NonNull
    protected abstract ViperPresenter.Factory<V, R, P> provideFactory();
}
