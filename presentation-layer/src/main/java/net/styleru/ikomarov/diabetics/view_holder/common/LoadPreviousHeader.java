package net.styleru.ikomarov.diabetics.view_holder.common;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.RequestManager;

import net.styleru.ikomarov.diabetics.R;

/**
 * Created by i_komarov on 27.05.17.
 */

public class LoadPreviousHeader extends BindableViewHolder<Boolean, View.OnClickListener> {

    private AppCompatButton previousCommentsButton;

    private View.OnClickListener listener;

    public LoadPreviousHeader(View itemView) {
        super(itemView);
        previousCommentsButton = (AppCompatButton) itemView.findViewById(R.id.fragment_comments_list_previous_comments_button);
    }

    @Override
    public void bind(RequestManager glide, int position, Boolean shouldShow, View.OnClickListener listener) {
        RecyclerView.LayoutParams param = (RecyclerView.LayoutParams)itemView.getLayoutParams();
        if(shouldShow) {
            param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            param.width = LinearLayout.LayoutParams.MATCH_PARENT;
            itemView.setVisibility(View.VISIBLE);
        } else {
            itemView.setVisibility(View.GONE);
            param.height = 0;
            param.width = 0;
        }

        itemView.setLayoutParams(param);

        this.listener = listener;
    }

    @Override
    public void bindCallbacks() {
        previousCommentsButton.setOnClickListener(listener);
    }

    @Override
    public void unbindCallbacks() {
        previousCommentsButton.setOnClickListener(null);
    }
}
