package net.styleru.ikomarov.diabetics.ui.transitions;

import android.transition.Fade;
import android.transition.TransitionSet;

/**
 * Created by i_komarov on 03.06.17.
 */

public class SequentialFadeTransition extends TransitionSet {

    public SequentialFadeTransition() {
        setOrdering(ORDERING_SEQUENTIAL);
        addTransition(new Fade());
    }
}
