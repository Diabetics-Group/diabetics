package net.styleru.ikomarov.diabetics.view_object.categories;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 02.05.17.
 */

public class CategoryViewObject implements Parcelable {

    @NonNull
    private final String id;

    @NonNull
    private final String title;

    @NonNull
    private final String lastUpdate;

    @NonNull
    private final Integer topicsCount;

    @NonNull
    private final Boolean isSynchronized;

    public CategoryViewObject(@NonNull String id,
                              @NonNull String title,
                              @NonNull String lastUpdate,
                              @NonNull Integer topicsCount,
                              @NonNull Boolean isSynchronized) {
        this.id = id;
        this.title = title;
        this.lastUpdate = lastUpdate;
        this.topicsCount = topicsCount;
        this.isSynchronized = isSynchronized;
    }

    protected CategoryViewObject(Parcel in) {
        id = in.readString();
        title = in.readString();
        lastUpdate = in.readString();
        topicsCount = in.readInt();
        isSynchronized = in.readByte() == (byte) 0;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getLastUpdate() {
        return lastUpdate;
    }

    @NonNull
    public Integer getTopicsCount() {
        return topicsCount;
    }

    @NonNull
    public Boolean getSynchronized() {
        return isSynchronized;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(lastUpdate);
        dest.writeInt(topicsCount != null ? topicsCount : 0);
        dest.writeByte(isSynchronized != null && isSynchronized ? (byte) 1 : (byte) 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CategoryViewObject> CREATOR = new Creator<CategoryViewObject>() {
        @Override
        public CategoryViewObject createFromParcel(Parcel in) {
            return new CategoryViewObject(in);
        }

        @Override
        public CategoryViewObject[] newArray(int size) {
            return new CategoryViewObject[size];
        }
    };
}
