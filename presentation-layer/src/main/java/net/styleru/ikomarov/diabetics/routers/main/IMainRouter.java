package net.styleru.ikomarov.diabetics.routers.main;

import android.support.annotation.StringRes;
import android.view.View;

import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;
import net.styleru.ikomarov.diabetics.view_object.chats.ChatViewObject;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;

/**
 * Created by i_komarov on 02.05.17.
 */

public interface IMainRouter {

    void navigateUp();

    void instantiateDefaultFragment();

    void navigateToCategories();

    void navigateToTopics(CategoryViewObject model);

    void navigateToTopicCreate(CategoryViewObject model);

    void navigateToComments(TopicViewObject model);

    void navigateToChats();

    void navigateToChat(ChatViewObject model);

    void navigateToParticipants();

    void navigateToProfile();

    void navigateToPreferences();

    void navigateToRate();

    void newSnackBarNotification(@StringRes int content);

    void newSnackBarNotification(String content);

    void newSnackBarNotification(@StringRes int content, View.OnClickListener callback);

    void newSnackBarNotification(String content, View.OnClickListener callback);
}
