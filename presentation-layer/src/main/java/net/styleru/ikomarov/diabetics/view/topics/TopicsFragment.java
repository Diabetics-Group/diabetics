package net.styleru.ikomarov.diabetics.view.topics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.adapter.topics.TopicsAdapter;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.routers.WizardRouter;
import net.styleru.ikomarov.diabetics.common.view.ViperFragment;
import net.styleru.ikomarov.diabetics.contracts.screens.ScreenContract;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IResourceManagersProvider;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IToolbarViewModelProvider;
import net.styleru.ikomarov.diabetics.presenter.topics.TopicsPresenter;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.diabetics.utils.pagination.PaginationListener;
import net.styleru.ikomarov.diabetics.utils.pagination.State;
import net.styleru.ikomarov.diabetics.view.main.IMainView;
import net.styleru.ikomarov.diabetics.view_holder.topics.TopicViewHolder;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;
import net.styleru.ikomarov.domain_layer.di.providers.IExecutionEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.di.providers.ITopicsModelProvider;
import net.styleru.ikomarov.domain_layer.use_cases.topics.TopicGetUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.topics.TopicsGetUseCase;

import java.util.Collections;
import java.util.List;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsFragment extends ViperFragment<ITopicsView, MainRouter, TopicsPresenter> implements ITopicsView, PaginationListener.Callback, TopicViewHolder.InteractionCallbacks {

    private TopicsFragmentState state;

    private RecyclerView list;

    private FloatingActionButton topicCreateButton;

    private TopicsAdapter adapter;

    private PaginationListener listener;

    @NonNull
    public static Fragment newInstance(@NonNull CategoryViewObject category) {
        Fragment fragment = new TopicsFragment();
        fragment.setArguments(TopicsFragmentState.createArguments(category));
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        state = new TopicsFragmentState(getArguments(), savedInstanceState);
    }

    @Override
    public void addTopics(List<TopicViewObject> items) {
        adapter.addItems(items);
    }

    @Override
    public void clicks(int position, TopicViewObject item) {
        getPresenter().onTopicClicks(item);
    }

    @Override
    public void onListStateChanged(State state) {
        getPresenter().loadTopics(state.getOffset(), state.getLimit());
    }

    @Override
    public void onActivityResultDelivered(int requestCode, int resultCode, Intent data) {
        if(requestCode == MainRouter.CODE_TOPIC_CREATE && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            addTopics(Collections.singletonList(
                    extras.getParcelable(WizardRouter.KEY_RESULT))
            );
        }
    }

    @Override
    protected void bindUserInterface(View root) {
        topicCreateButton = (FloatingActionButton) root.findViewById(R.id.fragment_topics_list_topic_create_button);
        list = (RecyclerView) root.findViewById(R.id.fragment_topics_list_recycler_view);
        list.setLayoutManager(new LinearLayoutManager(root.getContext()));
        list.setAdapter(adapter == null ? adapter = new TopicsAdapter(Glide.with(getActivity())) : adapter);
        listener = new PaginationListener(this);

        topicCreateButton.setOnClickListener(this::onTopicCreateButtonClicks);
    }

    private void onTopicCreateButtonClicks(View view) {
        getPresenter().createTopic();
    }

    @Override
    public void onResume() {
        super.onResume();
        list.addOnScrollListener(listener);
        adapter.setCallbacks(this);
        if(adapter.getItemCount() == 0) {
            getPresenter().loadTopics(0, listener.getLimit());
        }
    }

    @Override
    public void onPause() {
        adapter.setCallbacks(null);
        list.removeOnScrollListener(listener);
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        state.onSaveInstanceState(outState);
        state.saveListState(outState, list);
        state.saveAdapterState(outState, adapter);
    }

    @Override
    protected void onViewStateDelivered(Bundle savedInstanceState) {
        if(state.getListState() != null) {
            list.getLayoutManager().onRestoreInstanceState(state.getListState());
        }
        if(state.getAdapterState() != null) {
            adapter.onRestoreInstanceState(state.getAdapterState());
        }
    }

    @Override
    public void onDestroyView() {
        list.setAdapter(null);
        super.onDestroyView();
    }

    @NonNull
    @Override
    protected ITopicsView provideView() {
        return this;
    }

    @NonNull
    @Override
    protected MainRouter provideRouter() {
        return ((IMainView) getActivity()).getRouter();
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.fragment_topics_list;
    }

    @Override
    protected int provideScreenId() {
        return ScreenContract.TOPICS.getCode();
    }

    @NonNull
    @Override
    protected ViperPresenter.Factory<ITopicsView, MainRouter, TopicsPresenter> provideFactory() {
        IExecutionEnvironmentProvider executionEnvironmentProvider = (IExecutionEnvironmentProvider) getActivity().getApplication();
        ITopicsModelProvider modelProvider = (ITopicsModelProvider) getActivity().getApplication();
        IResourceManagersProvider resourceManagersProvider = (IResourceManagersProvider) getActivity();
        IToolbarViewModelProvider toolbarViewModelProvider = (IToolbarViewModelProvider) getActivity();

        return new TopicsPresenter.Factory(
                state.getModel(),
                resourceManagersProvider.provideLabelsManager(),
                toolbarViewModelProvider.provideToolbarModelMutator(),
                new TopicsGetUseCase(
                        modelProvider.provideTopicsRepository(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler()
                ),
                new TopicGetUseCase(
                        modelProvider.provideTopicsRepository(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler()
                ));
    }
}
