package net.styleru.ikomarov.diabetics.view.preferences;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.SwitchPreferenceCompat;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.view.ViperPreferencesFragment;
import net.styleru.ikomarov.data_layer.contracts.preferences.PreferenceKey;
import net.styleru.ikomarov.diabetics.contracts.screens.ScreenContract;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IResourceManagersProvider;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IToolbarViewModelProvider;
import net.styleru.ikomarov.diabetics.presenter.preferences.PreferencesPresenter;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.diabetics.view.main.IMainView;
import net.styleru.ikomarov.domain_layer.di.providers.IApplicationEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IExecutionEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.use_cases.cache.InvalidateCacheUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.cache.TotalSpaceUsedGetUseCase;

/**
 * Created by i_komarov on 13.05.17.
 */

public class PreferencesFragment extends ViperPreferencesFragment<IPreferencesView, MainRouter, PreferencesPresenter> implements IPreferencesView {

    private SwitchPreferenceCompat pushNotificationsSwitch;
    private Preference clearCacheButton;
    private Preference clearSessionButton;
    private Preference versionNameHolder;
    private Preference versionCodeHolder;
    private Preference reportBugButton;
    private Preference rateButton;

    public static Fragment newInstance() {
        return new PreferencesFragment();
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences_application);
        pushNotificationsSwitch = (SwitchPreferenceCompat) findPreference(PreferenceKey.PUSH_NOTIFICATIONS.getKey());
        clearCacheButton = findPreference(PreferenceKey.CLEAR_CACHE.getKey());
        clearCacheButton.setOnPreferenceClickListener(this::onClearCacheButtonClicks);
        clearSessionButton = findPreference(PreferenceKey.CLEAR_SESSION.getKey());
        clearSessionButton.setOnPreferenceClickListener(this::onClearSessionButtonClicks);
        versionNameHolder = findPreference(PreferenceKey.VERSION_NAME.getKey());
        versionCodeHolder = findPreference(PreferenceKey.VERSION_CODE.getKey());
        reportBugButton = findPreference(PreferenceKey.REPORT_BUG.getKey());
        reportBugButton.setOnPreferenceClickListener(this::onReportBugButtonClicks);
        rateButton = findPreference(PreferenceKey.RATE.getKey());
        rateButton.setOnPreferenceClickListener(this::onRateButtonClicks);
    }

    @Override
    public void setVersionName(String versionName) {
        versionNameHolder.setSummary(versionName);
    }

    @Override
    public void setVersionCode(int versionCode) {
        versionCodeHolder.setSummary(String.valueOf(versionCode));
    }

    @Override
    public void setTotalSpaceUsed(long totalSpaceUsedKilobytes) {
        clearCacheButton.setSummary(String.format(
                getActivity().getApplicationContext().getResources().getString(R.string.label_space_used),
                String.valueOf(totalSpaceUsedKilobytes))
        );
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityResultDelivered(int requestCode, int resultCode, Intent data) {

    }

    @Override
    protected void onViewStateDelivered(Bundle savedInstanceState) {

    }

    private boolean onClearCacheButtonClicks(Preference preference) {
        getPresenter().onClearCachePreferenceClicks();
        return true;
    }

    private boolean onClearSessionButtonClicks(Preference preference) {
        getPresenter().onClearSessionPreferenceClicks();
        return true;
    }

    private boolean onReportBugButtonClicks(Preference preference) {
        getPresenter().onReportBugButtonClicks();
        return true;
    }

    private boolean onRateButtonClicks(Preference preference) {
        getPresenter().onRateButtonClicks();
        return true;
    }

    @NonNull
    @Override
    protected IPreferencesView provideView() {
        return this;
    }

    @NonNull
    @Override
    protected MainRouter provideRouter() {
        return ((IMainView) getActivity()).getRouter();
    }

    @Override
    protected int provideScreenId() {
        return ScreenContract.PREFERENCES.getCode();
    }

    @NonNull
    @Override
    protected ViperPresenter.Factory<IPreferencesView, MainRouter, PreferencesPresenter> provideFactory() {
        IApplicationEnvironmentProvider applicationEnvironmentProvider = (IApplicationEnvironmentProvider) getActivity().getApplication();
        IExecutionEnvironmentProvider executionEnvironmentProvider = (IExecutionEnvironmentProvider) getActivity().getApplication();
        IResourceManagersProvider resourceManagersProvider = (IResourceManagersProvider) getActivity();
        IToolbarViewModelProvider toolbarViewModelProvider = (IToolbarViewModelProvider) getActivity();

        return new PreferencesPresenter.Factory(
                new TotalSpaceUsedGetUseCase(
                        applicationEnvironmentProvider.provideDatabaseService(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler()
                ),
                new InvalidateCacheUseCase(
                        applicationEnvironmentProvider.provideDatabaseService(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler()
                ),
                resourceManagersProvider.provideNotificationsManager(),
                resourceManagersProvider.provideLabelsManager(),
                toolbarViewModelProvider.provideToolbarModelMutator()
        );
    }
}
