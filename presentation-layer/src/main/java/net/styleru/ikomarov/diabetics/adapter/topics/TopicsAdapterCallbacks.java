package net.styleru.ikomarov.diabetics.adapter.topics;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;

import net.styleru.ikomarov.diabetics.contracts.locale.DateTimeContract;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;

import java.text.ParseException;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsAdapterCallbacks extends SortedListAdapterCallback<TopicViewObject> {

    public TopicsAdapterCallbacks(RecyclerView.Adapter adapter) {
        super(adapter);
    }

    @Override
    public int compare(TopicViewObject o1, TopicViewObject o2) {
        try {
            return DateTimeContract.parseDateFormat.parse(o2.getLastUpdate()).compareTo(DateTimeContract.parseDateFormat.parse(o1.getLastUpdate()));
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public boolean areContentsTheSame(TopicViewObject oldItem, TopicViewObject newItem) {
        return oldItem.getFirstName().equals(newItem.getFirstName()) &&
                oldItem.getLastName().equals(newItem.getLastName()) &&
                oldItem.getLastUpdate().equals(newItem.getLastUpdate()) &&
                oldItem.getTitle().equals(newItem.getTitle()) &&
                oldItem.getCommentsCount().equals(newItem.getCommentsCount());
    }

    @Override
    public boolean areItemsTheSame(TopicViewObject item1, TopicViewObject item2) {
        return item1.getId().equals(item2.getId());
    }
}
