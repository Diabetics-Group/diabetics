package net.styleru.ikomarov.diabetics.routers.authorization;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.common.routers.BaseRouter;
import net.styleru.ikomarov.diabetics.common.view.ITransactionFunction;
import net.styleru.ikomarov.diabetics.view.authorization.AuthorizationActivity;
import net.styleru.ikomarov.diabetics.view.authorization.IAuthorizationView;
import net.styleru.ikomarov.diabetics.view.authorization_code.AuthorizationCodeFragment;
import net.styleru.ikomarov.diabetics.view.authorization_phone.AuthorizationPhoneFragment;
import net.styleru.ikomarov.diabetics.view_model.user.UserViewModel;

/**
 * Created by i_komarov on 02.06.17.
 */

public class AuthorizationRouter extends BaseRouter<IAuthorizationView> implements IAuthorizationRouter {

    public static final String KEY_RESULT = AuthorizationRouter.class.getCanonicalName() + "." + "RESULT";

    public AuthorizationRouter(@NonNull AuthorizationActivity view) {
        super(view);
    }

    @Override
    public void navigateUp() {
        getView().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void instantiateDefaultFragment() {
        getView().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_authorization_fragment_container, AuthorizationPhoneFragment.newInstance())
                .commit();
    }

    @Override
    public void navigateToVerification(String phone, ITransactionFunction function) {
        FragmentTransaction transaction = getView().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_authorization_fragment_container, AuthorizationCodeFragment.newInstance(phone))
                .addToBackStack(null);

        if(function != null) {
            transaction = function.apply(transaction);
        }

        transaction.commit();
    }

    @Override
    public void returnOnSuccess(UserViewModel result) {
        Intent intent = new Intent();
        intent.putExtra(KEY_RESULT, result);
        getView().setResult(Activity.RESULT_OK, intent);
        getView().finish();
    }

    @Override
    public void returnOnCancel() {
        getView().setResult(Activity.RESULT_CANCELED);
        getView().finish();
    }
}
