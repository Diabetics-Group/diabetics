package net.styleru.ikomarov.diabetics.managers.view;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.common.view.ITransactionFunction;

/**
 * Created by i_komarov on 03.06.17.
 */

public interface ITransitionManager<Key> {

    @NonNull
    ITransactionFunction provideFunction(@NonNull Key key);
}
