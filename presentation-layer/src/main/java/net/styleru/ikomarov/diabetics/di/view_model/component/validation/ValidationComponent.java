package net.styleru.ikomarov.diabetics.di.view_model.component.validation;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.di.view_model.component.view_model.IViewModelComponent;
import net.styleru.ikomarov.diabetics.di.view_model.validation.ValidationModule;

/**
 * Created by i_komarov on 05.06.17.
 */

public class ValidationComponent implements IValidationComponent {

    @NonNull
    private final IViewModelComponent component;

    @NonNull
    private final ValidationModule validation;

    public ValidationComponent(@NonNull IViewModelComponent component) {
        this.component = component;
        this.validation = new ValidationModule(component);
    }

    @NonNull
    @Override
    public ValidationModule validation() {
        return validation;
    }
}
