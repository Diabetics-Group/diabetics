package net.styleru.ikomarov.diabetics.view.preferences;

import net.styleru.ikomarov.diabetics.common.view.IView;

/**
 * Created by i_komarov on 13.05.17.
 */

public interface IPreferencesView extends IView {

    void setVersionName(String versionName);

    void setVersionCode(int versionCode);

    void setTotalSpaceUsed(long totalSpaceUsedKilobytes);
}
