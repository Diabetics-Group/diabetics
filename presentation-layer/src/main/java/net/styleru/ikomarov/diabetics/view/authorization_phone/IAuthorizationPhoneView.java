package net.styleru.ikomarov.diabetics.view.authorization_phone;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.diabetics.common.view.IView;
import net.styleru.ikomarov.diabetics.managers.view.ITransitionManager;

/**
 * Created by i_komarov on 02.06.17.
 */

public interface IAuthorizationPhoneView extends IView {

    void setPhoneError(@NonNull String message);

    void onPhoneAuthenticationStarted();

    void onPhoneAuthenticationFinished();

    ITransitionManager<String> getTransitionManager();
}
