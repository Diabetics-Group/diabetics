package net.styleru.ikomarov.diabetics.application;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.services.database.IDatabaseService;
import net.styleru.ikomarov.domain_layer.di.graph.DependenciesGraph;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.providers.IApplicationEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IAuthenticationModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.ICategoriesModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.ICommentsModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IExecutionEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IGraphProvider;
import net.styleru.ikomarov.domain_layer.di.providers.ITopicsModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IUsersModelProvider;
import net.styleru.ikomarov.domain_layer.repository.authentication.IAuthenticationRepository;
import net.styleru.ikomarov.domain_layer.repository.categories.ICategoriesRepository;
import net.styleru.ikomarov.domain_layer.repository.comments.ICommentsRepository;
import net.styleru.ikomarov.domain_layer.repository.topics.ITopicsRepository;
import net.styleru.ikomarov.domain_layer.repository.users.IUsersRepository;

import io.reactivex.Scheduler;

/**
 * Created by i_komarov on 03.05.17.
 */

public class App extends Application implements IGraphProvider, IApplicationEnvironmentProvider, IExecutionEnvironmentProvider, ICategoriesModelProvider, ITopicsModelProvider, ICommentsModelProvider, IUsersModelProvider, IAuthenticationModelProvider {

    private DependenciesGraph graph;

    @Override
    public void onCreate() {
        super.onCreate();
        buildGraph();
    }

    private void buildGraph() {
        graph = new DependenciesGraph(this);
    }

    @NonNull
    @Override
    public IAppComponent provideGraphRoot() {
        return graph.provideGraphRoot();
    }

    @NonNull
    @Override
    public Context provideContext() {
        return graph.provideContext();
    }

    @NonNull
    @Override
    public IDatabaseService provideDatabaseService() {
        return graph.provideDatabaseService();
    }

    @NonNull
    @Override
    public Scheduler provideExecuteScheduler() {
        return graph.provideExecuteScheduler();
    }

    @NonNull
    @Override
    public Scheduler providePostScheduler() {
        return graph.providePostScheduler();
    }

    @NonNull
    @Override
    public ICategoriesRepository provideCategoriesRepository() {
        return graph.provideCategoriesRepository();
    }

    @NonNull
    @Override
    public ITopicsRepository provideTopicsRepository() {
        return graph.provideTopicsRepository();
    }

    @NonNull
    @Override
    public ICommentsRepository provideCommentsRepository() {
        return graph.provideCommentsRepository();
    }

    @NonNull
    @Override
    public IUsersRepository provideUsersRepository() {
        return graph.provideUsersRepository();
    }

    @NonNull
    @Override
    public IAuthenticationRepository provideAuthenticationRepository() {
        return graph.provideAuthenticationRepository();
    }
}
