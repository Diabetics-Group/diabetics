package net.styleru.ikomarov.diabetics.view.categories;

import net.styleru.ikomarov.diabetics.common.view.IView;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;

import java.util.List;

/**
 * Created by i_komarov on 02.05.17.
 */

public interface ICategoriesView extends IView {

    void addCategories(List<CategoryViewObject> categories);
}
