package net.styleru.ikomarov.diabetics.managers.resources.notifications;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 14.05.17.
 */

public interface INotificationsManager {

    @NonNull
    String getOperationDelayedNotification();

    @NonNull
    String getCacheClearedNotification();

    @NonNull
    String getSessionClearedNotification();

    @NonNull
    String getEmptyFieldNotification();

    @NonNull
    String getBadPhoneCountryCodeNotification();

    @NonNull
    String getBadPhoneLengthNotification();

    @NonNull
    String getBadCodeLengthNotification();
}
