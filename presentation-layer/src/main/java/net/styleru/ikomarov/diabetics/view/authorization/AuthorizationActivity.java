package net.styleru.ikomarov.diabetics.view.authorization;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.common.container.RetainContainer;
import net.styleru.ikomarov.diabetics.common.container.ViewModelComponentContainer;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.view.ViperActivity;
import net.styleru.ikomarov.diabetics.contracts.screens.ScreenContract;
import net.styleru.ikomarov.diabetics.di.view_model.component.view_model.IViewModelComponent;
import net.styleru.ikomarov.diabetics.di.view_model.component.view_model.ViewModelComponentFactory;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IResourceManagersProvider;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IValidationsProvider;
import net.styleru.ikomarov.diabetics.managers.resources.labels.ILabelsManager;
import net.styleru.ikomarov.diabetics.managers.resources.notifications.INotificationsManager;
import net.styleru.ikomarov.diabetics.presenter.authorization.AuthorizationPresenter;
import net.styleru.ikomarov.diabetics.routers.authorization.AuthorizationRouter;
import net.styleru.ikomarov.domain_layer.di.providers.IApplicationEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IGraphProvider;
import net.styleru.ikomarov.domain_layer.validation.base.IValidationStrategy;

/**
 * Created by i_komarov on 02.06.17.
 */

public class AuthorizationActivity extends ViperActivity<IAuthorizationView, AuthorizationRouter, AuthorizationPresenter> implements IAuthorizationView, IResourceManagersProvider, IValidationsProvider {

    @NonNull
    private static final String TAG_VIEW_MODEL_CONTAINER = AuthorizationActivity.class.getCanonicalName() + "." + "VMC";

    private AuthorizationRouter router;

    private AppCompatTextView titleHolder;

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getPresenter().onNavigateUp();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        verifyComponentProvidersExist();
    }

    @Override
    public void onResume() {
        super.onResume();
        Fragment fragment;
        if((fragment = getSupportFragmentManager().findFragmentById(R.id.activity_authorization_fragment_container)) == null || fragment.isDetached()) {
            getPresenter().onInstantiateDefaultFragment();
        }
    }

    @Override
    public AuthorizationRouter getRouter() {
        return router;
    }

    @Override
    protected void bindUserInterface() {
        titleHolder = (AppCompatTextView) findViewById(R.id.activity_authorization_title_label);

        if(router == null) {
            router = new AuthorizationRouter(
                    this
            );
        }
    }

    @Override
    protected void onActivityResultDelivered(int requestCode, int resultCode, Intent data) {
        //just ignore, nothing interesting lays here
    }

    @Override
    protected void onViewStateDelivered(Bundle savedInstanceState) {
        //just ignore, nothing interesting lays here
    }

    @NonNull
    @Override
    public ILabelsManager provideLabelsManager() {
        verifyComponentProvidersExist();
        return ((RetainContainer<IViewModelComponent>) getSupportFragmentManager()
                .findFragmentByTag(TAG_VIEW_MODEL_CONTAINER))
                .provideComponent()
                .labels()
                .provideLabelsManager();
    }

    @NonNull
    @Override
    public INotificationsManager provideNotificationsManager() {
        verifyComponentProvidersExist();
        return ((RetainContainer<IViewModelComponent>) getSupportFragmentManager()
                .findFragmentByTag(TAG_VIEW_MODEL_CONTAINER))
                .provideComponent()
                .notifications()
                .provideNotificationsManager();
    }

    @NonNull
    @Override
    public IValidationStrategy<String> providerPhoneValidationStrategy() {
        verifyComponentProvidersExist();
        return ((ViewModelComponentContainer) getSupportFragmentManager()
                .findFragmentByTag(TAG_VIEW_MODEL_CONTAINER))
                .provideValidationComponent()
                .validation()
                .providePhoneValidationStrategy();
    }

    @NonNull
    @Override
    public IValidationStrategy<String> provideCodeValidationStrategy() {
        verifyComponentProvidersExist();
        return ((ViewModelComponentContainer) getSupportFragmentManager()
                .findFragmentByTag(TAG_VIEW_MODEL_CONTAINER))
                .provideValidationComponent()
                .validation()
                .provideCodeValidationStrategy();
    }


    @NonNull
    @Override
    protected IAuthorizationView provideView() {
        return this;
    }

    @NonNull
    @Override
    protected AuthorizationRouter provideRouter() {
        return router;
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.activity_authorization;
    }

    @Override
    protected int provideScreenId() {
        return ScreenContract.AUTHORIZATION.getCode();
    }

    @NonNull
    @Override
    protected ViperPresenter.Factory<IAuthorizationView, AuthorizationRouter, AuthorizationPresenter> provideFactory() {


        return new AuthorizationPresenter.Factory(

        );
    }

    private void verifyComponentProvidersExist() {
        if(getSupportFragmentManager().findFragmentByTag(TAG_VIEW_MODEL_CONTAINER) == null) {
            RetainContainer<IViewModelComponent> container = new ViewModelComponentContainer();
            IGraphProvider graphProvider = (IGraphProvider) getApplication();
            container.setFactory(new ViewModelComponentFactory(graphProvider.provideGraphRoot()));
            getSupportFragmentManager().beginTransaction()
                    .add(container, TAG_VIEW_MODEL_CONTAINER)
                    .commitNow();
        }
    }
}
