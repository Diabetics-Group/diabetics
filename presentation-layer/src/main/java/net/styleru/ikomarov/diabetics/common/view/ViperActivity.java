package net.styleru.ikomarov.diabetics.common.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

import net.styleru.ikomarov.diabetics.common.presenter.PresenterLoader;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.routers.BaseRouter;

/**
 * Created by i_komarov on 02.05.17.
 */

public abstract class ViperActivity<V extends IView, R extends BaseRouter<V>, P extends ViperPresenter<V, R>> extends AppCompatActivity implements LoaderManager.LoaderCallbacks<P> {

    private ActivityResultBundle resultBundle;

    private Bundle savedInstanceState;

    private P presenter;

    @CallSuper
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(provideLayoutId());
        onHandleIntent(getIntent());
        resultBundle = new ActivityResultBundle();
        getSupportLoaderManager().initLoader(provideScreenId(), null, this);
        bindUserInterface();
    }

    @Override
    public final Loader<P> onCreateLoader(int id, Bundle args) {
        return new PresenterLoader<>(this, provideFactory());
    }

    @Override
    public final void onLoadFinished(Loader<P> loader, P presenter) {
        this.presenter = presenter;
    }

    @Override
    public final void onLoaderReset(Loader<P> loader) {
        this.presenter = null;
    }

    @Override
    public final void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        resultBundle.offer(new ActivityResult(requestCode, resultCode, data));
    }

    public void onHandleIntent(Intent intent) {

    }

    @Override
    public final void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
    }

    @CallSuper
    @Override
    public void onStart() {
        super.onStart();
        while(resultBundle.size() > 0) {
            ActivityResult result = resultBundle.poll();
            onActivityResultDelivered(result.getRequestCode(), result.getResultCode(), result.getData());
        }

        if(savedInstanceState != null) {
            onViewStateDelivered(savedInstanceState);
        }
    }

    @CallSuper
    @Override
    public void onResume() {
        super.onResume();
        presenter.attachView(provideView(), provideRouter());
    }

    @CallSuper
    @Override
    public void onPause() {
        presenter.detachView();
        super.onPause();
    }

    @CallSuper
    @Override
    public void onStop() {
        super.onStop();
    }

    protected final P getPresenter() {
        return this.presenter;
    }

    protected abstract void bindUserInterface();

    protected abstract void onActivityResultDelivered(int requestCode, int resultCode, Intent data);

    protected abstract void onViewStateDelivered(Bundle savedInstanceState);

    @NonNull
    protected abstract V provideView();

    @NonNull
    protected abstract R provideRouter();

    @LayoutRes
    protected abstract int provideLayoutId();

    protected abstract int provideScreenId();

    @NonNull
    protected abstract ViperPresenter.Factory<V, R, P> provideFactory();
}
