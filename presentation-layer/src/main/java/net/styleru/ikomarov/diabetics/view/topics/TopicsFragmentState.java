package net.styleru.ikomarov.diabetics.view.topics;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import net.styleru.ikomarov.diabetics.adapter.topics.TopicsAdapter;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsFragmentState {

    private static final String PREFIX = TopicsFragmentState.class.getCanonicalName() + ".";

    private static final String KEY_CATEGORY_MODEL = PREFIX + "MODEL";

    private static final String KEY_LIST_STATE = PREFIX + "LIST";

    private static final String KEY_ADAPTER_STATE = PREFIX + "ADAPTER";

    @NonNull
    private CategoryViewObject model;

    private Parcelable adapterState;

    private Parcelable listState;

    public static Bundle createArguments(CategoryViewObject category) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_CATEGORY_MODEL, category);
        return args;
    }

    public TopicsFragmentState(Bundle args, Bundle saved) {
        model = (args != null ? args : saved).getParcelable(KEY_CATEGORY_MODEL);
        adapterState = (saved != null) ? saved.getParcelable(KEY_ADAPTER_STATE) : null;
        listState = (saved != null) ? saved.getParcelable(KEY_LIST_STATE) : null;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_CATEGORY_MODEL, model);
    }

    @NonNull
    public CategoryViewObject getModel() {
        return this.model;
    }

    public void saveListState(Bundle outState, RecyclerView list) {
        outState.putParcelable(KEY_LIST_STATE, list.getLayoutManager().onSaveInstanceState());
    }

    public Parcelable getListState() {
        return listState;
    }

    public void saveAdapterState(Bundle outState, TopicsAdapter adapter) {
        outState.putParcelable(KEY_ADAPTER_STATE, adapter.onSaveInstanceState());
    }

    public Parcelable getAdapterState() {
        return adapterState;
    }
}
