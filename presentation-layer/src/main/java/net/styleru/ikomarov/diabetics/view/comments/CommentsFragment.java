package net.styleru.ikomarov.diabetics.view.comments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatMultiAutoCompleteTextView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.bumptech.glide.Glide;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.adapter.comments.CommentsAdapter;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.view.ViperFragment;
import net.styleru.ikomarov.diabetics.contracts.screens.ScreenContract;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IResourceManagersProvider;
import net.styleru.ikomarov.diabetics.di.view_model.providers.IToolbarViewModelProvider;
import net.styleru.ikomarov.diabetics.presenter.comments.CommentsPresenter;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.diabetics.ui.list.HeaderDecoration;
import net.styleru.ikomarov.diabetics.utils.pagination.PaginationListener;
import net.styleru.ikomarov.diabetics.utils.pagination.State;
import net.styleru.ikomarov.diabetics.view.main.IMainView;
import net.styleru.ikomarov.diabetics.view_object.comments.CommentViewObject;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;
import net.styleru.ikomarov.domain_layer.di.providers.ICommentsModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IExecutionEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.di.providers.ITopicsModelProvider;
import net.styleru.ikomarov.domain_layer.use_cases.comments.CommentCreateUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.comments.CommentsGetUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.topics.TopicGetUseCase;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsFragment extends ViperFragment<ICommentsView, MainRouter, CommentsPresenter> implements ICommentsView, PaginationListener.Callback {

    private AppCompatTextView commentsCountHolder;
    private CircleImageView imageHolder;
    private AppCompatTextView nameHolder;
    private AppCompatTextView titleHolder;
    private AppCompatTextView contentHolder;
    private RecyclerView list;
    private ImageButton attachContentButton;
    private AppCompatMultiAutoCompleteTextView commentTextHolder;
    private ImageButton createCommentButton;

    private PaginationListener listener;

    private CommentsAdapter adapter;

    private CommentsFragmentState state;

    @NonNull
    public static Fragment newInstance(@NonNull TopicViewObject model) {
        Fragment fragment = new CommentsFragment();
        fragment.setArguments(CommentsFragmentState.createArguments(model));
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        state = new CommentsFragmentState(getArguments(), savedInstanceState);
    }

    @Override
    public void hideLoadPreviousCommentsButton() {
        //previousCommentsButton.setVisibility(View.GONE);
        adapter.removeHeader();
    }

    @Override
    public void addComment(CommentViewObject item) {
        adapter.addItems(Collections.singletonList(item));
        list.scrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    public void addComments(List<CommentViewObject> items) {
        adapter.addItems(items);
    }

    @Override
    public void displayTopic(TopicViewObject item) {
        nameHolder.setText(item.getFirstName() + " " + item.getLastName());
        titleHolder.setText(item.getTitle());
        contentHolder.setText(item.getContent());
        commentsCountHolder.setText(item.getCommentsCount() != null && item.getCommentsCount() != 0 ?
                getActivity().getResources().getQuantityString(R.plurals.comments_count, item.getCommentsCount(), item.getCommentsCount()) :
                getActivity().getResources().getString(R.string.comments_count_zero)
        );

        if(!TextUtils.isEmpty(item.getImageUrl())) {
            Glide.with(getActivity()).load(item.getImageUrl()).into(imageHolder);
        }
    }

    @Override
    public void onListStateChanged(State state) {
        //TODO: uncomment if pagination needed
        //getPresenter().loadComments(adapter.lastUpdate(), state.getOffset(), state.getLimit());
    }

    @Override
    public void onActivityResultDelivered(int requestCode, int resultCode, Intent data) {

    }

    @Override
    protected void bindUserInterface(View root) {
        list = (RecyclerView) root.findViewById(R.id.fragment_comments_list_recycler_view);
        commentsCountHolder = (AppCompatTextView) root.findViewById(R.id.layout_topic_header_comments_count);
        imageHolder = (CircleImageView) root.findViewById(R.id.layout_topic_header_image);
        nameHolder = (AppCompatTextView) root.findViewById(R.id.layout_topic_header_name);
        titleHolder = (AppCompatTextView) root.findViewById(R.id.layout_topic_header_title);
        contentHolder = (AppCompatTextView) root.findViewById(R.id.layout_topic_header_content);
        attachContentButton = (ImageButton) root.findViewById(R.id.layout_message_composer_attach_button);
        commentTextHolder = (AppCompatMultiAutoCompleteTextView) root.findViewById(R.id.layout_message_composer_content_holder);
        createCommentButton = (ImageButton) root.findViewById(R.id.layout_message_composer_send_button);

        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setAdapter(adapter != null ? adapter : (adapter = new CommentsAdapter(Glide.with(getActivity()), this::onPreviousCommentsButtonClicks)));
        listener = new PaginationListener(this);

        attachContentButton.setOnClickListener(this::onAttachContentButtonClicks);
        createCommentButton.setOnClickListener(this::onCreateCommentButtonClicks);
    }

    private void onPreviousCommentsButtonClicks(View view) {
        Log.d("CommentsFragment", "onPreviousCommentsButtonClicks");
        getPresenter().loadComments(adapter.getRealItemCount(), 10);
    }

    private void onAttachContentButtonClicks(View view) {

    }

    private void onCreateCommentButtonClicks(View view) {
        Editable content = commentTextHolder.getText();
        if(!TextUtils.isEmpty(content)) {
            commentTextHolder.setText("");
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(commentTextHolder.getWindowToken(), 0);
            getPresenter().createComment(content.toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        list.addOnScrollListener(listener);
        if(state.shouldInitLoading()) {
            getPresenter().loadTopic();
        }
        if(adapter.getRealItemCount() == 0) {
            getPresenter().loadComments(0, 10);
        }
    }

    @Override
    public void onPause() {
        list.removeOnScrollListener(listener);
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        state.onSaveInstanceState(outState);
        state.saveListState(outState, list);
        state.saveAdapterState(outState, adapter);
    }

    @Override
    protected void onViewStateDelivered(Bundle savedInstanceState) {
        if(state.getAdapterState() != null) {
            adapter.onRestoreInstanceState(state.getAdapterState());
        }
        if(state.getListState() != null) {
            list.getLayoutManager().onRestoreInstanceState(state.getListState());
        }
    }

    @Override
    public void onDestroyView() {
        list.setAdapter(null);
        super.onDestroyView();
    }

    @NonNull
    @Override
    protected ICommentsView provideView() {
        return this;
    }

    @NonNull
    @Override
    protected MainRouter provideRouter() {
        return ((IMainView) getActivity()).getRouter();
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.fragment_comments_list;
    }

    @Override
    protected int provideScreenId() {
        return ScreenContract.COMMENTS.getCode();
    }

    @NonNull
    @Override
    protected ViperPresenter.Factory<ICommentsView, MainRouter, CommentsPresenter> provideFactory() {
        //obtain links to providers needed from the current Application context
        ICommentsModelProvider commentsModelProvider = (ICommentsModelProvider) getActivity().getApplication();
        ITopicsModelProvider topicsModelProvider = (ITopicsModelProvider) getActivity().getApplication();
        IExecutionEnvironmentProvider executionEnvironmentProvider = (IExecutionEnvironmentProvider) getActivity().getApplication();
        IResourceManagersProvider resourceManagersProvider = (IResourceManagersProvider) getActivity();
        IToolbarViewModelProvider toolbarViewModelProvider = (IToolbarViewModelProvider) getActivity();
        //create new instance of presenter factory to be used with retain-presenter framework
        return new CommentsPresenter.Factory(
                state.getModel(),
                resourceManagersProvider.provideLabelsManager(),
                resourceManagersProvider.provideNotificationsManager(),
                toolbarViewModelProvider.provideToolbarModelMutator(),
                new CommentCreateUseCase(
                        commentsModelProvider.provideCommentsRepository(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler()
                ),
                new CommentsGetUseCase(
                        commentsModelProvider.provideCommentsRepository(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler()
                ),
                new TopicGetUseCase(
                        topicsModelProvider.provideTopicsRepository(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler()
                )
        );
    }
}
