package net.styleru.ikomarov.diabetics.ui.input;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Created by i_komarov on 05.05.17.
 */

public class InputLimitController implements TextWatcher {

    private EditText inputHolder;

    private final int maxChars;

    public InputLimitController(int maxChars) {
        this.maxChars = maxChars;
    }

    public void bindInputHolderHolder(@NonNull EditText inputHolder) {
        this.inputHolder = inputHolder;
        this.inputHolder.addTextChangedListener(this);
    }

    public void unbindInputHolder() {
        this.inputHolder.removeTextChangedListener(this);
        this.inputHolder = null;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        final String text;
        final int diff = (text = inputHolder.getText().toString()).length() - maxChars;
        if(diff > 0) {
            inputHolder.setText(text.substring(0, text.length() - diff));
            inputHolder.setSelection(maxChars);
        }
    }
}
