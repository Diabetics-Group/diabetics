package net.styleru.ikomarov.diabetics.presenter.comments;

import android.support.annotation.NonNull;
import android.util.Log;

import net.styleru.ikomarov.data_layer.contracts.exception.EnvironmentExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.Reason;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.exceptions.ExceptionHandler;
import net.styleru.ikomarov.diabetics.managers.resources.labels.ILabelsManager;
import net.styleru.ikomarov.diabetics.managers.resources.notifications.INotificationsManager;
import net.styleru.ikomarov.diabetics.mapping.comments.CommentMapping;
import net.styleru.ikomarov.diabetics.mapping.comments.CommentsMapping;
import net.styleru.ikomarov.diabetics.mapping.topics.TopicMapping;
import net.styleru.ikomarov.diabetics.mapping.topics.TopicsMapping;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;
import net.styleru.ikomarov.diabetics.view.comments.ICommentsView;
import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelMutator;
import net.styleru.ikomarov.diabetics.view_object.comments.CommentViewObject;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;
import net.styleru.ikomarov.domain_layer.dto.comments.CommentDTO;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;
import net.styleru.ikomarov.domain_layer.use_cases.comments.CommentCreateUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.comments.CommentsGetUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.topics.TopicGetUseCase;

import java.util.List;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsPresenter extends ViperPresenter<ICommentsView, MainRouter> implements CommentCreateUseCase.Callbacks<CommentViewObject>, CommentsGetUseCase.Callbacks<CommentViewObject>, TopicGetUseCase.Callbacks<TopicViewObject> {

    @NonNull
    private final TopicViewObject model;

    @NonNull
    private final ILabelsManager labelsManager;

    @NonNull
    private final INotificationsManager notificationsManager;

    @NonNull
    private final IToolbarModelMutator toolbarModelMutator;

    @NonNull
    private final CommentCreateUseCase commentCreate;

    @NonNull
    private final CommentsGetUseCase commentsGet;

    @NonNull
    private final TopicGetUseCase topicGet;

    @NonNull
    private final Function<TopicDTO, TopicViewObject> topicMapper;

    @NonNull
    private final Function<List<TopicDTO>, List<TopicViewObject>> topicsMapper;

    @NonNull
    private final Function<CommentDTO, CommentViewObject> commentMapper;

    @NonNull
    private final Function<List<CommentDTO>, List<CommentViewObject>> commentsMapper;

    @NonNull
    private final ExceptionHandler handler;

    private CommentsPresenter(@NonNull TopicViewObject model,
                              @NonNull ILabelsManager labelsManager,
                              @NonNull INotificationsManager notificationsManager,
                              @NonNull IToolbarModelMutator toolbarModelMutator,
                              @NonNull CommentCreateUseCase commentCreate,
                              @NonNull CommentsGetUseCase commentsGet,
                              @NonNull TopicGetUseCase topicGet) {

        this.model = model;
        this.labelsManager = labelsManager;
        this.notificationsManager = notificationsManager;
        this.toolbarModelMutator = toolbarModelMutator;
        this.commentCreate = commentCreate;
        this.commentsGet = commentsGet;
        this.topicGet = topicGet;
        this.topicMapper = new TopicMapping();
        this.topicsMapper = new TopicsMapping(topicMapper);
        this.commentMapper = new CommentMapping();
        this.commentsMapper = new CommentsMapping(commentMapper);
        this.handler = new ExceptionHandler();
    }

    @Override
    protected void onViewAttach() {
        toolbarModelMutator.setTitle(labelsManager.getCommentsLabel());
        toolbarModelMutator.setSubtitle(model.getTitle());
        getView().displayTopic(model);
    }

    @Override
    protected void onViewDetach() {

    }

    @Override
    protected void onDestroy() {

    }

    public void createComment(@NonNull String text) {
        //TODO: replace "42" with real userId
        commentCreate.execute(
                commentMapper,
                this,
                model.getId(),
                "42",
                new CommentDTO.Builder(text)
        );
    }

    public void loadComments(int offset, int limit) {
        commentsGet.execute(
                commentsMapper,
                this,
                model.getId(),
                offset,
                limit
        );
    }

    public void loadTopic() {
        //TODO: uncomment, when fixed on back-end
        /*
        topicGet.execute(
                topicMapper,
                this,
                model.getId()
        );
        */
    }

    @Override
    public void onCommentCreated(CommentViewObject comment) {
        getView().addComment(comment);
    }

    @Override
    public void onCommentCreationFailed(ExceptionBundle error) {
        handler.handleDebug(error);
    }

    @Override
    public void onCommentCreationDelayed() {
        getRouter().newSnackBarNotification(notificationsManager.getOperationDelayedNotification());
    }

    @Override
    public void onCommentsLoaded(List<CommentViewObject> items) {
        getView().addComments(items);
    }

    @Override
    public void onCommentsLoadingFailed(ExceptionBundle error) {
        if(error.getReason() == Reason.ENVIRONMENT) {
            EnvironmentExceptionContract contract = error.getReason().getContract();
            EnvironmentExceptionContract.Type type = contract.extractType(error);
            if(type == EnvironmentExceptionContract.Type.REMOTE_DB_EMPTY || type == EnvironmentExceptionContract.Type.CACHE_EMPTY) {
                getView().hideLoadPreviousCommentsButton();
            }
        }

        handler.handleDebug(error);
    }

    @Override
    public void onTopicLoaded(TopicViewObject item) {
        getView().displayTopic(item);
    }

    @Override
    public void onTopicLoadingFailed(ExceptionBundle error) {
        handler.handleDebug(error);
    }

    public static final class Factory implements ViperPresenter.Factory<ICommentsView, MainRouter, CommentsPresenter> {

        @NonNull
        private final TopicViewObject model;

        @NonNull
        private final ILabelsManager labelsManager;

        @NonNull
        private final INotificationsManager notificationsManager;

        @NonNull
        private final IToolbarModelMutator toolbarModelMutator;

        @NonNull
        private final CommentCreateUseCase commentCreate;

        @NonNull
        private final CommentsGetUseCase commentsGet;

        @NonNull
        private final TopicGetUseCase topicGet;

        public Factory(@NonNull TopicViewObject model,
                       @NonNull ILabelsManager labelsManager,
                       @NonNull INotificationsManager notificationsManager,
                       @NonNull IToolbarModelMutator toolbarModelMutator,
                       @NonNull CommentCreateUseCase commentCreate,
                       @NonNull CommentsGetUseCase commentsGet,
                       @NonNull TopicGetUseCase topicGet) {

            this.model = model;
            this.labelsManager = labelsManager;
            this.notificationsManager = notificationsManager;
            this.toolbarModelMutator = toolbarModelMutator;
            this.commentCreate = commentCreate;
            this.commentsGet = commentsGet;
            this.topicGet = topicGet;
        }

        @Override
        public CommentsPresenter create() {
            return new CommentsPresenter(model, labelsManager, notificationsManager, toolbarModelMutator, commentCreate, commentsGet, topicGet);
        }
    }
}
