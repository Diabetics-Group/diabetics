package net.styleru.ikomarov.diabetics.view.topic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import net.styleru.ikomarov.diabetics.R;
import net.styleru.ikomarov.diabetics.common.presenter.ViperPresenter;
import net.styleru.ikomarov.diabetics.common.routers.WizardRouter;
import net.styleru.ikomarov.diabetics.common.view.ViperActivity;
import net.styleru.ikomarov.diabetics.contracts.screens.ScreenContract;
import net.styleru.ikomarov.diabetics.presenter.topic.TopicCreatePresenter;
import net.styleru.ikomarov.diabetics.ui.input.InputLimitController;
import net.styleru.ikomarov.diabetics.view_object.categories.CategoryViewObject;
import net.styleru.ikomarov.diabetics.view_object.topics.TopicViewObject;
import net.styleru.ikomarov.domain_layer.di.providers.IExecutionEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.di.providers.ITopicsModelProvider;
import net.styleru.ikomarov.domain_layer.use_cases.topics.TopicCreateUseCase;

/**
 * Created by i_komarov on 05.05.17.
 */

public class TopicCreateActivity extends ViperActivity<ITopicCreateView, WizardRouter<TopicViewObject, ITopicCreateView>, TopicCreatePresenter> implements ITopicCreateView {

    private AppCompatTextView categoryHolder;
    private AppCompatEditText titleHolder;
    private AppCompatEditText contentHolder;
    private AppCompatButton createTopicButton;
    private AppCompatButton cancelButton;

    private InputLimitController titleCharsLimitController;
    private InputLimitController contentCharsLimitController;

    private final int titleCharsLimit = 50;
    private final int contentCharsLimit = 400;

    private TopicCreateActivityState state;

    private Bundle savedInstanceState;

    public static Intent createIntent(Activity sender, CategoryViewObject model) {
        Intent intent = new Intent(sender, TopicCreateActivity.class);
        intent.putExtras(TopicCreateActivityState.asBundle(model));
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onHandleIntent(Intent intent) {
        state = new TopicCreateActivityState(getIntent(), savedInstanceState);
    }

    @Override
    public void showTitleError(@StringRes int message) {
        titleHolder.setError(getResources().getString(message));
    }

    @Override
    public void showTitleError(String message) {
        titleHolder.setError(message);
    }

    @Override
    public void showContentError(@StringRes int message) {
        contentHolder.setError(getResources().getString(message));
    }

    @Override
    public void showContentError(String message) {
        contentHolder.setError(message);
    }

    @Override
    protected void bindUserInterface() {
        categoryHolder = (AppCompatTextView) findViewById(R.id.activity_topic_create_category_holder);
        titleHolder = (AppCompatEditText) findViewById(R.id.activity_topic_create_title_holder);
        contentHolder = (AppCompatEditText) findViewById(R.id.activity_topic_create_content_holder);
        createTopicButton = (AppCompatButton) findViewById(R.id.activity_topic_create_topic_create_button);
        cancelButton = (AppCompatButton) findViewById(R.id.activity_topic_create_cancel_button);

        int fieldsBackground = getResources().getColor(R.color.colorIcons);

        categoryHolder.setBackgroundColor(fieldsBackground);
        titleHolder.setBackgroundColor(fieldsBackground);
        contentHolder.setBackgroundColor(fieldsBackground);

        contentHolder.setHorizontallyScrolling(false);

        createTopicButton.setOnClickListener(this::onTopicCreateButtonClicks);
        cancelButton.setOnClickListener(this::onCancelButtonClicks);
        titleCharsLimitController = new InputLimitController(titleCharsLimit);
        contentCharsLimitController = new InputLimitController(contentCharsLimit);
        categoryHolder.setText(state.getModel().getTitle());
    }

    @Override
    public void onStart() {
        super.onStart();
        titleCharsLimitController.bindInputHolderHolder(titleHolder);
        contentCharsLimitController.bindInputHolderHolder(contentHolder);
    }

    @Override
    public void onStop() {
        titleCharsLimitController.unbindInputHolder();
        contentCharsLimitController.unbindInputHolder();
        super.onStop();
    }

    @Override
    protected void onActivityResultDelivered(int requestCode, int resultCode, Intent data) {

    }

    @Override
    protected void onViewStateDelivered(Bundle savedInstanceState) {

    }

    private void onTopicCreateButtonClicks(View view) {
        getPresenter().createTopic(
                titleHolder.getText().toString(),
                contentHolder.getText().toString()
        );
    }

    private void onCancelButtonClicks(View view) {
        getPresenter().cancel();
    }

    @NonNull
    @Override
    protected ITopicCreateView provideView() {
        return this;
    }

    @NonNull
    @Override
    protected WizardRouter<TopicViewObject, ITopicCreateView> provideRouter() {
        return new WizardRouter<>(this);
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.activity_topic_create;
    }

    @Override
    protected int provideScreenId() {
        return ScreenContract.TOPICS_CREATE.getCode();
    }

    @NonNull
    @Override
    protected ViperPresenter.Factory<ITopicCreateView, WizardRouter<TopicViewObject, ITopicCreateView>, TopicCreatePresenter> provideFactory() {
        IExecutionEnvironmentProvider executionEnvironmentProvider = (IExecutionEnvironmentProvider) getApplication();
        ITopicsModelProvider modelProvider = (ITopicsModelProvider) getApplication();

        return new TopicCreatePresenter.Factory(
                state.getModel(),
                new TopicCreateUseCase(
                        modelProvider.provideTopicsRepository(),
                        executionEnvironmentProvider.provideExecuteScheduler(),
                        executionEnvironmentProvider.providePostScheduler()
                )
        );
    }
}
