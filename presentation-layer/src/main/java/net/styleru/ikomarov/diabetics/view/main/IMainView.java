package net.styleru.ikomarov.diabetics.view.main;

import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentManager;

import net.styleru.ikomarov.diabetics.common.view.IView;
import net.styleru.ikomarov.diabetics.routers.main.MainRouter;

/**
 * Created by i_komarov on 28.04.17.
 */

public interface IMainView extends IView {

    FragmentManager getSupportFragmentManager();

    void setDrawerIconEnabled(boolean enabled);

    void startActivity(Intent intent);

    void startActivityForResult(Intent intent, int requestCode);

    void setTitle(String title);

    void setSubtitle(String subtitle);

    void openDrawer();

    void closeDrawer();

    void setDrawerEnabled(boolean enabled);

    void setCheckedDrawerItem(@IdRes int id);

    MainRouter getRouter();
}
