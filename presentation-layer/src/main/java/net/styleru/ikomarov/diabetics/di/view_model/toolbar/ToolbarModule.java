package net.styleru.ikomarov.diabetics.di.view_model.toolbar;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelMutator;
import net.styleru.ikomarov.diabetics.view_model.toolbar.IToolbarModelAccessor;
import net.styleru.ikomarov.diabetics.view_model.toolbar.ToolbarModel;

/**
 * Created by i_komarov on 12.05.17.
 */

public class ToolbarModule {

    @NonNull
    private final Object lock = new Object();

    @Nullable
    private volatile ToolbarModel model;

    public ToolbarModule() {

    }

    @NonNull
    public IToolbarModelMutator provideToolbarModelMutator() {
        IToolbarModelMutator localInstance = model;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = model;
                if(localInstance == null) {
                    localInstance = model = new ToolbarModel();
                }
            }
        }

        return localInstance;
    }

    @NonNull
    public IToolbarModelAccessor provideToolbarModelObserver() {
        IToolbarModelAccessor localInstance = model;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = model;
                if(localInstance == null) {
                    localInstance = model = new ToolbarModel();
                }
            }
        }

        return localInstance;
    }
}
