package net.styleru.ikomarov.diabetics.contracts.user_role;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by i_komarov on 15.05.17.
 */

public enum UserRole {
    EXPERT("expert"),
    GUEST ("guest");

    @NonNull
    private static final Map<String, UserRole> rolesMap = new HashMap<>(UserRole.values().length);

    private static volatile boolean isInitialized = false;

    private static void staticInitialize() {
        for(UserRole role : UserRole.values()) {
            rolesMap.put(role.value, role);
        }
    }

    @NonNull
    private final String value;

    @NonNull
    public static UserRole forValue(@NonNull String value) {
        if(!isInitialized) {
            staticInitialize();
            isInitialized = true;
        }

        return rolesMap.get(value);
    }

    UserRole(@NonNull String value) {
        this.value = value;
    }

    @NonNull
    public String getValue() {
        return this.value;
    }
}
