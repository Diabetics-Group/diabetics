package net.styleru.ikomarov.domain_layer.di.endpoints.comments.environment;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.comments.consumer.CommentsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.comments.consumer.ICommentsComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.consumer.ISessionClientComponent;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsEnvironmentComponent implements ICommentsEnvironmentComponent {

    @NonNull
    private final IEnvironmentComponent environment;

    @NonNull
    private final ICacheComponent cache;

    @NonNull
    private final ISessionClientComponent clients;

    @NonNull
    private final CommentsLocalModule local;

    @NonNull
    private final CommentsRemoteModule remote;

    @NonNull
    private final CommentsMappingModule mapping;

    public CommentsEnvironmentComponent(@NonNull IEnvironmentComponent environment,
                                        @NonNull ICacheComponent cache,
                                        @NonNull ISessionClientComponent clients) {
        this.environment = environment;
        this.cache = cache;
        this.clients = clients;
        this.local = new CommentsLocalModule(environment);
        this.remote = new CommentsRemoteModule(clients);
        this.mapping = new CommentsMappingModule();
    }

    @NonNull
    @Override
    public ICommentsComponent plusCommentsComponent() {
        return new CommentsComponent(environment, cache, this);
    }

    @NonNull
    @Override
    public CommentsLocalModule local() {
        return local;
    }

    @NonNull
    @Override
    public CommentsRemoteModule remote() {
        return remote;
    }

    @NonNull
    @Override
    public CommentsMappingModule mapping() {
        return mapping;
    }
}
