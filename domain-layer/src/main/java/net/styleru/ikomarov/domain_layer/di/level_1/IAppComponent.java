package net.styleru.ikomarov.domain_layer.di.level_1;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;

/**
 * Created by i_komarov on 03.05.17.
 */

public interface IAppComponent {

    @NonNull
    AppModule app();

    @NonNull
    IEnvironmentComponent plusEnvironmentComponent();
}

