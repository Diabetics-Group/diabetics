package net.styleru.ikomarov.domain_layer.use_cases.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;
import net.styleru.ikomarov.domain_layer.repository.base.CreateResult;
import net.styleru.ikomarov.domain_layer.repository.topics.ITopicsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableMaybeObserver;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by i_komarov on 05.05.17.
 */

public class TopicCreateUseCase extends UseCase {

    @NonNull
    private final ITopicsRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public TopicCreateUseCase(@NonNull ITopicsRepository repository,
                              @NonNull Scheduler executeScheduler,
                              @NonNull Scheduler postScheduler) {
        super();
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<TopicDTO, T> mapper, Callbacks<T> callbacks, String categoryId, String userId, TopicDTO.Builder builder) {
        execute(repository.create(categoryId, userId, builder), mapper, callbacks);
    }

    protected final <T> void execute(Single<CreateResult<TopicDTO>> source, Function<TopicDTO, T> mapper, Callbacks<T> callbacks) {
        source.subscribeOn(executeScheduler)
                .filter(result -> {
                    if(result.getStatus() == OperationStatus.DELAYED) {
                        postScheduler.scheduleDirect(callbacks::onTopicCreationDelayed);
                        return false;
                    }

                    return true;
                })
                .map(CreateResult::getDto)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createTopicCreateEventObserver(callbacks));
    }

    private <T> DisposableMaybeObserver<T> createTopicCreateEventObserver(Callbacks<T> callbacks) {
        DisposableMaybeObserver<T> topicCreateEventObserver = new TopicCreateEventObserver<>(callbacks);
        addDisposable(topicCreateEventObserver);
        return topicCreateEventObserver;
    }

    public interface Callbacks<T> {

        void onTopicCreated(T item);

        void onTopicCreationFailed(ExceptionBundle error);

        void onTopicCreationDelayed();
    }
}
