package net.styleru.ikomarov.domain_layer.di.providers;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.repository.comments.ICommentsRepository;

/**
 * Created by i_komarov on 08.05.17.
 */

public interface ICommentsModelProvider {

    @NonNull
    ICommentsRepository provideCommentsRepository();
}
