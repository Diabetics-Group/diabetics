package net.styleru.ikomarov.domain_layer.contracts.jobs;

/**
 * Created by i_komarov on 11.05.17.
 */

public enum JobPriority {
    LOW  (0),
    MID  (500),
    HIGH (1000);

    private final int priority;

    JobPriority(int code) {
        this.priority = code;
    }

    public int priority() {
        return this.priority;
    }
}
