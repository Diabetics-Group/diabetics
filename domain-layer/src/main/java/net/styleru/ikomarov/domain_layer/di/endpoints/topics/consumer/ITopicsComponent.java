package net.styleru.ikomarov.domain_layer.di.endpoints.topics.consumer;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 04.05.17.
 */

public interface ITopicsComponent {

    @NonNull
    TopicsRepositoryModule repository();
}
