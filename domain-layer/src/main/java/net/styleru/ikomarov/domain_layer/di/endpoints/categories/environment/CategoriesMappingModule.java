package net.styleru.ikomarov.domain_layer.di.endpoints.categories.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.entities.local.categories.CategoryLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.categories.CategoryRemoteEntity;
import net.styleru.ikomarov.data_layer.mappings.remote_to_local.categories.RemoteToLocalCategoriesEntityMapping;
import net.styleru.ikomarov.data_layer.mappings.remote_to_local.categories.RemoteToLocalCategoryEntityMapping;

import java.util.List;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 04.05.17.
 */

public class CategoriesMappingModule {

    @NonNull
    private final Object singleLock = new Object();

    @NonNull
    private final Object listLock = new Object();

    @Nullable
    private volatile Function<CategoryRemoteEntity, CategoryLocalEntity> singleMapping;

    @Nullable
    private volatile Function<List<CategoryRemoteEntity>, List<CategoryLocalEntity>> listMapping;

    public CategoriesMappingModule() {

    }

    @NonNull
    public Function<CategoryRemoteEntity, CategoryLocalEntity> provideSingleMapping() {
        Function<CategoryRemoteEntity, CategoryLocalEntity> localInstance = singleMapping;
        if(localInstance == null) {
            synchronized (singleLock) {
                localInstance = singleMapping;
                if(localInstance == null) {
                    localInstance = singleMapping = new RemoteToLocalCategoryEntityMapping();
                }
            }
        }

        return localInstance;
    }

    @NonNull
    public Function<List<CategoryRemoteEntity>, List<CategoryLocalEntity>> provideListMapping() {
        Function<List<CategoryRemoteEntity>, List<CategoryLocalEntity>> localInstance = listMapping;
        if(localInstance == null) {
            synchronized (listLock) {
                localInstance = listMapping;
                if(localInstance == null) {
                    localInstance = listMapping = new RemoteToLocalCategoriesEntityMapping(provideSingleMapping());
                }
            }
        }

        return localInstance;
    }
}
