package net.styleru.ikomarov.domain_layer.di.level_3.session;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.authentication.AuthenticationService;
import net.styleru.ikomarov.data_layer.services.authentication.IAuthenticationService;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;

/**
 * Created by i_komarov on 04.06.17.
 */

public class AuthenticationModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent component;

    @Nullable
    private volatile IAuthenticationService service;

    public AuthenticationModule(@NonNull IEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public IAuthenticationService provideAuthenticationService() {
        IAuthenticationService localInstance = service;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = service;
                if(localInstance == null) {
                    localInstance = service = AuthenticationService.Factory.create(
                            component.sessionlessClients().provideRawClient()
                    );
                }
            }
        }

        return localInstance;
    }
}
