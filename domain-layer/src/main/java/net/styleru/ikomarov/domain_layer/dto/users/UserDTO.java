package net.styleru.ikomarov.domain_layer.dto.users;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.dto.base.AbstractDTO;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UserDTO extends AbstractDTO {

    @NonNull
    private final String firstName;

    @NonNull
    private final String lastName;

    @Nullable
    private final String imageUrl;

    @NonNull
    private final String role;

    public UserDTO(@NonNull String id,
                   @NonNull String firstName,
                   @NonNull String lastName,
                   @Nullable String imageUrl,
                   @NonNull String role) {

        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.role = role;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public String getRole() {
        return role;
    }
}
