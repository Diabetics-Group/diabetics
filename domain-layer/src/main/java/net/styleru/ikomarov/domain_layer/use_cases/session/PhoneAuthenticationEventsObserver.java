package net.styleru.ikomarov.domain_layer.use_cases.session;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

/**
 * Created by i_komarov on 04.06.17.
 */

public class PhoneAuthenticationEventsObserver extends UseCase.BaseSingleObserver<String> {

    @NonNull
    private final PhoneAuthenticationUseCase.Callbacks callbacks;

    public PhoneAuthenticationEventsObserver(@NonNull PhoneAuthenticationUseCase.Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onStart() {
        if(!isDisposed()) {
            callbacks.onPhoneAuthenticationStarted();
        }
    }

    @Override
    public void onSuccess(String value) {
        super.onSuccess(value);
        if(!isDisposed()) {
            callbacks.onPhoneAuthenticationSuccess(value);
        }
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        if(!isDisposed()) {
            callbacks.onPhoneAuthenticationFailure(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
        }
    }

    @Override
    public void onComplete() {
        if(!isDisposed()) {
            callbacks.onPhoneAuthenticationFinished();
        }
    }
}
