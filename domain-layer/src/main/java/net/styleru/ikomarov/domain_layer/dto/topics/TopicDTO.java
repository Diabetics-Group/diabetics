package net.styleru.ikomarov.domain_layer.dto.topics;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.entities.local.categories.CategoryLocalEntity;

import java.util.UUID;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicDTO {

    @NonNull
    private String id;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String imageUrl;

    @Nullable
    private String role;

    @NonNull
    private String title;

    @NonNull
    private String content;

    @Nullable
    private String lastUpdate;

    @Nullable
    private Long commentsCount;

    @NonNull
    private Boolean isSynchronized;

    private TopicDTO() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    @Nullable
    public String getFirstName() {
        return firstName;
    }

    @Nullable
    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    @Nullable
    public String getRole() {
        return role;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    @Nullable
    public String getLastUpdate() {
        return lastUpdate;
    }

    @Nullable
    public Long getCommentsCount() {
        return commentsCount;
    }

    @NonNull
    public Boolean getSynchronized() {
        return isSynchronized;
    }

    public static final class Builder {

        private TopicDTO dto;

        public Builder(@NonNull String title,
                       @NonNull String content) {

            dto = new TopicDTO();
            dto.title = title;
            dto.content = content;
        }

        public String getTitle() {
            return dto.title;
        }

        public String getContent() {
            return dto.content;
        }

        public SynchronizedBuilder mutateSynchronized(String id) {
            dto.id = id;
            return new SynchronizedBuilder(dto);
        }

        public TopicDTO create(UUID id) {
            dto.id = id.toString();
            dto.isSynchronized = false;
            return dto;
        }
    }

    public static final class SynchronizedBuilder {

        private TopicDTO dto;

        private SynchronizedBuilder(TopicDTO dto) {
            this.dto = dto;
            this.dto.isSynchronized = false;
        }

        public SynchronizedBuilder withUser(String firstName, String lastName, String imageUrl, String role) {
            this.dto.firstName = firstName;
            this.dto.lastName = lastName;
            this.dto.imageUrl = imageUrl;
            this.dto.role = role;
            return this;
        }

        public SynchronizedBuilder withTimestamp(String lastUpdate) {
            this.dto.lastUpdate = lastUpdate;
            return this;
        }

        public SynchronizedBuilder withCommentsCount(Long commentsCount) {
            this.dto.commentsCount = commentsCount;
            return this;
        }

        public TopicDTO create() {
            return this.dto;
        }
    }
}
