package net.styleru.ikomarov.domain_layer.mapping.comments;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.local.comments.CommentLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.comments.CommentDTO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 06.05.17.
 */

public class LocalCommentsMapper implements Function<List<CommentLocalEntity>, List<CommentDTO>> {

    @NonNull
    private final Function<CommentLocalEntity, CommentDTO> singleMapper;

    public LocalCommentsMapper(@NonNull Function<CommentLocalEntity, CommentDTO> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<CommentDTO> apply(List<CommentLocalEntity> commentLocalEntities) throws Exception {
        return Observable.fromIterable(commentLocalEntities)
                .map(singleMapper)
                .reduce(new ArrayList<CommentDTO>(), (list, element) -> {
                    list.add(element);
                    return list;
                }).blockingGet();
    }
}
