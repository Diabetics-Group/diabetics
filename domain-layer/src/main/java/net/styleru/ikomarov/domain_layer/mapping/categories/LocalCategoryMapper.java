package net.styleru.ikomarov.domain_layer.mapping.categories;

import net.styleru.ikomarov.data_layer.entities.local.categories.CategoryLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.categories.CategoryDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 02.05.17.
 */

public class LocalCategoryMapper implements Function<CategoryLocalEntity, CategoryDTO> {

    @Override
    public CategoryDTO apply(CategoryLocalEntity entity) throws Exception {
        return new CategoryDTO(
                entity.getId(),
                entity.getTitle(),
                entity.getLastUpdate(),
                entity.getTopicsCount(),
                entity.isSynchronized()
        );
    }
}
