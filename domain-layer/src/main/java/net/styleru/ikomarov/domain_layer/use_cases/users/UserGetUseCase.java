package net.styleru.ikomarov.domain_layer.use_cases.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;
import net.styleru.ikomarov.domain_layer.repository.users.IUsersRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UserGetUseCase extends UseCase {

    @NonNull
    private final IUsersRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public UserGetUseCase(@NonNull IUsersRepository repository,
                          @NonNull Scheduler executeScheduler,
                          @NonNull Scheduler postScheduler) {

        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<UserDTO, T> mapper, Callbacks<T> callbacks, String id) {
        execute(repository.get(id), mapper, callbacks);
    }

    private <T> void execute(Observable<UserDTO> upstream, Function<UserDTO, T> mapper, Callbacks<T> callbacks) {
        upstream.subscribeOn(executeScheduler)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createUserLoadEventObserver(callbacks));
    }

    private <T> DisposableObserver<T> createUserLoadEventObserver(Callbacks<T> callbacks) {
        DisposableObserver<T> userLoadEventObserver = new UserLoadEventObserver<T>(callbacks);
        addDisposable(userLoadEventObserver);
        return userLoadEventObserver;
    }

    public interface Callbacks<T> {

        void onUserLoaded(@NonNull T user);

        void onUserLoadingFailed(@NonNull ExceptionBundle error);
    }
}
