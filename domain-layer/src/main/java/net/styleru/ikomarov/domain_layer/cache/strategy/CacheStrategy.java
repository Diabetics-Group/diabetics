package net.styleru.ikomarov.domain_layer.cache.strategy;

import android.net.ConnectivityManager;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.managers.network.INetworkManager;
import net.styleru.ikomarov.domain_layer.contracts.cache.CachePolicy;

/**
 * Created by i_komarov on 30.04.17.
 */

public class CacheStrategy implements ICacheStrategy {

    @NonNull
    private final INetworkManager networkManager;

    public CacheStrategy(@NonNull INetworkManager networkManager) {
        this.networkManager = networkManager;
    }

    @NonNull
    @Override
    public CachePolicy currentPolicy() {
        Integer info = networkManager.blockingGetNetworkInfo();
        if(info != null && (info == ConnectivityManager.TYPE_WIFI || info == ConnectivityManager.TYPE_MOBILE)) {
            //if the connection present, no matter if it is stable or unstable, we want to force updates flow through internet
            return CachePolicy.CHECK_UPDATES;
        } else {
            //if there is no active and free to use connection, we just go to cache and show previously saved data
            return CachePolicy.CACHE_ONLY;
        }
    }
}
