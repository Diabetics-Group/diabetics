package net.styleru.ikomarov.domain_layer.di.endpoints.comments.environment;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.comments.consumer.ICommentsComponent;

/**
 * Created by i_komarov on 08.05.17.
 */

public interface ICommentsEnvironmentComponent {

    @NonNull
    ICommentsComponent plusCommentsComponent();

    @NonNull
    CommentsLocalModule local();

    @NonNull
    CommentsRemoteModule remote();

    @NonNull
    CommentsMappingModule mapping();
}
