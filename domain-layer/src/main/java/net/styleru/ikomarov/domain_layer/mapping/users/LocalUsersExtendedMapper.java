package net.styleru.ikomarov.domain_layer.mapping.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.users.UserExtendedDTO;
import net.styleru.ikomarov.domain_layer.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 05.06.17.
 */

public class LocalUsersExtendedMapper implements Function<List<UserLocalEntity>, List<UserExtendedDTO>> {

    @NonNull
    private final Function<UserLocalEntity, UserExtendedDTO> singleMapper;

    public LocalUsersExtendedMapper(@NonNull Function<UserLocalEntity, UserExtendedDTO> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<UserExtendedDTO> apply(List<UserLocalEntity> userLocalEntities) throws Exception {
        return Observable.fromIterable(userLocalEntities)
                .map(singleMapper)
                .compose(RxUtils.accumulate())
                .blockingFirst();
    }
}
