package net.styleru.ikomarov.domain_layer.di.endpoints.users.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.users.IUsersLocalService;
import net.styleru.ikomarov.data_layer.services.users.UsersLocalService;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;

/**
 * Created by i_komarov on 29.05.17.
 */

public class UsersLocalModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent component;

    @Nullable
    private volatile IUsersLocalService service;

    public UsersLocalModule(@NonNull IEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public IUsersLocalService provideLocalService() {
        IUsersLocalService localInstance = service;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = service;
                if(localInstance == null) {
                    localInstance = service = new UsersLocalService.Factory(component.db().provideDatabase()).create();
                }
            }
        }

        return localInstance;
    }
}
