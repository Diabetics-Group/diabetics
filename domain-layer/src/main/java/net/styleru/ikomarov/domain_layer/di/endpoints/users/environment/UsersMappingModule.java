package net.styleru.ikomarov.domain_layer.di.endpoints.users.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.entities.local.topics.TopicLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.topics.TopicRemoteEntity;
import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;
import net.styleru.ikomarov.data_layer.mappings.remote_to_local.topics.RemoteToLocalTopicEntityMapping;
import net.styleru.ikomarov.data_layer.mappings.remote_to_local.topics.RemoteToLocalTopicsEntityMapping;
import net.styleru.ikomarov.data_layer.mappings.remote_to_local.users.RemoteToLocalUserEntityMapping;
import net.styleru.ikomarov.data_layer.mappings.remote_to_local.users.RemoteToLocalUsersEntityMapping;

import java.util.List;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 29.05.17.
 */

public class UsersMappingModule {

    @NonNull
    private final Object singleLock = new Object();

    @NonNull
    private final Object listLock = new Object();

    @Nullable
    private volatile Function<UserRemoteEntity, UserLocalEntity> singleMapping;

    @Nullable
    private volatile Function<List<UserRemoteEntity>, List<UserLocalEntity>> listMapping;

    public UsersMappingModule() {

    }

    @NonNull
    public Function<UserRemoteEntity, UserLocalEntity> provideSingleMapping() {
        Function<UserRemoteEntity, UserLocalEntity> localInstance = singleMapping;
        if(localInstance == null) {
            synchronized (singleLock) {
                localInstance = singleMapping;
                if(localInstance == null) {
                    localInstance = singleMapping = new RemoteToLocalUserEntityMapping();
                }
            }
        }

        return localInstance;
    }

    @NonNull
    public Function<List<UserRemoteEntity>, List<UserLocalEntity>> provideListMapping() {
        Function<List<UserRemoteEntity>, List<UserLocalEntity>> localInstance = listMapping;
        if(localInstance == null) {
            synchronized (listLock) {
                localInstance = listMapping;
                if(localInstance == null) {
                    localInstance = listMapping = new RemoteToLocalUsersEntityMapping(provideSingleMapping());
                }
            }
        }

        return localInstance;
    }
}
