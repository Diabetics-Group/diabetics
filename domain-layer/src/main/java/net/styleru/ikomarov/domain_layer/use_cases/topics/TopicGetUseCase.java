package net.styleru.ikomarov.domain_layer.use_cases.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;
import net.styleru.ikomarov.domain_layer.repository.topics.ITopicsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 05.05.17.
 */

public class TopicGetUseCase extends UseCase {

    @NonNull
    private final ITopicsRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public TopicGetUseCase(@NonNull ITopicsRepository repository,
                            @NonNull Scheduler executeScheduler,
                            @NonNull Scheduler postScheduler) {

        super();
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<TopicDTO, T> mapper, Callbacks<T> callbacks, String id) {
        execute(repository.get(id), mapper, callbacks);
    }

    protected final <T> void execute(Observable<TopicDTO> source, Function<TopicDTO, T> mapper, Callbacks<T> callbacks) {
        source.subscribeOn(executeScheduler)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createTopicLoadEventObserver(callbacks));
    }

    private <T> DisposableObserver<T> createTopicLoadEventObserver(final Callbacks<T> callbacks) {
        DisposableObserver<T> topicLoadEventObserver = new TopicLoadEventObserver<>(callbacks);
        addDisposable(topicLoadEventObserver);
        return topicLoadEventObserver;
    }

    public interface Callbacks<T> {

        void onTopicLoaded(T item);

        void onTopicLoadingFailed(ExceptionBundle error);
    }
}
