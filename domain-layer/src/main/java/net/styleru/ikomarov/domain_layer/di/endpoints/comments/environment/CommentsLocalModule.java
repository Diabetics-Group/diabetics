package net.styleru.ikomarov.domain_layer.di.endpoints.comments.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.comments.CommentsLocalService;
import net.styleru.ikomarov.data_layer.services.comments.ICommentsLocalService;
import net.styleru.ikomarov.data_layer.services.comments.ICommentsRemoteService;
import net.styleru.ikomarov.data_layer.services.topics.ITopicsLocalService;
import net.styleru.ikomarov.data_layer.services.topics.TopicsLocalService;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsLocalModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent component;

    @Nullable
    private volatile ICommentsLocalService service;

    public CommentsLocalModule(@NonNull IEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public ICommentsLocalService provideLocalService() {
        ICommentsLocalService localInstance = service;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = service;
                if(localInstance == null) {
                    localInstance = service = new CommentsLocalService.Factory(component.db().provideDatabase()).create();
                }
            }
        }

        return localInstance;
    }
}
