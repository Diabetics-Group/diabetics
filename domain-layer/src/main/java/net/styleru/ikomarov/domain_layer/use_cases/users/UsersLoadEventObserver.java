package net.styleru.ikomarov.domain_layer.use_cases.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UsersLoadEventObserver<T> extends UseCase.BaseObserver<List<T>> {

    @NonNull
    private final UsersGetUseCase.Callbacks<T> callbacks;

    public UsersLoadEventObserver(@NonNull UsersGetUseCase.Callbacks<T> callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onNext(List<T> value) {
        if(!isDisposed()) {
            callbacks.onUserLoaded(value);
        }
    }

    @Override
    public void onError(Throwable e) {
        if(!isDisposed()) {
            callbacks.onUserLoadingFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
        }
    }

    @Override
    public void onComplete() {

    }
}
