package net.styleru.ikomarov.domain_layer.dto.base;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 02.05.17.
 */

public class AbstractDTO {

    @NonNull
    private String id;

    public AbstractDTO(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public String getId() {
        return id;
    }
}
