package net.styleru.ikomarov.domain_layer.use_cases.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

/**
 * Created by i_komarov on 05.05.17.
 */

public class TopicLoadEventObserver<T> extends UseCase.BaseObserver<T> {

    @NonNull
    private final TopicGetUseCase.Callbacks<T> callbacks;

    public TopicLoadEventObserver(@NonNull TopicGetUseCase.Callbacks<T> callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onNext(T value) {
        if(!isDisposed()) {
            callbacks.onTopicLoaded(value);
        }
    }

    @Override
    public void onError(Throwable e) {
        if(!isDisposed()) {
            callbacks.onTopicLoadingFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
        }
    }

    @Override
    public void onComplete() {

    }
}
