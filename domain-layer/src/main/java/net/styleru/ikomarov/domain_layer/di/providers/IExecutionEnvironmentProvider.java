package net.styleru.ikomarov.domain_layer.di.providers;

import io.reactivex.Scheduler;

/**
 * Created by i_komarov on 04.05.17.
 */

public interface IExecutionEnvironmentProvider {

    Scheduler provideExecuteScheduler();

    Scheduler providePostScheduler();
}
