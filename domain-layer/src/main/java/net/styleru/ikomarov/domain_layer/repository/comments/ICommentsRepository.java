package net.styleru.ikomarov.domain_layer.repository.comments;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.dto.comments.CommentDTO;
import net.styleru.ikomarov.domain_layer.repository.base.CreateResult;
import net.styleru.ikomarov.domain_layer.repository.base.IRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by i_komarov on 05.05.17.
 */

public interface ICommentsRepository extends IRepository {

    Single<CreateResult<CommentDTO>> create(@NonNull String userID, @NonNull String topicId, @NonNull CommentDTO.Builder builder);

    Observable<List<CommentDTO>> list(@NonNull String topicId, int offset, int limit);
}
