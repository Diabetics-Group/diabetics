package net.styleru.ikomarov.domain_layer.di.endpoints.users.environment;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.users.consumer.IUsersComponent;

/**
 * Created by i_komarov on 29.05.17.
 */

public interface IUsersEnvironmentComponent {

    @NonNull
    IUsersComponent plusUsersComponent();

    @NonNull
    UsersRemoteModule remote();

    @NonNull
    UsersLocalModule local();

    @NonNull
    UsersMappingModule mapping();
}
