package net.styleru.ikomarov.domain_layer.validation.base;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 04.06.17.
 */

public interface IValidationStrategy<T> {

    Boolean apply(T object) throws ExceptionBundle;
}
