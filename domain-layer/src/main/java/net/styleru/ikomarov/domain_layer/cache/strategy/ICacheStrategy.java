package net.styleru.ikomarov.domain_layer.cache.strategy;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.contracts.cache.CachePolicy;

/**
 * Created by i_komarov on 30.04.17.
 */

public interface ICacheStrategy {

    @NonNull
    CachePolicy currentPolicy();
}
