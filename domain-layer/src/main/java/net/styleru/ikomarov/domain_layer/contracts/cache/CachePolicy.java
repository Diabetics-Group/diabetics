package net.styleru.ikomarov.domain_layer.contracts.cache;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;

/**
 * Created by i_komarov on 30.04.17.
 */

public enum CachePolicy implements Parcelable {
    CACHE_ONLY    (0x0000000),
    CHECK_UPDATES (0x0000001),
    FORCE_NETWORK (0x0000002);

    private static final SparseArray<CachePolicy> policiesMap = new SparseArray<>(CachePolicy.values().length);

    static {
        for(CachePolicy policy : CachePolicy.values()) {
            policiesMap.put(policy.code, policy);
        }
    }

    private final int code;

    CachePolicy(int code) {
        this.code = code;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CachePolicy> CREATOR = new Creator<CachePolicy>() {
        @Override
        public CachePolicy createFromParcel(Parcel in) {
            return policiesMap.get(in.readInt());
        }

        @Override
        public CachePolicy[] newArray(int size) {
            return new CachePolicy[size];
        }
    };
}
