package net.styleru.ikomarov.domain_layer.contracts.jobs;

/**
 * Created by i_komarov on 11.05.17.
 */

public enum JobResourceType {
    CATEGORIES,
    TOPICS,
    COMMENTS;
}
