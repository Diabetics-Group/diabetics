package net.styleru.ikomarov.domain_layer.di.level_4.consumer;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_3.session.*;

/**
 * Created by i_komarov on 03.05.17.
 */

public class SessionClientComponent implements ISessionClientComponent {

    @NonNull
    private final SessionClientModule session;

    public SessionClientComponent(@NonNull ISessionComponent component) {
        this.session = new SessionClientModule(component);
    }

    @NonNull
    @Override
    public SessionClientModule session() {
        return session;
    }
}
