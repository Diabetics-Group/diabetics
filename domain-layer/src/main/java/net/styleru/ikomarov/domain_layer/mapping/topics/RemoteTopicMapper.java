package net.styleru.ikomarov.domain_layer.mapping.topics;

import net.styleru.ikomarov.data_layer.entities.remote.topics.TopicRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 04.05.17.
 */

public class RemoteTopicMapper implements Function<TopicRemoteEntity, TopicDTO> {

    @Override
    public TopicDTO apply(TopicRemoteEntity entity) throws Exception {
        return new TopicDTO.Builder(entity.getTitle(), entity.getContent())
                .mutateSynchronized(entity.getId())
                .withCommentsCount(entity.getCommentsCount())
                .withTimestamp(entity.getLastUpdate())
                .withUser(entity.getFirstName(), entity.getLastName(), entity.getImageUrl(), entity.getRole())
                .create();
    }
}
