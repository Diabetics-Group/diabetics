package net.styleru.ikomarov.domain_layer.mapping.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.local.topics.TopicLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 04.05.17.
 */

public class LocalTopicsMapper implements Function<List<TopicLocalEntity>, List<TopicDTO>> {

    @NonNull
    private final Function<TopicLocalEntity, TopicDTO> singleMapper;

    public LocalTopicsMapper(@NonNull Function<TopicLocalEntity, TopicDTO> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<TopicDTO> apply(List<TopicLocalEntity> topicLocalEntities) throws Exception {
        return Observable.fromIterable(topicLocalEntities)
                .map(singleMapper)
                .reduce(new ArrayList<TopicDTO>(), (list, element) -> {
                    list.add(element);
                    return list;
                })
                .blockingGet();
    }
}
