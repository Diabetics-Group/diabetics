package net.styleru.ikomarov.domain_layer.di.level_2;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.database.DatabaseService;
import net.styleru.ikomarov.data_layer.services.database.IDatabaseService;
import net.styleru.ikomarov.data_layer.database.factory.SQLiteFactory;
import net.styleru.ikomarov.data_layer.database.helper.DBOpenHelper;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 03.05.17.
 */

public class DatabaseModule {

    private final Object lockDB = new Object();

    private final Object lockControl = new Object();

    @NonNull
    private final DBOpenHelper helper;

    @NonNull
    private final SQLiteFactory factory;

    @Nullable
    private volatile SQLiteDatabase database;

    @NonNull
    private volatile IDatabaseService controller;

    public DatabaseModule(@NonNull IAppComponent component) {
        this.helper = new DBOpenHelper(component.app().provideContext());
        this.factory = new SQLiteFactory(helper);
    }

    @NonNull
    public SQLiteDatabase provideDatabase() {
        SQLiteDatabase localInstance = database;
        if(localInstance == null) {
            synchronized (lockDB) {
                localInstance = database;
                if(localInstance == null) {
                    localInstance = database = factory.create();
                }
            }
        }

        return localInstance;
    }

    @NonNull
    public IDatabaseService provideDatabaseService() {
        IDatabaseService localInstance = controller;
        if(localInstance == null) {
            synchronized (lockControl) {
                localInstance = controller;
                if(localInstance == null) {
                    localInstance = controller = new DatabaseService(helper);
                }
            }
        }

        return localInstance;
    }
}
