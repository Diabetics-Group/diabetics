package net.styleru.ikomarov.domain_layer.repository.comments;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.local.comments.CommentLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.comments.CommentRemoteEntity;
import net.styleru.ikomarov.data_layer.parsing.base.IParser;
import net.styleru.ikomarov.data_layer.services.comments.ICommentsLocalService;
import net.styleru.ikomarov.data_layer.services.comments.ICommentsRemoteService;

import net.styleru.ikomarov.domain_layer.cache.strategy.ICacheStrategy;
import net.styleru.ikomarov.domain_layer.contracts.cache.CachePolicy;
import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;
import net.styleru.ikomarov.domain_layer.dto.comments.CommentDTO;
import net.styleru.ikomarov.domain_layer.mapping.comments.LocalCommentMapper;
import net.styleru.ikomarov.domain_layer.mapping.comments.LocalCommentsMapper;
import net.styleru.ikomarov.domain_layer.mapping.comments.RemoteCommentMapper;
import net.styleru.ikomarov.domain_layer.mapping.comments.RemoteCommentsMapper;
import net.styleru.ikomarov.domain_layer.repository.base.CreateResult;
import net.styleru.ikomarov.domain_layer.utils.RepositoryUtils;

import java.net.HttpURLConnection;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.functions.Function;

import okhttp3.ResponseBody;

import retrofit2.Response;

/**
 * Created by i_komarov on 06.05.17.
 */

public class CommentsRepository implements ICommentsRepository {

    @NonNull
    private final IParser<ResponseBody, String> jsonParser;

    @NonNull
    private final ICacheStrategy strategy;

    @NonNull
    private final ICommentsRemoteService remoteService;

    @NonNull
    private final ICommentsLocalService localService;

    @NonNull
    private final Function<List<CommentRemoteEntity>, List<CommentLocalEntity>> mapping;

    @NonNull
    private final Function<CommentLocalEntity, CommentDTO> localSingleMapper;

    @NonNull
    private final Function<List<CommentLocalEntity>, List<CommentDTO>> localListMapper;

    @NonNull
    private final Function<CommentRemoteEntity, CommentDTO> remoteSingleMapper;

    @NonNull
    private final Function<List<CommentRemoteEntity>, List<CommentDTO>> remoteListMapper;

    private CommentsRepository(@NonNull IParser<ResponseBody, String> jsonParser,
                               @NonNull ICacheStrategy strategy,
                               @NonNull ICommentsRemoteService remoteService,
                               @NonNull ICommentsLocalService localService,
                               @NonNull Function<List<CommentRemoteEntity>, List<CommentLocalEntity>> mapping) {

        this.jsonParser = jsonParser;
        this.strategy = strategy;
        this.remoteService = remoteService;
        this.localService = localService;
        this.mapping = mapping;
        localSingleMapper = new LocalCommentMapper();
        localListMapper = new LocalCommentsMapper(localSingleMapper);
        remoteSingleMapper = new RemoteCommentMapper();
        remoteListMapper = new RemoteCommentsMapper(remoteSingleMapper);
    }

    @Override
    public Single<CreateResult<CommentDTO>> create(@NonNull String userId, @NonNull String topicId, @NonNull CommentDTO.Builder builder) {
        final CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            return Single.just(new CreateResult<>(OperationStatus.DELAYED, null));
        }

        return remoteService.create(userId, topicId, builder.getContent())
                .compose(withSingleCache(HttpURLConnection.HTTP_CREATED))
                .map(dto -> new CreateResult<>(OperationStatus.SUCCESS, dto));
    }

    @Override
    public Observable<List<CommentDTO>> list(@NonNull String topicId, int offset, int limit) {
        final CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            return listFromDatabase(CachePolicy.CACHE_ONLY, topicId, offset, limit).toObservable();
        } else if(policy == CachePolicy.FORCE_NETWORK) {
            return listFromNetwork(policy, topicId, offset, limit).toObservable();
        } else {
            return listFromDatabase(policy, topicId, offset, limit).toObservable()
                    .compose(RepositoryUtils.ignoreLocalDatabaseEmptyError())
                    .compose(offset == 0 ?
                            withFetchFromNetwork(policy, topicId, offset, limit) :
                            withOptionalPostload(policy, topicId, offset, limit)
            );
        }
    }

    private ObservableTransformer<List<CommentDTO>, List<CommentDTO>> withFetchFromNetwork(CachePolicy policy, String topicId, int offset, int limit) {
        return o -> o.flatMap(data -> data.size() == 0 ?
                listFromNetwork(policy, topicId, offset, limit).toObservable() :
                Observable.merge(
                        Observable.just(data),
                        listFromNetwork(policy, topicId, data.get(0).getDate())
                                .toObservable()
                                .onErrorResumeNext(Observable.never())
                )
        );
    }

    private ObservableTransformer<List<CommentDTO>, List<CommentDTO>> withOptionalPostload(CachePolicy policy, String topicId, int offset, int limit) {
        return o -> o.flatMap(data -> {
            final int delta = limit - data.size();
            return delta == 0 ? Observable.just(data) :
                    Observable.merge(
                            Observable.just(data),
                            listFromNetwork(policy, topicId, offset + data.size(), delta)
                                    .toObservable()
                    );
        });
    }

    private Single<List<CommentDTO>> listFromDatabase(@NonNull CachePolicy policy, @NonNull String topicId, int offset, int limit) {
        return localService.list(topicId, offset, limit)
                .map(localListMapper)
                .flatMap(RepositoryUtils.withCheckOnLocalDatabaseEmpty(policy));
    }

    private Single<List<CommentDTO>> listFromNetwork(@NonNull CachePolicy policy, @NonNull String topicId, @NonNull String lastModified) {
        return remoteService.list(lastModified, topicId)
                .compose(withListCache(HttpURLConnection.HTTP_OK))
                .flatMap(RepositoryUtils.withCheckOnRemoteDatabaseEmpty(policy));
    }

    private Single<List<CommentDTO>> listFromNetwork(@NonNull CachePolicy policy, @NonNull String topicId, int offset, int limit) {
        return remoteService.list(topicId, offset, limit)
                .compose(withListCache(HttpURLConnection.HTTP_OK))
                .flatMap(RepositoryUtils.withCheckOnRemoteDatabaseEmpty(policy));
    }

    private SingleTransformer<Response<ResponseContainer<CommentRemoteEntity>>, CommentDTO> withSingleCache(Integer... successCodes) {
        return (o) -> o.flatMap(RepositoryUtils.verifyResponse(jsonParser, HttpURLConnection.HTTP_OK))
                .map(ResponseContainer::getBody)
                .doOnSuccess(this::cacheSingle)
                .map(remoteSingleMapper);
    }

    private SingleTransformer<Response<ResponseContainer<List<CommentRemoteEntity>>>, List<CommentDTO>> withListCache(Integer... successCodes) {
        return (o) -> o.flatMap(RepositoryUtils.verifyResponse(jsonParser, HttpURLConnection.HTTP_OK))
                .map(ResponseContainer::getBody)
                .doOnSuccess(this::cache)
                .map(remoteListMapper);
    }

    private void cacheSingle(@NonNull CommentRemoteEntity data) {
        cache(Collections.singletonList(data));
    }

    private void cache(@NonNull List<CommentRemoteEntity> data) {
        Single.just(data)
                .map(mapping)
                .flatMap(localService::create)
                .subscribe();
    }

    private void invalidateSingle(@NonNull CommentRemoteEntity data) {
        invalidate(Collections.singletonList(data));
    }

    private void invalidate(@NonNull List<CommentRemoteEntity> data) {
        Observable.fromIterable(data)
                .map(CommentRemoteEntity::getId)
                .toList()
                .flatMap(localService::delete)
                .subscribe();
    }

    public static final class Factory {

        @NonNull
        private final IParser<ResponseBody, String> jsonParser;

        @NonNull
        private final ICacheStrategy strategy;

        @NonNull
        private final ICommentsRemoteService remoteService;

        @NonNull
        private final ICommentsLocalService localService;

        @NonNull
        private final Function<List<CommentRemoteEntity>, List<CommentLocalEntity>> mapping;

        public Factory(@NonNull IParser<ResponseBody, String> jsonParser,
                       @NonNull ICacheStrategy strategy,
                       @NonNull ICommentsRemoteService remoteService,
                       @NonNull ICommentsLocalService localService,
                       @NonNull Function<List<CommentRemoteEntity>, List<CommentLocalEntity>> mapping) {

            this.jsonParser = jsonParser;
            this.strategy = strategy;
            this.remoteService = remoteService;
            this.localService = localService;
            this.mapping = mapping;
        }

        public ICommentsRepository create() {
            return new CommentsRepository(jsonParser, strategy, remoteService, localService, mapping);
        }
    }
}
