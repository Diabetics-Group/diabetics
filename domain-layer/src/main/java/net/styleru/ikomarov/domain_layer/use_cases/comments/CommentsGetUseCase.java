package net.styleru.ikomarov.domain_layer.use_cases.comments;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.comments.CommentDTO;
import net.styleru.ikomarov.domain_layer.repository.comments.ICommentsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsGetUseCase extends UseCase {

    @NonNull
    private final ICommentsRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public CommentsGetUseCase(@NonNull ICommentsRepository repository,
                              @NonNull Scheduler executeScheduler,
                              @NonNull Scheduler postScheduler) {

        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(@NonNull Function<List<CommentDTO>, List<T>> mapper, @NonNull Callbacks<T> callbacks, @NonNull String topicId, int offset, int limit) {
        execute(repository.list(topicId, offset, limit), mapper, callbacks);
    }

    private <T> void execute(Observable<List<CommentDTO>> upstream, Function<List<CommentDTO>, List<T>> mapper, Callbacks<T> callbacks) {
        upstream.map(mapper)
                .subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .subscribe(createCommentsLoadEventObserver(callbacks));
    }

    private <T> DisposableObserver<List<T>> createCommentsLoadEventObserver(Callbacks<T> callbacks) {
        DisposableObserver<List<T>> commentsLoadEventObserver = new CommentsLoadEventObserver<T>(callbacks);
        addDisposable(commentsLoadEventObserver);
        return commentsLoadEventObserver;
    }

    public interface Callbacks<T> {

        void onCommentsLoaded(List<T> items);

        void onCommentsLoadingFailed(ExceptionBundle error);
    }
}
