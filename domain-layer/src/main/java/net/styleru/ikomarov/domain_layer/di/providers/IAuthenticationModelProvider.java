package net.styleru.ikomarov.domain_layer.di.providers;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.repository.authentication.IAuthenticationRepository;

/**
 * Created by i_komarov on 05.06.17.
 */

public interface IAuthenticationModelProvider {

    @NonNull
    IAuthenticationRepository provideAuthenticationRepository();
}
