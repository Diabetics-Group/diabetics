package net.styleru.ikomarov.domain_layer.dto.categories;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.dto.base.AbstractDTO;

/**
 * Created by i_komarov on 02.05.17.
 */

public class CategoryDTO extends AbstractDTO {

    @NonNull
    private final String title;

    @NonNull
    private final String lastUpdate;

    @NonNull
    private final Long topicsCount;

    @NonNull
    private final Boolean isSynchronized;

    public CategoryDTO(@NonNull String id, @NonNull String title, @NonNull String lastUpdate, @NonNull Long topicsCount, @NonNull Boolean isSynchronized) {
        super(id);
        this.title = title;
        this.lastUpdate = lastUpdate;
        this.topicsCount = topicsCount;
        this.isSynchronized = isSynchronized;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getLastUpdate() {
        return lastUpdate;
    }

    @NonNull
    public Long getTopicsCount() {
        return topicsCount;
    }

    @NonNull
    public Boolean isSynchronized() {
        return isSynchronized;
    }
}
