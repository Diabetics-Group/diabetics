package net.styleru.ikomarov.domain_layer.mapping.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.users.UserExtendedDTO;
import net.styleru.ikomarov.domain_layer.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 05.06.17.
 */

public class RemoteUsersExtendedMapper implements Function<List<UserRemoteEntity>, List<UserExtendedDTO>> {

    @NonNull
    private final Function<UserRemoteEntity, UserExtendedDTO> singleMapper;

    public RemoteUsersExtendedMapper(@NonNull Function<UserRemoteEntity, UserExtendedDTO> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<UserExtendedDTO> apply(List<UserRemoteEntity> userRemoteEntities) throws Exception {
        return Observable.fromIterable(userRemoteEntities)
                .map(singleMapper)
                .compose(RxUtils.accumulate())
                .blockingFirst();
    }
}
