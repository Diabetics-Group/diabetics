package net.styleru.ikomarov.domain_layer.mapping.users;

import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 15.05.17.
 */

public class LocalUserMapper implements Function<UserLocalEntity, UserDTO> {

    @Override
    public UserDTO apply(UserLocalEntity entity) throws Exception {
        return new UserDTO(
                entity.getId(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getImageUrl(),
                entity.getRole()
        );
    }
}
