package net.styleru.ikomarov.domain_layer.di.endpoints.topics.environment;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.topics.consumer.ITopicsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.topics.consumer.TopicsComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.consumer.ISessionClientComponent;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsEnvironmentComponent implements ITopicsEnvironmentComponent {

    @NonNull
    private final IEnvironmentComponent environment;

    @NonNull
    private final ISessionClientComponent clients;

    @NonNull
    private final ICacheComponent cache;

    @NonNull
    private final TopicsLocalModule local;

    @NonNull
    private final TopicsRemoteModule remote;

    @NonNull
    private final TopicsMappingModule mapping;

    public TopicsEnvironmentComponent(@NonNull IEnvironmentComponent environment,
                                      @NonNull ICacheComponent cache,
                                      @NonNull ISessionClientComponent clients) {

        this.environment = environment;
        this.cache = cache;
        this.clients = clients;
        this.local = new TopicsLocalModule(environment);
        this.remote = new TopicsRemoteModule(clients);
        this.mapping = new TopicsMappingModule();
    }

    @NonNull
    @Override
    public ITopicsComponent plusTopicsComponent() {
        return new TopicsComponent(environment, cache, this);
    }

    @NonNull
    @Override
    public TopicsLocalModule local() {
        return local;
    }

    @NonNull
    @Override
    public TopicsRemoteModule remote() {
        return remote;
    }

    @NonNull
    @Override
    public TopicsMappingModule mapping() {
        return mapping;
    }
}
