package net.styleru.ikomarov.domain_layer.di.providers;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 05.06.17.
 */

public interface IGraphProvider {

    @NonNull
    IAppComponent provideGraphRoot();
}
