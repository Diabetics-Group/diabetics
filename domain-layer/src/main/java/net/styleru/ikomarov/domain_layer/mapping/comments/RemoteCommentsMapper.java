package net.styleru.ikomarov.domain_layer.mapping.comments;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.remote.comments.CommentRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.comments.CommentDTO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 06.05.17.
 */

public class RemoteCommentsMapper implements Function<List<CommentRemoteEntity>, List<CommentDTO>> {

    @NonNull
    private final Function<CommentRemoteEntity, CommentDTO> singleMapper;

    public RemoteCommentsMapper(@NonNull Function<CommentRemoteEntity, CommentDTO> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<CommentDTO> apply(List<CommentRemoteEntity> commentRemoteEntities) throws Exception {
        return Observable.fromIterable(commentRemoteEntities)
                .map(singleMapper)
                .reduce(new ArrayList<CommentDTO>(), (list, element) -> {
                    list.add(element);
                    return list;
                }).blockingGet();
    }
}
