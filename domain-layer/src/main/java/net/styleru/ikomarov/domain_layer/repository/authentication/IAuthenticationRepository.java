package net.styleru.ikomarov.domain_layer.repository.authentication;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.dto.users.UserExtendedDTO;

import io.reactivex.Single;

/**
 * Created by i_komarov on 04.06.17.
 */

public interface IAuthenticationRepository {

    Single<Boolean> code(@NonNull String phone);

    Single<UserExtendedDTO> token(@NonNull String phone, @NonNull String code);
}
