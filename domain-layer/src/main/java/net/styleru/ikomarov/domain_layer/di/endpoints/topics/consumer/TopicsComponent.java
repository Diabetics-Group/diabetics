package net.styleru.ikomarov.domain_layer.di.endpoints.topics.consumer;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.topics.environment.ITopicsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsComponent implements ITopicsComponent {

    @NonNull
    private final TopicsRepositoryModule repository;

    public TopicsComponent(@NonNull IEnvironmentComponent environment,
                           @NonNull ICacheComponent cache,
                           @NonNull ITopicsEnvironmentComponent topicsEnvironment) {

        this.repository = new TopicsRepositoryModule(environment, cache, topicsEnvironment);
    }

    @NonNull
    @Override
    public TopicsRepositoryModule repository() {
        return repository;
    }
}
