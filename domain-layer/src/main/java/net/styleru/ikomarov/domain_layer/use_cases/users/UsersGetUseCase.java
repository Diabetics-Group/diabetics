package net.styleru.ikomarov.domain_layer.use_cases.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;
import net.styleru.ikomarov.domain_layer.repository.users.IUsersRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UsersGetUseCase extends UseCase {

    @NonNull
    private final IUsersRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public UsersGetUseCase(@NonNull IUsersRepository repository,
                          @NonNull Scheduler executeScheduler,
                          @NonNull Scheduler postScheduler) {

        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<List<UserDTO>, List<T>> mapper, Callbacks<T> callbacks, int offset, int limit) {
        execute(repository.list(offset, limit), mapper, callbacks);
    }

    private <T> void execute(Observable<List<UserDTO>> upstream, Function<List<UserDTO>, List<T>> mapper, Callbacks<T> callbacks) {
        upstream.subscribeOn(executeScheduler)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createUserLoadEventObserver(callbacks));
    }

    private <T> DisposableObserver<List<T>> createUserLoadEventObserver(Callbacks<T> callbacks) {
        DisposableObserver<List<T>> userLoadEventObserver = new UsersLoadEventObserver<>(callbacks);
        addDisposable(userLoadEventObserver);
        return userLoadEventObserver;
    }

    public interface Callbacks<T> {

        void onUserLoaded(@NonNull List<T> users);

        void onUserLoadingFailed(@NonNull ExceptionBundle error);
    }
}
