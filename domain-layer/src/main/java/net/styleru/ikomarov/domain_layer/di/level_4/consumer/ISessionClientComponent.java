package net.styleru.ikomarov.domain_layer.di.level_4.consumer;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 03.05.17.
 */

public interface ISessionClientComponent {

    @NonNull
    SessionClientModule session();
}
