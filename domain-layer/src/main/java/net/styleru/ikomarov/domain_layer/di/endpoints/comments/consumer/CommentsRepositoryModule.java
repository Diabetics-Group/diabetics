package net.styleru.ikomarov.domain_layer.di.endpoints.comments.consumer;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.di.endpoints.comments.environment.ICommentsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.repository.comments.CommentsRepository;
import net.styleru.ikomarov.domain_layer.repository.comments.ICommentsRepository;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsRepositoryModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent environment;

    @NonNull
    private final ICacheComponent cache;

    @NonNull
    private final ICommentsEnvironmentComponent commentsEnvironment;

    @Nullable
    private volatile ICommentsRepository repository;

    public CommentsRepositoryModule(@NonNull IEnvironmentComponent environment,
                                    @NonNull ICacheComponent cache,
                                    @NonNull ICommentsEnvironmentComponent commentsEnvironment) {

        this.environment = environment;
        this.cache = cache;
        this.commentsEnvironment = commentsEnvironment;
    }

    @NonNull
    public ICommentsRepository provideRepository() {
        ICommentsRepository localInstance = repository;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = repository;
                if(localInstance == null) {
                    localInstance = repository = new CommentsRepository.Factory(
                            environment.parse().provideJsonParser(),
                            cache.strategy().provideCacheStrategy(),
                            commentsEnvironment.remote().provideRemoteService(),
                            commentsEnvironment.local().provideLocalService(),
                            commentsEnvironment.mapping().provideListMapping()
                    ).create();
                }
            }
        }

        return localInstance;
    }
}
