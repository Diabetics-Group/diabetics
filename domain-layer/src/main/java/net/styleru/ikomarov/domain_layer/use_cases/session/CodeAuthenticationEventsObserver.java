package net.styleru.ikomarov.domain_layer.use_cases.session;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

/**
 * Created by i_komarov on 04.06.17.
 */

public class CodeAuthenticationEventsObserver<T> extends UseCase.BaseSingleObserver<T> {

    @NonNull
    private final CodeAuthenticationUseCase.Callbacks<T> callbacks;

    public CodeAuthenticationEventsObserver(@NonNull CodeAuthenticationUseCase.Callbacks<T> callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onStart() {
        if(!isDisposed()) {
            callbacks.onStartCodeAuthentication();
        }
    }

    @Override
    public void onSuccess(T value) {
        super.onSuccess(value);
        if(!isDisposed()) {
            callbacks.onCodeAuthenticationSuccess(value);
        }
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        if(!isDisposed()) {
            callbacks.onCodeVerificationFailure(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
        }
    }

    @Override
    public void onComplete() {
        if(!isDisposed()) {
            callbacks.onFinishCodeAuthentication();
        }
    }
}
