package net.styleru.ikomarov.domain_layer.di.endpoints.categories.environment;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.categories.consumer.CategoriesComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.categories.consumer.ICategoriesComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.consumer.ISessionClientComponent;

/**
 * Created by i_komarov on 04.05.17.
 */

public class CategoriesEnvironmentComponent implements ICategoriesEnvironmentComponent {

    @NonNull
    private final IEnvironmentComponent environment;

    @NonNull
    private final ISessionClientComponent clients;

    @NonNull
    private final ICacheComponent cache;

    @NonNull
    private final CategoriesRemoteModule remote;

    @NonNull
    private final CategoriesLocalModule local;

    @NonNull
    private final CategoriesMappingModule mapping;

    public CategoriesEnvironmentComponent(@NonNull IEnvironmentComponent environment, @NonNull ICacheComponent cache, @NonNull ISessionClientComponent clients) {
        this.environment = environment;
        this.cache = cache;
        this.clients = clients;

        this.remote = new CategoriesRemoteModule(clients);
        this.local = new CategoriesLocalModule(environment);
        this.mapping = new CategoriesMappingModule();
    }

    @NonNull
    @Override
    public ICategoriesComponent plusCategoriesComponent() {
        return new CategoriesComponent(environment, cache, this);
    }

    @NonNull
    @Override
    public CategoriesRemoteModule remote() {
        return remote;
    }

    @NonNull
    @Override
    public CategoriesLocalModule local() {
        return local;
    }

    @NonNull
    @Override
    public CategoriesMappingModule mapping() {
        return mapping;
    }
}
