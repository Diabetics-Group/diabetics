package net.styleru.ikomarov.domain_layer.repository.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.local.topics.TopicLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.topics.TopicRemoteEntity;
import net.styleru.ikomarov.data_layer.parsing.base.IParser;
import net.styleru.ikomarov.data_layer.services.topics.ITopicsLocalService;
import net.styleru.ikomarov.data_layer.services.topics.ITopicsRemoteService;

import net.styleru.ikomarov.domain_layer.cache.strategy.ICacheStrategy;
import net.styleru.ikomarov.domain_layer.contracts.cache.CachePolicy;
import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;
import net.styleru.ikomarov.domain_layer.mapping.topics.LocalTopicMapper;
import net.styleru.ikomarov.domain_layer.mapping.topics.LocalTopicsMapper;
import net.styleru.ikomarov.domain_layer.mapping.topics.RemoteTopicMapper;
import net.styleru.ikomarov.domain_layer.mapping.topics.RemoteTopicsMapper;
import net.styleru.ikomarov.domain_layer.repository.base.CreateResult;
import net.styleru.ikomarov.domain_layer.utils.RepositoryUtils;

import java.net.HttpURLConnection;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.functions.Function;

import okhttp3.ResponseBody;

import retrofit2.Response;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsRepository implements ITopicsRepository {

    @NonNull
    private final IParser<ResponseBody, String> jsonParser;

    @NonNull
    private final ICacheStrategy strategy;

    @NonNull
    private final ITopicsRemoteService remoteService;

    @NonNull
    private final ITopicsLocalService localService;

    @NonNull
    private final Function<List<TopicRemoteEntity>, List<TopicLocalEntity>> mapping;

    @NonNull
    private final Function<TopicLocalEntity, TopicDTO> localSingleMapper;

    @NonNull
    private final Function<List<TopicLocalEntity>, List<TopicDTO>> localListMapper;

    @NonNull
    private final Function<TopicRemoteEntity, TopicDTO> remoteSingleMapper;

    @NonNull
    private final Function<List<TopicRemoteEntity>, List<TopicDTO>> remoteListMapper;

    private TopicsRepository(@NonNull IParser<ResponseBody, String> jsonParser,
                            @NonNull ICacheStrategy strategy,
                            @NonNull ITopicsRemoteService remoteService,
                            @NonNull ITopicsLocalService localService,
                            @NonNull Function<List<TopicRemoteEntity>, List<TopicLocalEntity>> mapping) {
        this.jsonParser = jsonParser;
        this.strategy = strategy;
        this.remoteService = remoteService;
        this.localService = localService;
        this.mapping = mapping;
        this.localSingleMapper = new LocalTopicMapper();
        this.localListMapper = new LocalTopicsMapper(localSingleMapper);
        this.remoteSingleMapper = new RemoteTopicMapper();
        this.remoteListMapper = new RemoteTopicsMapper(remoteSingleMapper);
    }

    @Override
    public Single<CreateResult<TopicDTO>> create(@NonNull String categoryId, @NonNull String userId, @NonNull TopicDTO.Builder builder) {
        final CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            return Single.just(new CreateResult<>(OperationStatus.DELAYED, null));
        }

        return remoteService.create(userId, categoryId, builder.getTitle(), builder.getContent())
                .compose(withSingleCache(HttpURLConnection.HTTP_CREATED))
                .map(dto -> new CreateResult<>(OperationStatus.SUCCESS, dto));
    }

    @Override
    public Observable<TopicDTO> get(@NonNull String id) {
        final CachePolicy policy = strategy.currentPolicy();

        return Observable.mergeDelayError(
                policy != CachePolicy.FORCE_NETWORK ? getFromDatabase(id).toObservable() : Observable.never(),
                policy != CachePolicy.CACHE_ONLY ? getFromNetwork(id).toObservable() : Observable.never()
        );
    }

    @Override
    public Observable<List<TopicDTO>> list(@NonNull String categoryId, int offset, int limit) {
        final CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            return listFromDatabase(CachePolicy.CACHE_ONLY, categoryId, offset, limit).toObservable();
        } else if(policy == CachePolicy.FORCE_NETWORK) {
            return listFromNetwork(policy, categoryId, offset, limit).toObservable();
        } else {
            return listFromDatabase(policy, categoryId, offset, limit).toObservable()
                    .compose(RepositoryUtils.ignoreLocalDatabaseEmptyError())
                    .compose(offset == 0 ?
                            withFetchFromNetwork(policy, categoryId, offset, limit) :
                            withOptionalPostload(policy, categoryId, offset, limit)
                    );
        }
    }

    private ObservableTransformer<List<TopicDTO>, List<TopicDTO>> withFetchFromNetwork(CachePolicy policy, String topicId, int offset, int limit) {
        return o -> o.flatMap(data -> data.size() == 0 ?
                listFromNetwork(policy, topicId, offset, limit).toObservable() :
                Observable.merge(
                        Observable.just(data),
                        listFromNetwork(policy, topicId, data.get(0).getLastUpdate())
                                .toObservable()
                                .onErrorResumeNext(Observable.never())
                )
        );
    }

    private ObservableTransformer<List<TopicDTO>, List<TopicDTO>> withOptionalPostload(CachePolicy policy, String topicId, int offset, int limit) {
        return o -> o.flatMap(data -> {
            final int delta = limit - data.size();
            return delta == 0 ? Observable.just(data) :
                    Observable.merge(
                            Observable.just(data),
                            listFromNetwork(policy, topicId, offset + data.size(), delta)
                                    .toObservable()
                    );
        });
    }

    private Single<TopicDTO> getFromDatabase(@NonNull String id) {
        return localService.get(id)
                .map(localSingleMapper);
    }

    private Single<List<TopicDTO>> listFromDatabase(@NonNull CachePolicy policy, @NonNull String categoryId, int offset, int limit) {
        return localService.list(categoryId, offset, limit)
                .map(localListMapper)
                .flatMap(RepositoryUtils.withCheckOnLocalDatabaseEmpty(policy));
    }

    private Single<TopicDTO> getFromNetwork(@NonNull String id) {
        return remoteService.get(id)
                .compose(withListCache(HttpURLConnection.HTTP_OK))
                .map(list -> list.get(0));
    }

    private Single<List<TopicDTO>> listFromNetwork(@NonNull CachePolicy policy, @NonNull String categoryId, @NonNull String lastModified) {
        return remoteService.list(lastModified, categoryId)
                .compose(withListCache(HttpURLConnection.HTTP_OK))
                .flatMap(RepositoryUtils.withCheckOnRemoteDatabaseEmpty(policy));
    }

    private Single<List<TopicDTO>> listFromNetwork(@NonNull CachePolicy policy, @NonNull String categoryId, int offset, int limit) {
        return remoteService.list(categoryId, offset, limit)
                .compose(withListCache(HttpURLConnection.HTTP_OK))
                .flatMap(RepositoryUtils.withCheckOnRemoteDatabaseEmpty(policy));
    }

    private SingleTransformer<Response<ResponseContainer<TopicRemoteEntity>>, TopicDTO> withSingleCache(Integer... successCodes) {
        return (o) -> o.flatMap(RepositoryUtils.verifyResponse(jsonParser, successCodes))
                .map(ResponseContainer::getBody)
                .doOnSuccess(this::cacheSingle)
                .map(remoteSingleMapper);
    }

    private SingleTransformer<Response<ResponseContainer<List<TopicRemoteEntity>>>, List<TopicDTO>> withListCache(Integer... successCodes) {
        return (o) -> o.flatMap(RepositoryUtils.verifyResponse(jsonParser, successCodes))
                .map(ResponseContainer::getBody)
                .doOnSuccess(this::cache)
                .map(remoteListMapper);
    }

    private void cacheSingle(@NonNull TopicRemoteEntity data) {
        cache(Collections.singletonList(data));
    }

    private void cache(@NonNull List<TopicRemoteEntity> data) {
        Single.just(data)
                .map(mapping)
                .flatMap(localService::create)
                .subscribe();
    }

    private void invalidateSingle(@NonNull TopicRemoteEntity data) {
        invalidate(Collections.singletonList(data));
    }

    private void invalidate(@NonNull List<TopicRemoteEntity> data) {
        Observable.fromIterable(data)
                .map(TopicRemoteEntity::getId)
                .toList()
                .flatMap(localService::delete)
                .subscribe();
    }

    public static final class Factory {

        @NonNull
        private final IParser<ResponseBody, String> jsonParser;

        @NonNull
        private final ICacheStrategy strategy;

        @NonNull
        private final ITopicsRemoteService remoteService;

        @NonNull
        private final ITopicsLocalService localService;

        @NonNull
        private final Function<List<TopicRemoteEntity>, List<TopicLocalEntity>> mapping;

        public Factory(@NonNull IParser<ResponseBody, String> jsonParser,
                       @NonNull ICacheStrategy strategy,
                       @NonNull ITopicsRemoteService remoteService,
                       @NonNull ITopicsLocalService localService,
                       @NonNull Function<List<TopicRemoteEntity>, List<TopicLocalEntity>> mapping) {

            this.jsonParser = jsonParser;
            this.strategy = strategy;
            this.remoteService = remoteService;
            this.localService = localService;
            this.mapping = mapping;
        }

        public ITopicsRepository create() {
            return new TopicsRepository(jsonParser, strategy, remoteService, localService, mapping);
        }
    }
}
