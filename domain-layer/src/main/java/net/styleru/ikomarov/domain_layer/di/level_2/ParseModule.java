package net.styleru.ikomarov.domain_layer.di.level_2;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.parsing.base.IParser;
import net.styleru.ikomarov.data_layer.parsing.json.JsonParser;

import okhttp3.ResponseBody;

/**
 * Created by i_komarov on 04.05.17.
 */

public class ParseModule {

    @NonNull
    private final Object lock = new Object();

    @Nullable
    private volatile IParser<ResponseBody, String> jsonParser;

    public ParseModule() {

    }

    public IParser<ResponseBody, String> provideJsonParser() {
        IParser<ResponseBody, String> localInstance = jsonParser;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = jsonParser;
                if(localInstance == null) {
                    localInstance = jsonParser = new JsonParser();
                }
            }
        }

        return localInstance;
    }
}
