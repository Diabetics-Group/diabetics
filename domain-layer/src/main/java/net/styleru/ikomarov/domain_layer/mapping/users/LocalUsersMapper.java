package net.styleru.ikomarov.domain_layer.mapping.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 15.05.17.
 */

public class LocalUsersMapper implements Function<List<UserLocalEntity>, List<UserDTO>> {

    @NonNull
    private final Function<UserLocalEntity, UserDTO> singleMapper;

    public LocalUsersMapper(@NonNull Function<UserLocalEntity, UserDTO> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<UserDTO> apply(List<UserLocalEntity> userLocalEntities) throws Exception {
        return Observable.fromIterable(userLocalEntities)
                .map(singleMapper)
                .reduce(new ArrayList<UserDTO>(userLocalEntities.size()), (list, element) -> {
                    list.add(element);
                    return list;
                })
                .blockingGet();
    }
}
