package net.styleru.ikomarov.domain_layer.di.endpoints.authentication;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;

/**
 * Created by i_komarov on 05.06.17.
 */

public class AuthenticationComponent implements IAuthenticationComponent {

    @NonNull
    private final ISessionComponent sessionComponent;

    @NonNull
    private final IEnvironmentComponent environmentComponent;

    @NonNull
    private final AuthenticationModule authentication;

    public AuthenticationComponent(@NonNull ISessionComponent sessionComponent, @NonNull IEnvironmentComponent environmentComponent) {
        this.sessionComponent = sessionComponent;
        this.environmentComponent = environmentComponent;
        this.authentication = new AuthenticationModule(sessionComponent, environmentComponent);
    }

    @NonNull
    @Override
    public AuthenticationModule authentication() {
        return authentication;
    }
}
