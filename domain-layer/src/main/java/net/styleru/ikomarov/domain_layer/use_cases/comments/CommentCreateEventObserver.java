package net.styleru.ikomarov.domain_layer.use_cases.comments;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

/**
 * Created by i_komarov on 27.05.17.
 */

public class CommentCreateEventObserver<T> extends UseCase.BaseMaybeObserver<T> {

    @NonNull
    private final CommentCreateUseCase.Callbacks<T> callbacks;

    public CommentCreateEventObserver(@NonNull CommentCreateUseCase.Callbacks<T> callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onSuccess(T value) {
        if(!isDisposed()) {
            callbacks.onCommentCreated(value);
        }
    }

    @Override
    public void onError(Throwable e) {
        if(!isDisposed()) {
            callbacks.onCommentCreationFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
        }
    }
}
