package net.styleru.ikomarov.domain_layer.mapping.users;

import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.users.UserExtendedDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 05.06.17.
 */

public class LocalUserExtendedMapper implements Function<UserLocalEntity, UserExtendedDTO> {

    @Override
    public UserExtendedDTO apply(UserLocalEntity entity) throws Exception {
        return new UserExtendedDTO(
                entity.getId(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getImageUrl(),
                entity.getRole(),
                entity.getToken()
        );
    }
}
