package net.styleru.ikomarov.domain_layer.contracts.jobs;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 11.05.17.
 */

public enum JobType {
    CATEGORIES_FETCH ("categires_fetch"),
    CATEGORIES_GET   ("categories_get"),
    TOPICS_FETCH     ("topics_fetch"),
    TOPICS_GET       ("topics_get"),
    TOPICS_POST      ("topics_post"),
    TOPICS_DELETE    ("topics_delete"),
    COMMENTS_FETCH   ("comments_fetch"),
    COMMENTS_GET     ("comments_get"),
    COMMENTS_POST    ("comments_post"),
    COMMENTS_DELETE  ("comments_delete");

    private final String type;

    JobType(String type) {
        this.type = type;
    }

    @NonNull
    public String type() {
        return this.type;
    }
}
