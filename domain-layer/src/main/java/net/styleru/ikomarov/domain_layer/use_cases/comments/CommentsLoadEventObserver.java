package net.styleru.ikomarov.domain_layer.use_cases.comments;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsLoadEventObserver<T> extends UseCase.BaseObserver<List<T>> {

    @NonNull
    private final CommentsGetUseCase.Callbacks<T> callbacks;

    public CommentsLoadEventObserver(@NonNull CommentsGetUseCase.Callbacks<T> callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onNext(List<T> value) {
        if(!isDisposed()) {
            callbacks.onCommentsLoaded(value);
        }
    }

    @Override
    public void onError(Throwable e) {
        if(!isDisposed()) {
            callbacks.onCommentsLoadingFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
        }
    }

    @Override
    public void onComplete() {

    }
}
