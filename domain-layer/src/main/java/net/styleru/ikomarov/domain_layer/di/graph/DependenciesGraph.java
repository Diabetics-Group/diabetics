package net.styleru.ikomarov.domain_layer.di.graph;

import android.content.Context;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.services.database.IDatabaseService;
import net.styleru.ikomarov.domain_layer.di.endpoints.authentication.AuthenticationComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.authentication.IAuthenticationComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.categories.consumer.ICategoriesComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.categories.environment.CategoriesEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.categories.environment.ICategoriesEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.comments.consumer.ICommentsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.comments.environment.CommentsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.comments.environment.ICommentsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.topics.consumer.ITopicsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.topics.environment.ITopicsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.topics.environment.TopicsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.users.consumer.IUsersComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.users.environment.IUsersEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.users.environment.UsersEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_1.AppComponent;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.consumer.ISessionClientComponent;
import net.styleru.ikomarov.domain_layer.di.providers.IApplicationEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IAuthenticationModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.ICategoriesModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.ICommentsModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IExecutionEnvironmentProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IGraphProvider;
import net.styleru.ikomarov.domain_layer.di.providers.ITopicsModelProvider;
import net.styleru.ikomarov.domain_layer.di.providers.IUsersModelProvider;
import net.styleru.ikomarov.domain_layer.repository.authentication.IAuthenticationRepository;
import net.styleru.ikomarov.domain_layer.repository.categories.ICategoriesRepository;
import net.styleru.ikomarov.domain_layer.repository.comments.ICommentsRepository;
import net.styleru.ikomarov.domain_layer.repository.topics.ITopicsRepository;
import net.styleru.ikomarov.domain_layer.repository.users.IUsersRepository;

import io.reactivex.Scheduler;

/**
 * Created by i_komarov on 11.05.17.
 */

public class DependenciesGraph implements IGraphProvider, IApplicationEnvironmentProvider, IExecutionEnvironmentProvider, ICategoriesModelProvider, ITopicsModelProvider, ICommentsModelProvider, IUsersModelProvider, IAuthenticationModelProvider {

    @NonNull
    private final IAppComponent app;

    @NonNull
    private final IEnvironmentComponent environment;

    @NonNull
    private final ICacheComponent cache;

    @NonNull
    private final ISessionComponent session;

    @NonNull
    private final ISessionClientComponent clients;

    @NonNull
    private final ICategoriesComponent categories;

    @NonNull
    private final ITopicsComponent topics;

    @NonNull
    private final ICommentsComponent comments;

    @NonNull
    private final IUsersComponent users;

    @NonNull
    private final IAuthenticationComponent authentication;

    public DependenciesGraph(@NonNull Context context) {
        //Level 1
        this.app = context.getApplicationContext() == null ? AppComponent.fromExternalProcess(context) : AppComponent.fromAppProcess(context.getApplicationContext());
        //Level 2
        this.environment = app.plusEnvironmentComponent();
        //Level 3
        this.cache = environment.plusCacheComponent();
        this.session = environment.plusSessionComponent();
        //Level 4
        this.clients = session.plusSessionClientComponent();
        //CUSTOM ENDPOINTS BELOW
        //Categories
        ICategoriesEnvironmentComponent categoriesEnvironment = new CategoriesEnvironmentComponent(environment, cache, clients);
        this.categories = categoriesEnvironment.plusCategoriesComponent();
        //Topics
        ITopicsEnvironmentComponent topicsEnvironment = new TopicsEnvironmentComponent(environment, cache, clients);
        this.topics = topicsEnvironment.plusTopicsComponent();
        //Comments
        ICommentsEnvironmentComponent commentsEnvironment = new CommentsEnvironmentComponent(environment, cache, clients);
        this.comments = commentsEnvironment.plusCommentsComponent();
        //Users
        IUsersEnvironmentComponent usersEnvironment = new UsersEnvironmentComponent(environment, cache, clients);
        this.users = usersEnvironment.plusUsersComponent();
        //Authentication
        this.authentication = new AuthenticationComponent(session, environment);
    }

    @NonNull
    @Override
    public IAppComponent provideGraphRoot() {
        return app;
    }

    @NonNull
    @Override
    public Context provideContext() {
        return app.app().provideContext();
    }

    @NonNull
    @Override
    public IDatabaseService provideDatabaseService() {
        return environment.db().provideDatabaseService();
    }

    @Override
    public Scheduler provideExecuteScheduler() {
        return app.app().prodiveSchedulerIO();
    }

    @Override
    public Scheduler providePostScheduler() {
        return app.app().provideSchedulerMain();
    }

    @NonNull
    @Override
    public ITopicsRepository provideTopicsRepository() {
        return topics.repository().provideRepository();
    }

    @NonNull
    @Override
    public ICommentsRepository provideCommentsRepository() {
        return comments.repository().provideRepository();
    }

    @NonNull
    @Override
    public ICategoriesRepository provideCategoriesRepository() {
        return categories.repository().provideRepository();
    }

    @NonNull
    @Override
    public IUsersRepository provideUsersRepository() {
        return users.repository().provideRepository();
    }

    @NonNull
    @Override
    public IAuthenticationRepository provideAuthenticationRepository() {
        return authentication.authentication().provideAuthenticationRepository();
    }
}
