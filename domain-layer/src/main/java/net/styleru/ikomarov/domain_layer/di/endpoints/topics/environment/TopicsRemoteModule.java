package net.styleru.ikomarov.domain_layer.di.endpoints.topics.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.categories.CategoriesRemoteService;
import net.styleru.ikomarov.data_layer.services.categories.ICategoriesRemoteService;
import net.styleru.ikomarov.data_layer.services.topics.ITopicsRemoteService;
import net.styleru.ikomarov.data_layer.services.topics.TopicsRemoteService;
import net.styleru.ikomarov.domain_layer.di.level_4.consumer.ISessionClientComponent;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsRemoteModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final ISessionClientComponent session;

    @Nullable
    private volatile ITopicsRemoteService service;

    public TopicsRemoteModule(@NonNull ISessionClientComponent session) {
        this.session = session;
    }

    @NonNull
    public ITopicsRemoteService provideRemoteService() {
        ITopicsRemoteService localInstance = service;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = service;
                if(localInstance == null) {
                    localInstance = service = new TopicsRemoteService.Factory(session.session().provideSessionClient()).create();
                }
            }
        }

        return localInstance;
    }
}
