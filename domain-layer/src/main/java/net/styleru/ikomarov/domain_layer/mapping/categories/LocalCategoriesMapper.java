package net.styleru.ikomarov.domain_layer.mapping.categories;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.local.categories.CategoryLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.categories.CategoryDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 02.05.17.
 */

public class LocalCategoriesMapper implements Function<List<CategoryLocalEntity>, List<CategoryDTO>> {

    @NonNull
    private final Function<CategoryLocalEntity, CategoryDTO> singleMapper;

    public LocalCategoriesMapper(@NonNull Function<CategoryLocalEntity, CategoryDTO> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<CategoryDTO> apply(List<CategoryLocalEntity> entities) throws Exception {
        return Observable.fromIterable(entities)
                .map(singleMapper)
                .toList()
                .blockingGet();
    }
}
