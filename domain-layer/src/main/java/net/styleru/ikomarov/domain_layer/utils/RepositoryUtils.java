package net.styleru.ikomarov.domain_layer.utils;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.exception.EnvironmentExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.HttpExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.Reason;
import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.parsing.base.IParser;
import net.styleru.ikomarov.domain_layer.contracts.cache.CachePolicy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by i_komarov on 31.05.17.
 */

public class RepositoryUtils {

    /**
     * Replace upstream with an ObservableSource that emits an error with
     * {@link net.styleru.ikomarov.data_layer.contracts.exception.EnvironmentExceptionContract.Type#CACHE_EMPTY} message
     * if policy is {@link CachePolicy#CACHE_ONLY}
     * */
    public static <T> Function<List<T>, SingleSource<List<T>>> withCheckOnLocalDatabaseEmpty(CachePolicy policy) {
        return (list) -> {
            if(policy == CachePolicy.CACHE_ONLY && list.isEmpty()) {
                ExceptionBundle error = new ExceptionBundle(Reason.ENVIRONMENT);
                EnvironmentExceptionContract contract = Reason.ENVIRONMENT.getContract();
                contract.putType(error, EnvironmentExceptionContract.Type.CACHE_EMPTY);
                return Single.error(error);
            } else {
                return Single.just(list);
            }
        };
    }

    /**
     * Replace upstream with an ObservableSource that emits an error with
     * {@link net.styleru.ikomarov.data_layer.contracts.exception.EnvironmentExceptionContract.Type#REMOTE_DB_EMPTY} message
     * if policy is not {@link CachePolicy#CACHE_ONLY}
     * */
    public static <T> Function<List<T>, SingleSource<List<T>>> withCheckOnRemoteDatabaseEmpty(CachePolicy policy) {
        return (list) -> {
            if (policy != CachePolicy.CACHE_ONLY && list.isEmpty()) {
                ExceptionBundle error = new ExceptionBundle(Reason.ENVIRONMENT);
                EnvironmentExceptionContract contract = Reason.ENVIRONMENT.getContract();
                contract.putType(error, EnvironmentExceptionContract.Type.REMOTE_DB_EMPTY);
                return Single.error(error);
            } else {
                return Single.just(list);
            }
        };
    }

    /**
     * Replaces upstream with an ObservableSource that either emits an error or extracted data from {@link Response}
     * depending on outer conditions as matching any of expected response codes to one presented in a response
     * */
    public static <T> Function<Response<ResponseContainer<T>>, SingleSource<ResponseContainer<T>>> verifyResponse(@NonNull IParser<ResponseBody, String> jsonParser, Integer... codesExpected) {
        return (response) -> {
            boolean fitsConditions = false;
            int responseCode = response.code();

            for(Integer codeExpected : codesExpected) {
                if(fitsConditions = (responseCode == codeExpected)) {
                    fitsConditions = true;
                    break;
                }
            }

            if(fitsConditions) {
                ResponseContainer<T> body = response.body();
                return body != null ? Single.just(body) : Single.just(ResponseContainer.<T>newSuccess(responseCode, null));
            } else {
                ExceptionBundle error = new ExceptionBundle(Reason.HTTP);
                HttpExceptionContract contract = error.getReason().getContract();
                contract.putCode(error, response.code());
                contract.putMessage(error, response.message());
                ResponseBody errorBody;
                if(null != (errorBody = response.errorBody())) {
                    try {
                        //TODO: deserialize json into a POJO error representation AFTER HTML/XML/OTHER FORMAT CHECK. JSON ONLY!!!
                        contract.putJson(error, parseResponseBody(jsonParser, errorBody));
                    } catch(IOException ignored) {}
                }
                return Single.error(error);
            }
        };
    }

    /**
     * Replaces upstream containing {@link ExceptionBundle} with {@link Reason}
     * equal to {@link Reason#ENVIRONMENT} and type of {@link EnvironmentExceptionContract.Type#CACHE_EMPTY}
     * with a stream with single item emitted - empty array
     *
     * @return {@link ObservableTransformer} that either emits an exception or list (list might ne empty as well)
     * */
    public static <T> ObservableTransformer<List<T>, List<T>> ignoreLocalDatabaseEmptyError() {
        return o -> o.onErrorResumeNext(
                thr -> thr instanceof ExceptionBundle &&
                        ((ExceptionBundle) thr).getReason() == Reason.ENVIRONMENT &&
                        ((EnvironmentExceptionContract) Reason.ENVIRONMENT.getContract()).extractType((ExceptionBundle) thr) == EnvironmentExceptionContract.Type.CACHE_EMPTY ?
                        Observable.just(new ArrayList<>()) :
                        Observable.error(thr)
        );
    }

    /**
     * Performs a parse operation {@link ResponseBody} -> {@link String} representing incoming JSON
     * */
    public static String parseResponseBody(@NonNull IParser<ResponseBody, String> jsonParser, ResponseBody responseBody) throws IOException {
        return jsonParser.parse(responseBody);
    }
}
