package net.styleru.ikomarov.domain_layer.repository.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;
import net.styleru.ikomarov.data_layer.parsing.base.IParser;
import net.styleru.ikomarov.data_layer.services.users.IUsersLocalService;
import net.styleru.ikomarov.data_layer.services.users.IUsersRemoteService;
import net.styleru.ikomarov.domain_layer.cache.strategy.ICacheStrategy;
import net.styleru.ikomarov.domain_layer.contracts.cache.CachePolicy;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;
import net.styleru.ikomarov.domain_layer.mapping.users.LocalUserMapper;
import net.styleru.ikomarov.domain_layer.mapping.users.LocalUsersMapper;
import net.styleru.ikomarov.domain_layer.mapping.users.RemoteUserMapper;
import net.styleru.ikomarov.domain_layer.mapping.users.RemoteUsersMapper;
import net.styleru.ikomarov.domain_layer.utils.RepositoryUtils;

import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UsersRepository implements IUsersRepository {

    @NonNull
    private final IParser<ResponseBody, String> jsonParser;

    @NonNull
    private final ICacheStrategy strategy;

    @NonNull
    private final IUsersRemoteService remoteService;

    @NonNull
    private final IUsersLocalService localService;

    @NonNull
    private final Function<List<UserRemoteEntity>, List<UserLocalEntity>> mapping;

    @NonNull
    private final Function<UserLocalEntity, UserDTO> localSingleMapper;

    @NonNull
    private final Function<List<UserLocalEntity>, List<UserDTO>> localListMapper;

    @NonNull
    private final Function<UserRemoteEntity, UserDTO> remoteSingleMapper;

    @NonNull
    private final Function<List<UserRemoteEntity>, List<UserDTO>> remoteListMapper;

    private UsersRepository(@NonNull IParser<ResponseBody, String> jsonParser,
                              @NonNull ICacheStrategy strategy,
                              @NonNull IUsersRemoteService remoteService,
                              @NonNull IUsersLocalService localService,
                              @NonNull Function<List<UserRemoteEntity>, List<UserLocalEntity>> mapping) {

        this.jsonParser = jsonParser;
        this.strategy = strategy;
        this.remoteService = remoteService;
        this.localService = localService;
        this.mapping = mapping;
        this.localSingleMapper = new LocalUserMapper();
        this.localListMapper = new LocalUsersMapper(localSingleMapper);
        this.remoteSingleMapper = new RemoteUserMapper();
        this.remoteListMapper = new RemoteUsersMapper(remoteSingleMapper);
    }

    @Override
    public Observable<UserDTO> get(@NonNull String userId) {
        CachePolicy policy = strategy.currentPolicy();

        return Observable.mergeDelayError(
                policy != CachePolicy.FORCE_NETWORK ? getFromDatabase(policy, userId).toObservable() : Observable.never(),
                policy != CachePolicy.CACHE_ONLY ? getFromNetwork(policy, userId).toObservable() : Observable.never()
        );
    }

    @Override
    public Observable<List<UserDTO>> list(int offset, int limit) {
        CachePolicy policy = strategy.currentPolicy();

        return Observable.mergeDelayError(
                policy != CachePolicy.FORCE_NETWORK ? listFromDatabase(policy, offset, limit).toObservable() : Observable.never(),
                policy != CachePolicy.CACHE_ONLY ? listFromNetwork(policy, offset, limit).toObservable() : Observable.never()
        );
    }

    private Single<UserDTO> getFromDatabase(@NonNull CachePolicy policy, @NonNull String id) {
        return localService.get(id)
                .map(localSingleMapper);
    }

    private Single<List<UserDTO>> listFromDatabase(@NonNull CachePolicy policy, int offset, int limit) {
        return localService.list(offset, limit)
                .map(localListMapper)
                .flatMap(RepositoryUtils.withCheckOnLocalDatabaseEmpty(policy));
    }

    private Single<UserDTO> getFromNetwork(@NonNull CachePolicy policy, @NonNull String id) {
        return remoteService.get(id)
                .compose(withSingleCache(HttpURLConnection.HTTP_OK));
    }

    private Single<List<UserDTO>> listFromNetwork(@NonNull CachePolicy policy, int offset, int limit) {
        return remoteService.list(offset, limit)
                .compose(withListCache(HttpURLConnection.HTTP_OK, HttpURLConnection.HTTP_PARTIAL))
                .flatMap(RepositoryUtils.withCheckOnRemoteDatabaseEmpty(policy));
    }

    private SingleTransformer<Response<ResponseContainer<UserRemoteEntity>>, UserDTO> withSingleCache(Integer... successCodes) {
        return (o) -> o.flatMap(RepositoryUtils.verifyResponse(jsonParser, successCodes))
                .map(ResponseContainer::getBody)
                .doOnSuccess(this::cacheSingle)
                .map(remoteSingleMapper);
    }

    private SingleTransformer<Response<ResponseContainer<List<UserRemoteEntity>>>, List<UserDTO>> withListCache(Integer... successCodes) {
        return (o) -> o.flatMap(RepositoryUtils.verifyResponse(jsonParser, successCodes))
                .map(ResponseContainer::getBody)
                .doOnSuccess(this::cache)
                .map(remoteListMapper);
    }

    private void cacheSingle(@NonNull UserRemoteEntity data) {
        cache(Collections.singletonList(data));
    }

    private void cache(@NonNull List<UserRemoteEntity> data) {
        Single.just(data)
                .map(mapping)
                .flatMap(localService::create)
                .subscribe();
    }

    private void invalidateSingle(@NonNull UserRemoteEntity data) {
        invalidate(Collections.singletonList(data));
    }

    private void invalidate(@NonNull List<UserRemoteEntity> data) {
        Observable.fromIterable(data)
                .map(UserRemoteEntity::getId)
                .toList()
                .flatMap(localService::delete)
                .subscribe();
    }

    public static final class Factory {

        @NonNull
        private final IParser<ResponseBody, String> jsonParser;

        @NonNull
        private final ICacheStrategy strategy;

        @NonNull
        private final IUsersRemoteService remoteService;

        @NonNull
        private final IUsersLocalService localService;

        @NonNull
        private final Function<List<UserRemoteEntity>, List<UserLocalEntity>> mapping;

        public Factory(@NonNull IParser<ResponseBody, String> jsonParser,
                       @NonNull ICacheStrategy strategy,
                       @NonNull IUsersRemoteService remoteService,
                       @NonNull IUsersLocalService localService,
                       @NonNull Function<List<UserRemoteEntity>, List<UserLocalEntity>> mapping) {

            this.jsonParser = jsonParser;
            this.strategy = strategy;
            this.remoteService = remoteService;
            this.localService = localService;
            this.mapping = mapping;
        }

        public IUsersRepository create() {
            return new UsersRepository(jsonParser, strategy, remoteService, localService, mapping);
        }
    }
}
