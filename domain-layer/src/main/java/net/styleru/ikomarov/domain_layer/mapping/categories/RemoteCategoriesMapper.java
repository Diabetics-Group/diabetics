package net.styleru.ikomarov.domain_layer.mapping.categories;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.remote.categories.CategoryRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.categories.CategoryDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 02.05.17.
 */

public class RemoteCategoriesMapper implements Function<List<CategoryRemoteEntity>, List<CategoryDTO>> {

    @NonNull
    private final Function<CategoryRemoteEntity, CategoryDTO> singleMapper;

    public RemoteCategoriesMapper(@NonNull Function<CategoryRemoteEntity, CategoryDTO> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<CategoryDTO> apply(List<CategoryRemoteEntity> categoryRemoteEntities) throws Exception {
        return Observable.fromIterable(categoryRemoteEntities)
                .map(singleMapper)
                .toList()
                .blockingGet();
    }
}
