package net.styleru.ikomarov.domain_layer.repository.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;

import java.util.UUID;

/**
 * Created by i_komarov on 04.05.17.
 */

public class CreateResult<DTO> {

    @NonNull
    private final OperationStatus status;

    @Nullable
    private final DTO dto;

    public CreateResult(@NonNull OperationStatus status, @Nullable DTO dto) {
        this.status = status;
        this.dto = dto;
    }

    @NonNull
    public OperationStatus getStatus() {
        return status;
    }

    @Nullable
    public DTO getDto() {
        return dto;
    }
}
