package net.styleru.ikomarov.domain_layer.di.providers;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.repository.topics.ITopicsRepository;

/**
 * Created by i_komarov on 04.05.17.
 */

public interface ITopicsModelProvider {

    @NonNull
    ITopicsRepository provideTopicsRepository();
}
