package net.styleru.ikomarov.domain_layer.di.endpoints.users.consumer;

import android.support.annotation.NonNull;


/**
 * Created by i_komarov on 29.05.17.
 */

public interface IUsersComponent {

    @NonNull
    UsersRepositoryModule repository();
}
