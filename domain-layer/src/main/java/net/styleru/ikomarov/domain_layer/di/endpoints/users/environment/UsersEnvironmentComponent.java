package net.styleru.ikomarov.domain_layer.di.endpoints.users.environment;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.users.consumer.IUsersComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.users.consumer.UsersComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.consumer.ISessionClientComponent;

/**
 * Created by i_komarov on 29.05.17.
 */

public class UsersEnvironmentComponent implements IUsersEnvironmentComponent {

    @NonNull
    private final IEnvironmentComponent environment;

    @NonNull
    private final ICacheComponent cache;

    @NonNull
    private final ISessionClientComponent clients;

    @NonNull
    private final UsersRemoteModule remote;

    @NonNull
    private final UsersLocalModule local;

    @NonNull
    private final UsersMappingModule mapping;

    public UsersEnvironmentComponent(@NonNull IEnvironmentComponent environment,
                                     @NonNull ICacheComponent cache,
                                     @NonNull ISessionClientComponent clients) {
        this.environment = environment;
        this.cache = cache;
        this.clients = clients;
        this.remote = new UsersRemoteModule(clients);
        this.local = new UsersLocalModule(environment);
        this.mapping = new UsersMappingModule();
    }

    @NonNull
    @Override
    public IUsersComponent plusUsersComponent() {
        return new UsersComponent(environment, cache, this);
    }

    @NonNull
    @Override
    public UsersRemoteModule remote() {
        return remote;
    }

    @NonNull
    @Override
    public UsersLocalModule local() {
        return local;
    }

    @NonNull
    @Override
    public UsersMappingModule mapping() {
        return mapping;
    }
}
