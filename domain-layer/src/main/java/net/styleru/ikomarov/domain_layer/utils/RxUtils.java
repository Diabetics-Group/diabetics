package net.styleru.ikomarov.domain_layer.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import io.reactivex.ObservableTransformer;

/**
 * Created by i_komarov on 29.04.17.
 */

public class RxUtils {

    private RxUtils() {
        throw new IllegalStateException("No instances please!");
    }

    public static <T> ObservableTransformer<List<T>, List<T>> shuffle() {
        return (o) -> o.map(list -> {
            long seed = System.nanoTime();
            Collections.shuffle(list, new Random(seed));
            return list;
        });
    }

    public static <T> ObservableTransformer<T, List<T>> accumulate() {
        return (o) -> o.reduce((List<T>) new ArrayList<T>(), (accumulator, chunk) -> {
            accumulator.add(chunk);
            return accumulator;
        }).toObservable();
    }
}
