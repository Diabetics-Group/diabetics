package net.styleru.ikomarov.domain_layer.di.endpoints.topics.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.entities.local.topics.TopicLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.topics.TopicRemoteEntity;
import net.styleru.ikomarov.data_layer.mappings.remote_to_local.topics.RemoteToLocalTopicEntityMapping;
import net.styleru.ikomarov.data_layer.mappings.remote_to_local.topics.RemoteToLocalTopicsEntityMapping;

import java.util.List;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsMappingModule {

    @NonNull
    private final Object singleLock = new Object();

    @NonNull
    private final Object listLock = new Object();

    @Nullable
    private volatile Function<TopicRemoteEntity, TopicLocalEntity> singleMapping;

    @Nullable
    private volatile Function<List<TopicRemoteEntity>, List<TopicLocalEntity>> listMapping;

    public TopicsMappingModule() {

    }

    @NonNull
    public Function<TopicRemoteEntity, TopicLocalEntity> provideSingleMapping() {
        Function<TopicRemoteEntity, TopicLocalEntity> localInstance = singleMapping;
        if(localInstance == null) {
            synchronized (singleLock) {
                localInstance = singleMapping;
                if(localInstance == null) {
                    localInstance = singleMapping = new RemoteToLocalTopicEntityMapping();
                }
            }
        }

        return localInstance;
    }

    @NonNull
    public Function<List<TopicRemoteEntity>, List<TopicLocalEntity>> provideListMapping() {
        Function<List<TopicRemoteEntity>, List<TopicLocalEntity>> localInstance = listMapping;
        if(localInstance == null) {
            synchronized (listLock) {
                localInstance = listMapping;
                if(localInstance == null) {
                    localInstance = listMapping = new RemoteToLocalTopicsEntityMapping(provideSingleMapping());
                }
            }
        }

        return localInstance;
    }
}
