package net.styleru.ikomarov.domain_layer.di.providers;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.repository.users.IUsersRepository;

/**
 * Created by i_komarov on 29.05.17.
 */

public interface IUsersModelProvider {

    @NonNull
    IUsersRepository provideUsersRepository();
}
