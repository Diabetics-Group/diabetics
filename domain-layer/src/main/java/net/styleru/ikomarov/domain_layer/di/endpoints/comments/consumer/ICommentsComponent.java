package net.styleru.ikomarov.domain_layer.di.endpoints.comments.consumer;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 08.05.17.
 */

public interface ICommentsComponent {

    @NonNull
    CommentsRepositoryModule repository();
}
