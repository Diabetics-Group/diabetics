package net.styleru.ikomarov.domain_layer.di.level_2;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.managers.network.INetworkManager;
import net.styleru.ikomarov.data_layer.managers.network.NetworkManager;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 03.05.17.
 */

public class NetworkModule {

    private final Object lockNM = new Object();

    @NonNull
    private final IAppComponent component;

    @Nullable
    private volatile INetworkManager networkManager;

    public NetworkModule(@NonNull IAppComponent component) {
        this.component = component;
    }

    @NonNull
    public INetworkManager provideNetworkManager() {
        INetworkManager localInstance = networkManager;
        if(localInstance == null) {
            synchronized (lockNM) {
                localInstance = networkManager;
                if(localInstance == null) {
                    localInstance = networkManager = new NetworkManager(component.app().provideContext());
                }
            }
        }

        return localInstance;
    }
}

