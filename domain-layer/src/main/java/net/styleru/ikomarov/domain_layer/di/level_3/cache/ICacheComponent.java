package net.styleru.ikomarov.domain_layer.di.level_3.cache;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 03.05.17.
 */

public interface ICacheComponent {

    @NonNull
    CacheStrategyModule strategy();
}
