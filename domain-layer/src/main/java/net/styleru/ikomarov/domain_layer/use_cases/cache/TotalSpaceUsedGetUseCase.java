package net.styleru.ikomarov.domain_layer.use_cases.cache;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.services.database.IDatabaseService;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by i_komarov on 14.05.17.
 */

public class TotalSpaceUsedGetUseCase extends UseCase {

    @NonNull
    private final IDatabaseService service;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public TotalSpaceUsedGetUseCase(@NonNull IDatabaseService service,
                                    @NonNull Scheduler executeScheduler,
                                    @NonNull Scheduler postScheduler) {

        this.service = service;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public void execute(Callbacks callbacks) {
        service.totalSpaceUsed()
                .subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .subscribe(createTotalSpaceUsedLoadEventObserver(callbacks));
    }

    private DisposableSingleObserver<Long> createTotalSpaceUsedLoadEventObserver(Callbacks callbacks) {
        DisposableSingleObserver<Long> totalSpaceUsedLoadEventObserver = new TotalSpaceUsedGetEventObserver(callbacks);
        addDisposable(totalSpaceUsedLoadEventObserver);
        return totalSpaceUsedLoadEventObserver;
    }

    public interface Callbacks {

        void onTotalSpaceUsedLoaded(long totalSpace);

        void onTotalSpaceUsedLoadingFailure(ExceptionBundle error);
    }
}
