package net.styleru.ikomarov.domain_layer.di.level_2;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.preferences.user.IUserPreferences;
import net.styleru.ikomarov.data_layer.preferences.user.UserPreferences;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 03.05.17.
 */

public class PreferencesModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IAppComponent component;

    @Nullable
    private volatile IUserPreferences users;

    public PreferencesModule(@NonNull IAppComponent component) {
        this.component = component;
    }

    @NonNull
    public IUserPreferences provideUserPreferences() {
        IUserPreferences localInstance = users;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = users;
                if(localInstance == null) {
                    localInstance = users = new UserPreferences.Factory(component.app().provideContext()).create();
                }
            }
        }

        return localInstance;
    }
}
