package net.styleru.ikomarov.domain_layer.di.endpoints.authentication;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;
import net.styleru.ikomarov.domain_layer.repository.authentication.AuthenticationRepository;
import net.styleru.ikomarov.domain_layer.repository.authentication.IAuthenticationRepository;

/**
 * Created by i_komarov on 05.06.17.
 */

public class AuthenticationModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final ISessionComponent sessionComponent;

    @NonNull
    private final IEnvironmentComponent environmentComponent;

    @Nullable
    private volatile IAuthenticationRepository repository;

    public AuthenticationModule(@NonNull ISessionComponent sessionComponent,
                                @NonNull IEnvironmentComponent environmentComponent) {
        this.sessionComponent = sessionComponent;
        this.environmentComponent = environmentComponent;
    }

    @NonNull
    public IAuthenticationRepository provideAuthenticationRepository() {
        IAuthenticationRepository localInstance = repository;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = repository;
                if(localInstance == null) {
                    localInstance = repository = AuthenticationRepository.Factory.create(
                            environmentComponent.parse().provideJsonParser(),
                            sessionComponent.authentication().provideAuthenticationService()
                    );
                }
            }
        }

        return localInstance;
    }
}
