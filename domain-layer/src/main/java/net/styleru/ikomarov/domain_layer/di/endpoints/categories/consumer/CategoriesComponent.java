package net.styleru.ikomarov.domain_layer.di.endpoints.categories.consumer;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.categories.environment.ICategoriesEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;

/**
 * Created by i_komarov on 04.05.17.
 */

public class CategoriesComponent implements ICategoriesComponent {

    @NonNull
    private final IEnvironmentComponent environment;

    @NonNull
    private final ICacheComponent cache;

    @NonNull
    private final ICategoriesEnvironmentComponent categorieEnvironment;

    @NonNull
    private final CategoriesRepositoryModule repository;

    public CategoriesComponent(@NonNull IEnvironmentComponent environment,
                               @NonNull ICacheComponent cache,
                               @NonNull ICategoriesEnvironmentComponent categoriesEnvironment) {

        this.environment = environment;
        this.cache = cache;
        this.categorieEnvironment = categoriesEnvironment;
        this.repository = new CategoriesRepositoryModule(environment, cache, categoriesEnvironment);
    }

    @NonNull
    @Override
    public CategoriesRepositoryModule repository() {
        return repository;
    }
}
