package net.styleru.ikomarov.domain_layer.mapping.comments;

import net.styleru.ikomarov.data_layer.entities.local.comments.CommentLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.comments.CommentDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 06.05.17.
 */

public class LocalCommentMapper implements Function<CommentLocalEntity, CommentDTO> {
    @Override
    public CommentDTO apply(CommentLocalEntity entity) throws Exception {
        return new CommentDTO.Builder(entity.getContent())
                .mutateSynchronized(entity.getId())
                .withDate(entity.getDate())
                .withUser(entity.getFirstName(), entity.getLastName(), entity.getImageUrl(), entity.getRole())
                .create();
    }
}
