package net.styleru.ikomarov.domain_layer.di.level_2;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;

/**
 * Created by i_komarov on 03.05.17.
 */

public interface IEnvironmentComponent {

    @NonNull
    ICacheComponent plusCacheComponent();

    @NonNull
    ISessionComponent plusSessionComponent();

    @NonNull
    DatabaseModule db();

    @NonNull
    ErrorTranslationModule errors();

    @NonNull
    LoggingModule logging();

    @NonNull
    NetworkModule network();

    @NonNull
    PreferencesModule preferences();

    @NonNull
    SessionlessClientModule sessionlessClients();

    @NonNull
    ParseModule parse();
}
