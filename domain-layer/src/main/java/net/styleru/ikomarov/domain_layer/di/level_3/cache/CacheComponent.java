package net.styleru.ikomarov.domain_layer.di.level_3.cache;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;

/**
 * Created by i_komarov on 03.05.17.
 */

public class CacheComponent implements ICacheComponent {

    @NonNull
    private final IEnvironmentComponent parent;

    @NonNull
    private final CacheStrategyModule strategy;

    public CacheComponent(@NonNull IEnvironmentComponent parent) {
        this.parent = parent;
        this.strategy = new CacheStrategyModule(parent);
    }

    @NonNull
    @Override
    public CacheStrategyModule strategy() {
        return strategy;
    }
}
