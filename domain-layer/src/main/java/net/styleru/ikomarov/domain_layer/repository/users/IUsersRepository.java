package net.styleru.ikomarov.domain_layer.repository.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 15.05.17.
 */

public interface IUsersRepository {

    Observable<UserDTO> get(@NonNull String userId);

    Observable<List<UserDTO>> list(int offset, int limit);
}
