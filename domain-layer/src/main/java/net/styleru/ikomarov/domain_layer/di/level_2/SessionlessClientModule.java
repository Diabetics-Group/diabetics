package net.styleru.ikomarov.domain_layer.di.level_2;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.http.base.IClientFactory;
import net.styleru.ikomarov.data_layer.http.clients.SessionlessClientsFactory;

import okhttp3.OkHttpClient;

/**
 * Created by i_komarov on 03.05.17.
 */

public class SessionlessClientModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IClientFactory factory;

    @Nullable
    private volatile OkHttpClient client;

    public SessionlessClientModule() {
        factory = new SessionlessClientsFactory();
    }

    @NonNull
    public OkHttpClient provideRawClient() {
        OkHttpClient localInstance = client;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = client;
                if(localInstance == null) {
                    localInstance = client = factory.create();
                }
            }
        }

        return localInstance;
    }
}
