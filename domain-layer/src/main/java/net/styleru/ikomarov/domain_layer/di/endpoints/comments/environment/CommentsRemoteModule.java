package net.styleru.ikomarov.domain_layer.di.endpoints.comments.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.comments.CommentsRemoteService;
import net.styleru.ikomarov.data_layer.services.comments.ICommentsRemoteService;
import net.styleru.ikomarov.domain_layer.di.level_4.consumer.ISessionClientComponent;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsRemoteModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final ISessionClientComponent session;

    @Nullable
    private volatile ICommentsRemoteService service;

    public CommentsRemoteModule(@NonNull ISessionClientComponent session) {
        this.session = session;
    }

    @NonNull
    public ICommentsRemoteService provideRemoteService() {
        ICommentsRemoteService localInstance = service;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = service;
                if(localInstance == null) {
                    localInstance = service = new CommentsRemoteService.Factory(session.session().provideSessionClient()).create();
                }
            }
        }

        return localInstance;
    }
}
