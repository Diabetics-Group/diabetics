package net.styleru.ikomarov.domain_layer.use_cases.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UserLoadEventObserver<T> extends UseCase.BaseObserver<T> {

    @NonNull
    private final UserGetUseCase.Callbacks callbacks;

    public UserLoadEventObserver(@NonNull UserGetUseCase.Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onNext(T value) {
        if(!isDisposed()) {
            callbacks.onUserLoaded(value);
        }
    }

    @Override
    public void onError(Throwable e) {
        if(!isDisposed()) {
            callbacks.onUserLoadingFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
        }
    }

    @Override
    public void onComplete() {

    }
}
