package net.styleru.ikomarov.domain_layer.mapping.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 15.05.17.
 */

public class RemoteUsersMapper implements Function<List<UserRemoteEntity>, List<UserDTO>> {

    @NonNull
    private final Function<UserRemoteEntity, UserDTO> singleMapper;

    public RemoteUsersMapper(@NonNull Function<UserRemoteEntity, UserDTO> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<UserDTO> apply(List<UserRemoteEntity> userRemoteEntities) throws Exception {
        return Observable.fromIterable(userRemoteEntities)
                .map(singleMapper)
                .reduce(new ArrayList<UserDTO>(userRemoteEntities.size()), (list, element) -> {
                    list.add(element);
                    return list;
                })
                .blockingGet();
    }
}
