package net.styleru.ikomarov.domain_layer.use_cases.cache;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

/**
 * Created by i_komarov on 14.05.17.
 */

public class TotalSpaceUsedGetEventObserver extends UseCase.BaseSingleObserver<Long> {

    @NonNull
    private final TotalSpaceUsedGetUseCase.Callbacks callbacks;

    public TotalSpaceUsedGetEventObserver(@NonNull TotalSpaceUsedGetUseCase.Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onSuccess(Long value) {
        if(!isDisposed()) {
            callbacks.onTotalSpaceUsedLoaded(value);
        }
    }

    @Override
    public void onError(Throwable e) {
        if(!isDisposed()) {
            callbacks.onTotalSpaceUsedLoadingFailure(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
        }
    }
}
