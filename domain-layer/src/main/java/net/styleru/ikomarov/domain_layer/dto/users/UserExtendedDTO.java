package net.styleru.ikomarov.domain_layer.dto.users;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by i_komarov on 05.06.17.
 */

public class UserExtendedDTO extends UserDTO {

    @NonNull
    private final String token;

    public UserExtendedDTO(@NonNull String id,
                           @NonNull String firstName,
                           @NonNull String lastName,
                           @Nullable String imageUrl,
                           @NonNull String role,
                           @NonNull String token) {

        super(id, firstName, lastName, imageUrl, role);
        this.token = token;
    }

    @NonNull
    public String getToken() {
        return token;
    }
}
