package net.styleru.ikomarov.domain_layer.use_cases.comments;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;
import net.styleru.ikomarov.domain_layer.dto.comments.CommentDTO;
import net.styleru.ikomarov.domain_layer.repository.base.CreateResult;
import net.styleru.ikomarov.domain_layer.repository.comments.ICommentsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableMaybeObserver;

/**
 * Created by i_komarov on 27.05.17.
 */

public class CommentCreateUseCase extends UseCase {

    @NonNull
    private final ICommentsRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public CommentCreateUseCase(@NonNull ICommentsRepository repository,
                                @NonNull Scheduler executeScheduler,
                                @NonNull Scheduler postScheduler) {
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<CommentDTO, T> mapper, Callbacks<T> callbacks, String topicId, String userId, CommentDTO.Builder builder) {
        execute(repository.create(userId, topicId, builder), mapper, callbacks);
    }

    protected final <T> void execute(Single<CreateResult<CommentDTO>> upstream, Function<CommentDTO, T> mapper,  Callbacks<T> callbacks) {
        upstream.subscribeOn(executeScheduler)
                .filter(result -> {
                    if(result.getStatus() == OperationStatus.DELAYED) {
                        postScheduler.scheduleDirect(callbacks::onCommentCreationDelayed);
                        return false;
                    }

                    return true;
                })
                .map(CreateResult::getDto)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createCommentCreateEventObserver(callbacks));
    }

    private <T> DisposableMaybeObserver<T> createCommentCreateEventObserver(Callbacks<T> callbacks) {
        DisposableMaybeObserver<T> commentCreateEventObserver = new CommentCreateEventObserver<T>(callbacks);
        addDisposable(commentCreateEventObserver);
        return commentCreateEventObserver;
    }

    public interface Callbacks<T> {

        void onCommentCreated(T comment);

        void onCommentCreationFailed(ExceptionBundle error);

        void onCommentCreationDelayed();
    }
}
