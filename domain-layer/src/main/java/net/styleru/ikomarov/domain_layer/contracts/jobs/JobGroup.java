package net.styleru.ikomarov.domain_layer.contracts.jobs;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 11.05.17.
 */

public enum JobGroup {
    SYNCHRONIZATION ("sync"),
    ACTION          ("action");

    @NonNull
    private final String key;

    JobGroup(String key) {
        this.key = key;
    }

    @NonNull
    public String key() {
        return key;
    }
}
