package net.styleru.ikomarov.domain_layer.di.providers;

import android.content.Context;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.services.database.IDatabaseService;

/**
 * Created by i_komarov on 12.05.17.
 */

public interface IApplicationEnvironmentProvider {

    @NonNull
    Context provideContext();

    @NonNull
    IDatabaseService provideDatabaseService();
}
