package net.styleru.ikomarov.domain_layer.di.endpoints.authentication;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 05.06.17.
 */

public interface IAuthenticationComponent {

    @NonNull
    AuthenticationModule authentication();
}
