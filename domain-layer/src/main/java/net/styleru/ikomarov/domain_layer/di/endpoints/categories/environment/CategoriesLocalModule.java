package net.styleru.ikomarov.domain_layer.di.endpoints.categories.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.categories.CategoriesLocalService;
import net.styleru.ikomarov.data_layer.services.categories.ICategoriesLocalService;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;

/**
 * Created by i_komarov on 04.05.17.
 */

public class CategoriesLocalModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent component;

    @Nullable
    private volatile ICategoriesLocalService service;

    public CategoriesLocalModule(@NonNull IEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public ICategoriesLocalService provideLocalService() {
        ICategoriesLocalService localInstance = service;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = service;
                if(localInstance == null) {
                    localInstance = service = new CategoriesLocalService.Factory(component.db().provideDatabase()).create();
                }
            }
        }

        return localInstance;
    }
}
