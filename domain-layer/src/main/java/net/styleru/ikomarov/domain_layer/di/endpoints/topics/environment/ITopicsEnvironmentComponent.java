package net.styleru.ikomarov.domain_layer.di.endpoints.topics.environment;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.topics.consumer.ITopicsComponent;

/**
 * Created by i_komarov on 04.05.17.
 */

public interface ITopicsEnvironmentComponent {

    @NonNull
    ITopicsComponent plusTopicsComponent();

    @NonNull
    TopicsLocalModule local();

    @NonNull
    TopicsRemoteModule remote();

    @NonNull
    TopicsMappingModule mapping();
}
