package net.styleru.ikomarov.domain_layer.validation.base;

import android.util.Log;
import android.util.Pair;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

import io.reactivex.Single;

/**
 * Created by i_komarov on 05.06.17.
 */

public class ValidationUtils {

    private ValidationUtils() {
        throw new IllegalStateException("No instances please!");
    }

    public static <T> Single<T> createSingleWithPreValidation(T element, IValidationStrategy<T> validator) {
        return Single.create(emitter -> {
            try {
                validator.apply(element);
                emitter.onSuccess(element);
            } catch(ExceptionBundle e) {
                emitter.onError(e);
            }
        });
    }

    public static <T1, T2> Single<Pair<T1, T2>> createSingleWithPreValidation(T1 element, IValidationStrategy<T1> validator, T2 extra) {
        return Single.create(emitter -> {
            try {
                validator.apply(element);
                emitter.onSuccess(new Pair<T1, T2>(element, extra));
            } catch(ExceptionBundle e) {
                emitter.onError(e.copy());
            }
        });
    }
}
