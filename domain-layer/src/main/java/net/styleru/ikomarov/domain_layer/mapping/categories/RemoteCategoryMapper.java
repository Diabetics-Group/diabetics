package net.styleru.ikomarov.domain_layer.mapping.categories;

import net.styleru.ikomarov.data_layer.entities.remote.categories.CategoryRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.categories.CategoryDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 02.05.17.
 */

public class RemoteCategoryMapper implements Function<CategoryRemoteEntity, CategoryDTO> {

    @Override
    public CategoryDTO apply(CategoryRemoteEntity entity) throws Exception {
        return new CategoryDTO(
                entity.getId(),
                entity.getName(),
                entity.getLastUpdate(),
                entity.getTopicsCount(),
                true
        );
    }
}
