package net.styleru.ikomarov.domain_layer.di.endpoints.categories.consumer;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 04.05.17.
 */

public interface ICategoriesComponent {

    @NonNull
    CategoriesRepositoryModule repository();
}
