package net.styleru.ikomarov.domain_layer.use_cases.cache;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.services.database.IDatabaseService;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by i_komarov on 13.05.17.
 */

public class InvalidateCacheUseCase extends UseCase {

    @NonNull
    private final IDatabaseService service;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public InvalidateCacheUseCase(@NonNull IDatabaseService service, @NonNull Scheduler executeScheduler, @NonNull Scheduler postScheduler) {
        this.service = service;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public void execute(Callbacks callbacks) {
        service.invalidateCache()
                .subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .subscribe(createInvalidateCacheEventObserver(callbacks));
    }

    private DisposableSingleObserver<Boolean> createInvalidateCacheEventObserver(Callbacks callbacks) {
        DisposableSingleObserver<Boolean> invalidateCacheEventObserver = new InvalidateCacheResultObserver(callbacks);
        addDisposable(invalidateCacheEventObserver);
        return invalidateCacheEventObserver;
    }

    public interface Callbacks {

        void onCacheInvalidated();

        void onCacheInvalidationFailure(ExceptionBundle error);
    }
}
