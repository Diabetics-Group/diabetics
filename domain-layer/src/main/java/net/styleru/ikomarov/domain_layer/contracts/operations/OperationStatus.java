package net.styleru.ikomarov.domain_layer.contracts.operations;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;

/**
 * Created by i_komarov on 04.05.17.
 */

public enum OperationStatus implements Parcelable {
    SUCCESS(0x0000000),
    DELAYED(0x0000001);

    private static final SparseArray<OperationStatus> statusMap = new SparseArray<>(OperationStatus.values().length);

    static {
        for(OperationStatus status : OperationStatus.values()) {
            statusMap.put(status.code, status);
        }
    }

    private final int code;

    public static OperationStatus forCode(int code) {
        return statusMap.get(code);
    }

    OperationStatus(int code) {
        this.code = code;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OperationStatus> CREATOR = new Creator<OperationStatus>() {
        @Override
        public OperationStatus createFromParcel(Parcel in) {
            return OperationStatus.forCode(in.readInt());
        }

        @Override
        public OperationStatus[] newArray(int size) {
            return new OperationStatus[size];
        }
    };
}
