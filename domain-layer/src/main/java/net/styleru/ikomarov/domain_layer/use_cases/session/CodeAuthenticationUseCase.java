package net.styleru.ikomarov.domain_layer.use_cases.session;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.users.UserExtendedDTO;
import net.styleru.ikomarov.domain_layer.repository.authentication.IAuthenticationRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;
import net.styleru.ikomarov.domain_layer.validation.base.IValidationStrategy;
import net.styleru.ikomarov.domain_layer.validation.base.ValidationUtils;

import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by i_komarov on 04.06.17.
 */

public class CodeAuthenticationUseCase extends UseCase {

    @NonNull
    private final IAuthenticationRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    @NonNull
    private final IValidationStrategy<String> codeValidationStrategy;

    public CodeAuthenticationUseCase(@NonNull IAuthenticationRepository repository,
                                     @NonNull Scheduler executeScheduler,
                                     @NonNull Scheduler postScheduler,
                                     @NonNull IValidationStrategy<String> codeValidationStrategy) {
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
        this.codeValidationStrategy = codeValidationStrategy;
    }

    public <T> void execute(Callbacks<T> callbacks, Function<UserExtendedDTO, T> mapper, String phone, String code) {
        ValidationUtils.createSingleWithPreValidation(code, codeValidationStrategy, phone)
                .flatMap(data -> repository.token(data.second, data.first))
                .map(mapper)
                .subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .subscribe(createCodeVerificationEventObserver(callbacks));
    }

    private <T> DisposableSingleObserver<T> createCodeVerificationEventObserver(Callbacks<T> callbacks) {
        DisposableSingleObserver<T> codeVerificationEventObserver = new CodeAuthenticationEventsObserver(callbacks);
        addDisposable(codeVerificationEventObserver);
        return codeVerificationEventObserver;
    }

    public interface Callbacks<T> {

        void onStartCodeAuthentication();

        void onCodeAuthenticationSuccess(@NonNull T model);

        void onCodeVerificationFailure(ExceptionBundle error);

        void onFinishCodeAuthentication();
    }
}
