package net.styleru.ikomarov.domain_layer.use_cases.session;

import android.support.annotation.NonNull;
import android.util.Log;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.repository.authentication.IAuthenticationRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;
import net.styleru.ikomarov.domain_layer.validation.base.IValidationStrategy;
import net.styleru.ikomarov.domain_layer.validation.base.ValidationUtils;

import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by i_komarov on 04.06.17.
 */

public class PhoneAuthenticationUseCase extends UseCase {

    @NonNull
    private final IAuthenticationRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    @NonNull
    private final IValidationStrategy<String> phoneValidationStrategy;

    public PhoneAuthenticationUseCase(@NonNull IAuthenticationRepository repository,
                                      @NonNull Scheduler executeScheduler,
                                      @NonNull Scheduler postScheduler,
                                      @NonNull IValidationStrategy<String> phoneValidationStrategy) {
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
        this.phoneValidationStrategy = phoneValidationStrategy;
    }

    public void execute(@NonNull Callbacks callbacks, @NonNull String phone, boolean shouldUseValidation) {
        ValidationUtils.createSingleWithPreValidation(phone, shouldUseValidation ? phoneValidationStrategy : (var) -> true)
                .flatMap(repository::code)
                .subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .map(var -> phone)
                .subscribe(createPhoneVerificationEventObserver(callbacks));
    }

    private DisposableSingleObserver<String> createPhoneVerificationEventObserver(Callbacks callbacks) {
        DisposableSingleObserver<String> phoneVerificationEventObserver = new PhoneAuthenticationEventsObserver(callbacks);
        addDisposable(phoneVerificationEventObserver);
        return phoneVerificationEventObserver;
    }

    public interface Callbacks {

        void onPhoneAuthenticationStarted();

        void onPhoneAuthenticationSuccess(@NonNull String phone);

        void onPhoneAuthenticationFailure(ExceptionBundle error);

        void onPhoneAuthenticationFinished();
    }
}
