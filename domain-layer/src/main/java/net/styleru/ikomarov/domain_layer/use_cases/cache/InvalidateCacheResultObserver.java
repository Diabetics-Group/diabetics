package net.styleru.ikomarov.domain_layer.use_cases.cache;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

/**
 * Created by i_komarov on 13.05.17.
 */

public class InvalidateCacheResultObserver extends UseCase.BaseSingleObserver<Boolean> {

    @NonNull
    private final InvalidateCacheUseCase.Callbacks callbacks;

    public InvalidateCacheResultObserver(@NonNull InvalidateCacheUseCase.Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onSuccess(Boolean value) {
        if(!isDisposed()) {
            callbacks.onCacheInvalidated();
        }
    }

    @Override
    public void onError(Throwable e) {
        if(!isDisposed()) {
            callbacks.onCacheInvalidationFailure(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
        }
    }
}
