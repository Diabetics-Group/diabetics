package net.styleru.ikomarov.domain_layer.use_cases.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsLoadEventObserver<T> extends UseCase.BaseObserver<List<T>> {

    @NonNull
    private final TopicsGetUseCase.Callbacks<T> callbacks;

    public TopicsLoadEventObserver(@NonNull TopicsGetUseCase.Callbacks<T> callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onNext(List<T> value) {
        if(!isDisposed()) {
            callbacks.onTopicsLoaded(value);
        }
    }

    @Override
    public void onError(Throwable e) {
        if(!isDisposed()) {
            callbacks.onTopicsLoadingFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
        }
    }

    @Override
    public void onComplete() {

    }
}
