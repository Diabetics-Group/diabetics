package net.styleru.ikomarov.domain_layer.repository.categories;

import net.styleru.ikomarov.domain_layer.dto.categories.CategoryDTO;
import net.styleru.ikomarov.domain_layer.repository.base.IRepository;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 02.05.17.
 */

public interface ICategoriesRepository extends IRepository {

    Observable<List<CategoryDTO>> list(int offset, int limit);
}
