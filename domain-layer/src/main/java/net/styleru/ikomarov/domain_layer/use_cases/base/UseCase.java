package net.styleru.ikomarov.domain_layer.use_cases.base;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.exception.InternalExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.Reason;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;
import net.styleru.ikomarov.domain_layer.repository.base.CreateResult;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.MaybeTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.SingleTransformer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableMaybeObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by i_komarov on 29.04.17.
 */

public class UseCase {

    private CompositeDisposable disposables = new CompositeDisposable();

    protected final void addDisposable(Disposable disposable) {
        this.disposables.add(disposable);
    }

    public final void dispose() {
        disposables.clear();
    }

    public static abstract class BaseObserver<E> extends DisposableObserver<E> {

        @NonNull
        protected final ExceptionBundle newUseCaseError(Throwable e) {
            ExceptionBundle error = new ExceptionBundle(Reason.INTERNAL);
            InternalExceptionContract contract = Reason.INTERNAL.getContract();
            contract.putThrowable(error, e);
            contract.putMessage(error, e.getMessage());
            return error;
        }
    }

    public static abstract class BaseSingleObserver<E> extends DisposableSingleObserver<E> {

        @Override
        public void onStart() {

        }

        public void onComplete() {

        }

        @Override
        @CallSuper
        public void onError(Throwable error) {
            this.onComplete();
        }

        @Override
        @CallSuper
        public void onSuccess(E event) {
            this.onComplete();
        }

        @NonNull
        protected final ExceptionBundle newUseCaseError(Throwable e) {
            ExceptionBundle error = new ExceptionBundle(Reason.INTERNAL);
            InternalExceptionContract contract = Reason.INTERNAL.getContract();
            contract.putThrowable(error, e);
            contract.putMessage(error, e.getMessage());
            return error;
        }
    }

    public static abstract class BaseMaybeObserver<E> extends DisposableMaybeObserver<E> {

        @Override
        public void onStart() {

        }

        @Override
        public void onComplete() {

        }

        @NonNull
        protected final ExceptionBundle newUseCaseError(Throwable e) {
            ExceptionBundle error = new ExceptionBundle(Reason.INTERNAL);
            InternalExceptionContract contract = Reason.INTERNAL.getContract();
            contract.putThrowable(error, e);
            contract.putMessage(error, e.getMessage());
            return error;
        }
    }
}
