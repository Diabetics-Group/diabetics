package net.styleru.ikomarov.domain_layer.di.endpoints.users.consumer;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.di.endpoints.users.environment.IUsersEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.repository.users.IUsersRepository;
import net.styleru.ikomarov.domain_layer.repository.users.UsersRepository;

/**
 * Created by i_komarov on 29.05.17.
 */

public class UsersRepositoryModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent environment;

    @NonNull
    private final ICacheComponent cache;

    @NonNull
    private final IUsersEnvironmentComponent usersEnvironment;

    @Nullable
    private volatile IUsersRepository repository;

    public UsersRepositoryModule(@NonNull IEnvironmentComponent environment,
                                  @NonNull ICacheComponent cache,
                                  @NonNull IUsersEnvironmentComponent usersEnvironment) {

        this.environment = environment;
        this.cache = cache;
        this.usersEnvironment = usersEnvironment;
    }

    @NonNull
    public IUsersRepository provideRepository() {
        IUsersRepository localInstance = repository;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = repository;
                if(localInstance == null) {
                    localInstance = repository = new UsersRepository.Factory(
                            environment.parse().provideJsonParser(),
                            cache.strategy().provideCacheStrategy(),
                            usersEnvironment.remote().provideRemoteService(),
                            usersEnvironment.local().provideLocalService(),
                            usersEnvironment.mapping().provideListMapping()
                    ).create();
                }
            }
        }

        return localInstance;
    }
}
