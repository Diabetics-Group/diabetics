package net.styleru.ikomarov.domain_layer.mapping.comments;

import net.styleru.ikomarov.data_layer.entities.remote.comments.CommentRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.comments.CommentDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 06.05.17.
 */

public class RemoteCommentMapper implements Function<CommentRemoteEntity, CommentDTO> {

    @Override
    public CommentDTO apply(CommentRemoteEntity entity) throws Exception {
        return new CommentDTO.Builder(entity.getContent())
                .mutateSynchronized(entity.getId())
                .withDate(entity.getDate())
                .withUser(entity.getFirstName(), entity.getLastName(), entity.getImageUrl(), entity.getRole())
                .create();
    }
}
