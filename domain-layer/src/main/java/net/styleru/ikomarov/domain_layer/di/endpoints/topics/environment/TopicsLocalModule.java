package net.styleru.ikomarov.domain_layer.di.endpoints.topics.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.categories.CategoriesLocalService;
import net.styleru.ikomarov.data_layer.services.categories.ICategoriesLocalService;
import net.styleru.ikomarov.data_layer.services.topics.ITopicsLocalService;
import net.styleru.ikomarov.data_layer.services.topics.TopicsLocalService;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsLocalModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent component;

    @Nullable
    private volatile ITopicsLocalService service;

    public TopicsLocalModule(@NonNull IEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public ITopicsLocalService provideLocalService() {
        ITopicsLocalService localInstance = service;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = service;
                if(localInstance == null) {
                    localInstance = service = new TopicsLocalService.Factory(component.db().provideDatabase()).create();
                }
            }
        }

        return localInstance;
    }
}
