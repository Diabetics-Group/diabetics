package net.styleru.ikomarov.domain_layer.di.level_3.cache;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.cache.strategy.CacheStrategy;
import net.styleru.ikomarov.domain_layer.cache.strategy.ICacheStrategy;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;

/**
 * Created by i_komarov on 03.05.17.
 */

public class CacheStrategyModule {

    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent component;

    @Nullable
    private volatile ICacheStrategy strategy;

    public CacheStrategyModule(@NonNull IEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public ICacheStrategy provideCacheStrategy() {
        ICacheStrategy localInstance = strategy;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = strategy;
                if(localInstance == null) {
                    localInstance = strategy = new CacheStrategy(component.network().provideNetworkManager());
                }
            }
        }

        return localInstance;
    }
}
