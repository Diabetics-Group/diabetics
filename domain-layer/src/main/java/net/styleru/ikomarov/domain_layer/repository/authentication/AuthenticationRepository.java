package net.styleru.ikomarov.domain_layer.repository.authentication;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;
import net.styleru.ikomarov.data_layer.parsing.base.IParser;
import net.styleru.ikomarov.data_layer.services.authentication.IAuthenticationService;
import net.styleru.ikomarov.domain_layer.dto.users.UserExtendedDTO;
import net.styleru.ikomarov.domain_layer.mapping.users.LocalUserExtendedMapper;
import net.styleru.ikomarov.domain_layer.mapping.users.LocalUsersExtendedMapper;
import net.styleru.ikomarov.domain_layer.mapping.users.RemoteUserExtendedMapper;
import net.styleru.ikomarov.domain_layer.mapping.users.RemoteUsersExtendedMapper;
import net.styleru.ikomarov.domain_layer.utils.RepositoryUtils;

import java.net.HttpURLConnection;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;

/**
 * Created by i_komarov on 04.06.17.
 */

public class AuthenticationRepository implements IAuthenticationRepository {

    @NonNull
    private final IParser<ResponseBody, String> jsonParser;

    @NonNull
    private final IAuthenticationService service;

    @NonNull
    private final Function<UserLocalEntity, UserExtendedDTO> localSingleMapper;

    @NonNull
    private final Function<List<UserLocalEntity>, List<UserExtendedDTO>> localListMapper;

    @NonNull
    private final Function<UserRemoteEntity, UserExtendedDTO> remoteSingleMapper;

    @NonNull
    private final Function<List<UserRemoteEntity>, List<UserExtendedDTO>> remoteListMapper;



    private AuthenticationRepository(@NonNull IParser<ResponseBody, String> jsonParser,
                                    @NonNull IAuthenticationService service) {

        this.jsonParser = jsonParser;
        this.service = service;
        this.localSingleMapper = new LocalUserExtendedMapper();
        this.localListMapper = new LocalUsersExtendedMapper(localSingleMapper);
        this.remoteSingleMapper = new RemoteUserExtendedMapper();
        this.remoteListMapper = new RemoteUsersExtendedMapper(remoteSingleMapper);
    }

    @Override
    public Single<Boolean> code(@NonNull String phone) {
        return service.code(phone)
                .flatMap(RepositoryUtils.verifyResponse(jsonParser, HttpURLConnection.HTTP_NO_CONTENT))
                .map(response -> true);
    }

    @Override
    public Single<UserExtendedDTO> token(@NonNull String phone, @NonNull String code) {
        return service.authorize(phone, code)
                .flatMap(RepositoryUtils.verifyResponse(jsonParser, HttpURLConnection.HTTP_OK, HttpURLConnection.HTTP_CREATED))
                .map(ResponseContainer::getBody)
                .map(remoteSingleMapper);
    }

    public static final class Factory {

        private Factory() {
            throw new IllegalStateException("No instances please!");
        }

        @NonNull
        public static IAuthenticationRepository create(@NonNull IParser<ResponseBody, String> jsonParser, IAuthenticationService service) {
            return new AuthenticationRepository(jsonParser, service);
        }
    }
}
