package net.styleru.ikomarov.domain_layer.di.level_3.session;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_4.consumer.ISessionClientComponent;

/**
 * Created by i_komarov on 03.05.17.
 */

public interface ISessionComponent {

    @NonNull
    ISessionClientComponent plusSessionClientComponent();

    @NonNull
    SessionModule session();

    @NonNull
    AuthenticationModule authentication();
}
