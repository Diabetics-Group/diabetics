package net.styleru.ikomarov.domain_layer.mapping.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.remote.topics.TopicRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 04.05.17.
 */

public class RemoteTopicsMapper implements Function<List<TopicRemoteEntity>, List<TopicDTO>> {

    @NonNull
    private final Function<TopicRemoteEntity, TopicDTO> singleMapper;

    public RemoteTopicsMapper(@NonNull Function<TopicRemoteEntity, TopicDTO> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<TopicDTO> apply(List<TopicRemoteEntity> topicRemoteEntities) throws Exception {
        return Observable.fromIterable(topicRemoteEntities)
                .map(singleMapper)
                .reduce(new ArrayList<TopicDTO>(), (list, element) -> {
                    list.add(element);
                    return list;
                }).blockingGet();
    }
}
