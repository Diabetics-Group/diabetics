package net.styleru.ikomarov.domain_layer.di.endpoints.comments.consumer;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.comments.environment.ICommentsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsComponent implements ICommentsComponent {

    @NonNull
    private final CommentsRepositoryModule repository;

    public CommentsComponent(@NonNull IEnvironmentComponent environment,
                           @NonNull ICacheComponent cache,
                           @NonNull ICommentsEnvironmentComponent commentsEnvironment) {

        this.repository = new CommentsRepositoryModule(environment, cache, commentsEnvironment);
    }

    @NonNull
    @Override
    public CommentsRepositoryModule repository() {
        return repository;
    }
}
