package net.styleru.ikomarov.domain_layer.di.level_3.session;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.managers.session.ISessionManager;
import net.styleru.ikomarov.data_layer.managers.session.SessionManager;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;

/**
 * Created by i_komarov on 03.05.17.
 */

public class SessionModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent environment;

    @Nullable
    private volatile ISessionManager manager;

    public SessionModule(@NonNull IEnvironmentComponent environment) {
        this.environment = environment;
    }

    @NonNull
    public ISessionManager provideSessionManager() {
        ISessionManager localInstance = manager;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = manager;
                if(localInstance == null) {
                    localInstance = manager = new SessionManager(
                            environment.preferences().provideUserPreferences(),
                            environment.network().provideNetworkManager(),
                            environment.sessionlessClients().provideRawClient()
                    );
                }
            }
        }

        return localInstance;
    }
}
