package net.styleru.ikomarov.domain_layer.di.endpoints.users.consumer;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.users.environment.IUsersEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;

/**
 * Created by i_komarov on 29.05.17.
 */

public class UsersComponent implements IUsersComponent {

    @NonNull
    private final UsersRepositoryModule repository;

    public UsersComponent(@NonNull IEnvironmentComponent environment,
                           @NonNull ICacheComponent cache,
                           @NonNull IUsersEnvironmentComponent usersEnvironment) {

        this.repository = new UsersRepositoryModule(environment, cache, usersEnvironment);
    }

    @NonNull
    @Override
    public UsersRepositoryModule repository() {
        return repository;
    }
}
