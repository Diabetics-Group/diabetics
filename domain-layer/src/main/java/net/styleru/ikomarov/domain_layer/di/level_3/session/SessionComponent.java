package net.styleru.ikomarov.domain_layer.di.level_3.session;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.consumer.ISessionClientComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.consumer.SessionClientComponent;

/**
 * Created by i_komarov on 03.05.17.
 */

public class SessionComponent implements ISessionComponent {

    @NonNull
    private final SessionModule session;

    @NonNull
    private final AuthenticationModule authentication;

    public SessionComponent(@NonNull IEnvironmentComponent environment) {
        this.session = new SessionModule(environment);
        this.authentication = new AuthenticationModule(environment);
    }

    @NonNull
    @Override
    public ISessionClientComponent plusSessionClientComponent() {
        return new SessionClientComponent(this);
    }

    @NonNull
    @Override
    public SessionModule session() {
        return session;
    }

    @NonNull
    @Override
    public AuthenticationModule authentication() {
        return authentication;
    }
}
