package net.styleru.ikomarov.domain_layer.di.endpoints.comments.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.entities.local.comments.CommentLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.comments.CommentRemoteEntity;
import net.styleru.ikomarov.data_layer.mappings.remote_to_local.comments.RemoteToLocalCommentEntityMapping;
import net.styleru.ikomarov.data_layer.mappings.remote_to_local.comments.RemoteToLocalCommentsEntityMapping;

import java.util.List;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.05.17.
 */

public class CommentsMappingModule {

    @NonNull
    private final Object singleLock = new Object();

    @NonNull
    private final Object listLock = new Object();

    @Nullable
    private volatile Function<CommentRemoteEntity, CommentLocalEntity> singleMapping;

    @Nullable
    private volatile Function<List<CommentRemoteEntity>, List<CommentLocalEntity>> listMapping;

    public CommentsMappingModule() {

    }

    @NonNull
    public Function<CommentRemoteEntity, CommentLocalEntity> provideSingleMapping() {
        Function<CommentRemoteEntity, CommentLocalEntity> localInstance = singleMapping;
        if(localInstance == null) {
            synchronized (singleLock) {
                localInstance = singleMapping;
                if(localInstance == null) {
                    localInstance = singleMapping = new RemoteToLocalCommentEntityMapping();
                }
            }
        }

        return localInstance;
    }

    @NonNull
    public Function<List<CommentRemoteEntity>, List<CommentLocalEntity>> provideListMapping() {
        Function<List<CommentRemoteEntity>, List<CommentLocalEntity>> localInstance = listMapping;
        if(localInstance == null) {
            synchronized (listLock) {
                localInstance = listMapping;
                if(localInstance == null) {
                    localInstance = listMapping = new RemoteToLocalCommentsEntityMapping(provideSingleMapping());
                }
            }
        }

        return localInstance;
    }
}
