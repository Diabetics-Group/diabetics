package net.styleru.ikomarov.domain_layer.dto.comments;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.dto.base.AbstractDTO;

import java.util.UUID;

/**
 * Created by i_komarov on 06.05.17.
 */

public class CommentDTO extends AbstractDTO {

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @Nullable
    private String imageUrl;

    @NonNull
    private String role;

    @NonNull
    private String content;

    @NonNull
    private String date;

    @NonNull
    private Boolean isSynchronized;

    public CommentDTO(@NonNull String id) {
        super(id);
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public String getRole() {
        return role;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    @NonNull
    public Boolean isSynchronized() {
        return isSynchronized;
    }

    public static final class Builder {

        private String content;



        public Builder(String content) {
            this.content = content;
        }

        public SynchronizedBuilder mutateSynchronized(String id) {
            CommentDTO dto = new CommentDTO(id);
            dto.content = content;
            return new SynchronizedBuilder(dto);
        }

        public CommentDTO create(UUID id) {
            CommentDTO dto = new CommentDTO(id.toString());
            dto.content = content;
            dto.isSynchronized = false;
            return dto;
        }

        public String getContent() {
            return this.content;
        }
    }

    public static final class SynchronizedBuilder {

        private CommentDTO dto;

        private SynchronizedBuilder(CommentDTO dto) {
            this.dto = dto;
            this.dto.isSynchronized = true;

        }

        public SynchronizedBuilder withUser(String firstName, String lastName, String imageUrl, String role) {
            dto.firstName = firstName;
            dto.lastName = lastName;
            dto.imageUrl = imageUrl;
            dto.role = role;
            return this;
        }

        public SynchronizedBuilder withDate(String lastUpdate) {
            dto.date = lastUpdate;
            return this;
        }

        public CommentDTO create() {
            return dto;
        }
    }
}
