package net.styleru.ikomarov.domain_layer.mapping.users;

import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 15.05.17.
 */

public class RemoteUserMapper implements Function<UserRemoteEntity, UserDTO> {

    @Override
    public UserDTO apply(UserRemoteEntity entity) throws Exception {
        return new UserDTO(
                entity.getId(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getImageUrl(),
                entity.getRole()
        );
    }
}
