package net.styleru.ikomarov.domain_layer.di.level_2;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.contracts.error.ErrorCodeTranslator;
import net.styleru.ikomarov.data_layer.contracts.error.ICodeTranslator;

/**
 * Created by i_komarov on 03.05.17.
 */

public class ErrorTranslationModule {

    private final Object lock = new Object();

    @Nullable
    private volatile ICodeTranslator diabeticsTranslator;

    public ErrorTranslationModule() {

    }

    @NonNull
    public ICodeTranslator provideDiabeticsTranslator() {
        ICodeTranslator localInstance = diabeticsTranslator;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = diabeticsTranslator;
                if(localInstance == null) {
                    localInstance = diabeticsTranslator = new ErrorCodeTranslator();
                }
            }
        }

        return localInstance;
    }
}
