package net.styleru.ikomarov.domain_layer.di.endpoints.topics.consumer;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.di.endpoints.topics.environment.ITopicsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.repository.categories.CategoriesRepository;
import net.styleru.ikomarov.domain_layer.repository.topics.ITopicsRepository;
import net.styleru.ikomarov.domain_layer.repository.topics.TopicsRepository;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsRepositoryModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent environment;

    @NonNull
    private final ICacheComponent cache;

    @NonNull
    private final ITopicsEnvironmentComponent topicsEnvironment;

    @Nullable
    private volatile ITopicsRepository repository;

    public TopicsRepositoryModule(@NonNull IEnvironmentComponent environment,
                                      @NonNull ICacheComponent cache,
                                      @NonNull ITopicsEnvironmentComponent topicsEnvironment) {

        this.environment = environment;
        this.cache = cache;
        this.topicsEnvironment = topicsEnvironment;
    }

    @NonNull
    public ITopicsRepository provideRepository() {
        ITopicsRepository localInstance = repository;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = repository;
                if(localInstance == null) {
                    localInstance = repository = new TopicsRepository.Factory(
                            environment.parse().provideJsonParser(),
                            cache.strategy().provideCacheStrategy(),
                            topicsEnvironment.remote().provideRemoteService(),
                            topicsEnvironment.local().provideLocalService(),
                            topicsEnvironment.mapping().provideListMapping()
                    ).create();
                }
            }
        }

        return localInstance;
    }
}
