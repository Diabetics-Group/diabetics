package net.styleru.ikomarov.domain_layer.repository.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;
import net.styleru.ikomarov.domain_layer.repository.base.CreateResult;
import net.styleru.ikomarov.domain_layer.repository.base.IRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by i_komarov on 04.05.17.
 */

public interface ITopicsRepository extends IRepository {

    Single<CreateResult<TopicDTO>> create(@NonNull String categoryId, @NonNull String userId, @NonNull TopicDTO.Builder builder);

    Observable<TopicDTO> get(@NonNull String id);

    Observable<List<TopicDTO>> list(@NonNull String categoryId, int offset, int limit);
}
