package net.styleru.ikomarov.domain_layer.di.endpoints.categories.consumer;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.di.endpoints.categories.environment.ICategoriesEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.consumer.ISessionClientComponent;
import net.styleru.ikomarov.domain_layer.repository.categories.CategoriesRepository;
import net.styleru.ikomarov.domain_layer.repository.categories.ICategoriesRepository;

/**
 * Created by i_komarov on 04.05.17.
 */

public class CategoriesRepositoryModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent environment;

    @NonNull
    private final ICacheComponent cache;

    @NonNull
    private final ICategoriesEnvironmentComponent categoriesEnvironment;

    @Nullable
    private volatile ICategoriesRepository repository;

    public CategoriesRepositoryModule(@NonNull IEnvironmentComponent environment,
                                      @NonNull ICacheComponent cache,
                                      @NonNull ICategoriesEnvironmentComponent categoriesEnvironment) {

        this.environment = environment;
        this.cache = cache;
        this.categoriesEnvironment = categoriesEnvironment;
    }

    @NonNull
    public ICategoriesRepository provideRepository() {
        ICategoriesRepository localInstance = repository;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = repository;
                if(localInstance == null) {
                    localInstance = repository = new CategoriesRepository.Factory(
                            environment.parse().provideJsonParser(),
                            cache.strategy().provideCacheStrategy(),
                            categoriesEnvironment.remote().provideRemoteService(),
                            categoriesEnvironment.local().provideLocalService(),
                            categoriesEnvironment.mapping().provideListMapping()
                    ).create();
                }
            }
        }

        return localInstance;
    }
}
