package net.styleru.ikomarov.domain_layer.use_cases.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.topics.TopicDTO;
import net.styleru.ikomarov.domain_layer.repository.topics.ITopicsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 04.05.17.
 */

public class TopicsGetUseCase extends UseCase {

    @NonNull
    private final ITopicsRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public TopicsGetUseCase(@NonNull ITopicsRepository repository,
                            @NonNull Scheduler executeScheduler,
                            @NonNull Scheduler postScheduler) {

        super();
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<List<TopicDTO>, List<T>> mapper, Callbacks<T> callbacks, String categoryId, int offset, int limit) {
        execute(repository.list(categoryId, offset, limit), mapper, callbacks);
    }

    protected final <T> void execute(Observable<List<TopicDTO>> source, Function<List<TopicDTO>, List<T>> mapper, Callbacks<T> callbacks) {
        source.subscribeOn(executeScheduler)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createTopicsLoadEventObserver(callbacks));
    }

    private <T> DisposableObserver<List<T>> createTopicsLoadEventObserver(final Callbacks<T> callbacks) {
        DisposableObserver<List<T>> topicsLoadEventObserver = new TopicsLoadEventObserver<>(callbacks);
        addDisposable(topicsLoadEventObserver);
        return topicsLoadEventObserver;
    }

    public interface Callbacks<T> {

        void onTopicsLoaded(@NonNull List<T> viewObjectList);

        void onTopicsLoadingFailed(@NonNull ExceptionBundle error);
    }
}

