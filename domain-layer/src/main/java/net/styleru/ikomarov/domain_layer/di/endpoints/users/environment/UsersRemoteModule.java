package net.styleru.ikomarov.domain_layer.di.endpoints.users.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.users.IUsersRemoteService;
import net.styleru.ikomarov.data_layer.services.users.UsersRemoteService;
import net.styleru.ikomarov.domain_layer.di.level_4.consumer.ISessionClientComponent;

/**
 * Created by i_komarov on 29.05.17.
 */

public class UsersRemoteModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final ISessionClientComponent session;

    @Nullable
    private volatile IUsersRemoteService service;

    public UsersRemoteModule(@NonNull ISessionClientComponent session) {
        this.session = session;
    }

    @NonNull
    public IUsersRemoteService provideRemoteService() {
        IUsersRemoteService localInstance = service;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = service;
                if(localInstance == null) {
                    localInstance = service = new UsersRemoteService.Factory(session.session().provideSessionClient()).create();
                }
            }
        }

        return localInstance;
    }
}
