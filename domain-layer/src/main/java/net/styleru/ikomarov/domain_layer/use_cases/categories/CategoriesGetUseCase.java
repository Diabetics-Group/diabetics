package net.styleru.ikomarov.domain_layer.use_cases.categories;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.categories.CategoryDTO;
import net.styleru.ikomarov.domain_layer.repository.categories.ICategoriesRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 02.05.17.
 */

public class CategoriesGetUseCase extends UseCase {

    @NonNull
    private final ICategoriesRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public CategoriesGetUseCase(@NonNull ICategoriesRepository repository,
                                @NonNull Scheduler executeScheduler,
                                @NonNull Scheduler postScheduler) {

        super();
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<List<CategoryDTO>, List<T>> mapper, Callbacks<T> callbacks, int offset, int limit) {
        execute(repository.list(offset, limit), mapper, callbacks);
    }

    protected final <T> void execute(Observable<List<CategoryDTO>> source, Function<List<CategoryDTO>, List<T>> mapper, Callbacks<T> callbacks) {
        source.subscribeOn(executeScheduler)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createCategoriesLoadEventObserver(callbacks));
    }

    private <T> DisposableObserver<List<T>> createCategoriesLoadEventObserver(final Callbacks<T> callbacks) {
        DisposableObserver<List<T>> categoriesLoadEventObserver = new CategoriesLoadEventObserver<>(callbacks);
        addDisposable(categoriesLoadEventObserver);
        return categoriesLoadEventObserver;
    }

    public interface Callbacks<T> {

        void onCategoriesLoaded(@NonNull List<T> viewObjectList);

        void onCategoriesLoadingFailed(@NonNull ExceptionBundle error);
    }
}
