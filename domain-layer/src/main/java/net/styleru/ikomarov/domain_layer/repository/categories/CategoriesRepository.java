package net.styleru.ikomarov.domain_layer.repository.categories;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.local.categories.CategoryLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.categories.CategoryRemoteEntity;
import net.styleru.ikomarov.data_layer.parsing.base.IParser;
import net.styleru.ikomarov.data_layer.services.categories.ICategoriesLocalService;
import net.styleru.ikomarov.data_layer.services.categories.ICategoriesRemoteService;

import net.styleru.ikomarov.domain_layer.cache.strategy.ICacheStrategy;
import net.styleru.ikomarov.domain_layer.contracts.cache.CachePolicy;
import net.styleru.ikomarov.domain_layer.dto.categories.CategoryDTO;
import net.styleru.ikomarov.domain_layer.mapping.categories.LocalCategoriesMapper;
import net.styleru.ikomarov.domain_layer.mapping.categories.LocalCategoryMapper;
import net.styleru.ikomarov.domain_layer.mapping.categories.RemoteCategoriesMapper;
import net.styleru.ikomarov.domain_layer.mapping.categories.RemoteCategoryMapper;
import net.styleru.ikomarov.domain_layer.utils.RepositoryUtils;

import java.net.HttpURLConnection;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.functions.Function;

import okhttp3.ResponseBody;

import retrofit2.Response;

/**
 * Created by i_komarov on 02.05.17.
 */

public class CategoriesRepository implements ICategoriesRepository {

    @NonNull
    private final IParser<ResponseBody, String> jsonParser;

    @NonNull
    private final ICacheStrategy strategy;

    @NonNull
    private final ICategoriesRemoteService remoteService;

    @NonNull
    private final ICategoriesLocalService localService;

    @NonNull
    private final Function<List<CategoryRemoteEntity>, List<CategoryLocalEntity>> mapping;

    @NonNull
    private final Function<CategoryLocalEntity, CategoryDTO> localSingleMapper;

    @NonNull
    private final Function<List<CategoryLocalEntity>, List<CategoryDTO>> localListMapper;

    @NonNull
    private final Function<CategoryRemoteEntity, CategoryDTO> remoteSingleMapper;

    @NonNull
    private final Function<List<CategoryRemoteEntity>, List<CategoryDTO>> remoteListMapper;

    private CategoriesRepository(@NonNull IParser<ResponseBody, String> jsonParser,
                                 @NonNull ICacheStrategy strategy,
                                 @NonNull ICategoriesRemoteService remoteService,
                                 @NonNull ICategoriesLocalService localService,
                                 @NonNull Function<List<CategoryRemoteEntity>, List<CategoryLocalEntity>> mapping) {

        this.jsonParser = jsonParser;
        this.strategy = strategy;
        this.remoteService = remoteService;
        this.localService = localService;
        this.mapping = mapping;
        this.localSingleMapper = new LocalCategoryMapper();
        this.localListMapper = new LocalCategoriesMapper(localSingleMapper);
        this.remoteSingleMapper = new RemoteCategoryMapper();
        this.remoteListMapper = new RemoteCategoriesMapper(remoteSingleMapper);
    }

    @Override
    public Observable<List<CategoryDTO>> list(int offset, int limit) {
        final CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            return listFromDatabase(CachePolicy.CACHE_ONLY, offset, limit).toObservable();
        } else if(policy == CachePolicy.FORCE_NETWORK) {
            return listFromNetwork(policy, offset, limit).toObservable();
        } else {
            return listFromDatabase(policy, offset, limit).toObservable()
                    .compose(RepositoryUtils.ignoreLocalDatabaseEmptyError())
                    .compose(offset == 0 ?
                            withFetchFromNetwork(policy, offset, limit) :
                            withOptionalPostload(policy, offset, limit)
            );
        }
    }

    private ObservableTransformer<List<CategoryDTO>, List<CategoryDTO>> withFetchFromNetwork(CachePolicy policy, int offset, int limit) {
        return o -> o.flatMap(data -> data.size() == 0 ?
                listFromNetwork(policy, offset, limit).toObservable() :
                Observable.merge(
                        Observable.just(data),
                        listFromNetwork(policy, data.get(0).getLastUpdate())
                                .toObservable()
                                .onErrorResumeNext(Observable.never())
                )
        );
    }

    private ObservableTransformer<List<CategoryDTO>, List<CategoryDTO>> withOptionalPostload(CachePolicy policy, int offset, int limit) {
        return o -> o.flatMap(data -> {
            final int delta = limit - data.size();
            return delta == 0 ? Observable.just(data) :
                    Observable.merge(
                            Observable.just(data),
                            listFromNetwork(policy, offset + data.size(), delta)
                                    .toObservable()
                    );
        });
    }

    private Single<List<CategoryDTO>> listFromDatabase(CachePolicy policy, int offset, int limit) {
        return localService.list(offset, limit)
                .map(localListMapper)
                .flatMap(RepositoryUtils.withCheckOnLocalDatabaseEmpty(policy));
    }

    private Single<List<CategoryDTO>> listFromNetwork(@NonNull CachePolicy policy, @NonNull String lastModified) {
        return remoteService.list(lastModified)
                .compose(withListCache(HttpURLConnection.HTTP_OK, HttpURLConnection.HTTP_PARTIAL))
                .flatMap(RepositoryUtils.withCheckOnRemoteDatabaseEmpty(policy));
    }

    private Single<List<CategoryDTO>> listFromNetwork(@NonNull CachePolicy policy, int offset, int limit) {
        return remoteService.list(offset, limit)
                .compose(withListCache(HttpURLConnection.HTTP_OK, HttpURLConnection.HTTP_PARTIAL))
                .flatMap(RepositoryUtils.withCheckOnRemoteDatabaseEmpty(policy));
    }

    private SingleTransformer<Response<ResponseContainer<CategoryRemoteEntity>>, CategoryDTO> withSingleCache(Integer... successCodes) {
        return (o) -> o.flatMap(RepositoryUtils.verifyResponse(jsonParser, successCodes))
                .map(ResponseContainer::getBody)
                .doOnSuccess(this::cacheSingle)
                .map(remoteSingleMapper);
    }

    private SingleTransformer<Response<ResponseContainer<List<CategoryRemoteEntity>>>, List<CategoryDTO>> withListCache(Integer... successCodes) {
        return (o) -> o.flatMap(RepositoryUtils.verifyResponse(jsonParser, successCodes))
                .map(ResponseContainer::getBody)
                .doOnSuccess(this::cache)
                .map(remoteListMapper);
    }

    private void cacheSingle(CategoryRemoteEntity data) {
        cache(Collections.singletonList(data));
    }

    private void cache(List<CategoryRemoteEntity> data) {
        Single.just(data)
                .map(mapping)
                .flatMap(localService::create)
                .subscribe();
    }

    private void invalidateSingle(CategoryRemoteEntity data) {
        invalidate(Collections.singletonList(data));
    }

    private void invalidate(List<CategoryRemoteEntity> data) {
        Observable.fromIterable(data)
                .map(CategoryRemoteEntity::getId)
                .toList()
                .flatMap(localService::delete)
                .subscribe();
    }

    public static final class Factory {

        @NonNull
        private final IParser<ResponseBody, String> jsonParser;

        @NonNull
        private final ICacheStrategy strategy;

        @NonNull
        private final ICategoriesRemoteService remoteService;

        @NonNull
        private final ICategoriesLocalService localService;

        @NonNull
        private final Function<List<CategoryRemoteEntity>, List<CategoryLocalEntity>> mapping;

        public Factory(@NonNull IParser<ResponseBody, String> jsonParser,
                       @NonNull ICacheStrategy strategy,
                       @NonNull ICategoriesRemoteService remoteService,
                       @NonNull ICategoriesLocalService localService,
                       @NonNull Function<List<CategoryRemoteEntity>, List<CategoryLocalEntity>> mapping) {

            this.jsonParser = jsonParser;
            this.strategy = strategy;
            this.remoteService = remoteService;
            this.localService = localService;
            this.mapping = mapping;
        }

        @NonNull
        public ICategoriesRepository create() {
            return new CategoriesRepository(
                    jsonParser,
                    strategy,
                    remoteService,
                    localService,
                    mapping
            );
        }
    }
}
