package net.styleru.ikomarov.domain_layer.di.endpoints.categories.environment;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.categories.consumer.ICategoriesComponent;
import net.styleru.ikomarov.domain_layer.repository.categories.ICategoriesRepository;

/**
 * Created by i_komarov on 04.05.17.
 */

public interface ICategoriesEnvironmentComponent {

    @NonNull
    ICategoriesComponent plusCategoriesComponent();

    @NonNull
    CategoriesLocalModule local();

    @NonNull
    CategoriesRemoteModule remote();

    @NonNull
    CategoriesMappingModule mapping();
}
