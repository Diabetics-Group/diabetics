package net.styleru.ikomarov.domain_layer.di.level_2;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.CacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.session.SessionComponent;

/**
 * Created by i_komarov on 03.05.17.
 */

public class EnvironmentComponent implements IEnvironmentComponent {

    @NonNull
    private final DatabaseModule db;

    @NonNull
    private final ErrorTranslationModule errors;

    @NonNull
    private final LoggingModule logging;

    @NonNull
    private final NetworkModule network;

    @NonNull
    private final PreferencesModule preferences;

    @NonNull
    private final SessionlessClientModule rawClients;

    @NonNull
    private final ParseModule parse;

    public EnvironmentComponent(@NonNull IAppComponent component) {
        db = new DatabaseModule(component);
        errors = new ErrorTranslationModule();
        logging = new LoggingModule(component);
        network = new NetworkModule(component);
        preferences = new PreferencesModule(component);
        rawClients = new SessionlessClientModule();
        parse = new ParseModule();
    }

    @NonNull
    @Override
    public ICacheComponent plusCacheComponent() {
        return new CacheComponent(this);
    }

    @NonNull
    @Override
    public ISessionComponent plusSessionComponent() {
        return new SessionComponent(this);
    }

    @NonNull
    @Override
    public DatabaseModule db() {
        return db;
    }

    @NonNull
    @Override
    public ErrorTranslationModule errors() {
        return errors;
    }

    @NonNull
    @Override
    public LoggingModule logging() {
        return logging;
    }

    @NonNull
    @Override
    public NetworkModule network() {
        return network;
    }

    @NonNull
    @Override
    public PreferencesModule preferences() {
        return preferences;
    }

    @NonNull
    @Override
    public SessionlessClientModule sessionlessClients() {
        return rawClients;
    }

    @NonNull
    @Override
    public ParseModule parse() {
        return parse;
    }
}
