package net.styleru.ikomarov.domain_layer.di.level_4.consumer;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.http.base.IClientFactory;
import net.styleru.ikomarov.data_layer.http.interceptors.SessionIdentifyingInterceptor;
import net.styleru.ikomarov.data_layer.http.clients.SessionClientsFactory;
import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;

import okhttp3.OkHttpClient;

/**
 * Created by i_komarov on 03.05.17.
 */

public class SessionClientModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final ISessionComponent component;

    @NonNull
    private final IClientFactory factory;

    @Nullable
    private volatile OkHttpClient client;

    public SessionClientModule(@NonNull ISessionComponent component) {
        this.component = component;
        this.factory = new SessionClientsFactory(new SessionIdentifyingInterceptor(component.session().provideSessionManager()));
    }

    @NonNull
    public OkHttpClient provideSessionClient() {
        OkHttpClient localInstance = client;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = client;
                if(localInstance == null) {
                    localInstance = client = factory.create();
                }
            }
        }

        return localInstance;
    }
}
