package net.styleru.ikomarov.encryption.algorithms.elgamal.engine;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.encryption.algorithms.base.IKeyPair;
import net.styleru.ikomarov.encryption.algorithms.elgamal.data.ElGamalEncryptedData;
import net.styleru.ikomarov.encryption.algorithms.elgamal.data.ElGamalPlainData;
import net.styleru.ikomarov.encryption.algorithms.elgamal.generator.ElGamalGenerator;
import net.styleru.ikomarov.encryption.algorithms.elgamal.keys.ElGamalKeyPair;
import net.styleru.ikomarov.encryption.algorithms.elgamal.keys.ElGamalPrivateKey;
import net.styleru.ikomarov.encryption.algorithms.elgamal.keys.ElGamalPublicKey;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.Random;


/**
 * Created by i_komarov on 01.06.17.
 */
public class ElGamalEngineTest {

    @NonNull
    private ElGamalGenerator generator;

    @NonNull
    private ElGamalEngine engine;

    @NonNull
    private IKeyPair<ElGamalPublicKey, ElGamalPrivateKey> keys;

    @Before
    public void setup() {
        generator = new ElGamalGenerator();
        engine = new ElGamalEngine(keys = generator.generate());
    }

    @Test
    public void test_encrypt_decrypt() {
        ElGamalPlainData sourceData = new ElGamalPlainData(new BigInteger("Fuck the one who created the cryptography".getBytes(Charset.forName("UTF-8"))));
        ElGamalEncryptedData encryptedData = engine.encrypt(sourceData);
        ElGamalPlainData decryptedData = engine.decrypt(encryptedData);
        System.out.println(new String(decryptedData.getData().toByteArray()));
        assert decryptedData.getData().equals(sourceData.getData());
    }
}