package net.styleru.ikomarov.encryption.algorithms.elgamal.data;

import android.support.annotation.NonNull;

import java.math.BigInteger;

/**
 * Created by i_komarov on 01.06.17.
 */

public class ElGamalEncryptedData {

    @NonNull
    private final BigInteger a;

    @NonNull
    private final BigInteger b;

    public ElGamalEncryptedData(@NonNull BigInteger a, @NonNull BigInteger b) {
        this.a = a;
        this.b = b;
    }

    @NonNull
    public BigInteger getA() {
        return a;
    }

    @NonNull
    public BigInteger getB() {
        return b;
    }
}
