package net.styleru.ikomarov.encryption.algorithms.api;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 31.05.17.
 */

public interface IDecoder {

    @NonNull
    String decode(@NonNull String in) throws Exception;
}
