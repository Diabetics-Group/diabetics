package net.styleru.ikomarov.encryption.algorithms.elgamal.engine;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.encryption.algorithms.base.ICryptographyEngine;
import net.styleru.ikomarov.encryption.algorithms.base.IKeyPair;
import net.styleru.ikomarov.encryption.algorithms.elgamal.data.ElGamalEncryptedData;
import net.styleru.ikomarov.encryption.algorithms.elgamal.data.ElGamalPlainData;
import net.styleru.ikomarov.encryption.algorithms.elgamal.keys.ElGamalKeyPair;
import net.styleru.ikomarov.encryption.algorithms.elgamal.keys.ElGamalPrivateKey;
import net.styleru.ikomarov.encryption.algorithms.elgamal.keys.ElGamalPublicKey;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by i_komarov on 01.06.17.
 */

public class ElGamalEngine {

    @NonNull
    private final IKeyPair<ElGamalPublicKey, ElGamalPrivateKey> keys;

    @NonNull
    private final SecureRandom random;

    public ElGamalEngine(@NonNull IKeyPair<ElGamalPublicKey, ElGamalPrivateKey> keys) {
        this.keys = keys;
        this.random = new SecureRandom();
    }

    @NonNull
    public ElGamalEncryptedData encrypt(@NonNull ElGamalPlainData message) {
        if(message.getData().compareTo(keys.getPublicKey().getPrime()) >= 0) {
            throw new IllegalStateException("Message can not be more than the prime");
        }

        BigInteger sessionKey = generateSessionKey(random);
        return new ElGamalEncryptedData(
                keys.getPublicKey().getGenerator().modPow(sessionKey, keys.getPublicKey().getPrime()),
                keys.getPublicKey().getY().modPow(sessionKey, keys.getPublicKey().getPrime())
                        .multiply(message.getData())
                        .mod(keys.getPublicKey().getPrime())
        );
    }

    @NonNull
    public ElGamalPlainData decrypt(@NonNull ElGamalEncryptedData data) {
        return new ElGamalPlainData(
                data.getA().modPow(keys.getPrivateKey().getX(), keys.getPublicKey().getPrime())
                        .modInverse(keys.getPublicKey().getPrime())
                        .multiply(data.getB())
                        .mod(keys.getPublicKey().getPrime())
        );
    }

    @NonNull
    private BigInteger generateSessionKey(@NonNull SecureRandom random) {
        BigInteger key;
        key = new BigInteger(keys.getPublicKey().getPrime().bitCount() - 1, random);
        return key;
    }
}
