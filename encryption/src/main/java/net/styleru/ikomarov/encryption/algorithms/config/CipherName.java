package net.styleru.ikomarov.encryption.algorithms.config;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 31.05.17.
 */

public enum CipherName {
    ELGAMAL("ElGamal"),
    RSA("RSA");

    @NonNull
    private final String value;

    CipherName(@NonNull String value) {
        this.value = value;
    }

    @NonNull
    public String getValue() {
        return this.value;
    }
}
