package net.styleru.ikomarov.encryption.algorithms.base;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 01.06.17.
 */

public interface IHashEngine {

    @NonNull
    byte[] hash(@NonNull byte[] message);
}
