package net.styleru.ikomarov.encryption.algorithms.elgamal.keys;

import android.support.annotation.NonNull;

import java.math.BigInteger;

/**
 * Created by i_komarov on 01.06.17.
 */

public class ElGamalPublicKey {

    @NonNull
    private final BigInteger prime;

    @NonNull
    private final BigInteger generator;

    @NonNull
    private final BigInteger y;

    public ElGamalPublicKey(@NonNull BigInteger prime,
                            @NonNull BigInteger generator,
                            @NonNull BigInteger y) {
        this.prime = prime;
        this.generator = generator;
        this.y = y;
    }


    @NonNull
    public BigInteger getPrime() {
        return prime;
    }

    @NonNull
    public BigInteger getGenerator() {
        return generator;
    }

    @NonNull
    public BigInteger getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ElGamalPublicKey that = (ElGamalPublicKey) o;

        if (!prime.equals(that.prime)) return false;
        if (!generator.equals(that.generator)) return false;
        return y.equals(that.y);

    }

    @Override
    public int hashCode() {
        int result = prime.hashCode();
        result = 31 * result + generator.hashCode();
        result = 31 * result + y.hashCode();
        return result;
    }
}
