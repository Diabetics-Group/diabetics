package net.styleru.ikomarov.encryption.algorithms.config;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 31.05.17.
 */

public enum CipherMode {
    /**
     * No mode
     * */
    NONE("NONE"),
    /**
     * Cipher Block Chaining Mode, as defined in <a href=http://csrc.nist.gov/publications/PubsFIPS.html>FIPS PUB 81</a>.
     * */
    CBC("CBC"),
    /**
     * Cipher Feedback Mode, as defined in <a href=http://csrc.nist.gov/publications/PubsFIPS.html>FIPS PUB 81</a>.
     *
     * Using modes such as CFB and OFB, block ciphers can encrypt data in units smaller than the cipher's actual
     * block size. When requesting such a mode, you may optionally specify the number of bits to be processed at
     * a time by appending this number to the mode name as shown in the "DES/CFB8/NoPadding" and "DES/OFB32/PKCS5Padding"
     * transformations. If no such number is specified, a provider-specific default is used.
     * (For example, the SunJCE provider uses a default of 64 bits for DES.) Thus, block ciphers can be turned
     * into byte-oriented stream ciphers by using an 8-bit mode such as CFB8 or OFB8.
     * */
    CFB("CFB"),
    /**
     * Cipher Feedback Mode, as defined in <a href=http://csrc.nist.gov/publications/PubsFIPS.html>FIPS PUB 81</a>.
     *
     * Using modes such as CFB and OFB, block ciphers can encrypt data in units smaller than the cipher's actual
     * block size. When requesting such a mode, you may optionally specify the number of bits to be processed at
     * a time by appending this number to the mode name as shown in the "DES/CFB8/NoPadding" and "DES/OFB32/PKCS5Padding"
     * transformations. If no such number is specified, a provider-specific default is used.
     * (For example, the SunJCE provider uses a default of 64 bits for DES.) Thus, block ciphers can be turned
     * into byte-oriented stream ciphers by using an 8-bit mode such as CFB8 or OFB8.
     * */
    CFBx("CFBx"),
    /**
     * A simplification of OFB, Counter mode updates the input block as a counter.
     * */
    CTR("CTR"),
    /**
     * Cipher Text Stealing, as described in Bruce Schneier's book Applied Cryptography-Second Edition,
     * John Wiley and Sons, 1996.
     * */
    CTS("CTS"),
    /**
     * Electronic Codebook Mode, as defined in <a href=http://csrc.nist.gov/publications/PubsFIPS.html>FIPS PUB 81</a>
     * (generally this mode should not be used for multiple blocks of data).
     * */
    ECB("ECB"),
    /**
     * Output Feedback Mode, as defined in <a href=http://csrc.nist.gov/publications/PubsFIPS.html>FIPS PUB 81</a>.
     * Using modes such as CFB and OFB, block ciphers can encrypt data in units smaller than the cipher's actual block size.
     * When requesting such a mode, you may optionally specify the number of bits to be processed at a time by appending
     * this number to the mode name as shown in the "DES/CFB8/NoPadding" and "DES/OFB32/PKCS5Padding" transformations.
     * If no such number is specified, a provider-specific default is used. (For example, the SunJCE provider uses
     * a default of 64 bits for DES.) Thus, block ciphers can be turned into byte-oriented stream ciphers by using
     * an 8-bit mode such as CFB8 or OFB8.
     * */
    OFB("OFB"),
    /**
     * Output Feedback Mode, as defined in <a href=http://csrc.nist.gov/publications/PubsFIPS.html>FIPS PUB 81</a>.
     * Using modes such as CFB and OFB, block ciphers can encrypt data in units smaller than the cipher's actual block size.
     * When requesting such a mode, you may optionally specify the number of bits to be processed at a time by appending
     * this number to the mode name as shown in the "DES/CFB8/NoPadding" and "DES/OFB32/PKCS5Padding" transformations.
     * If no such number is specified, a provider-specific default is used. (For example, the SunJCE provider uses
     * a default of 64 bits for DES.) Thus, block ciphers can be turned into byte-oriented stream ciphers by using
     * an 8-bit mode such as CFB8 or OFB8.
     * */
    OFBx("OFBx"),
    /**
     * Propagating Cipher Block Chaining, as defined by <a href=http://web.mit.edu/kerberos/>Kerberos V4</a>.
     * */
    PCBC("PCBC");

    @NonNull
    private final String value;

    CipherMode(@NonNull String value) {
        this.value = value;
    }

    @NonNull
    public String getValue() {
        return this.value;
    }
}
