package net.styleru.ikomarov.encryption.algorithms.elgamal.keys;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.encryption.algorithms.base.IKeyPair;

/**
 * Created by i_komarov on 01.06.17.
 */

public class ElGamalKeyPair implements IKeyPair<ElGamalPublicKey, ElGamalPrivateKey> {

    @NonNull
    private final ElGamalPublicKey publicKey;

    @NonNull
    private final ElGamalPrivateKey privateKey;

    public ElGamalKeyPair(@NonNull ElGamalPublicKey publicKey, @NonNull ElGamalPrivateKey privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    @NonNull
    @Override
    public ElGamalPublicKey getPublicKey() {
        return this.publicKey;
    }

    @NonNull
    @Override
    public ElGamalPrivateKey getPrivateKey() {
        return this.privateKey;
    }
}
