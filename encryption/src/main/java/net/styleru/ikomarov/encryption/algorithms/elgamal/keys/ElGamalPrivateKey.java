package net.styleru.ikomarov.encryption.algorithms.elgamal.keys;

import android.support.annotation.NonNull;

import java.math.BigInteger;

/**
 * Created by i_komarov on 01.06.17.
 */

public class ElGamalPrivateKey {

    @NonNull
    private final BigInteger x;

    public ElGamalPrivateKey(@NonNull BigInteger x) {
        this.x = x;
    }

    @NonNull
    public BigInteger getX() {
        return x;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ElGamalPrivateKey that = (ElGamalPrivateKey) o;

        return x.equals(that.x);

    }

    @Override
    public int hashCode() {
        return x.hashCode();
    }
}
