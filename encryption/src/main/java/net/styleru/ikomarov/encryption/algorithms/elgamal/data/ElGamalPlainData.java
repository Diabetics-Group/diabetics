package net.styleru.ikomarov.encryption.algorithms.elgamal.data;

import android.support.annotation.NonNull;

import java.math.BigInteger;

/**
 * Created by i_komarov on 01.06.17.
 */

public class ElGamalPlainData {

    @NonNull
    private final BigInteger data;

    public ElGamalPlainData(@NonNull BigInteger data) {
        this.data = data;
    }

    @NonNull
    public BigInteger getData() {
        return data;
    }
}
