package net.styleru.ikomarov.data_layer.managers.base;

import android.support.annotation.NonNull;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by i_komarov on 26.04.17.
 */

final class IdentifiableObservableCallbackObservableOnSubscribe<E> implements ObservableOnSubscribe<E> {

    @NonNull
    private final String key;

    @NonNull
    private final CallbacksManager<E, IdentifiableObservableCallback<E>> manager;

    IdentifiableObservableCallbackObservableOnSubscribe(@NonNull String key, @NonNull CallbacksManager<E, IdentifiableObservableCallback<E>> manager) {
        this.key = key;
        this.manager = manager;
    }

    @Override
    public void subscribe(ObservableEmitter<E> e) throws Exception {
        IdentifiableObservableCallback<E> callbacks = new IdentifiableObservableCallback<E>(key, e);
        e.setDisposable(callbacks.createDisposable(manager));
        manager.registerListener(key, new IdentifiableObservableCallback<>(key, e));
    }
}
