package net.styleru.ikomarov.data_layer.mappings.base;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 27.04.17.
 */

public class ResponseContainer {

    @NonNull
    public static final String FIELD_BODY = "body";

    @NonNull
    public static final String FIELD_ERROR = "error";

    @NonNull
    public static final String FIELD_CODE = "code";
}
