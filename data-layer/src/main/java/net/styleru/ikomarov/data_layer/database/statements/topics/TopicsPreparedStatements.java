package net.styleru.ikomarov.data_layer.database.statements.topics;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.mappings.local.topics.TopicsLocalEntityMapping;

/**
 * Created by i_komarov on 26.04.17.
 */

public class TopicsPreparedStatements {

    @NonNull
    private final String SELECT_ALL;

    @NonNull
    private final String SELECT_OFFSET_LIMIT;

    @NonNull
    private final String SELECT_NOT_SYNCHRONIZED;

    @NonNull
    private final String SELECT_SINGLE;

    @NonNull
    private final SQLiteStatement INSERT_SINGLE;

    @NonNull
    private final SQLiteStatement DELETE_SINGLE;

    @NonNull
    private final SQLiteStatement DELETE_BY_CATEGORY;

    public TopicsPreparedStatements(@NonNull SQLiteDatabase db) {
        SELECT_ALL = "SELECT * FROM " + TopicsLocalEntityMapping.TABLE + " WHERE " + TopicsLocalEntityMapping.COLUMN_CATEGORY_ID + " = " + "?";
        SELECT_OFFSET_LIMIT = "SELECT * FROM " + TopicsLocalEntityMapping.TABLE + " WHERE " + TopicsLocalEntityMapping.COLUMN_CATEGORY_ID + " = " + "?" + " LIMIT ? OFFSET ?";
        SELECT_NOT_SYNCHRONIZED = "SELECT * FROM " + TopicsLocalEntityMapping.TABLE + " WHERE " + TopicsLocalEntityMapping.COLUMN_SYNCHRONIZATION_STATUS + " = 0";
        SELECT_SINGLE = "SELECT * FROM " + TopicsLocalEntityMapping.TABLE + " WHERE " + TopicsLocalEntityMapping.COLUMN_ID + " = " + "?";
        INSERT_SINGLE = db.compileStatement(
                "INSERT OR REPLACE INTO " + TopicsLocalEntityMapping.TABLE + " ( " +
                        TopicsLocalEntityMapping.COLUMN_ID + "," +
                        TopicsLocalEntityMapping.COLUMN_USER_ID + "," +
                        TopicsLocalEntityMapping.COLUMN_FIRST_NAME + "," +
                        TopicsLocalEntityMapping.COLUMN_LAST_NAME + "," +
                        TopicsLocalEntityMapping.COLUMN_IMAGE_URL + "," +
                        TopicsLocalEntityMapping.COLUMN_ROLE + "," +
                        TopicsLocalEntityMapping.COLUMN_CATEGORY_ID + "," +
                        TopicsLocalEntityMapping.COLUMN_TITLE + "," +
                        TopicsLocalEntityMapping.COLUMN_CONTENT + "," +
                        TopicsLocalEntityMapping.COLUMN_LAST_UPDATE + "," +
                        TopicsLocalEntityMapping.COLUMN_COMMENTS_COUNT + "," +
                        TopicsLocalEntityMapping.COLUMN_SYNCHRONIZATION_STATUS + ")" +
                " VALUES (" +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" +
                ");"
        );
        DELETE_SINGLE = db.compileStatement("DELETE FROM " + TopicsLocalEntityMapping.TABLE + " WHERE " + TopicsLocalEntityMapping.COLUMN_ID + " = " + "?");
        DELETE_BY_CATEGORY = db.compileStatement("DELETE FROM " + TopicsLocalEntityMapping.TABLE + " WHERE " + TopicsLocalEntityMapping.COLUMN_CATEGORY_ID + " = " + "?");
    }

    @NonNull
    public String selectAll(@NonNull String id) {
        String selectAll =  selectAll();
        selectAll = selectAll.replaceFirst("\\?", "\"" + id + "\"");
        return selectAll;
    }

    @NonNull
    private String selectAll() {
        return SELECT_ALL;
    }

    @NonNull
    public String selectOffsetLimit(@NonNull String id, int offset, int limit) {
        String selectAll =  selectAll();
        selectAll = selectAll.replaceFirst("\\?", "\"" + id + "\"");
        selectAll = selectAll.replaceFirst("\\?", String.valueOf(limit));
        selectAll = selectAll.replaceFirst("\\?", String.valueOf(offset));
        return selectAll;
    }

    @NonNull
    private String selectOffsetLimit() {
        return SELECT_OFFSET_LIMIT;
    }

    @NonNull
    public String selectNotSynchronized() {
        return SELECT_NOT_SYNCHRONIZED;
    }

    @NonNull
    public String selectSingle(@NonNull String id) {
        String selectSingle =  selectSingle();
        selectSingle = selectSingle.replaceFirst("\\?", "\"" + id + "\"");
        return selectSingle;
    }

    @NonNull
    private String selectSingle() {
        return SELECT_SINGLE;
    }

    @NonNull
    public SQLiteStatement insertSingle() {
        return INSERT_SINGLE;
    }

    @NonNull
    public SQLiteStatement deleteSingle() {
        return DELETE_SINGLE;
    }

    @NonNull
    public SQLiteStatement deleteByCategory() {
        return DELETE_BY_CATEGORY;
    }
}
