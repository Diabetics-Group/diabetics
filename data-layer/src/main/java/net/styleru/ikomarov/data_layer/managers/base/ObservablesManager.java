package net.styleru.ikomarov.data_layer.managers.base;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 26.04.17.
 */

public class ObservablesManager<E> {

    private final CallbacksManager<E, IdentifiableObservableCallback<E>> manager;

    public ObservablesManager() {
        this.manager = new CallbacksManager<>();
    }

    public Observable<E> createChannel(String key) {
        return Observable.create(new IdentifiableObservableCallbackObservableOnSubscribe<>(key, manager));
    }

    public void notifyAll(E event) {
        manager.notifyAll(event);
    }
}
