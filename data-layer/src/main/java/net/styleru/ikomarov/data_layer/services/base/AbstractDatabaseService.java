package net.styleru.ikomarov.data_layer.services.base;

import android.content.OperationApplicationException;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.os.OperationCanceledException;

import net.styleru.ikomarov.data_layer.contracts.exception.DatabaseExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.Reason;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

import java.io.FileNotFoundException;

/**
 * Created by i_komarov on 29.04.17.
 */

public abstract class AbstractDatabaseService {

    /**
     * This function parses the exception thrown into an instance of {@link net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle}.
     * @return an instance of {@link net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle} filled in according to {@link net.styleru.ikomarov.data_layer.contracts.exception.DatabaseExceptionContract}
     * */
    protected final ExceptionBundle parseException(Exception e) {
        int code;
        e.printStackTrace();
        ExceptionBundle error = new ExceptionBundle(Reason.DATABASE);
        DatabaseExceptionContract contract = Reason.DATABASE.getContract();
        contract.putCode(error, code = parseExceptionCode(e));
        contract.putIsSQLite(error, isSQLiteExceptionCode(code));
        contract.putMessage(error, e.getMessage());
        contract.putThrowable(error, e);
        return error;
    }

    /**
     * This function parses the exception into code indicating its type
     * @return -1 if the exception is unknown or the code, indicating particular exception type
     * */
    private int parseExceptionCode(Exception e) {
        int code;
        if (e instanceof FileNotFoundException) {
            code = 1;
        } else if (e instanceof IllegalArgumentException) {
            code = 2;
        } else if (e instanceof UnsupportedOperationException) {
            code = 3;
        } else if (e instanceof SQLiteAbortException) {
            code = 4;
        } else if (e instanceof SQLiteConstraintException) {
            code = 5;
        } else if (e instanceof SQLiteDatabaseCorruptException) {
            code = 6;
        } else if (e instanceof SQLiteFullException) {
            code = 7;
        } else if (e instanceof SQLiteDiskIOException) {
            code = 8;
        } else if (e instanceof SQLiteException) {
            code = 9;
        } else if (e instanceof OperationApplicationException) {
            code = 10;
        } else if (e instanceof OperationCanceledException) {
            code = 11;
        } else {
            code = -1;
        }

        return code;
    }

    /**
     * This function checks whether the code matches any of SQLite exceptions or not.
     * @return boolean indicating whether the code passed is matching the SQLite exception
     * */
    private boolean isSQLiteExceptionCode(int code) {
        return code != -1 &&
               code != 1 &&
               code != 3 &&
               code != 10 &&
               code != 11;
    }
}
