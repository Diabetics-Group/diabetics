package net.styleru.ikomarov.data_layer.preferences.app;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.BuildConfig;
import net.styleru.ikomarov.data_layer.contracts.preferences.PreferenceKey;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by i_komarov on 14.05.17.
 */

public class AppPreferences implements IAppPreferences, SharedPreferences.OnSharedPreferenceChangeListener {

    @NonNull
    private static final String DATABASE_NAME;

    @NonNull
    private static final Integer DATABASE_VERSION;

    @NonNull
    private static final Boolean OAUTH2_ENABLED;

    @NonNull
    private static final Boolean BUILD_DEBUG;

    static {
        DATABASE_NAME = BuildConfig.DATABASE_NAME;
        DATABASE_VERSION = BuildConfig.DATABASE_VERSION;
        OAUTH2_ENABLED = BuildConfig.OAUTH2_ENABLED;
        BUILD_DEBUG = BuildConfig.IS_DEBUG_BUILD;
    }

    @NonNull
    private final ReentrantLock lock;

    @NonNull
    private final SharedPreferences preferences;

    @NonNull
    private final List<IPreferencesChangeListener> listeners;

    public AppPreferences(@NonNull SharedPreferences preferences) {
        this.lock = new ReentrantLock(true);
        this.preferences = preferences;
        this.listeners = new LinkedList<>();
        this.init();
        this.preferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        try {
            lock.lock();
            for(IPreferencesChangeListener listener : listeners) {
                listener.onPreferenceChanged(sharedPreferences, PreferenceKey.forKey(key));
            }
        } finally {
            lock.unlock();
        }
    }

    @NonNull
    @Override
    public Boolean notificationsEnabled() {
        return preferences.getBoolean(PreferenceKey.PUSH_NOTIFICATIONS.getKey(), true);
    }

    @NonNull
    @Override
    public String versionName() {
        return preferences.getString(PreferenceKey.VERSION_NAME.getKey(), "0.0.0");
    }

    @NonNull
    @Override
    public Integer versionCode() {
        return preferences.getInt(PreferenceKey.VERSION_CODE.getKey(), 0);
    }

    @NonNull
    @Override
    public String databaseName() {
        return preferences.getString(PreferenceKey.DATABASE_NAME.getKey(), "%noname%");
    }

    @NonNull
    @Override
    public Integer databaseVersion() {
        return preferences.getInt(PreferenceKey.DATABASE_VERSION.getKey(), 0);
    }

    @NonNull
    @Override
    public Boolean isOAuth2FlowEnabled() {
        return preferences.getBoolean(PreferenceKey.OAUTH2_ENABLED.getKey(), false);
    }

    @NonNull
    @Override
    public Boolean isBuildDebug() {
        return preferences.getBoolean(PreferenceKey.BUILD_DEBUG.getKey(), true);
    }

    @Override
    public void registerPreferencesChangeListener(@NonNull IPreferencesChangeListener listener) {
        if(!listeners.contains(listener)) {
            try {
                lock.lock();
                if(!listeners.contains(listener)) {
                    listeners.add(listener);
                }
            } finally {
                lock.unlock();
            }
        }
    }

    @Override
    public void unregisterPreferencesChangeListener(@NonNull IPreferencesChangeListener listener) {
        if(listeners.contains(listener)) {
            try {
                lock.lock();
                if(listeners.contains(listener)) {
                    listeners.remove(listener);
                }
            } finally {
                lock.unlock();
            }
        }
    }

    private void init() {
        this.preferences.edit().putString(PreferenceKey.DATABASE_NAME.getKey(), DATABASE_NAME)
                .putInt(PreferenceKey.DATABASE_VERSION.getKey(), DATABASE_VERSION)
                .putBoolean(PreferenceKey.OAUTH2_ENABLED.getKey(), OAUTH2_ENABLED)
                .putBoolean(PreferenceKey.BUILD_DEBUG.getKey(), BUILD_DEBUG)
                .apply();
    }
}
