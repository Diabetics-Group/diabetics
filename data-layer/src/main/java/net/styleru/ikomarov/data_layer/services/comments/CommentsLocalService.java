package net.styleru.ikomarov.data_layer.services.comments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.database.results.DeleteResults;
import net.styleru.ikomarov.data_layer.database.results.InsertResults;
import net.styleru.ikomarov.data_layer.database.statements.comments.CommentsPreparedStatements;
import net.styleru.ikomarov.data_layer.entities.local.comments.CommentLocalEntity;
import net.styleru.ikomarov.data_layer.mappings.local.comments.CommentsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.services.base.AbstractDatabaseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import io.reactivex.Single;

/**
 * Created by i_komarov on 28.04.17.
 */

public class CommentsLocalService extends AbstractDatabaseService implements ICommentsLocalService {

    @NonNull
    private final ReentrantLock lock;

    @NonNull
    private final SQLiteDatabase db;

    @NonNull
    private final CommentsPreparedStatements statements;

    private CommentsLocalService(@NonNull SQLiteDatabase db) {
        this.lock = new ReentrantLock(true);
        this.db = db;
        this.statements = new CommentsPreparedStatements(db);
    }

    @Override
    public Single<InsertResults> create(List<CommentLocalEntity> entities) {
        return Single.create(emitter -> {
            try {
                long insertedRows = 0L;
                long updatedRows = 0L;
                Map<String, Boolean> statuses = new HashMap<>(entities.size());

                lock.lock();
                db.execSQL("PRAGMA synchronous=\'OFF\'");
                db.beginTransaction();

                SQLiteStatement statement = statements.insertSingle();
                for(CommentLocalEntity entity : entities) {
                    statement.clearBindings();
                    String id;

                    statement.bindString(1, id = entity.getId());
                    statement.bindString(2, entity.getTopicId());
                    statement.bindString(3, entity.getUserId());
                    statement.bindString(4, entity.getFirstName());
                    statement.bindString(5, entity.getLastName());
                    String imageUrl;
                    if(null != (imageUrl = entity.getImageUrl())) {
                        statement.bindString(6, imageUrl);
                    }
                    statement.bindString(7, entity.getRole());
                    statement.bindString(8, entity.getDate());
                    statement.bindString(9, entity.getContent());
                    statement.bindLong(10, entity.isSynchronized() ? 1L : 0L);

                    long status = statement.executeInsert();
                    if(status != -1) {
                        if(statuses.get(id) != null) {
                            updatedRows++;
                        } else {
                            insertedRows++;
                        }

                        statuses.put(id, true);
                    } else {
                        statuses.put(id, false);
                    }
                }

                db.setTransactionSuccessful();

                emitter.onSuccess(
                        new InsertResults(
                                CommentsLocalEntityMapping.TABLE,
                                statement.toString(),
                                insertedRows,
                                updatedRows,
                                statuses
                        )
                );
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                db.endTransaction();
                db.execSQL("PRAGMA synchronous=\'NORMAL\'");
                lock.unlock();
            }
        });
    }

    @Override
    public Single<List<CommentLocalEntity>> list(@NonNull String topicId) {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectAll(topicId), null);
                emitter.onSuccess(CommentLocalEntity.listFromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<List<CommentLocalEntity>> list(@NonNull String topicId, int offset, int limit) {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectOffsetLimit(topicId, offset, limit), null);
                emitter.onSuccess(CommentLocalEntity.listFromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<List<CommentLocalEntity>> listNotSynchronized() {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectNotSynchronized(), null);
                emitter.onSuccess(CommentLocalEntity.listFromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<DeleteResults> delete(@NonNull List<String> ids) {
        return Single.create(emitter -> {
            try {
                long deletedRows = 0L;
                Map<String, Boolean> statuses = new HashMap<>(ids.size());

                lock.lock();
                db.execSQL("PRAGMA synchronous=\'OFF\'");
                db.beginTransaction();

                SQLiteStatement statement = statements.deleteSingle();
                for(String id : ids) {
                    statement.clearBindings();
                    statement.bindString(1, id);

                    long status = statement.executeUpdateDelete();
                    if(status != 0 && status != -1) {
                        deletedRows++;
                        statuses.put(id, true);
                    } else {
                        statuses.put(id, false);
                    }
                }

                db.setTransactionSuccessful();

                emitter.onSuccess(
                        new DeleteResults(
                                CommentsLocalEntityMapping.TABLE,
                                statement.toString(),
                                deletedRows,
                                statuses
                        )
                );
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                db.endTransaction();
                db.execSQL("PRAGMA synchronous=\'NORMAL\'");
                lock.unlock();
            }
        });
    }

    @Override
    public Single<DeleteResults> deleteByTopic(@NonNull String topicId) {
        return Single.create(emitter -> {
            try {
                long deletedRows = 0L;
                Map<String, Boolean> statuses = new HashMap<>(0);

                lock.lock();
                db.execSQL("PRAGMA synchronous=\'OFF\'");
                db.beginTransaction();

                SQLiteStatement statement = statements.deleteByTopic();
                statement.clearBindings();
                statement.bindString(1, topicId);

                long status = statement.executeUpdateDelete();
                if(status != 0L && status != -1L) {
                    deletedRows += status;
                }

                db.setTransactionSuccessful();

                emitter.onSuccess(
                        new DeleteResults(
                                CommentsLocalEntityMapping.TABLE,
                                statement.toString(),
                                deletedRows,
                                statuses
                        )
                );
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                db.endTransaction();
                db.execSQL("PRAGMA synchronous=\'NORMAL\'");
                lock.unlock();
            }
        });
    }

    public static final class Factory {

        @NonNull
        private final SQLiteDatabase db;

        public Factory(@NonNull SQLiteDatabase db) {
            this.db = db;
        }

        @NonNull
        public ICommentsLocalService create() {
            return new CommentsLocalService(db);
        }
    }
}
