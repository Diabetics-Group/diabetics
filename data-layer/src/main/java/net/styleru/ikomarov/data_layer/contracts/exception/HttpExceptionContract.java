package net.styleru.ikomarov.data_layer.contracts.exception;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.base.ContractAccessor;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 27.04.17.
 */

public class HttpExceptionContract extends ContractAccessor {

    @NonNull
    private static final String PREFIX = HttpExceptionContract.class.getCanonicalName() + ".";

    @NonNull
    private final String KEY_CODE = PREFIX + "CODE";

    @NonNull
    private final String KEY_MESSAGE = PREFIX + "MESSAGE";

    @NonNull
    private final String KEY_JSON = PREFIX + "JSON";

    public HttpExceptionContract() {

    }

    public int extractCode(@NonNull ExceptionBundle bundle) {
        return bundle.getInt(KEY_CODE);
    }

    public void putCode(@NonNull ExceptionBundle bundle, int code) {
        bundle.putInt(KEY_CODE, code);
    }

    public String extractMessage(@NonNull ExceptionBundle bundle) {
        return bundle.getString(KEY_MESSAGE);
    }

    public void putMessage(@NonNull ExceptionBundle bundle, String message) {
        bundle.putString(KEY_MESSAGE, message);
    }

    public String extractJson(@NonNull ExceptionBundle bundle) {
        return bundle.getString(KEY_JSON);
    }

    public void putJson(@NonNull ExceptionBundle bundle, String json) {
        bundle.putString(KEY_JSON, json);
    }
}
