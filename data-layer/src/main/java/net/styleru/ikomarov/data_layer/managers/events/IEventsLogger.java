package net.styleru.ikomarov.data_layer.managers.events;

import net.styleru.ikomarov.data_layer.contracts.exception.Reason;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 03.05.17.
 */

public interface IEventsLogger {

    void logException(Exception exception);

    void logEvent(String clazz, String method, ExceptionBundle error);
}
