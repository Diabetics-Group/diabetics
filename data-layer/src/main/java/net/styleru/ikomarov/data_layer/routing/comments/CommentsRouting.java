package net.styleru.ikomarov.data_layer.routing.comments;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 28.04.17.
 */

public class CommentsRouting {

    @NonNull
    public static final String FIELD_USER_ID = "userID";

    @NonNull
    public static final String FIELD_TOPIC_ID = "questionID";

    @NonNull
    public static final String FIELD_COMMENT_ID = "answerID";

    @NonNull
    public static final String FIELD_CONTENT = "text";

    private CommentsRouting() {
        throw new IllegalStateException("No instances please!");
    }
}
