package net.styleru.ikomarov.data_layer.parsing.json;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.parsing.base.IParser;

import java.io.IOException;
import java.nio.charset.Charset;

import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

/**
 * Created by i_komarov on 30.04.17.
 */

public class JsonParser implements IParser<ResponseBody, String> {

    @NonNull
    private static final String CHARSET = "UTF-8";

    @Override
    public String parse(ResponseBody in) throws IOException {
        BufferedSource source = in.source();
        source.request(Long.MAX_VALUE);
        Buffer buffer = source.buffer();

        Charset charset = Charset.forName(CHARSET);
        return buffer.readString(charset);
    }
}
