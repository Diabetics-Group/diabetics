package net.styleru.ikomarov.data_layer.mappings.local.users;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UsersLocalEntityMapping {

    @NonNull
    public static final String TABLE = "users";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_FIRST_NAME = "FIRST_NAME";

    @NonNull
    public static final String COLUMN_LAST_NAME = "LAST_NAME";

    @NonNull
    public static final String COLUMN_IMAGE_URL = "IMAGE_URL";

    @NonNull
    public static final String COLUMN_ROLE = "ROLE";

    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_FIRST_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_FIRST_NAME;
    public static final String COLUMN_LAST_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_LAST_NAME;
    public static final String COLUMN_IMAGE_URL_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_IMAGE_URL;
    public static final String COLUMN_ROLE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ROLE;

    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    private UsersLocalEntityMapping() {
        throw new IllegalStateException("No instances please!");
    }

    @NonNull
    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE + "(" +
                    COLUMN_ID + " TEXT NOT NULL PRIMARY KEY" + "," +
                    COLUMN_FIRST_NAME + " TEXT" + "," +
                    COLUMN_LAST_NAME + " TEXT" + "," +
                    COLUMN_IMAGE_URL + " TEXT" + "," +
                    COLUMN_ROLE + " TEXT" +
                ");";
    }

    @NonNull
    public static String getDropTableQuery() {
        return "DROP TABLE IF EXISTS " + TABLE + ";";
    }
}
