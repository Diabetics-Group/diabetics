package net.styleru.ikomarov.data_layer.contracts.exception;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.base.ContractAccessor;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 04.06.17.
 */

public class ValidationExceptionContract extends ContractAccessor {

    @NonNull
    private static final String PREFIX = ValidationExceptionContract.class.getCanonicalName() + ".";

    @NonNull
    private static final String KEY_MESSAGE = PREFIX + "MESSAGE";

    public ValidationExceptionContract() {

    }

    public String extractMessage(@NonNull ExceptionBundle error) {
        return error.getString(KEY_MESSAGE);
    }

    public void putMessage(@NonNull ExceptionBundle error, @NonNull String message) {
        error.putString(KEY_MESSAGE, message);
    }
}
