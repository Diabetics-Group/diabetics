package net.styleru.ikomarov.data_layer.entities.remote.topics;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mappings.remote.topics.TopicRemoteEntityMapping.*;

/**
 * Created by i_komarov on 27.04.17.
 */

public class TopicRemoteEntity {

    @NonNull
    @SerializedName(FIELD_ID)
    private final String id;

    @NonNull
    @SerializedName(FIELD_CATEGORY_ID)
    private final String categoryId;

    @NonNull
    @SerializedName(FIELD_USER_IR)
    private final String userId;

    @NonNull
    @SerializedName(FIELD_FIRST_NAME)
    private final String firstName;

    @NonNull
    @SerializedName(FIELD_LAST_NAME)
    private final String lastName;

    @NonNull
    @SerializedName(FIELD_IMAGE_URL)
    private final String imageUrl;

    @NonNull
    @SerializedName(FIELD_ROLE)
    private final String role;

    @NonNull
    @SerializedName(FIELD_TITLE)
    private final String title;

    @NonNull
    @SerializedName(FIELD_CONTENT)
    private final String content;

    @NonNull
    @SerializedName(FIELD_LAST_UPDATE)
    private final String lastUpdate;

    @NonNull
    @SerializedName(FIELD_COMMENTS_COUNT)
    private final Long commentsCount;

    public TopicRemoteEntity(@NonNull String id,
                             @NonNull String categoryId,
                             @NonNull String userId,
                             @NonNull String firstName,
                             @NonNull String lastName,
                             @NonNull String imageUrl,
                             @NonNull String role,
                             @NonNull String title,
                             @NonNull String content,
                             @NonNull String lastUpdate,
                             @NonNull Long commentsCount) {
        this.id = id;
        this.categoryId = categoryId;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.role = role;
        this.title = title;
        this.content = content;
        this.lastUpdate = lastUpdate;
        this.commentsCount = commentsCount;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getCategoryId() {
        return categoryId;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public String getRole() {
        return role;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    @NonNull
    public String getLastUpdate() {
        return lastUpdate;
    }

    @NonNull
    public Long getCommentsCount() {
        return commentsCount;
    }
}
