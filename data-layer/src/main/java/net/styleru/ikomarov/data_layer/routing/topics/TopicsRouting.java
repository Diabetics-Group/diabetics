package net.styleru.ikomarov.data_layer.routing.topics;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 28.04.17.
 */

public class TopicsRouting {

    @NonNull
    public static final String FIELD_USER_ID = "userID";

    @NonNull
    public static final String FIELD_CATEGORY_ID = "categoryID";

    @NonNull
    public static final String FIELD_TOPIC_ID = "questionID";

    @NonNull
    public static final String FIELD_TITLE = "title";

    @NonNull
    public static final String FIELD_CONTENT = "text";

    private TopicsRouting() {
        throw new IllegalStateException("No instances please!");
    }
}
