package net.styleru.ikomarov.data_layer.mappings.local.categories;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

/**
 * Created by i_komarov on 26.04.17.
 */

public class CategoriesLocalEntityMapping {

    @NonNull
    public static final String TABLE = "categories";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_TITLE = "TITLE";

    @NonNull
    public static final String COLUMN_LAST_UPDATE = "LAST_UPDATE";

    @NonNull
    public static final String COLUMN_TOPICS_COUNT = "QUESTION_COUNT";

    @NonNull
    public static final String COLUMN_SYNCHRONIZATION_STATUS = "SYNCHRONIZED";

    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_TITLE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_TITLE;
    public static final String COLUMN_LAST_UPDATE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_LAST_UPDATE;
    public static final String COLUMN_TOPICS_COUNT_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_TOPICS_COUNT;
    public static final String COLUMN_SYNCHRONIZATION_STATUS_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_SYNCHRONIZATION_STATUS;

    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    private CategoriesLocalEntityMapping() {
        throw new IllegalStateException("No instances please");
    }

    @NonNull
    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE +
                "(" +
                    COLUMN_ID + " TEXT NOT NULL PRIMARY KEY," +
                    COLUMN_TITLE + " TEXT," +
                    COLUMN_LAST_UPDATE + " TIMESTAMP DEFAULT(STRFTIME(" + "\'%Y-%m-%d %H:%M:%f\'" + "," + "\'NOW\'" + "))," +
                    COLUMN_TOPICS_COUNT + " INTEGER NOT NULL DEFAULT(0)," +
                    COLUMN_SYNCHRONIZATION_STATUS + " BOOLEAN DEFAULT(0)" +
                ");";
    }

    @NonNull
    public static String getDropTableQuery() {
        return "DROP TABLE IF EXISTS " + TABLE + ";";
    }
}
