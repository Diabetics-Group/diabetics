package net.styleru.ikomarov.data_layer.services.categories;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.database.results.DeleteResults;
import net.styleru.ikomarov.data_layer.database.results.InsertResults;
import net.styleru.ikomarov.data_layer.entities.local.categories.CategoryLocalEntity;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by i_komarov on 26.04.17.
 */

public interface ICategoriesLocalService {

    Single<InsertResults> create(@NonNull List<CategoryLocalEntity> entities);

    Single<CategoryLocalEntity> get(@NonNull String id);

    Single<List<CategoryLocalEntity>> listNotSynchronized();

    Single<List<CategoryLocalEntity>> list();

    Single<List<CategoryLocalEntity>> list(int offset, int limit);

    Single<DeleteResults> delete(@NonNull List<String> categoryIds);
}
