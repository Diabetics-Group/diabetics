package net.styleru.ikomarov.data_layer.services.categories;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.NonNull;
import android.util.Log;

import net.styleru.ikomarov.data_layer.database.results.DeleteResults;
import net.styleru.ikomarov.data_layer.database.results.InsertResults;
import net.styleru.ikomarov.data_layer.database.statements.categories.CategoriesPreparedStatements;
import net.styleru.ikomarov.data_layer.entities.local.categories.CategoryLocalEntity;
import net.styleru.ikomarov.data_layer.mappings.local.categories.CategoriesLocalEntityMapping;
import net.styleru.ikomarov.data_layer.services.base.AbstractDatabaseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import io.reactivex.Single;

/**
 * Created by i_komarov on 27.04.17.
 */

public class CategoriesLocalService extends AbstractDatabaseService implements ICategoriesLocalService {

    @NonNull
    private final ReentrantLock lock;

    @NonNull
    private final SQLiteDatabase db;

    @NonNull
    private final CategoriesPreparedStatements statements;

    private CategoriesLocalService(@NonNull SQLiteDatabase db) {
        this.lock = new ReentrantLock(true);
        this.db = db;
        this.statements = new CategoriesPreparedStatements(db);
    }

    @Override
    public Single<InsertResults> create(@NonNull List<CategoryLocalEntity> entities) {
        return Single.create(emitter -> {
            try {
                long insertedRows = 0L;
                long updatedRows = 0L;
                Map<String, Boolean> statuses = new HashMap<>(entities.size());

                lock.lock();
                db.execSQL("PRAGMA synchronous=\'OFF\'");
                db.beginTransaction();

                SQLiteStatement statement = statements.insertSingle();

                for(CategoryLocalEntity entity : entities) {
                    statement.clearBindings();

                    String id;

                    statement.bindString(1, id = entity.getId());
                    statement.bindString(2, entity.getTitle());
                    statement.bindString(3, entity.getLastUpdate());
                    statement.bindLong(4, entity.getTopicsCount());
                    statement.bindLong(5, entity.isSynchronized() ? 1L : 0L);

                    long status = statement.executeInsert();
                    if(status != -1) {
                        if(statuses.get(entity.getId()) != null) {
                            updatedRows++;
                        } else {
                            insertedRows++;
                        }

                        statuses.put(id, true);
                    } else {
                        statuses.put(id, false);
                    }
                }

                db.setTransactionSuccessful();

                InsertResults results = new InsertResults(
                        CategoriesLocalEntityMapping.TABLE,
                        statement.toString(),
                        insertedRows,
                        updatedRows,
                        statuses
                );

                emitter.onSuccess(results);
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                db.endTransaction();
                db.execSQL("PRAGMA synchronous=\'NORMAL\'");
                lock.unlock();
            }
        });
    }

    @Override
    public Single<CategoryLocalEntity> get(@NonNull String id) {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectSingle(id), null);
                emitter.onSuccess(CategoryLocalEntity.fromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<List<CategoryLocalEntity>> listNotSynchronized() {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectNotSynchronized(), null);
                emitter.onSuccess(CategoryLocalEntity.listFromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<List<CategoryLocalEntity>> list() {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectAll(), null);
                emitter.onSuccess(CategoryLocalEntity.listFromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<List<CategoryLocalEntity>> list(int offset, int limit) {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectOffsetLimit(offset, limit), null);
                emitter.onSuccess(CategoryLocalEntity.listFromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<DeleteResults> delete(@NonNull List<String> categoryIds) {
        return Single.create(emitter -> {
            try {
                long deletedRows = 0L;
                Map<String, Boolean> statuses = new HashMap<>(categoryIds.size());

                lock.lock();
                db.execSQL("PRAGMA synchronous=\'OFF\'");
                db.beginTransaction();

                SQLiteStatement statement = statements.deleteSingle();

                for(String id : categoryIds) {
                    statement.clearBindings();
                    statement.bindString(1, id);

                    long status = statement.executeUpdateDelete();
                    if(status != 0L && status != -1L) {
                        statuses.put(id, true);
                        deletedRows++;
                    } else {
                        statuses.put(id, false);
                    }
                }

                emitter.onSuccess(
                        new DeleteResults(
                                CategoriesLocalEntityMapping.TABLE,
                                statement.toString(),
                                deletedRows,
                                statuses
                        )
                );
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                db.endTransaction();
                db.execSQL("PRAGMA synchronous=\'NORMAL\'");
                lock.unlock();
            }
        });
    }

    public static final class Factory {

        @NonNull
        private final SQLiteDatabase db;

        public Factory(@NonNull SQLiteDatabase db) {
            this.db = db;
        }

        @NonNull
        public ICategoriesLocalService create() {
            return new CategoriesLocalService(db);
        }
    }
}
