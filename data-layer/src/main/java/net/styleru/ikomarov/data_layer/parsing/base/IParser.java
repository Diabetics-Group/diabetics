package net.styleru.ikomarov.data_layer.parsing.base;

import java.io.IOException;

/**
 * Created by i_komarov on 30.04.17.
 */

public interface IParser<T, R> {

    R parse(T in) throws IOException;
}
