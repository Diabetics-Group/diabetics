package net.styleru.ikomarov.data_layer.database.statements.base;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 13.05.17.
 */

public class SQLitePermanentStatements {

    @NonNull
    private final SQLiteStatement VACUUM;

    @NonNull
    private final SQLiteStatement SYNCHRONOUS_OFF;

    @NonNull
    private final SQLiteStatement SYNCHRONOUS_NORMAL;

    @NonNull
    private final SQLiteDatabase db;

    public SQLitePermanentStatements(@NonNull SQLiteDatabase db) {
        this.db = db;
        this.VACUUM = db.compileStatement("VACUUM");
        this.SYNCHRONOUS_OFF = db.compileStatement("PRAGMA synchronous=\'OFF\'");
        this.SYNCHRONOUS_NORMAL = db.compileStatement("PRAGMA synchronous=\'NORMAL\'");
    }

    @NonNull
    public SQLiteStatement getVacuumStatement() {
        return this.VACUUM;
    }

    @NonNull
    public SQLiteStatement getSynchronousToggle(boolean enable) {
        return enable ? SYNCHRONOUS_NORMAL : SYNCHRONOUS_OFF;
    }
}
