package net.styleru.ikomarov.data_layer.services.authentication;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;
import net.styleru.ikomarov.data_layer.routing.users.UsersRouting;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by i_komarov on 15.05.17.
 */

public interface IAuthenticationService {

    @FormUrlEncoded
    @POST(Endpoints.ENDPOINT_PHONE_CODE)
    Single<Response<ResponseContainer<Void>>> code(
            @Field(UsersRouting.FIELD_PHONE) String phone
    );

    @FormUrlEncoded
    @POST(Endpoints.ENDPOINT_AUTHORIZE)
    Single<Response<ResponseContainer<UserRemoteEntity>>> authorize(
            @Field(UsersRouting.FIELD_PHONE) String phone,
            @Field(UsersRouting.FIELD_CODE) String code
    );
}
