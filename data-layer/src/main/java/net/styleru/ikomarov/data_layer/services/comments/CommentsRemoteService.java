package net.styleru.ikomarov.data_layer.services.comments;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.remote.categories.CategoryRemoteEntity;
import net.styleru.ikomarov.data_layer.entities.remote.comments.CommentRemoteEntity;
import net.styleru.ikomarov.data_layer.routing.base.DefaultParams;
import net.styleru.ikomarov.data_layer.routing.comments.CommentsRouting;
import net.styleru.ikomarov.data_layer.services.base.AbstractNetworkService;

import java.util.List;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Query;

/**
 * Created by i_komarov on 27.04.17.
 */

public class CommentsRemoteService extends AbstractNetworkService<ICommentsRemoteService> implements ICommentsRemoteService {

    private CommentsRemoteService(@NonNull OkHttpClient client) {
        super(Endpoints.URL_DIABETICS, client);
    }

    @Override
    public Single<Response<ResponseContainer<CommentRemoteEntity>>> create(String userId, String topicId, String content) {
        return getService().create(userId, topicId, content)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<CommentRemoteEntity>>>> list(String topicId) {
        return getService().list(topicId)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<CommentRemoteEntity>>>> list(String lastModified, String topicId) {
        return getService().list(lastModified, topicId)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<CommentRemoteEntity>>>> list(String topicId, int offset, int limit) {
        return getService().list(topicId, offset, limit)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<CommentRemoteEntity>>>> list(String topicId, String lastModified, int offset, int limit) {
        return getService().list(lastModified, topicId, offset, limit)
                .compose(handleError());
    }

    @Override
    public Single<Response<Object>> delete(String userId, String commentId) {
        return getService().delete(userId, commentId)
                .compose(handleError());
    }

    @Override
    protected ICommentsRemoteService createService(Retrofit retrofit) {
        return retrofit.create(ICommentsRemoteService.class);
    }

    @Override
    protected Gson getGson() {
        return new Gson();
    }

    public static final class Factory {

        @NonNull
        private final OkHttpClient client;

        public Factory(@NonNull OkHttpClient client) {
            this.client = client;
        }

        @NonNull
        public ICommentsRemoteService create() {
            return new CommentsRemoteService(client);
        }
    }
}
