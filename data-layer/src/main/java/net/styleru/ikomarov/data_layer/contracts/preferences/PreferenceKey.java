package net.styleru.ikomarov.data_layer.contracts.preferences;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by i_komarov on 13.05.17.
 */

public enum PreferenceKey {
    PUSH_NOTIFICATIONS("push_notifications"),
    CLEAR_CACHE("clear_cache"),
    CLEAR_SESSION("clear_session_data"),
    VERSION_NAME("version_name"),
    VERSION_CODE("version_code"),
    REPORT_BUG("report_bug"),
    RATE("rate"),
    DATABASE_NAME("database_name"),
    DATABASE_VERSION("database_version"),
    OAUTH2_ENABLED("oauth2_enabled"),
    BUILD_DEBUG("build_debug");

    @NonNull
    private static final Map<String, PreferenceKey> keysMap = new HashMap<>();

    static {
        for(PreferenceKey key : PreferenceKey.values()) {
            keysMap.put(key.key, key);
        }
    }

    @NonNull
    private final String key;

    @NonNull
    public static PreferenceKey forKey(String key) {
        return keysMap.get(key);
    }

    PreferenceKey(@NonNull String key) {
        this.key = key;
    }

    @NonNull
    public String getKey() {
        return this.key;
    }
}
