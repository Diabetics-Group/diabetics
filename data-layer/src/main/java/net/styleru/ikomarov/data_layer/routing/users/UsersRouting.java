package net.styleru.ikomarov.data_layer.routing.users;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 29.04.17.
 */

public class UsersRouting {

    @NonNull
    public static final String FIELD_PHONE = "phone";

    @NonNull
    public static final String FIELD_CODE = "code";

    @NonNull
    public static final String FIELD_USER_ID = "userID";

    @NonNull
    public static final String FIELD_FIRST_NAME = "name";

    @NonNull
    public static final String FIELD_LAST_NAME = "surname";

    @NonNull
    public static final String FIELD_TOKEN = "token";
}
