package net.styleru.ikomarov.data_layer.contracts.error;

import net.styleru.ikomarov.data_layer.R;

/**
 * Created by i_komarov on 03.05.17.
 */

public class ErrorCodeTranslator implements ICodeTranslator {

    @Override
    public int getLocalizedMessage(int responseCode) {
        return R.string.diabetics_error_unknown;
    }
}
