package net.styleru.ikomarov.data_layer.database.factory;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.database.helper.DBOpenHelper;

/**
 * Created by i_komarov on 03.05.17.
 */

public class SQLiteFactory {

    @NonNull
    private DBOpenHelper helper;

    public SQLiteFactory(@NonNull DBOpenHelper helper) {
        this.helper = helper;
    }

    public SQLiteDatabase create() {
        return helper.getWritableDatabase();
    }
}
