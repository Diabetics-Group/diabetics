package net.styleru.ikomarov.data_layer.contracts.error;

import android.support.annotation.StringRes;

/**
 * Created by i_komarov on 03.05.17.
 */

public interface ICodeTranslator {

    @StringRes
    int getLocalizedMessage(int responseCode);
}
