package net.styleru.ikomarov.data_layer.managers.session;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.contracts.session.SessionStatus;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.managers.network.INetworkManager;
import net.styleru.ikomarov.data_layer.preferences.user.IUserPreferences;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by i_komarov on 03.05.17.
 */

public class SessionManager implements ISessionManager {

    @NonNull
    private static final String STUB = "STUB";

    @NonNull
    private final IUserPreferences preferences;

    @NonNull
    private final INetworkManager networkManager;

    @NonNull
    private final Retrofit retrofit;

    @NonNull
    private final SessionCallbacksManager manager;

    @Nullable
    private volatile UserLocalEntity user;

    @NonNull
    private volatile boolean isAuthenticating;

    public SessionManager(@NonNull IUserPreferences preferences, @NonNull INetworkManager networkManager, @NonNull OkHttpClient client) {
        this.preferences = preferences;
        this.networkManager = networkManager;
        this.manager = new SessionCallbacksManager();
        this.retrofit = new Retrofit.Builder().client(client).baseUrl(Endpoints.URL_DIABETICS)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        user = preferences.load();
        isAuthenticating = false;
    }

    @Override
    public Single<SessionStatus> getSessionStatus() {
        return Single.create(emitter -> {
            if(user == null || null == user.getToken()) {
                emitter.onSuccess(SessionStatus.INACTIVE);
            } else if(isAuthenticating) {
                emitter.onSuccess(SessionStatus.AUTHENTICATING);
            } else {
                emitter.onSuccess(SessionStatus.ACTIVE);
            }
        });
    }

    @Override
    public Single<String> getSessionToken() {
        return Single.create(emitter -> {
            emitter.onSuccess(STUB);
        });
    }

    @Override
    public String blockingGetSessionToken() throws ExceptionBundle {
        return STUB;
    }

    @Override
    public Single<String> refreshAndGetSessionToken() {
        return Single.create(emitter -> {
            emitter.onSuccess(STUB);
        });
    }

    @Override
    public String blockingRefreshAndGetSessionToken() throws ExceptionBundle {
        return STUB;
    }

    @Override
    public void registerCallback(String key, AuthenticationCallbacks callbacks) {
        manager.register(key, callbacks);
    }

    @Override
    public void unregisterCallback(String key) {
        manager.unregister(key);
    }

    @Override
    public void deauthenticate() {
        preferences.invalidate();
        manager.onDeauthenticated();
    }

    @Override
    public void authenticate(String code) throws ExceptionBundle {
        //TODO: implement authentication
        manager.onAuthenticated();
    }
}
