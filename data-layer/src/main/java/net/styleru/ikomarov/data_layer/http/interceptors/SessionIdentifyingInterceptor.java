package net.styleru.ikomarov.data_layer.http.interceptors;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.BuildConfig;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.managers.session.ISessionManager;
import net.styleru.ikomarov.data_layer.routing.base.DefaultParams;

import java.io.IOException;
import java.net.HttpURLConnection;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by i_komarov on 03.05.17.
 */

public class SessionIdentifyingInterceptor implements Interceptor {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final ISessionManager manager;

    @Nullable
    private volatile String token;

    @NonNull
    private final Boolean shouldUseOAuth2 = BuildConfig.OAUTH2_ENABLED;

    public SessionIdentifyingInterceptor(@NonNull ISessionManager manager) {
        this.manager = manager;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response;
        if(shouldUseOAuth2) {
            response = chain.proceed(
                    chain.request().newBuilder()
                            .addHeader(DefaultParams.HEADER_AUTHORIZATION, getToken())
                            .build()
            );

            if(response.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                response = chain.proceed(
                        chain.request().newBuilder()
                                .addHeader(DefaultParams.HEADER_AUTHORIZATION, refreshAndGetToken())
                                .build()
                );
            }
        } else {
            response = chain.proceed(chain.request());
        }

        return response;
    }

    @Nullable
    private String getToken() {
        synchronized (lock) {
            try {
                return token == null ? token = manager.blockingGetSessionToken() : token;
            } catch (ExceptionBundle bundle) {
                bundle.printStackTrace();
                return null;
            }
        }
    }

    @Nullable
    private String refreshAndGetToken() {
        synchronized (lock) {
            try {
                return token = manager.blockingRefreshAndGetSessionToken();
            } catch (ExceptionBundle bundle) {
                bundle.printStackTrace();
                return null;
            }
        }
    }
}
