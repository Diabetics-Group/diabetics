package net.styleru.ikomarov.data_layer.http.clients;

import net.styleru.ikomarov.data_layer.BuildConfig;
import net.styleru.ikomarov.data_layer.http.base.IClientFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by i_komarov on 03.05.17.
 */

public class SessionlessClientsFactory implements IClientFactory {

    private final int CONNECT_TIMEOUT = 15;
    private final int READ_TIMEOUT = 15;
    private final int WRITE_TIMEOUT = 15;

    private boolean isDebug = BuildConfig.IS_DEBUG_BUILD;

    public SessionlessClientsFactory() {

    }

    @Override
    public OkHttpClient create() {
        return prepareBuilder().build();
    }

    protected final OkHttpClient.Builder prepareBuilder() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);

        if(isDebug) {
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }

        return builder;
    }
}
