package net.styleru.ikomarov.data_layer.services.database;

import io.reactivex.Single;

/**
 * Created by i_komarov on 13.05.17.
 */

public interface IDatabaseService {

    Single<Boolean> invalidateCache();

    Single<Long> totalSpaceUsed();

    Single<Boolean> verifyDatabaseIntegrity();
}
