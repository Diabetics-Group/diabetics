package net.styleru.ikomarov.data_layer.services.authentication;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;
import net.styleru.ikomarov.data_layer.services.base.AbstractNetworkService;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by i_komarov on 04.06.17.
 */

public class AuthenticationService extends AbstractNetworkService<IAuthenticationService> implements IAuthenticationService {

    private AuthenticationService(@NonNull OkHttpClient client) {
        super(Endpoints.URL_DIABETICS, client);
    }

    @Override
    public Single<Response<ResponseContainer<Void>>> code(String phone) {
        return getService().code(phone)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<UserRemoteEntity>>> authorize(String phone, String code) {
        return getService().authorize(phone, code)
                .compose(handleError());
    }

    @Override
    protected IAuthenticationService createService(Retrofit retrofit) {
        return retrofit.create(IAuthenticationService.class);
    }

    @Override
    protected Gson getGson() {
        return new GsonBuilder().create();
    }

    public static final class Factory {

        private Factory() {

        }

        @NonNull
        public static IAuthenticationService create(@NonNull OkHttpClient client) {
            return new AuthenticationService(client);
        }
    }
}
