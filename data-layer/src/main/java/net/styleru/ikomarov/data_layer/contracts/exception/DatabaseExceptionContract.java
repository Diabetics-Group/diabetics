package net.styleru.ikomarov.data_layer.contracts.exception;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.base.ContractAccessor;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 29.04.17.
 */

public class DatabaseExceptionContract extends ContractAccessor {

    @NonNull
    private static final String PREFIX = DatabaseExceptionContract.class.getCanonicalName() + ".";

    @NonNull
    private final String KEY_SQLITE = PREFIX + "SQLITE";

    @NonNull
    private final String KEY_CODE = PREFIX + "CODE";

    @NonNull
    private final String KEY_MESSAGE = PREFIX + "MESSAGE";

    @NonNull
    private final String KEY_THROWABLE = PREFIX + "THROWABLE";

    public DatabaseExceptionContract() {

    }

    public boolean extractIsSQLite(@NonNull ExceptionBundle bundle) {
        return bundle.getBoolean(KEY_SQLITE);
    }

    public void putIsSQLite(@NonNull ExceptionBundle bundle, boolean isSQLite) {
        bundle.putBoolean(KEY_SQLITE, isSQLite);
    }

    public int extractCode(@NonNull ExceptionBundle bundle) {
        return bundle.getInt(KEY_CODE);
    }

    public void putCode(@NonNull ExceptionBundle bundle, int code) {
        bundle.putInt(KEY_CODE, code);
    }

    public String extractMessage(@NonNull ExceptionBundle bundle) {
        return bundle.getString(KEY_MESSAGE);
    }

    public void putMessage(@NonNull ExceptionBundle bundle, String message) {
        bundle.putString(KEY_MESSAGE, message);
    }

    public Throwable extractThrowable(@NonNull ExceptionBundle bundle) {
        return (Throwable) bundle.getSerializable(KEY_THROWABLE);
    }

    public void putThrowable(@NonNull ExceptionBundle bundle, Throwable thr) {
        bundle.putSerializable(KEY_THROWABLE, thr);
    }
}
