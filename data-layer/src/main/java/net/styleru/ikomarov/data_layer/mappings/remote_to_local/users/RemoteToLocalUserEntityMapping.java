package net.styleru.ikomarov.data_layer.mappings.remote_to_local.users;

import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 15.05.17.
 */

public class RemoteToLocalUserEntityMapping implements Function<UserRemoteEntity, UserLocalEntity> {

    @Override
    public UserLocalEntity apply(UserRemoteEntity entity) throws Exception {
        return new UserLocalEntity(
                entity.getId(),
                entity.getToken(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getImageUrl(),
                entity.getPhone(),
                entity.getRole()
        );
    }
}
