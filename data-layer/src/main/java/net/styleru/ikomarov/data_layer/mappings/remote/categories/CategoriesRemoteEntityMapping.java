package net.styleru.ikomarov.data_layer.mappings.remote.categories;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 27.04.17.
 */

public class CategoriesRemoteEntityMapping {

    @NonNull
    public static final String FIELD_ID = "id";

    @NonNull
    public static final String FIELD_NAME = "name";

    @NonNull
    public static final String FIELD_LAST_UPDATE = "lastUpdate";

    @NonNull
    public static final String FIELD_COUNT = "count";
}
