package net.styleru.ikomarov.data_layer.managers.network;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.managers.base.ObservablesManager;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by i_komarov on 26.04.17.
 */

public class NetworkManager implements INetworkManager {

    @NonNull
    private final IntentFilter filter;

    @NonNull
    private final ConnectivityManager connectivity;

    @NonNull
    private final ObservablesManager<Integer> observables;

    @NonNull
    private final NetworkEventBroadcastReceiver receiver;

    public NetworkManager(@NonNull Context context) {
        this.filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        this.connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        this.observables = new ObservablesManager<>();
        this.receiver = new NetworkEventBroadcastReceiver(connectivity, observables);
        context.registerReceiver(receiver, filter);
    }

    @Override
    public Observable<Integer> getNetworkEventsObservable(String key) {
        return observables.createChannel(key);
    }

    @Override
    public Single<Integer> getNetworkInfo() {
        return Single.just(connectivity.getActiveNetworkInfo() != null ? connectivity.getActiveNetworkInfo().getType() : NetworkEventBroadcastReceiver.TYPE_DISCONNECTED);
    }

    @Override
    public Integer blockingGetNetworkInfo() {
        return connectivity.getActiveNetworkInfo() != null ? connectivity.getActiveNetworkInfo().getType() : NetworkEventBroadcastReceiver.TYPE_DISCONNECTED;
    }
}
