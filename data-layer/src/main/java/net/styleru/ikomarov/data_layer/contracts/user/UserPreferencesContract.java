package net.styleru.ikomarov.data_layer.contracts.user;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.base.ContractAccessor;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;

/**
 * Created by i_komarov on 30.04.17.
 */

public class UserPreferencesContract extends ContractAccessor {

    @NonNull
    private static final String PREFIX = UserPreferencesContract.class.getCanonicalName() + ".";

    @NonNull
    private final String KEY_USER_ID = PREFIX + "USER_ID";

    @NonNull
    private final String KEY_SESSION_TOKEN = PREFIX + "SESSION_TOKEN";

    @NonNull
    private final String KEY_USER_FIRST_NAME = PREFIX + "FIRST_NAME";

    @NonNull
    private final String KEY_USER_LAST_NAME = PREFIX + "LAST_NAME";

    @NonNull
    private final String KEY_USER_IMAGE_URL = PREFIX + "IMAGE_URL";

    @NonNull
    private final String KEY_USER_PHONE = PREFIX + "PHONE";

    @NonNull
    private final String KEY_USER_ROLE = PREFIX + "ROLE";

    public UserPreferencesContract() {

    }

    public UserLocalEntity load(@NonNull SharedPreferences preferences) {
        return new UserLocalEntity(
                preferences.getString(KEY_USER_ID, null),
                preferences.getString(KEY_SESSION_TOKEN, null),
                preferences.getString(KEY_USER_FIRST_NAME, null),
                preferences.getString(KEY_USER_LAST_NAME, null),
                preferences.getString(KEY_USER_IMAGE_URL, null),
                preferences.getString(KEY_USER_PHONE, null),
                preferences.getString(KEY_USER_ROLE, null)
        );
    }

    public void save(@NonNull SharedPreferences preferences, @NonNull UserLocalEntity entity) {
        preferences.edit()
                .putString(KEY_USER_ID, entity.getId())
                .putString(KEY_SESSION_TOKEN, entity.getToken())
                .putString(KEY_USER_FIRST_NAME, entity.getFirstName())
                .putString(KEY_USER_LAST_NAME, entity.getLastName())
                .putString(KEY_USER_IMAGE_URL, entity.getImageUrl())
                .putString(KEY_USER_PHONE, entity.getPhone())
                .putString(KEY_USER_ROLE, entity.getRole())
        .apply();
    }

    public void invalidate(@NonNull SharedPreferences preferences) {
        preferences.edit()
                .putString(KEY_USER_ID, null)
                .putString(KEY_SESSION_TOKEN, null)
                .putString(KEY_USER_FIRST_NAME, null)
                .putString(KEY_USER_LAST_NAME, null)
                .putString(KEY_USER_IMAGE_URL, null)
                .putString(KEY_USER_PHONE, null)
                .putString(KEY_USER_ROLE, null)
        .apply();
    }
}
