package net.styleru.ikomarov.data_layer.entities.remote.categories;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mappings.remote.categories.CategoriesRemoteEntityMapping.*;

/**
 * Created by i_komarov on 27.04.17.
 */

public class CategoryRemoteEntity {

    @NonNull
    @SerializedName(FIELD_ID)
    private String id;

    @NonNull
    @SerializedName(FIELD_NAME)
    private String name;

    @NonNull
    @SerializedName(FIELD_LAST_UPDATE)
    private String lastUpdate;

    @NonNull
    @SerializedName(FIELD_COUNT)
    private Long topicsCount;

    public CategoryRemoteEntity(@NonNull String id,
                                @NonNull String name,
                                @NonNull String lastUpdate,
                                @NonNull Long topicsCount) {
        this.id = id;
        this.name = name;
        this.lastUpdate = lastUpdate;
        this.topicsCount = topicsCount;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getLastUpdate() {
        return lastUpdate;
    }

    @NonNull
    public Long getTopicsCount() {
        return topicsCount;
    }
}
