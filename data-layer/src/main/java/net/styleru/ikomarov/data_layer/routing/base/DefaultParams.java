package net.styleru.ikomarov.data_layer.routing.base;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 28.04.17.
 */

public class DefaultParams {

    @NonNull
    public static final String HEADER_AUTHORIZATION = "Authorization";

    @NonNull
    public static final String HEADER_IF_MODIFIED_SINCE = "IF-MODIFIED-SINCE";

    @NonNull
    public static final String FIELD_OFFSET = "first";

    @NonNull
    public static final String FIELD_LIMIT = "offset";

    @NonNull
    public static final String FIELD_SEARCH_QUERY = "search";

    private DefaultParams() {
        throw new IllegalStateException("No instances please!");
    }
}
