package net.styleru.ikomarov.data_layer.mappings.remote.comments;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 27.04.17.
 */

public class CommentsRemoteEntityMapping {

    @NonNull
    public static final String FIELD_ID = "id";

    @NonNull
    public static final String FIELD_USER_ID = "userID";

    @NonNull
    public static final String FIELD_TOPIC_ID = "questionID";

    @NonNull
    public static final String FIELD_DATE = "date";

    @NonNull
    public static final String FIELD_CONTENT = "text";

    @NonNull
    public static final String FIELD_FIRST_NAME = "name";

    @NonNull
    public static final String FIELD_LAST_NAME = "surname";

    @NonNull
    public static final String FIELD_IMAGE_URL = "image";

    @NonNull
    public static final String FIELD_ROLE = "role";
}
