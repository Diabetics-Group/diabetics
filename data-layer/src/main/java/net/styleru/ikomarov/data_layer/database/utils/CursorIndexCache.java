package net.styleru.ikomarov.data_layer.database.utils;

import android.database.Cursor;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by i_komarov on 27.04.17.
 */

public class CursorIndexCache {

    @NonNull
    private final Map<String, Integer> indexMap;

    public CursorIndexCache() {
        this.indexMap = new HashMap<>();
    }

    @NonNull
    public Integer index(Cursor cursor, String columnName) {
        Integer index;
        if(null == (index = indexMap.get(columnName))) {
            indexMap.put(columnName, index = cursor.getColumnIndex(columnName));
        }

        return index;
    }
}
