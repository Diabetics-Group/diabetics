package net.styleru.ikomarov.data_layer.managers.session;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.managers.base.ICallback;

/**
 * Created by i_komarov on 03.05.17.
 */

public interface AuthenticationCallbacks {

    void onAuthenticated();

    void onDeauthenticated();

    void onAuthenticationFailed(ExceptionBundle error);
}
