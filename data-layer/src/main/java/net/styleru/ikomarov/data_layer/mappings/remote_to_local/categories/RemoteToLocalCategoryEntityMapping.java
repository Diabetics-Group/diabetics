package net.styleru.ikomarov.data_layer.mappings.remote_to_local.categories;

import net.styleru.ikomarov.data_layer.entities.local.categories.CategoryLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.categories.CategoryRemoteEntity;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 30.04.17.
 */

public class RemoteToLocalCategoryEntityMapping implements Function<CategoryRemoteEntity, CategoryLocalEntity> {

    @Override
    public CategoryLocalEntity apply(CategoryRemoteEntity entity) throws Exception {
        return new CategoryLocalEntity.Builder(entity.getName(), entity.getLastUpdate())
                .mutateSynchronized(entity.getId())
                .withTopicsCount(entity.getTopicsCount())
                .create();
    }
}
