package net.styleru.ikomarov.data_layer.mappings.remote_to_local.comments;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.local.comments.CommentLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.comments.CommentRemoteEntity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 30.04.17.
 */

public class RemoteToLocalCommentsEntityMapping implements Function<List<CommentRemoteEntity>, List<CommentLocalEntity>> {

    @NonNull
    private final Function<CommentRemoteEntity, CommentLocalEntity> singleMapper;

    public RemoteToLocalCommentsEntityMapping(@NonNull Function<CommentRemoteEntity, CommentLocalEntity> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<CommentLocalEntity> apply(List<CommentRemoteEntity> commentRemoteEntities) throws Exception {
        return Observable.fromIterable(commentRemoteEntities)
                .map(singleMapper)
                .reduce(new ArrayList<CommentLocalEntity>(), (list, element) -> {
                    list.add(element);
                    return list;
                })
                .blockingGet();
    }
}
