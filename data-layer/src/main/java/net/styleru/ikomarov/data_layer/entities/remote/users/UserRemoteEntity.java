package net.styleru.ikomarov.data_layer.entities.remote.users;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mappings.remote.users.UsersRemoteEntityMapping.*;

/**
 * Created by i_komarov on 29.04.17.
 */

public class UserRemoteEntity {

    @NonNull
    @SerializedName(FIELD_ID)
    private final String id;

    @NonNull
    @SerializedName(FIELD_PHONE)
    private final String phone;

    @NonNull
    @SerializedName(FIELD_FIRST_NAME)
    private final String firstName;

    @NonNull
    @SerializedName(FIELD_LAST_NAME)
    private final String lastName;

    @NonNull
    @SerializedName(FIELD_IMAGE_URL)
    private final String imageUrl;

    @NonNull
    @SerializedName(FIELD_ROLE)
    private final String role;

    @NonNull
    @SerializedName(FIELD_TOKEN)
    private final String token;

    public UserRemoteEntity(@NonNull String id, @NonNull String phone, @NonNull String firstName, @NonNull String lastName, @NonNull String imageUrl, @NonNull String role, @NonNull String token) {
        this.id = id;
        this.phone = phone;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.role = role;
        this.token = token;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getPhone() {
        return phone;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public String getRole() {
        return role;
    }

    @NonNull
    public String getToken() {
        return token;
    }
}
