package net.styleru.ikomarov.data_layer.managers.network;

import android.net.NetworkInfo;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by i_komarov on 26.04.17.
 */

public interface INetworkManager {

    Observable<Integer> getNetworkEventsObservable(String key);

    Single<Integer> getNetworkInfo();

    Integer blockingGetNetworkInfo();
}
