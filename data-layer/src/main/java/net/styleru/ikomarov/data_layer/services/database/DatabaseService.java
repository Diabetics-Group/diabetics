package net.styleru.ikomarov.data_layer.services.database;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.database.helper.DBOpenHelper;
import net.styleru.ikomarov.data_layer.database.statements.base.SQLitePermanentStatements;
import net.styleru.ikomarov.data_layer.services.base.AbstractDatabaseService;

import java.io.File;

import io.reactivex.Single;

/**
 * Created by i_komarov on 13.05.17.
 */

public class DatabaseService extends AbstractDatabaseService implements IDatabaseService {

    @NonNull
    private final DBOpenHelper helper;

    @NonNull
    private final SQLitePermanentStatements statements;

    public DatabaseService(@NonNull DBOpenHelper helper) {
        this.helper = helper;
        this.statements = new SQLitePermanentStatements(helper.getWritableDatabase());
    }

    @Override
    public Single<Boolean> invalidateCache() {
        return Single.create((emitter) -> {
            try {
                helper.dropTables(helper.getWritableDatabase());
                statements.getVacuumStatement().execute();
                helper.createTables(helper.getWritableDatabase());
                emitter.onSuccess(true);
            } catch(Exception e) {
                emitter.onError(parseException(e));
            }
        });
    }

    @Override
    public Single<Long> totalSpaceUsed() {
        return Single.create(emitter -> {
            try {
                File dbFile = new File(helper.getWritableDatabase().getPath());
                long totalSpaceUsed = dbFile.length();
                emitter.onSuccess(totalSpaceUsed);
            } catch(Exception e) {
                emitter.onError(parseException(e));
            }
        });
    }

    @Override
    public Single<Boolean> verifyDatabaseIntegrity() {
        return Single.create(emitter -> {
            try {
                boolean isDatabaseIntegrityOk = helper.getWritableDatabase().isDatabaseIntegrityOk();
                emitter.onSuccess(isDatabaseIntegrityOk);
            } catch(Exception e) {
                emitter.onError(parseException(e));
            }
        });
    }
}
