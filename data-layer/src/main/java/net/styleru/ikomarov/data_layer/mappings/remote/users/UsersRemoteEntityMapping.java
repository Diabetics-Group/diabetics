package net.styleru.ikomarov.data_layer.mappings.remote.users;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 29.04.17.
 */

public class UsersRemoteEntityMapping {

    @NonNull
    public static final String FIELD_ID = "id";

    @NonNull
    public static final String FIELD_PHONE = "phone";

    @NonNull
    public static final String FIELD_FIRST_NAME = "name";

    @NonNull
    public static final String FIELD_LAST_NAME = "surname";

    @NonNull
    public static final String FIELD_IMAGE_URL = "image";

    @NonNull
    public static final String FIELD_ROLE = "role";

    @NonNull
    public static final String FIELD_TOKEN = "token";

    private UsersRemoteEntityMapping() {
        throw new IllegalStateException("No instances please!");
    }
}
