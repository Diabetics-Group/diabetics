package net.styleru.ikomarov.data_layer.mappings.remote_to_local.comments;

import net.styleru.ikomarov.data_layer.entities.local.comments.CommentLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.comments.CommentRemoteEntity;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 30.04.17.
 */

public class RemoteToLocalCommentEntityMapping implements Function<CommentRemoteEntity, CommentLocalEntity> {

    @Override
    public CommentLocalEntity apply(CommentRemoteEntity entity) throws Exception {
        return new CommentLocalEntity.Builder(
                        entity.getTopicId(),
                        entity.getUserId(),
                        entity.getFirstName(),
                        entity.getLastName(),
                        entity.getImageUrl(),
                        entity.getRole(),
                        entity.getDate(),
                        entity.getContent()
                )
                .mutateSynchronized(entity.getId())
                .create();
    }
}
