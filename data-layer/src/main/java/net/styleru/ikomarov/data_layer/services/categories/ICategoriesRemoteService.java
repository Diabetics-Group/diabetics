package net.styleru.ikomarov.data_layer.services.categories;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.remote.categories.CategoryRemoteEntity;
import net.styleru.ikomarov.data_layer.routing.base.DefaultParams;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by i_komarov on 26.04.17.
 */

public interface ICategoriesRemoteService {

    @GET(Endpoints.ENDPOINT_CATEGORIES)
    Single<Response<ResponseContainer<List<CategoryRemoteEntity>>>> list(

    );

    @GET(Endpoints.ENDPOINT_CATEGORIES)
    Single<Response<ResponseContainer<List<CategoryRemoteEntity>>>> list(
            @Header(DefaultParams.HEADER_IF_MODIFIED_SINCE) String lastModified
    );

    @GET(Endpoints.ENDPOINT_CATEGORIES)
    Single<Response<ResponseContainer<List<CategoryRemoteEntity>>>> list(
            @Query(value = DefaultParams.FIELD_OFFSET, encoded = true) int offset,
            @Query(value = DefaultParams.FIELD_LIMIT, encoded = true) int limit
    );

    @GET(Endpoints.ENDPOINT_CATEGORIES)
    Single<Response<ResponseContainer<List<CategoryRemoteEntity>>>> list(
            @Header(DefaultParams.HEADER_IF_MODIFIED_SINCE) String lastUpdate,
            @Query(value = DefaultParams.FIELD_OFFSET, encoded = true) int offset,
            @Query(value = DefaultParams.FIELD_LIMIT, encoded = true) int limit
    );
}
