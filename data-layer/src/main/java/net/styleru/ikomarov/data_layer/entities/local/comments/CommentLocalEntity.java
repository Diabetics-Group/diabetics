package net.styleru.ikomarov.data_layer.entities.local.comments;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import net.styleru.ikomarov.data_layer.database.utils.CursorIndexCache;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static net.styleru.ikomarov.data_layer.mappings.local.categories.CategoriesLocalEntityMapping.COLUMN_ID;
import static net.styleru.ikomarov.data_layer.mappings.local.categories.CategoriesLocalEntityMapping.COLUMN_SYNCHRONIZATION_STATUS;
import static net.styleru.ikomarov.data_layer.mappings.local.comments.CommentsLocalEntityMapping.*;

/**
 * Created by i_komarov on 26.04.17.
 */

@StorIOSQLiteType(table = TABLE)
public class CommentLocalEntity {

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_ID, key = true)
    String id;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_TOPIC_ID)
    String topicId;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_USER_ID)
    String userId;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_FIRST_NAME)
    String firstName;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_LAST_NAME)
    String lastName;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_IMAGE_URL)
    String imageUrl;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_ROLE)
    String role;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_DATE, ignoreNull = true)
    String date;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_CONTENT)
    String content;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_SYNCHRONIZATION_STATUS, ignoreNull = true)
    Boolean isSynchronized;

    @NonNull
    public static CommentLocalEntity fromCursor(@NonNull Cursor cursor) {
        CommentLocalEntity entity = new CommentLocalEntity();

        entity.id = cursor.getString(cursor.getColumnIndex(COLUMN_ID));
        entity.topicId = cursor.getString(cursor.getColumnIndex(COLUMN_TOPIC_ID));
        entity.userId = cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID));
        entity.firstName = cursor.getString(cursor.getColumnIndex(COLUMN_FIRST_NAME));
        entity.lastName = cursor.getString(cursor.getColumnIndex(COLUMN_LAST_NAME));
        entity.imageUrl = cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE_URL));
        entity.role = cursor.getString(cursor.getColumnIndex(COLUMN_ROLE));
        entity.date = cursor.getString(cursor.getColumnIndex(COLUMN_DATE));
        entity.content = cursor.getString(cursor.getColumnIndex(COLUMN_CONTENT));
        entity.isSynchronized = cursor.getInt(cursor.getColumnIndex(COLUMN_SYNCHRONIZATION_STATUS)) == 1;

        return entity;
    }

    @NonNull
    public static List<CommentLocalEntity> listFromCursor(@NonNull Cursor cursor) {
        CursorIndexCache cache = new CursorIndexCache();
        List<CommentLocalEntity> entities = new ArrayList<>(cursor.getCount());

        if (cursor.moveToFirst()) {
            do {
                CommentLocalEntity entity = new CommentLocalEntity();

                entity.id = cursor.getString(cache.index(cursor, COLUMN_ID));
                entity.topicId = cursor.getString(cache.index(cursor, COLUMN_TOPIC_ID));
                entity.userId = cursor.getString(cache.index(cursor, COLUMN_USER_ID));
                entity.firstName = cursor.getString(cache.index(cursor, COLUMN_FIRST_NAME));
                entity.lastName = cursor.getString(cache.index(cursor, COLUMN_LAST_NAME));
                entity.imageUrl = cursor.getString(cache.index(cursor, COLUMN_IMAGE_URL));
                entity.role = cursor.getString(cache.index(cursor, COLUMN_ROLE));
                entity.date = cursor.getString(cache.index(cursor, COLUMN_DATE));
                entity.content = cursor.getString(cache.index(cursor, COLUMN_CONTENT));
                entity.isSynchronized = cursor.getInt(cache.index(cursor, COLUMN_SYNCHRONIZATION_STATUS)) == 1;

                entities.add(entity);
            } while(cursor.moveToNext());
        }

        return Collections.unmodifiableList(entities);
    }

    CommentLocalEntity() {

    }

    @Nullable
    public String getId() {
        return id;
    }

    @NonNull
    public String getTopicId() {
        return topicId;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public String getRole() {
        return role;
    }

    @Nullable
    public String getDate() {
        return date;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    @NonNull
    public Boolean isSynchronized() {
        return isSynchronized;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommentLocalEntity that = (CommentLocalEntity) o;

        if (!id.equals(that.id)) return false;
        if (!topicId.equals(that.topicId)) return false;
        if (!userId.equals(that.userId)) return false;
        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        if (imageUrl != null ? !imageUrl.equals(that.imageUrl) : that.imageUrl != null)
            return false;
        if (!role.equals(that.role)) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (!content.equals(that.content)) return false;
        return isSynchronized.equals(that.isSynchronized);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + topicId.hashCode();
        result = 31 * result + userId.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + role.hashCode();
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + content.hashCode();
        result = 31 * result + isSynchronized.hashCode();
        return result;
    }

    public static final class Builder {

        @NonNull
        private final CommentLocalEntity entity;

        public Builder(@NonNull String topicId,
                       @NonNull String userId,
                       @NonNull String firstName,
                       @NonNull String lastName,
                       @Nullable String imageUrl,
                       @NonNull String role,
                       @NonNull String date,
                       @NonNull String content) {

            entity = new CommentLocalEntity();
            entity.topicId = topicId;
            entity.userId = userId;
            entity.firstName = firstName;
            entity.lastName = lastName;
            entity.imageUrl = imageUrl;
            entity.role = role;
            entity.date = date;
            entity.content = content;
        }

        @NonNull
        public SynchronizedBuilder mutateSynchronized(@NonNull String id) {
            entity.id = id;
            return new SynchronizedBuilder(entity);
        }

        @NonNull
        public CommentLocalEntity create(@NonNull UUID generatedId) {
            entity.id = generatedId.toString();
            entity.isSynchronized = false;
            return entity;
        }
    }

    public static final class SynchronizedBuilder {

        @NonNull
        private final CommentLocalEntity entity;

        private SynchronizedBuilder(@NonNull CommentLocalEntity preBuiltEntity) {
            entity = preBuiltEntity;
        }

        @NonNull
        public CommentLocalEntity create() {
            entity.isSynchronized = true;
            return entity;
        }
    }
}
