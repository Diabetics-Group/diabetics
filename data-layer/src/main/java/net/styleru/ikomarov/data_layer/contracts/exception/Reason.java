package net.styleru.ikomarov.data_layer.contracts.exception;

import net.styleru.ikomarov.data_layer.contracts.base.ContractAccessor;

/**
 * Created by i_komarov on 26.04.17.
 */

public enum Reason {

    NETWORK {
        @Override
        public NetworkExceptionContract getContract() {
            return new NetworkExceptionContract();
        }
    },
    HTTP {
        @Override
        public HttpExceptionContract getContract() {
            return new HttpExceptionContract();
        }
    },
    LAYER {
        @Override
        public LayerExceptionContract getContract() {
            return new LayerExceptionContract();
        }
    },
    INTERNAL {
        @Override
        public InternalExceptionContract getContract() {
            return new InternalExceptionContract();
        }
    },
    DATABASE {
        @Override
        public DatabaseExceptionContract getContract() {
            return new DatabaseExceptionContract();
        }
    },
    ENVIRONMENT {
        @Override
        public EnvironmentExceptionContract getContract() {
            return new EnvironmentExceptionContract();
        }
    },
    VALIDATION {
        @Override
        public ValidationExceptionContract getContract() {
            return new ValidationExceptionContract();
        }
    };

    public abstract <T extends ContractAccessor> T getContract();
}
