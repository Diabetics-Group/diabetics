package net.styleru.ikomarov.data_layer.managers.base;

import android.support.annotation.NonNull;

import io.reactivex.ObservableEmitter;
import io.reactivex.disposables.Disposable;

/**
 * Created by i_komarov on 26.04.17.
 */

final class IdentifiableObservableCallback<E> implements ICallback<E> {

    @NonNull
    private final String key;

    @NonNull
    private final ObservableEmitter<E> emitter;

    IdentifiableObservableCallback(@NonNull String key, @NonNull ObservableEmitter<E> emitter) {
        this.key = key;
        this.emitter = emitter;
    }

    @Override
    public void onRegistered() {

    }

    @Override
    public void onEvent(E event) {
        if(!emitter.isDisposed()) {
            emitter.onNext(event);
        }
    }

    @Override
    public void onUnregistered() {

    }

    Disposable createDisposable(CallbacksManager<E, IdentifiableObservableCallback<E>> manager) {
        return new CallbackDisposable<>(key, manager);
    }

    private static final class CallbackDisposable<E> implements Disposable {

        @NonNull
        private final String key;

        @NonNull
        private final CallbacksManager<E, IdentifiableObservableCallback<E>> manager;

        @NonNull
        private volatile boolean isDisposed = false;

        private CallbackDisposable(@NonNull String key, @NonNull CallbacksManager<E, IdentifiableObservableCallback<E>> manager) {
            this.key = key;
            this.manager = manager;
        }

        @Override
        public void dispose() {
            isDisposed = true;
            manager.unregisterListener(key);
        }

        @Override
        public boolean isDisposed() {
            return isDisposed;
        }
    }
}
