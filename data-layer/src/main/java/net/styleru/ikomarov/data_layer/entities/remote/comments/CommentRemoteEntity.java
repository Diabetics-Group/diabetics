package net.styleru.ikomarov.data_layer.entities.remote.comments;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mappings.remote.comments.CommentsRemoteEntityMapping.*;

/**
 * Created by i_komarov on 27.04.17.
 */

public class CommentRemoteEntity {

    @NonNull
    @SerializedName(FIELD_ID)
    private final String id;

    @NonNull
    @SerializedName(FIELD_TOPIC_ID)
    private final String topicId;

    @NonNull
    @SerializedName(FIELD_USER_ID)
    private final String userId;

    @NonNull
    @SerializedName(FIELD_FIRST_NAME)
    private final String firstName;

    @NonNull
    @SerializedName(FIELD_LAST_NAME)
    private final String lastName;

    @Nullable
    @SerializedName(FIELD_IMAGE_URL)
    private final String imageUrl;

    @NonNull
    @SerializedName(FIELD_ROLE)
    private final String role;

    @NonNull
    @SerializedName(FIELD_DATE)
    private final String date;

    @Nullable
    @SerializedName(FIELD_CONTENT)
    private final String content;

    public CommentRemoteEntity(@NonNull String id,
                               @NonNull String topicId,
                               @NonNull String userId,
                               @NonNull String firstName,
                               @NonNull String lastName,
                               @Nullable String imageUrl,
                               @NonNull String role,
                               @NonNull String date,
                               @Nullable String content) {
        this.id = id;
        this.topicId = topicId;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.role = role;
        this.date = date;
        this.content = content;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getTopicId() {
        return topicId;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public String getRole() {
        return role;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    @Nullable
    public String getContent() {
        return content;
    }
}
