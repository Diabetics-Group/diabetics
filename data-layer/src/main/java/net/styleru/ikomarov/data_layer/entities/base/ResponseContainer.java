package net.styleru.ikomarov.data_layer.entities.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mappings.base.ResponseContainer.*;

/**
 * Created by i_komarov on 27.04.17.
 */

public class ResponseContainer<T> {

    @NonNull
    @SerializedName(FIELD_CODE)
    private Integer code;

    @Nullable
    @SerializedName(FIELD_BODY)
    private T body;

    @Nullable
    @SerializedName(FIELD_ERROR)
    private JsonObject error;

    public static <T> ResponseContainer<T> newSuccess(@NonNull Integer code, @Nullable T body) {
        return new ResponseContainer<T>(code, body, null);
    }

    public static <T> ResponseContainer<T> newError(@NonNull Integer code, @Nullable JsonObject error) {
        return new ResponseContainer<T>(code, null, error);
    }

    private ResponseContainer(@NonNull Integer code, @Nullable T body, @Nullable JsonObject error) {
        this.code = code;
        this.body = body;
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    @Nullable
    public T getBody() {
        return body;
    }

    @Nullable
    public JsonObject getError() {
        return error;
    }
}
