package net.styleru.ikomarov.data_layer.mappings.remote_to_local.categories;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.local.categories.CategoryLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.categories.CategoryRemoteEntity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 30.04.17.
 */

public class RemoteToLocalCategoriesEntityMapping implements Function<List<CategoryRemoteEntity>, List<CategoryLocalEntity>> {

    @NonNull
    private final Function<CategoryRemoteEntity, CategoryLocalEntity> singleMapper;

    public RemoteToLocalCategoriesEntityMapping(@NonNull Function<CategoryRemoteEntity, CategoryLocalEntity> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<CategoryLocalEntity> apply(List<CategoryRemoteEntity> categoryRemoteEntities) throws Exception {
        return Observable.fromIterable(categoryRemoteEntities)
                .map(singleMapper)
                .reduce(new ArrayList<CategoryLocalEntity>(), (list, element) -> {
                    list.add(element);
                    return list;
                })
                .blockingGet();
    }
}
