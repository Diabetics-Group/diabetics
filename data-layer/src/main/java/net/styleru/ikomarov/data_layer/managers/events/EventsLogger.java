package net.styleru.ikomarov.data_layer.managers.events;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 03.05.17.
 */

public class EventsLogger implements IEventsLogger {

    @Override
    public void logException(Exception exception) {

    }

    @Override
    public void logEvent(String clazz, String method, ExceptionBundle error) {

    }
}
