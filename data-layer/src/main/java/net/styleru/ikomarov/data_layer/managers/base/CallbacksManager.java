package net.styleru.ikomarov.data_layer.managers.base;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by i_komarov on 26.04.17.
 */

public class CallbacksManager<E, L extends ICallback<E>> {

    protected final ReentrantLock lock;

    private final Map<String, L> callbacks;

    public CallbacksManager() {
        this.lock = new ReentrantLock(true);
        this.callbacks = new HashMap<>();
    }

    public void registerListener(String key, L listener) {
        try {
            lock.lock();
            final L existing;
            if(null != (existing = callbacks.get(key))) {
                callbacks.remove(key);
                existing.onUnregistered();
            }

            callbacks.put(key, listener);
            listener.onRegistered();
        } finally {
            lock.unlock();
        }
    }

    public void unregisterListener(String key) {
        try {
            lock.lock();
            final L existing;
            if(null != (existing = callbacks.get(key))) {
                callbacks.remove(key);
                existing.onUnregistered();
            }
        } finally {
            lock.unlock();
        }
    }

    public void notifyAll(E event) {
        try {
            lock.lock();
            for(Map.Entry<String, L> entry : callbacks()) {
                entry.getValue().onEvent(event);
            }
        } finally {
            lock.unlock();
        }
    }

    public final Set<Map.Entry<String, L>> callbacks() {
        return callbacks.entrySet();
    }
}
