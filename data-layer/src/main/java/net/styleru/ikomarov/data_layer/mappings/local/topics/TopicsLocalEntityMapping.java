package net.styleru.ikomarov.data_layer.mappings.local.topics;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

/**
 * Created by i_komarov on 26.04.17.
 */

public class TopicsLocalEntityMapping {

    @NonNull
    public static final String TABLE = "questions";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_USER_ID = "USER_ID";

    @NonNull
    public static final String COLUMN_FIRST_NAME = "FIRST_NAME";

    @NonNull
    public static final String COLUMN_LAST_NAME = "LAST_NAME";

    @NonNull
    public static final String COLUMN_IMAGE_URL = "IMAGE_URL";

    @NonNull
    public static final String COLUMN_ROLE = "ROLE";

    @NonNull
    public static final String COLUMN_CATEGORY_ID = "CATEGORY_ID";

    @NonNull
    public static final String COLUMN_TITLE = "TITLE";

    @NonNull
    public static final String COLUMN_CONTENT = "CONTENT";

    @NonNull
    public static final String COLUMN_LAST_UPDATE = "LAST_UPDATE";

    @NonNull
    public static final String COLUMN_COMMENTS_COUNT = "COMMENTS_COUNT";

    @NonNull
    public static final String COLUMN_SYNCHRONIZATION_STATUS = "SYNCHRONIZED";

    public static final String COLUMN_ID_WITH_TABLE = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_USER_ID_WITH_TABLE = TABLE + "." + COLUMN_USER_ID;
    public static final String COLUMN_FIRST_NAME_WITH_TABLE = TABLE + "." + COLUMN_FIRST_NAME;
    public static final String COLUMN_LAST_NAME_WITH_TABLE = TABLE + "." + COLUMN_LAST_NAME;
    public static final String COLUMN_IMAGE_URL_WITH_TABLE = TABLE + "." + COLUMN_IMAGE_URL;
    public static final String COLUMN_ROLE_WITH_TABLE = TABLE + "." + COLUMN_ROLE;
    public static final String COLUMN_CATEGORY_ID_WITH_TABLE = TABLE + "." + COLUMN_CATEGORY_ID;
    public static final String COLUMN_TITLE_WITH_TABLE = TABLE + "." + COLUMN_TITLE;
    public static final String COLUMN_CONTENT_WITH_TABLE = TABLE + "." + COLUMN_CONTENT;
    public static final String COLUMN_LAST_UPDATE_WITH_TABLE = TABLE + "." + COLUMN_LAST_UPDATE;
    public static final String COLUMN_COMMENTS_COUNT_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_COMMENTS_COUNT;
    public static final String COLUMN_SYNCHRONIZATION_STATUS_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_SYNCHRONIZATION_STATUS;

    @NonNull
    public static Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    private TopicsLocalEntityMapping() {
        throw new IllegalStateException("No instances please");
    }

    @NonNull
    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE +
                "(" +
                    COLUMN_ID + " TEXT NOT NULL PRIMARY KEY," +
                    COLUMN_USER_ID + " TEXT," +
                    COLUMN_FIRST_NAME + " TEXT," +
                    COLUMN_LAST_NAME + " TEXT," +
                    COLUMN_IMAGE_URL + " TEXT," +
                    COLUMN_ROLE + " TEXT," +
                    COLUMN_CATEGORY_ID + " TEXT," +
                    COLUMN_TITLE + " TEXT," +
                    COLUMN_CONTENT + " TEXT," +
                    COLUMN_LAST_UPDATE + " TIMESTAMP NOT NULL DEFAULT(STRFTIME(" + "\'%Y-%m-%d %H:%M:%f\'" + "," + "\'NOW\'" + "))," +
                    COLUMN_COMMENTS_COUNT + " INTEGER DEFAULT(0)," +
                    COLUMN_SYNCHRONIZATION_STATUS + " BOOLEAN DEFAULT(0)" +
                ");";
    }

    @NonNull
    public static String getDropTableQuery() {
        return "DROP TABLE IF EXISTS " + TABLE + ";";
    }
}
