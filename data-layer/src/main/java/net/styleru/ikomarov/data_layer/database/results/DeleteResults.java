package net.styleru.ikomarov.data_layer.database.results;

import android.support.annotation.NonNull;

import java.util.Map;

/**
 * Created by i_komarov on 30.04.17.
 */

public class DeleteResults {

    @NonNull
    private final String table;

    @NonNull
    private final String query;

    @NonNull
    private final Long deletedRows;

    @NonNull
    private final Map<String, Boolean> statuses;

    public DeleteResults(@NonNull String table, @NonNull String query, @NonNull Long deletedRows, @NonNull Map<String, Boolean> statuses) {
        this.table = table;
        this.query = query;
        this.deletedRows = deletedRows;
        this.statuses = statuses;
    }

    @NonNull
    public String getTable() {
        return table;
    }

    @NonNull
    public String getQuery() {
        return query;
    }

    @NonNull
    public Long getDeletedRows() {
        return deletedRows;
    }

    @NonNull
    public Map<String, Boolean> getStatuses() {
        return statuses;
    }
}
