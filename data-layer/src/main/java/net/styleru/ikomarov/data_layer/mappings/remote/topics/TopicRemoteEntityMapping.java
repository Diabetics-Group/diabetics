package net.styleru.ikomarov.data_layer.mappings.remote.topics;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 27.04.17.
 */

public class TopicRemoteEntityMapping {

    @NonNull
    public static final String FIELD_ID = "questionID";

    @NonNull
    public static final String FIELD_USER_IR = "userID";

    @NonNull
    public static final String FIELD_CATEGORY_ID = "categoryID";

    @NonNull
    public static final String FIELD_FIRST_NAME = "name";

    @NonNull
    public static final String FIELD_LAST_NAME = "surname";

    @NonNull
    public static final String FIELD_IMAGE_URL = "image";

    @NonNull
    public static final String FIELD_ROLE = "role";

    @NonNull
    public static final String FIELD_TITLE = "title";

    @NonNull
    public static final String FIELD_CONTENT = "text";

    @NonNull
    public static final String FIELD_LAST_UPDATE = "lastUpdate";

    @NonNull
    public static final String FIELD_COMMENTS_COUNT = "count";
}
