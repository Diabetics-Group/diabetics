package net.styleru.ikomarov.data_layer.services.users;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.database.results.DeleteResults;
import net.styleru.ikomarov.data_layer.database.results.InsertResults;
import net.styleru.ikomarov.data_layer.database.statements.users.UsersPreparedStatements;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.data_layer.mappings.local.topics.TopicsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.services.base.AbstractDatabaseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import io.reactivex.Single;

/**
 * Created by i_komarov on 29.04.17.
 */

public class UsersLocalService extends AbstractDatabaseService implements IUsersLocalService {

    @NonNull
    private final ReentrantLock lock;

    @NonNull
    private final SQLiteDatabase db;

    @NonNull
    private final UsersPreparedStatements statements;

    private UsersLocalService(@NonNull SQLiteDatabase db) {
        this.lock = new ReentrantLock(true);
        this.db = db;
        this.statements = new UsersPreparedStatements(db);
    }

    @Override
    public Single<InsertResults> create(@NonNull List<UserLocalEntity> entities) {
        return Single.create(emitter -> {
            try {
                long insertedRows = 0L;
                long updatedRows = 0L;
                Map<String, Boolean> statuses = new HashMap<>(entities.size());

                lock.lock();
                db.execSQL("PRAGMA synchronous=\'OFF\'");
                db.beginTransaction();

                SQLiteStatement statement = statements.insertSingle();

                for(UserLocalEntity entity : entities) {
                    statement.clearBindings();
                    String id;

                    statement.bindString(1, id = entity.getId());
                    statement.bindString(2, entity.getFirstName());
                    statement.bindString(3, entity.getLastName());

                    String imageUrl;
                    if(null != (imageUrl = entity.getImageUrl())) {
                        statement.bindString(4, imageUrl);
                    }

                    statement.bindString(5, entity.getRole());

                    long status = statement.executeInsert();
                    if(status != -1L) {
                        if(statuses.get(id) != null) {
                            updatedRows++;
                        } else {
                            insertedRows++;
                        }

                        statuses.put(id, true);
                    } else {
                        statuses.put(id, false);
                    }
                }

                db.setTransactionSuccessful();

                emitter.onSuccess(
                        new InsertResults(
                                TopicsLocalEntityMapping.TABLE,
                                statement.toString(),
                                insertedRows,
                                updatedRows,
                                statuses
                        ));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                db.endTransaction();
                db.execSQL("PRAGMA synchronous=\'NORMAL\'");
                lock.unlock();
            }
        });
    }

    @Override
    public Single<UserLocalEntity> get(@NonNull String userId) {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectSingle(userId), null);
                emitter.onSuccess(UserLocalEntity.fromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<List<UserLocalEntity>> list(int offset, int limit) {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectOffsetLimit(offset, limit), null);
                emitter.onSuccess(UserLocalEntity.listFromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<DeleteResults> delete(@NonNull List<String> userIds) {
        return Single.create(emitter -> {
            try {
                long deletedRows = 0L;
                Map<String, Boolean> statuses = new HashMap<>(userIds.size());

                lock.lock();
                db.execSQL("PRAGMA synchronous=\'OFF\'");
                db.beginTransaction();

                SQLiteStatement statement = statements.deleteSingle();

                for(String id : userIds) {
                    statement.clearBindings();
                    statement.bindString(1, id);

                    long status = statement.executeUpdateDelete();
                    if(status != 0L && status != -1L) {
                        deletedRows++;
                        statuses.put(id, true);
                    } else {
                        statuses.put(id, false);
                    }
                }

                db.setTransactionSuccessful();

                emitter.onSuccess(
                        new DeleteResults(
                                TopicsLocalEntityMapping.TABLE,
                                statement.toString(),
                                deletedRows,
                                statuses
                        ));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                db.endTransaction();
                db.execSQL("PRAGMA synchronous=\'NORMAL\'");
                lock.unlock();
            }
        });
    }

    public static final class Factory {

        @NonNull
        private final SQLiteDatabase db;

        public Factory(@NonNull SQLiteDatabase db) {
            this.db = db;
        }

        public IUsersLocalService create() {
            return new UsersLocalService(db);
        }
    }
}
