package net.styleru.ikomarov.data_layer.entities.local.topics;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import net.styleru.ikomarov.data_layer.database.utils.CursorIndexCache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static net.styleru.ikomarov.data_layer.mappings.local.topics.TopicsLocalEntityMapping.*;

/**
 * Created by i_komarov on 26.04.17.
 */

@StorIOSQLiteType(table = TABLE)
public class TopicLocalEntity {

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_ID, key = true)
    String id;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_USER_ID, ignoreNull = true)
    String userId;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_FIRST_NAME, ignoreNull = true)
    String firstName;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_LAST_NAME, ignoreNull = true)
    String lastName;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_IMAGE_URL, ignoreNull = true)
    String imageUrl;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_ROLE, ignoreNull = true)
    String role;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_CATEGORY_ID, ignoreNull = true)
    String categoryId;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_TITLE, ignoreNull = true)
    String title;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_CONTENT, ignoreNull = true)
    String content;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_LAST_UPDATE, ignoreNull = true)
    String lastUpdate;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_COMMENTS_COUNT, ignoreNull = true)
    Long commentsCount;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_SYNCHRONIZATION_STATUS, ignoreNull = true)
    Boolean isSynchronized;

    @NonNull
    public static TopicLocalEntity fromCursor(@NonNull Cursor cursor) {
        TopicLocalEntity entity = new TopicLocalEntity();

        entity.id = cursor.getString(cursor.getColumnIndex(COLUMN_ID));
        entity.userId = cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID));
        entity.firstName = cursor.getString(cursor.getColumnIndex(COLUMN_FIRST_NAME));
        entity.lastName = cursor.getString(cursor.getColumnIndex(COLUMN_LAST_NAME));
        entity.imageUrl = cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE_URL));
        entity.role = cursor.getString(cursor.getColumnIndex(COLUMN_ROLE));
        entity.categoryId = cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_ID));
        entity.title = cursor.getString(cursor.getColumnIndex(COLUMN_TITLE));
        entity.content = cursor.getString(cursor.getColumnIndex(COLUMN_CONTENT));
        entity.lastUpdate = cursor.getString(cursor.getColumnIndex(COLUMN_LAST_UPDATE));
        entity.commentsCount = cursor.getLong(cursor.getColumnIndex(COLUMN_COMMENTS_COUNT));
        entity.isSynchronized = cursor.getInt(cursor.getColumnIndex(COLUMN_SYNCHRONIZATION_STATUS)) == 1;

        return entity;
    }

    @NonNull
    public static List<TopicLocalEntity> listFromCursor(@NonNull Cursor cursor) {
        CursorIndexCache cache = new CursorIndexCache();
        List<TopicLocalEntity> entities = new ArrayList<>(cursor.getCount());
        if(cursor.moveToFirst()) {
            do {
                TopicLocalEntity entity = new TopicLocalEntity();

                entity.id = cursor.getString(cache.index(cursor, COLUMN_ID));
                entity.userId = cursor.getString(cache.index(cursor, COLUMN_USER_ID));
                entity.firstName = cursor.getString(cache.index(cursor, COLUMN_FIRST_NAME));
                entity.lastName = cursor.getString(cache.index(cursor, COLUMN_LAST_NAME));
                entity.imageUrl = cursor.getString(cache.index(cursor, COLUMN_IMAGE_URL));
                entity.role = cursor.getString(cache.index(cursor, COLUMN_ROLE));
                entity.categoryId = cursor.getString(cache.index(cursor, COLUMN_CATEGORY_ID));
                entity.title = cursor.getString(cache.index(cursor, COLUMN_TITLE));
                entity.content = cursor.getString(cache.index(cursor, COLUMN_CONTENT));
                entity.lastUpdate = cursor.getString(cache.index(cursor, COLUMN_LAST_UPDATE));
                entity.commentsCount = cursor.getLong(cache.index(cursor, COLUMN_COMMENTS_COUNT));
                entity.isSynchronized = cursor.getInt(cache.index(cursor, COLUMN_SYNCHRONIZATION_STATUS)) == 1;

                entities.add(entity);
            } while(cursor.moveToNext());
        }

        return Collections.unmodifiableList(entities);
    }

    TopicLocalEntity() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public String getRole() {
        return role;
    }

    @NonNull
    public String getCategoryId() {
        return categoryId;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    @NonNull
    public String getLastUpdate() {
        return lastUpdate;
    }

    @NonNull
    public Long getCommentsCount() {
        return commentsCount;
    }

    @NonNull
    public Boolean isSynchronized() {
        return isSynchronized;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TopicLocalEntity that = (TopicLocalEntity) o;

        if (!id.equals(that.id)) return false;
        if (!userId.equals(that.userId)) return false;
        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        if (imageUrl != null ? !imageUrl.equals(that.imageUrl) : that.imageUrl != null)
            return false;
        if (!role.equals(that.role)) return false;
        if (!categoryId.equals(that.categoryId)) return false;
        if (!title.equals(that.title)) return false;
        if (!content.equals(that.content)) return false;
        if (!lastUpdate.equals(that.lastUpdate)) return false;
        if (!commentsCount.equals(that.commentsCount)) return false;
        return isSynchronized.equals(that.isSynchronized);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + userId.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + role.hashCode();
        result = 31 * result + categoryId.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + content.hashCode();
        result = 31 * result + lastUpdate.hashCode();
        result = 31 * result + commentsCount.hashCode();
        result = 31 * result + isSynchronized.hashCode();
        return result;
    }

    public static final class Builder {

        @NonNull
        private final TopicLocalEntity entity;

        public Builder(@NonNull String userId,
                       @NonNull String firstName,
                       @NonNull String lastName,
                       @Nullable String imageUrl,
                       @NonNull String role,
                       @NonNull String categoryId,
                       @NonNull String title,
                       @NonNull String content,
                       @NonNull String lastUpdate) {
            entity = new TopicLocalEntity();
            entity.userId = userId;
            entity.firstName = firstName;
            entity.lastName = lastName;
            entity.imageUrl = imageUrl;
            entity.role = role;
            entity.categoryId = categoryId;
            entity.title = title;
            entity.content = content;
            entity.lastUpdate = lastUpdate;
        }

        @NonNull
        public SynchronizedBuilder mutateSynchronized(String id) {
            entity.id = id;
            return new SynchronizedBuilder(entity);
        }

        @NonNull
        public TopicLocalEntity create(UUID generatedId) {
            entity.id = generatedId.toString();
            entity.commentsCount = 0L;
            entity.isSynchronized = false;
            return entity;
        }
    }

    public static final class SynchronizedBuilder {

        @NonNull
        private final TopicLocalEntity entity;

        private SynchronizedBuilder(@NonNull TopicLocalEntity preBuiltEntity) {
            entity = preBuiltEntity;
        }

        @NonNull
        public SynchronizedBuilder withCommentsCount(@NonNull Long commentsCount) {
            entity.commentsCount = commentsCount;
            return this;
        }

        @NonNull
        public TopicLocalEntity create() {
            entity.isSynchronized = true;
            return entity;
        }
    }
}
