package net.styleru.ikomarov.data_layer.mappings.remote_to_local.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.local.topics.TopicLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.topics.TopicRemoteEntity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 30.04.17.
 */

public class RemoteToLocalTopicsEntityMapping implements Function<List<TopicRemoteEntity>, List<TopicLocalEntity>> {

    @NonNull
    private final Function<TopicRemoteEntity, TopicLocalEntity> singleMapper;

    public RemoteToLocalTopicsEntityMapping(@NonNull Function<TopicRemoteEntity, TopicLocalEntity> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<TopicLocalEntity> apply(List<TopicRemoteEntity> topicRemoteEntities) throws Exception {
        return Observable.fromIterable(topicRemoteEntities)
                .map(singleMapper)
                .reduce(new ArrayList<TopicLocalEntity>(), (list, element) -> {
                    list.add(element);
                    return list;
                })
                .blockingGet();
    }
}
