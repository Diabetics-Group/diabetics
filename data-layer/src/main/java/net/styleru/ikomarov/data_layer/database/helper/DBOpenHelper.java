package net.styleru.ikomarov.data_layer.database.helper;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.BuildConfig;
import net.styleru.ikomarov.data_layer.mappings.local.categories.CategoriesLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mappings.local.comments.CommentsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mappings.local.topics.TopicsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mappings.local.users.UsersLocalEntityMapping;

/**
 * Created by i_komarov on 27.04.17.
 */

public class DBOpenHelper extends SQLiteOpenHelper {

    public DBOpenHelper(@NonNull Context context) {
        super(context, BuildConfig.DATABASE_NAME, null, BuildConfig.DATABASE_VERSION);
    }

    public DBOpenHelper(@NonNull Context context, @NonNull DatabaseErrorHandler errorHandler) {
        super(context, BuildConfig.DATABASE_NAME, null, BuildConfig.DATABASE_VERSION, errorHandler);
    }

    @Override
    public void onCreate(@NonNull SQLiteDatabase db) {
        db.execSQL("PRAGMA recursive_triggers=\'ON\'");
        createTables(db);
    }

    @Override
    public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO: replace with correct schema change code as soon as new schema is applied
        dropTables(db);
        createTables(db);
    }

    public void dropTables(@NonNull SQLiteDatabase db) {
        db.execSQL(CategoriesLocalEntityMapping.getDropTableQuery());
        db.execSQL(TopicsLocalEntityMapping.getDropTableQuery());
        db.execSQL(CommentsLocalEntityMapping.getDropTableQuery());
        db.execSQL(UsersLocalEntityMapping.getDropTableQuery());
    }

    public void createTables(@NonNull SQLiteDatabase db) {
        db.execSQL(CategoriesLocalEntityMapping.getCreateTableQuery());
        db.execSQL(TopicsLocalEntityMapping.getCreateTableQuery());
        db.execSQL(CommentsLocalEntityMapping.getCreateTableQuery());
        db.execSQL(UsersLocalEntityMapping.getCreateTableQuery());
    }
}
