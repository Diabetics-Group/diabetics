package net.styleru.ikomarov.data_layer.contracts.exception;

import android.support.annotation.NonNull;
import android.util.SparseArray;

import net.styleru.ikomarov.data_layer.contracts.base.ContractAccessor;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 27.04.17.
 */

public class NetworkExceptionContract extends ContractAccessor {

    @NonNull
    private static final String PREFIX = NetworkExceptionContract.class.getCanonicalName() + ".";

    @NonNull
    private final String KEY_TYPE = PREFIX + "TYPE";

    public Type extractType(@NonNull final ExceptionBundle bundle) {
        return Type.deserialize(bundle.getInt(KEY_TYPE));
    }

    public void putType(@NonNull final ExceptionBundle bundle, Type type) {
        bundle.putInt(KEY_TYPE, type.code);
    }

    public enum Type {
        UNAVAILABLE         (0x0000000),
        SSL_HANDSHAKE_FAILED(0x0000001);

        private static final SparseArray<Type> typesMap = new SparseArray<>(Type.values().length);

        static {
            for(Type type : Type.values()) {
                typesMap.put(type.code, type);
            }
        }

        private final int code;

        public static Type deserialize(int code) {
            return typesMap.get(code);
        }

        Type(int code) {
            this.code = code;
        }
    }
}
