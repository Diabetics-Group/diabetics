package net.styleru.ikomarov.data_layer.contracts.exception;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.base.ContractAccessor;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 27.04.17.
 */

public class InternalExceptionContract extends ContractAccessor {

    @NonNull
    private static final String PREFIX = InternalExceptionContract.class.getCanonicalName() + ".";

    @NonNull
    private final String KEY_THROWABLE = PREFIX + "CODE";

    @NonNull
    private final String KEY_MESSAGE = PREFIX + "MESSAGE";

    public InternalExceptionContract() {

    }

    @NonNull
    public Throwable extractThrowable(@NonNull final ExceptionBundle bundle) {
        return (Throwable) bundle.getSerializable(KEY_THROWABLE);
    }

    public void putThrowable(@NonNull final ExceptionBundle bundle, @NonNull Throwable throwable) {
        bundle.putSerializable(KEY_THROWABLE, throwable);
    }

    @NonNull
    public String extractMessage(@NonNull final ExceptionBundle bundle) {
        return bundle.getString(KEY_MESSAGE);
    }

    public void putMessage(@NonNull final ExceptionBundle bundle, @NonNull String message) {
        bundle.putString(KEY_MESSAGE, message);
    }
}
