package net.styleru.ikomarov.data_layer.services.topics;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.remote.topics.TopicRemoteEntity;
import net.styleru.ikomarov.data_layer.services.base.AbstractNetworkService;

import java.util.List;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by i_komarov on 28.04.17.
 */

public class TopicsRemoteService extends AbstractNetworkService<ITopicsRemoteService> implements ITopicsRemoteService {

    private TopicsRemoteService(@NonNull OkHttpClient client) {
        super(Endpoints.URL_DIABETICS, client);
    }

    @Override
    public Single<Response<ResponseContainer<TopicRemoteEntity>>> create(String userId, String categoryId, String title, String content) {
        return getService().create(userId, categoryId, title, content)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<TopicRemoteEntity>>>> get(String topicId) {
        return getService().get(topicId)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<TopicRemoteEntity>>>> list(String categoryId) {
        return getService().list(categoryId)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<TopicRemoteEntity>>>> list(String lastModified, String categoryId) {
        return getService().list(lastModified, categoryId)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<TopicRemoteEntity>>>> list(String categoryId, int offset, int limit) {
        return getService().list(categoryId, offset, limit)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<TopicRemoteEntity>>>> list(String lastModified, String categoryId, int offset, int limit) {
        return getService().list(lastModified, categoryId, offset, limit)
                .compose(handleError());
    }

    @Override
    protected ITopicsRemoteService createService(Retrofit retrofit) {
        return retrofit.create(ITopicsRemoteService.class);
    }

    @Override
    protected Gson getGson() {
        return new Gson();
    }

    public static final class Factory {

        @NonNull
        private final OkHttpClient client;

        public Factory(@NonNull OkHttpClient client) {
            this.client = client;
        }

        @NonNull
        public ITopicsRemoteService create() {
            return new TopicsRemoteService(client);
        }
    }
}
