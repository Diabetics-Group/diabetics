package net.styleru.ikomarov.data_layer.database.statements.categories;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.mappings.local.categories.CategoriesLocalEntityMapping;

/**
 * Created by i_komarov on 26.04.17.
 */

public class CategoriesPreparedStatements {

    @NonNull
    private final String SELECT_ALL;

    @NonNull
    private final String SELECT_OFFSET_LIMIT;

    @NonNull
    private final String SELECT_NOT_SYNCHRONIZED;

    @NonNull
    private final String SELECT_SINGLE;

    @NonNull
    private final SQLiteStatement INSERT_SINGLE;

    @NonNull
    private final SQLiteStatement DELETE_SINGLE;

    public CategoriesPreparedStatements(@NonNull SQLiteDatabase db) {
        SELECT_ALL = "SELECT * FROM " + CategoriesLocalEntityMapping.TABLE;
        SELECT_OFFSET_LIMIT = "SELECT * FROM " + CategoriesLocalEntityMapping.TABLE + " LIMIT ? OFFSET ?";
        SELECT_NOT_SYNCHRONIZED = "SELECT * FROM " + CategoriesLocalEntityMapping.TABLE + " WHERE " + CategoriesLocalEntityMapping.COLUMN_SYNCHRONIZATION_STATUS + " = 0";
        SELECT_SINGLE = "SELECT * FROM " + CategoriesLocalEntityMapping.TABLE + " WHERE " + CategoriesLocalEntityMapping.COLUMN_ID + " = " + "?";
        INSERT_SINGLE = db.compileStatement(
                "INSERT OR REPLACE INTO " + CategoriesLocalEntityMapping.TABLE + " (" +
                        CategoriesLocalEntityMapping.COLUMN_ID + "," +
                        CategoriesLocalEntityMapping.COLUMN_TITLE + "," +
                        CategoriesLocalEntityMapping.COLUMN_LAST_UPDATE + "," +
                        CategoriesLocalEntityMapping.COLUMN_TOPICS_COUNT + "," +
                        CategoriesLocalEntityMapping.COLUMN_SYNCHRONIZATION_STATUS + ")" +
                " VALUES " + "(" +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" +
                ");"
        );

        DELETE_SINGLE = db.compileStatement("DELETE FROM " + CategoriesLocalEntityMapping.TABLE + " WHERE " + CategoriesLocalEntityMapping.COLUMN_ID + " = " + "?");
    }

    @NonNull
    public String selectAll() {
        return SELECT_ALL;
    }

    @NonNull
    public String selectOffsetLimit(int offset, int limit) {
        String selectOffsetLimit = this.selectOffsetLimit();
        selectOffsetLimit = selectOffsetLimit.replaceFirst("\\?", String.valueOf(limit));
        selectOffsetLimit = selectOffsetLimit.replaceFirst("\\?", String.valueOf(offset));
        return selectOffsetLimit;
    }

    @NonNull
    private String selectOffsetLimit() {
        return SELECT_OFFSET_LIMIT;
    }

    @NonNull
    public String selectNotSynchronized() {
        return SELECT_NOT_SYNCHRONIZED;
    }

    @NonNull
    public String selectSingle(String id) {
        String selectSingle = this.selectSingle();
        selectSingle = selectSingle.replaceFirst("\\?", "\"" + id + "\"");
        return selectSingle;
    }

    @NonNull
    private String selectSingle() {
        return SELECT_SINGLE;
    }

    @NonNull
    public SQLiteStatement insertSingle() {
        return INSERT_SINGLE;
    }

    @NonNull
    public SQLiteStatement deleteSingle() {
        return DELETE_SINGLE;
    }
}
