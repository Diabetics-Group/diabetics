package net.styleru.ikomarov.data_layer.services.comments;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.remote.categories.CategoryRemoteEntity;
import net.styleru.ikomarov.data_layer.entities.remote.comments.CommentRemoteEntity;
import net.styleru.ikomarov.data_layer.routing.comments.CommentsRouting;
import net.styleru.ikomarov.data_layer.routing.base.DefaultParams;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by i_komarov on 26.04.17.
 */

public interface ICommentsRemoteService {

    @FormUrlEncoded
    @POST(Endpoints.ENDPOINT_COMMENTS)
    Single<Response<ResponseContainer<CommentRemoteEntity>>> create(
            @Field(CommentsRouting.FIELD_USER_ID) String userId,
            @Field(CommentsRouting.FIELD_TOPIC_ID) String topicId,
            @Field(CommentsRouting.FIELD_CONTENT) String content
    );

    @GET(Endpoints.ENDPOINT_COMMENTS)
    Single<Response<ResponseContainer<List<CommentRemoteEntity>>>> list(
            @Header(CommentsRouting.FIELD_TOPIC_ID) String topicId
    );

    @GET(Endpoints.ENDPOINT_COMMENTS)
    Single<Response<ResponseContainer<List<CommentRemoteEntity>>>> list(
            @Header(DefaultParams.HEADER_IF_MODIFIED_SINCE) String lastModified,
            @Query(CommentsRouting.FIELD_TOPIC_ID) String topicId
    );

    @GET(Endpoints.ENDPOINT_COMMENTS)
    Single<Response<ResponseContainer<List<CommentRemoteEntity>>>> list(
            @Query(CommentsRouting.FIELD_TOPIC_ID) String topicId,
            @Query(value = DefaultParams.FIELD_OFFSET, encoded = true) int offset,
            @Query(value = DefaultParams.FIELD_LIMIT, encoded = true) int limit
    );

    @GET(Endpoints.ENDPOINT_COMMENTS)
    Single<Response<ResponseContainer<List<CommentRemoteEntity>>>> list(
            @Header(DefaultParams.HEADER_IF_MODIFIED_SINCE) String lastModified,
            @Query(CommentsRouting.FIELD_TOPIC_ID) String topicId,
            @Query(value = DefaultParams.FIELD_OFFSET, encoded = true) int offset,
            @Query(value = DefaultParams.FIELD_LIMIT, encoded = true) int limit
    );

    @DELETE(Endpoints.ENDPOINT_COMMENTS)
    Single<Response<Object>> delete(
            @Query(CommentsRouting.FIELD_USER_ID) String userId,
            @Query(CommentsRouting.FIELD_COMMENT_ID) String commentId
    );
}
