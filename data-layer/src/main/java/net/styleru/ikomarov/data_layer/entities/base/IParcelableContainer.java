package net.styleru.ikomarov.data_layer.entities.base;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by i_komarov on 27.04.17.
 */

public interface IParcelableContainer {

    void putString(String key, String value);

    void putInt(String key, int value);

    void putBoolean(String key, boolean value);

    void putParcelable(String key, Parcelable value);

    void putSerializable(String key, Serializable value);

    String getString(String key);

    int getInt(String key);

    boolean getBoolean(String key);

    Parcelable getParcelable(String key);

    Serializable getSerializable(String key);
}
