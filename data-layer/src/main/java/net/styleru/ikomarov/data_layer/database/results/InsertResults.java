package net.styleru.ikomarov.data_layer.database.results;

import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.Map;

/**
 * Created by i_komarov on 29.04.17.
 */

public class InsertResults {

    @NonNull
    private final String table;

    @NonNull
    private final String query;

    @NonNull
    private final Long insertedRows;

    @NonNull
    private final Long updatedRows;

    @NonNull
    private final Map<String, Boolean> statuses;

    public InsertResults(@NonNull String table, @NonNull String query, @NonNull Long insertedRows, @NonNull Long updatedRows, @NonNull Map<String, Boolean> statuses) {
        this.table = table;
        this.query = query;
        this.insertedRows = insertedRows;
        this.updatedRows = updatedRows;
        this.statuses = statuses;
    }

    @NonNull
    public String getTable() {
        return table;
    }

    @NonNull
    public String getQuery() {
        return query;
    }

    @NonNull
    public Long getInsertedRows() {
        return insertedRows;
    }

    @NonNull
    public Long getUpdatedRows() {
        return updatedRows;
    }

    @NonNull
    public Map<String, Boolean> getStatuses() {
        return statuses;
    }

    @Override
    public String toString() {
        return "InsertResults{" +
                "table='" + table + '\'' +
                ", query='" + query + '\'' +
                ", insertedRows=" + insertedRows +
                ", updatedRows=" + updatedRows +
                ", statuses=" + Arrays.toString(statuses.entrySet().toArray(new Map.Entry[statuses.size()])) +
        '}';
    }
}
