package net.styleru.ikomarov.data_layer.services.topics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.database.results.DeleteResults;
import net.styleru.ikomarov.data_layer.database.results.InsertResults;
import net.styleru.ikomarov.data_layer.entities.local.topics.TopicLocalEntity;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by i_komarov on 26.04.17.
 */

public interface ITopicsLocalService {

    Single<InsertResults> create(@NonNull List<TopicLocalEntity> entities);

    Single<TopicLocalEntity> get(@NonNull String id);

    Single<List<TopicLocalEntity>> listNotSynchronized();

    Single<List<TopicLocalEntity>> list(@NonNull String categoryId);

    Single<List<TopicLocalEntity>> list(@NonNull String categoryId, int offset, int limit);

    Single<DeleteResults> delete(@NonNull List<String> topicIds);

    Single<DeleteResults> deleteByCategory(@NonNull String categoryId);
}
