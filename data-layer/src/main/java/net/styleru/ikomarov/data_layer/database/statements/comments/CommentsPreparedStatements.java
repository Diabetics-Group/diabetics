package net.styleru.ikomarov.data_layer.database.statements.comments;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.mappings.local.comments.CommentsLocalEntityMapping;

/**
 * Created by i_komarov on 26.04.17.
 */

public class CommentsPreparedStatements {

    @NonNull
    private final String SELECT_ALL;

    @NonNull
    private final String SELECT_OFFSET_LIMIT;

    @NonNull
    private final String SELECT_NOT_SYNCHRONIZED;

    @NonNull
    private final SQLiteStatement INSERT_SINGLE;

    @NonNull
    private final SQLiteStatement DELETE_SINGLE;

    @NonNull
    private final SQLiteStatement DELETE_BY_TOPIC;

    public CommentsPreparedStatements(@NonNull SQLiteDatabase db) {
        SELECT_ALL = "SELECT * FROM " + CommentsLocalEntityMapping.TABLE + " WHERE " + CommentsLocalEntityMapping.COLUMN_TOPIC_ID + " = " + "? ORDER BY " + CommentsLocalEntityMapping.COLUMN_DATE + " DESC";
        SELECT_OFFSET_LIMIT = "SELECT * FROM " + CommentsLocalEntityMapping.TABLE + " WHERE " + CommentsLocalEntityMapping.COLUMN_TOPIC_ID + " = " + "? ORDER BY " + CommentsLocalEntityMapping.COLUMN_DATE + " DESC LIMIT ? OFFSET ?";
        SELECT_NOT_SYNCHRONIZED = "SELECT * FROM " + CommentsLocalEntityMapping.TABLE + " WHERE " + CommentsLocalEntityMapping.COLUMN_SYNCHRONIZATION_STATUS + " = 0 ORDER BY " + CommentsLocalEntityMapping.COLUMN_DATE + " DESC";
        INSERT_SINGLE = db.compileStatement(
                "INSERT OR REPLACE INTO " + CommentsLocalEntityMapping.TABLE + " (" +
                        CommentsLocalEntityMapping.COLUMN_ID + "," +
                        CommentsLocalEntityMapping.COLUMN_TOPIC_ID + "," +
                        CommentsLocalEntityMapping.COLUMN_USER_ID + "," +
                        CommentsLocalEntityMapping.COLUMN_FIRST_NAME + "," +
                        CommentsLocalEntityMapping.COLUMN_LAST_NAME + "," +
                        CommentsLocalEntityMapping.COLUMN_IMAGE_URL + "," +
                        CommentsLocalEntityMapping.COLUMN_ROLE + "," +
                        CommentsLocalEntityMapping.COLUMN_DATE + "," +
                        CommentsLocalEntityMapping.COLUMN_CONTENT + "," +
                        CommentsLocalEntityMapping.COLUMN_SYNCHRONIZATION_STATUS + ")" +
                " VALUES (" +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" +
                ");"
        );
        DELETE_SINGLE = db.compileStatement("DELETE FROM " + CommentsLocalEntityMapping.TABLE + " WHERE " + CommentsLocalEntityMapping.COLUMN_ID + " = " + "?");
        DELETE_BY_TOPIC = db.compileStatement("DELETE FROM " + CommentsLocalEntityMapping.TABLE + " WHERE " + CommentsLocalEntityMapping.COLUMN_TOPIC_ID + " = " + "?");
    }

    @NonNull
    public String selectAll(String topicId) {
        return SELECT_ALL.replaceFirst("\\?", topicId);
    }

    @NonNull
    public String selectOffsetLimit(String topicId, int offset, int limit) {
        return SELECT_OFFSET_LIMIT.replaceFirst("\\?", topicId).replaceFirst("\\?", String.valueOf(limit)).replaceFirst("\\?", String.valueOf(offset));
    }

    @NonNull
    public String selectNotSynchronized() {
        return SELECT_NOT_SYNCHRONIZED;
    }

    @NonNull
    public SQLiteStatement insertSingle() {
        return INSERT_SINGLE;
    }

    @NonNull
    public SQLiteStatement deleteSingle() {
        return DELETE_SINGLE;
    }

    @NonNull
    public SQLiteStatement deleteByTopic() {
        return DELETE_BY_TOPIC;
    }
}
