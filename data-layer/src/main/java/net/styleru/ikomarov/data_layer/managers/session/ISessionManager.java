package net.styleru.ikomarov.data_layer.managers.session;

import net.styleru.ikomarov.data_layer.contracts.session.SessionStatus;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

import io.reactivex.Single;

/**
 * Created by i_komarov on 03.05.17.
 */

public interface ISessionManager {

    Single<SessionStatus> getSessionStatus();

    Single<String> getSessionToken();

    String blockingGetSessionToken() throws ExceptionBundle;

    Single<String> refreshAndGetSessionToken();

    String blockingRefreshAndGetSessionToken() throws ExceptionBundle;

    void registerCallback(String key, AuthenticationCallbacks callbacks);

    void unregisterCallback(String key);

    void deauthenticate();

    void authenticate(String code) throws ExceptionBundle;
}
