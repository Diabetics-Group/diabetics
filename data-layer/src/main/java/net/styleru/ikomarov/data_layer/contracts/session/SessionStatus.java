package net.styleru.ikomarov.data_layer.contracts.session;

import android.util.SparseArray;

/**
 * Created by i_komarov on 03.05.17.
 */

public enum  SessionStatus {
    ACTIVE         (0x0000000),
    INACTIVE       (0x0000001),
    AUTHENTICATING (0x0000002);

    private static final SparseArray<SessionStatus> statusMap;

    static {
        statusMap = new SparseArray<>(SessionStatus.values().length);

        for(SessionStatus val : SessionStatus.values()) {
            statusMap.put(val.getCode(), val);
        }
    }

    private final int code;

    public static SessionStatus forValue(int code) {
        return statusMap.get(code);
    }

    SessionStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}
