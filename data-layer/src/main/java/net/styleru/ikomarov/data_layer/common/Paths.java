package net.styleru.ikomarov.data_layer.common;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 26.04.17.
 */

public class Paths {

    @NonNull
    public static final String PATH_PARENT = "{PARENT-ID}";

    @NonNull
    public static final String PATH_SELF = "{SELF-ID}";
}
