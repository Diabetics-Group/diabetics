package net.styleru.ikomarov.data_layer.services.topics;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.remote.topics.TopicRemoteEntity;
import net.styleru.ikomarov.data_layer.routing.base.DefaultParams;
import net.styleru.ikomarov.data_layer.routing.topics.TopicsRouting;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by i_komarov on 26.04.17.
 */

public interface ITopicsRemoteService {

    @FormUrlEncoded
    @POST(Endpoints.ENDPOINT_TOPICS)
    Single<Response<ResponseContainer<TopicRemoteEntity>>> create(
            @Field(TopicsRouting.FIELD_USER_ID) String userId,
            @Field(TopicsRouting.FIELD_CATEGORY_ID) String categoryId,
            @Field(TopicsRouting.FIELD_TITLE) String title,
            @Field(TopicsRouting.FIELD_CONTENT) String content
    );

    @GET(Endpoints.ENDPOINT_TOPICS)
    Single<Response<ResponseContainer<List<TopicRemoteEntity>>>> get(
            @Query(TopicsRouting.FIELD_TOPIC_ID) String topicId
    );

    @GET(Endpoints.ENDPOINT_TOPICS)
    Single<Response<ResponseContainer<List<TopicRemoteEntity>>>> list(
            @Query(TopicsRouting.FIELD_CATEGORY_ID) String categoryId
    );

    @GET(Endpoints.ENDPOINT_TOPICS)
    Single<Response<ResponseContainer<List<TopicRemoteEntity>>>> list(
            @Header(DefaultParams.HEADER_IF_MODIFIED_SINCE) String lastModified,
            @Query(TopicsRouting.FIELD_CATEGORY_ID) String categoryId
    );

    @GET(Endpoints.ENDPOINT_TOPICS)
    Single<Response<ResponseContainer<List<TopicRemoteEntity>>>> list(
            @Query(TopicsRouting.FIELD_CATEGORY_ID) String categoryId,
            @Query(value = DefaultParams.FIELD_OFFSET, encoded = true) int offset,
            @Query(value = DefaultParams.FIELD_LIMIT, encoded = true) int limit
    );

    @GET(Endpoints.ENDPOINT_TOPICS)
    Single<Response<ResponseContainer<List<TopicRemoteEntity>>>> list(
            @Header(DefaultParams.HEADER_IF_MODIFIED_SINCE) String lastModified,
            @Query(TopicsRouting.FIELD_CATEGORY_ID) String categoryId,
            @Query(value = DefaultParams.FIELD_OFFSET, encoded = true) int offset,
            @Query(value = DefaultParams.FIELD_LIMIT, encoded = true) int limit
    );
}
