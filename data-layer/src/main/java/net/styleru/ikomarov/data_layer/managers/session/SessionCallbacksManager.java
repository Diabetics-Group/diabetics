package net.styleru.ikomarov.data_layer.managers.session;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by i_komarov on 03.05.17.
 */

public class SessionCallbacksManager implements AuthenticationCallbacks {

    @NonNull
    private final ReentrantLock lock;

    @NonNull
    private final Map<String, AuthenticationCallbacks> callbacks;

    public SessionCallbacksManager() {
        this.lock = new ReentrantLock(true);
        this.callbacks = new HashMap<>();
    }

    public void register(@NonNull String key, @NonNull AuthenticationCallbacks callbacks) {
        try {
            lock.lock();
            this.callbacks.put(key, callbacks);
        } finally {
            lock.unlock();
        }
    }

    public void unregister(@NonNull String key) {
        try {
            lock.lock();
            this.callbacks.remove(key);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void onAuthenticated() {
        try {
            lock.lock();
            for(Map.Entry<String, AuthenticationCallbacks> callback : callbacks.entrySet()) {
                callback.getValue().onAuthenticated();
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void onDeauthenticated() {
        try {
            lock.lock();
            for(Map.Entry<String, AuthenticationCallbacks> callback : callbacks.entrySet()) {
                callback.getValue().onDeauthenticated();
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void onAuthenticationFailed(ExceptionBundle error) {
        try {
            lock.lock();
            for(Map.Entry<String, AuthenticationCallbacks> callback : callbacks.entrySet()) {
                callback.getValue().onAuthenticationFailed(error);
            }
        } finally {
            lock.unlock();
        }
    }
}
