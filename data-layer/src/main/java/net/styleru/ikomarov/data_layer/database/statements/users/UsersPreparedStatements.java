package net.styleru.ikomarov.data_layer.database.statements.users;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.mappings.local.users.UsersLocalEntityMapping;

/**
 * Created by i_komarov on 15.05.17.
 */

public class UsersPreparedStatements {

    @NonNull
    private final String SELECT_ALL;

    @NonNull
    private final String SELECT_OFFSET_LIMIT;

    @NonNull
    private final String SELECT_SINGLE;

    @NonNull
    private final SQLiteStatement INSERT_SINGLE;

    @NonNull
    private final SQLiteStatement DELETE_SINGLE;

    public UsersPreparedStatements(@NonNull SQLiteDatabase db) {
        SELECT_ALL = "SELECT * FROM " + UsersLocalEntityMapping.TABLE;
        SELECT_OFFSET_LIMIT = "SELECT * FROM " + UsersLocalEntityMapping.TABLE + " LIMIT ? OFFSET ?";
        SELECT_SINGLE = "SELECT * FROM " + UsersLocalEntityMapping.TABLE + " WHERE " + UsersLocalEntityMapping.COLUMN_ID + " = ?";
        INSERT_SINGLE = db.compileStatement(
                "INSERT OR REPLACE INTO " + UsersLocalEntityMapping.TABLE + "(" +
                        UsersLocalEntityMapping.COLUMN_ID + "," +
                        UsersLocalEntityMapping.COLUMN_FIRST_NAME + "," +
                        UsersLocalEntityMapping.COLUMN_LAST_NAME + "," +
                        UsersLocalEntityMapping.COLUMN_IMAGE_URL + "," +
                        UsersLocalEntityMapping.COLUMN_ROLE + ")" +
                " VALUES (" +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" + "," +
                        "?" +
                ");"

        );
        DELETE_SINGLE = db.compileStatement("DELETE FROM " + UsersLocalEntityMapping.TABLE + " WHERE " + UsersLocalEntityMapping.COLUMN_ID + " = ?");
    }

    @NonNull
    public String selectAll() {
        return SELECT_ALL;
    }

    @NonNull
    public String selectOffsetLimit(int offset, int limit) {
        return SELECT_OFFSET_LIMIT.replaceFirst("\\?", String.valueOf(limit))
                .replaceFirst("\\?", String.valueOf(offset));
    }

    @NonNull
    public String selectSingle(String id) {
        return SELECT_SINGLE.replaceFirst("\\?", "\"" + id + "\"");
    }

    @NonNull
    public SQLiteStatement insertSingle() {
        return INSERT_SINGLE;
    }

    @NonNull
    public SQLiteStatement deleteSingle() {
        return DELETE_SINGLE;
    }
}
