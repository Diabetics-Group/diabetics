package net.styleru.ikomarov.data_layer.http.clients;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.http.base.IClientFactory;
import net.styleru.ikomarov.data_layer.http.interceptors.SessionIdentifyingInterceptor;

import okhttp3.OkHttpClient;

/**
 * Created by i_komarov on 03.05.17.
 */

public class SessionClientsFactory extends SessionlessClientsFactory implements IClientFactory {

    @NonNull
    private final SessionIdentifyingInterceptor interceptor;

    public SessionClientsFactory(@NonNull SessionIdentifyingInterceptor interceptor) {
        this.interceptor = interceptor;
    }

    @Override
    public OkHttpClient create() {
        return prepareBuilder().addInterceptor(interceptor)
                .build();
    }
}
