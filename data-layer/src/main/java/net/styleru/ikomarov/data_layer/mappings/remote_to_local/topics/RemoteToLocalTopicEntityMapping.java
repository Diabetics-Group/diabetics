package net.styleru.ikomarov.data_layer.mappings.remote_to_local.topics;

import net.styleru.ikomarov.data_layer.entities.local.topics.TopicLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.topics.TopicRemoteEntity;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 30.04.17.
 */

public class RemoteToLocalTopicEntityMapping implements Function<TopicRemoteEntity, TopicLocalEntity> {

    @Override
    public TopicLocalEntity apply(TopicRemoteEntity entity) throws Exception {
        return new TopicLocalEntity.Builder(
                        entity.getUserId(),
                        entity.getFirstName(),
                        entity.getLastName(),
                        entity.getImageUrl(),
                        entity.getRole(),
                        entity.getCategoryId(),
                        entity.getTitle(),
                        entity.getContent(),
                        entity.getLastUpdate()
                ).mutateSynchronized(entity.getId())
                .withCommentsCount(entity.getCommentsCount())
                .create();
    }
}
