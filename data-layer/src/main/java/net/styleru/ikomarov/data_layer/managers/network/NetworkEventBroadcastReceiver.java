package net.styleru.ikomarov.data_layer.managers.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.managers.base.ObservablesManager;

/**
 * Created by i_komarov on 26.04.17.
 */

public class NetworkEventBroadcastReceiver extends BroadcastReceiver {

    @NonNull
    public static final Integer TYPE_DISCONNECTED = Integer.MIN_VALUE;

    @NonNull
    private final ConnectivityManager connectivity;

    @NonNull
    private final ObservablesManager<Integer> manager;

    public NetworkEventBroadcastReceiver(@NonNull ConnectivityManager connectivity, @NonNull ObservablesManager<Integer> manager) {
        super();
        this.connectivity = connectivity;
        this.manager = manager;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        manager.notifyAll(connectivity.getActiveNetworkInfo() != null ? connectivity.getActiveNetworkInfo().getType() : TYPE_DISCONNECTED);
    }
}
