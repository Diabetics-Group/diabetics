package net.styleru.ikomarov.data_layer.preferences.app;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.preferences.PreferenceKey;

/**
 * Created by i_komarov on 14.05.17.
 */

public interface IPreferencesChangeListener {

    void onPreferenceChanged(@NonNull SharedPreferences preferences, @NonNull PreferenceKey key);
}
