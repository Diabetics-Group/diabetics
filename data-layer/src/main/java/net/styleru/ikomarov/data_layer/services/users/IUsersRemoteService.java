package net.styleru.ikomarov.data_layer.services.users;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;
import net.styleru.ikomarov.data_layer.routing.base.DefaultParams;
import net.styleru.ikomarov.data_layer.routing.users.UsersRouting;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by i_komarov on 26.04.17.
 */

public interface IUsersRemoteService {

    @GET(Endpoints.ENDPOINT_USERS)
    Single<Response<ResponseContainer<UserRemoteEntity>>> get(
            @Query(UsersRouting.FIELD_USER_ID) String userId
    );

    @GET(Endpoints.ENDPOINT_USERS)
    Single<Response<ResponseContainer<List<UserRemoteEntity>>>> list(
            @Query(DefaultParams.FIELD_OFFSET) int offset,
            @Query(DefaultParams.FIELD_LIMIT) int limit
    );
}
