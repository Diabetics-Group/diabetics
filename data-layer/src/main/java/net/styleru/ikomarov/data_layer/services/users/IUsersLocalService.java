package net.styleru.ikomarov.data_layer.services.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.database.results.DeleteResults;
import net.styleru.ikomarov.data_layer.database.results.InsertResults;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by i_komarov on 26.04.17.
 */

public interface IUsersLocalService {

    Single<InsertResults> create(@NonNull List<UserLocalEntity> entities);

    Single<UserLocalEntity> get(@NonNull String userId);

    Single<List<UserLocalEntity>> list(int offset, int limit);

    Single<DeleteResults> delete(@NonNull List<String> userIds);
}
