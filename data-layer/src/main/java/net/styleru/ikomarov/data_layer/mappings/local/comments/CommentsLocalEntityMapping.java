package net.styleru.ikomarov.data_layer.mappings.local.comments;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

/**
 * Created by i_komarov on 26.04.17.
 */

public class CommentsLocalEntityMapping {

    @NonNull
    public static final String TABLE = "answers";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_TOPIC_ID = "QUESTION_ID";

    @NonNull
    public static final String COLUMN_USER_ID = "USER_ID";

    @NonNull
    public static final String COLUMN_FIRST_NAME = "FIRST_NAME";

    @NonNull
    public static final String COLUMN_LAST_NAME = "LAST_NAME";

    @NonNull
    public static final String COLUMN_IMAGE_URL = "IMAGE_URL";

    @NonNull
    public static final String COLUMN_ROLE = "ROLE";

    @NonNull
    public static final String COLUMN_DATE = "DATE";

    @NonNull
    public static final String COLUMN_CONTENT = "CONTENT";

    @NonNull
    public static final String COLUMN_SYNCHRONIZATION_STATUS = "SYNCHRONIZED";


    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_TOPIC_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_TOPIC_ID;
    public static final String COLUMN_USER_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_USER_ID;
    public static final String COLUMN_FIRST_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_FIRST_NAME;
    public static final String COLUMN_LAST_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_LAST_NAME;
    public static final String COLUMN_IMAGE_URL_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_IMAGE_URL;
    public static final String COLUMN_ROLE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ROLE;
    public static final String COLUMN_DATE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_DATE;
    public static final String COLUMN_CONTENT_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_CONTENT;
    public static final String COLUMN_SYNCHRONIZATION_STATUS_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_SYNCHRONIZATION_STATUS;

    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    private CommentsLocalEntityMapping() {
        throw new IllegalStateException("No instances please");
    }

    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE +
                "(" +
                    COLUMN_ID + " TEXT NOT NULL PRIMARY KEY," +
                    COLUMN_TOPIC_ID + " TEXT," +
                    COLUMN_USER_ID + " TEXT," +
                    COLUMN_FIRST_NAME + " TEXT," +
                    COLUMN_LAST_NAME + " TEXT," +
                    COLUMN_IMAGE_URL + " TEXT," +
                    COLUMN_ROLE + " TEXT," +
                    COLUMN_DATE + " TIMESTAMP NOT NULL DEFAULT(STRFTIME(" + "\'%Y-%m-%d %H:%M:%f\'" + "," + "\'NOW\'" + "))," +
                    COLUMN_CONTENT + " TEXT," +
                    COLUMN_SYNCHRONIZATION_STATUS + " BOOLEAN DEFAULT(0)" +
                ");";

    }

    @NonNull
    public static String getDropTableQuery() {
        return "DROP TABLE IF EXISTS " + TABLE + ";";
    }
}
