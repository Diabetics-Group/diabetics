package net.styleru.ikomarov.data_layer.services.topics;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.database.results.DeleteResults;
import net.styleru.ikomarov.data_layer.database.results.InsertResults;
import net.styleru.ikomarov.data_layer.database.statements.topics.TopicsPreparedStatements;
import net.styleru.ikomarov.data_layer.entities.local.topics.TopicLocalEntity;
import net.styleru.ikomarov.data_layer.mappings.local.topics.TopicsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.services.base.AbstractDatabaseService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import io.reactivex.Single;

/**
 * Created by i_komarov on 28.04.17.
 */

public class TopicsLocalService extends AbstractDatabaseService implements ITopicsLocalService {

    @NonNull
    private final ReentrantLock lock;

    @NonNull
    private final SQLiteDatabase db;

    @NonNull
    private final TopicsPreparedStatements statements;

    private TopicsLocalService(@NonNull SQLiteDatabase db) {
        this.lock = new ReentrantLock(true);
        this.db = db;
        this.statements = new TopicsPreparedStatements(db);
    }

    @Override
    public Single<InsertResults> create(@NonNull List<TopicLocalEntity> entities) {
        return Single.create(emitter -> {
            try {
                long insertedRows = 0L;
                long updatedRows = 0L;
                Map<String, Boolean> statuses = new HashMap<>(entities.size());

                lock.lock();
                db.execSQL("PRAGMA synchronous=\'OFF\'");
                db.beginTransaction();

                SQLiteStatement statement = statements.insertSingle();

                for(TopicLocalEntity entity : entities) {
                    statement.clearBindings();

                    String id;

                    statement.bindString(1, id = entity.getId());
                    statement.bindString(2, entity.getUserId());
                    statement.bindString(3, entity.getFirstName());
                    statement.bindString(4, entity.getLastName());

                    String imageUrl;
                    if(null != (imageUrl = entity.getImageUrl())) {
                        statement.bindString(5, imageUrl);
                    }

                    statement.bindString(6, entity.getRole());
                    statement.bindString(7, entity.getCategoryId());
                    statement.bindString(8, entity.getTitle());
                    statement.bindString(9, entity.getContent());
                    statement.bindString(10, entity.getLastUpdate());
                    statement.bindLong(11, entity.getCommentsCount());
                    statement.bindLong(12, entity.isSynchronized() ? 1L : 0L);

                    long status = statement.executeInsert();
                    if(status != -1L) {
                        if(statuses.get(id) != null) {
                            updatedRows++;
                        } else {
                            insertedRows++;
                        }

                        statuses.put(id, true);
                    } else {
                        statuses.put(id, false);
                    }
                }

                db.setTransactionSuccessful();

                emitter.onSuccess(
                        new InsertResults(
                                TopicsLocalEntityMapping.TABLE,
                                statement.toString(),
                                insertedRows,
                                updatedRows,
                                statuses
                ));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                db.endTransaction();
                db.execSQL("PRAGMA synchronous=\'NORMAL\'");
                lock.unlock();
            }
        });
    }

    @Override
    public Single<TopicLocalEntity> get(@NonNull String id) {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectSingle(id), null);
                emitter.onSuccess(TopicLocalEntity.fromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<List<TopicLocalEntity>> listNotSynchronized() {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectNotSynchronized(), null);
                emitter.onSuccess(TopicLocalEntity.listFromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<List<TopicLocalEntity>> list(@NonNull String categoryId) {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectAll(categoryId), null);
                emitter.onSuccess(TopicLocalEntity.listFromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<List<TopicLocalEntity>> list(@NonNull String categoryId, int offset, int limit) {
        return Single.create(emitter -> {
            Cursor cursor = null;

            try {
                lock.lock();
                cursor = db.rawQuery(statements.selectOffsetLimit(categoryId, offset, limit), null);
                emitter.onSuccess(TopicLocalEntity.listFromCursor(cursor));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                if(cursor != null) {
                    cursor.close();
                }

                lock.unlock();
            }
        });
    }

    @Override
    public Single<DeleteResults> delete(@NonNull List<String> topicIds) {
        return Single.create(emitter -> {
            try {
                long deletedRows = 0L;
                Map<String, Boolean> statuses = new HashMap<>(topicIds.size());

                lock.lock();
                db.execSQL("PRAGMA synchronous=\'OFF\'");
                db.beginTransaction();

                SQLiteStatement statement = statements.deleteSingle();

                for(String id : topicIds) {
                    statement.clearBindings();
                    statement.bindString(1, id);

                    long status = statement.executeUpdateDelete();
                    if(status != 0L && status != -1L) {
                        deletedRows++;
                        statuses.put(id, true);
                    } else {
                        statuses.put(id, false);
                    }
                }

                db.setTransactionSuccessful();

                emitter.onSuccess(
                        new DeleteResults(
                                TopicsLocalEntityMapping.TABLE,
                                statement.toString(),
                                deletedRows,
                                statuses
                        ));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                db.endTransaction();
                db.execSQL("PRAGMA synchronous=\'NORMAL\'");
                lock.unlock();
            }
        });
    }

    @Override
    public Single<DeleteResults> deleteByCategory(@NonNull String categoryId) {
        return Single.create(emitter -> {
            try {
                long deletedRows = 0L;
                Map<String, Boolean> statuses = new HashMap<>(0);

                lock.lock();
                db.execSQL("PRAGMA synchronous=\'OFF\'");
                db.beginTransaction();

                SQLiteStatement statement = statements.deleteByCategory();

                statement.clearBindings();
                statement.bindString(1, categoryId);

                long status = statement.executeUpdateDelete();

                if(status != 0L && status != -1L) {
                    deletedRows += status;
                }

                db.setTransactionSuccessful();

                emitter.onSuccess(
                        new DeleteResults(
                                TopicsLocalEntityMapping.TABLE,
                                statement.toString(),
                                deletedRows,
                                statuses
                        ));
            } catch(Exception e) {
                emitter.onError(parseException(e));
            } finally {
                db.endTransaction();
                db.execSQL("PRAGMA synchronous=\'NORMAL\'");
                lock.unlock();
            }
        });
    }

    public static final class Factory {

        @NonNull
        private final SQLiteDatabase db;

        public Factory(@NonNull SQLiteDatabase db) {
            this.db = db;
        }

        @NonNull
        public ITopicsLocalService create() {
            return new TopicsLocalService(db);
        }
    }
}
