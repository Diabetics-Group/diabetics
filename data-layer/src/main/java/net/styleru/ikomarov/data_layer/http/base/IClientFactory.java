package net.styleru.ikomarov.data_layer.http.base;

import okhttp3.OkHttpClient;

/**
 * Created by i_komarov on 03.05.17.
 */

public interface IClientFactory {

    OkHttpClient create();
}
