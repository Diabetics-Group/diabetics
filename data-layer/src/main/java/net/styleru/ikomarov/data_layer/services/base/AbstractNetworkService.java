package net.styleru.ikomarov.data_layer.services.base;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import net.styleru.ikomarov.data_layer.contracts.exception.InternalExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.NetworkExceptionContract;
import net.styleru.ikomarov.data_layer.contracts.exception.Reason;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

import java.net.UnknownHostException;

import javax.net.ssl.SSLHandshakeException;

import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import okhttp3.OkHttpClient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by i_komarov on 27.04.17.
 */

public abstract class AbstractNetworkService<S> {

    private Retrofit retrofit;

    private S service;

    public AbstractNetworkService(@NonNull String baseUrl, @NonNull OkHttpClient client) {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .client(client)
                .build();

        bindService();
    }

    private void bindService() {
        this.service = createService(retrofit);
    }

    protected abstract S createService(Retrofit retrofit);

    protected abstract Gson getGson();

    protected S getService() {
        return this.service;
    }

    protected final <T> SingleTransformer<T, T> handleError() {
        return upstream -> upstream.onErrorResumeNext(
                throwable -> {
                    return Single.error(parseException(throwable));
                }
        );
    }

    private ExceptionBundle parseException(Throwable thr) {
        ExceptionBundle error;

        if(thr instanceof UnknownHostException) {
            error = new ExceptionBundle(Reason.NETWORK);
            Reason.NETWORK.<NetworkExceptionContract>getContract().putType(error, NetworkExceptionContract.Type.UNAVAILABLE);
        } else if(thr instanceof SSLHandshakeException) {
            error = new ExceptionBundle(Reason.NETWORK);
            Reason.NETWORK.<NetworkExceptionContract>getContract().putType(error, NetworkExceptionContract.Type.SSL_HANDSHAKE_FAILED);
        } else if(thr.getCause() != null && thr.getCause() instanceof ExceptionBundle) {
            error = (ExceptionBundle) thr.getCause();
        } else {
            error = new ExceptionBundle(Reason.INTERNAL);
            InternalExceptionContract contract = Reason.INTERNAL.getContract();
            contract.putThrowable(error, thr);
            contract.putMessage(error, thr.getMessage());
        }

        return error;
    }
}
