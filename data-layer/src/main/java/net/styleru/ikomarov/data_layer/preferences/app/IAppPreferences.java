package net.styleru.ikomarov.data_layer.preferences.app;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 14.05.17.
 */

public interface IAppPreferences {

    @NonNull
    Boolean notificationsEnabled();

    @NonNull
    String versionName();

    @NonNull
    Integer versionCode();

    @NonNull
    String databaseName();

    @NonNull
    Integer databaseVersion();

    @NonNull
    Boolean isOAuth2FlowEnabled();

    @NonNull
    Boolean isBuildDebug();

    void registerPreferencesChangeListener(@NonNull IPreferencesChangeListener listener);

    void unregisterPreferencesChangeListener(@NonNull IPreferencesChangeListener listener);
}
