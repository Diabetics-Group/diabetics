package net.styleru.ikomarov.data_layer.entities.local.categories;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import net.styleru.ikomarov.data_layer.database.utils.CursorIndexCache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static net.styleru.ikomarov.data_layer.mappings.local.categories.CategoriesLocalEntityMapping.*;

/**
 * Created by i_komarov on 26.04.17.
 */

@StorIOSQLiteType(table = TABLE)
public class CategoryLocalEntity {

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_ID, key = true)
    String id;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_TITLE, ignoreNull = true)
    String title;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_LAST_UPDATE, ignoreNull = true)
    String lastUpdate;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_TOPICS_COUNT, ignoreNull = true)
    Long topicsCount;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_SYNCHRONIZATION_STATUS, ignoreNull = true)
    Boolean isSynchronized;

    @NonNull
    public static CategoryLocalEntity fromCursor(@NonNull Cursor cursor) {
        CategoryLocalEntity entity = new CategoryLocalEntity();

        entity.id = cursor.getString(cursor.getColumnIndex(COLUMN_ID));
        entity.title = cursor.getString(cursor.getColumnIndex(COLUMN_TITLE));
        entity.lastUpdate = cursor.getString(cursor.getColumnIndex(COLUMN_LAST_UPDATE));
        entity.topicsCount = cursor.getLong(cursor.getColumnIndex(COLUMN_TOPICS_COUNT));
        entity.isSynchronized = cursor.getLong(cursor.getColumnIndex(COLUMN_SYNCHRONIZATION_STATUS)) == 1L;

        return entity;
    }

    @NonNull
    public static List<CategoryLocalEntity> listFromCursor(@NonNull Cursor cursor) {
        CursorIndexCache cache = new CursorIndexCache();
        List<CategoryLocalEntity> entities = new ArrayList<>(cursor.getCount());
        if(cursor.moveToFirst()) {
            do {
                CategoryLocalEntity entity = new CategoryLocalEntity();
                entity.id = cursor.getString(cache.index(cursor, COLUMN_ID));
                entity.title = cursor.getString(cache.index(cursor, COLUMN_TITLE));
                entity.lastUpdate = cursor.getString(cache.index(cursor, COLUMN_LAST_UPDATE));
                entity.topicsCount = cursor.getLong(cache.index(cursor, COLUMN_TOPICS_COUNT));
                entity.isSynchronized = cursor.getLong(cache.index(cursor, COLUMN_SYNCHRONIZATION_STATUS)) == 1L;
                entities.add(entity);
            } while(cursor.moveToNext());
        }

        return Collections.unmodifiableList(entities);
    }

    CategoryLocalEntity() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getLastUpdate() {
        return lastUpdate;
    }

    @NonNull
    public Long getTopicsCount() {
        return topicsCount;
    }

    @NonNull
    public Boolean isSynchronized() {
        return isSynchronized;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryLocalEntity that = (CategoryLocalEntity) o;

        if (!id.equals(that.id)) return false;
        if (!title.equals(that.title)) return false;
        if (lastUpdate != null ? !lastUpdate.equals(that.lastUpdate) : that.lastUpdate != null)
            return false;
        if (topicsCount != null ? !topicsCount.equals(that.topicsCount) : that.topicsCount != null)
            return false;
        return isSynchronized.equals(that.isSynchronized);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + (lastUpdate != null ? lastUpdate.hashCode() : 0);
        result = 31 * result + (topicsCount != null ? topicsCount.hashCode() : 0);
        result = 31 * result + isSynchronized.hashCode();
        return result;
    }

    public static final class Builder {

        @NonNull
        private CategoryLocalEntity entity;

        public Builder(@NonNull String title, @NonNull String lastUpdate) {
            entity = new CategoryLocalEntity();
            entity.title = title;
            entity.lastUpdate = lastUpdate;
        }

        @NonNull
        public SynchronizedBuilder mutateSynchronized(@NonNull String id) {
            entity.id = id;
            return new SynchronizedBuilder(entity);
        }

        @NonNull
        public CategoryLocalEntity create(@NonNull UUID generatedId) {
            entity.id = generatedId.toString();
            entity.isSynchronized = false;
            return this.entity;
        }
    }

    public static final class SynchronizedBuilder {

        @NonNull
        private final CategoryLocalEntity entity;

        private SynchronizedBuilder(@NonNull CategoryLocalEntity preBuiltEntity) {
            entity = preBuiltEntity;
            entity.isSynchronized = true;
        }

        @NonNull
        public SynchronizedBuilder withTopicsCount(@NonNull Long topicsCount) {
            entity.topicsCount = topicsCount;
            return this;
        }

        @NonNull
        public CategoryLocalEntity create() {
            return entity;
        }
    }

    @Override
    public String toString() {
        return "CategoryLocalEntity{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", lastUpdate='" + lastUpdate + '\'' +
                ", topicsCount=" + topicsCount +
                ", isSynchronized=" + isSynchronized +
                '}';
    }
}
