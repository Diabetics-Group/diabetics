package net.styleru.ikomarov.data_layer.managers.base;

/**
 * Created by i_komarov on 26.04.17.
 */

public interface ICallback<E> {

    void onRegistered();

    void onEvent(E event);

    void onUnregistered();
}
