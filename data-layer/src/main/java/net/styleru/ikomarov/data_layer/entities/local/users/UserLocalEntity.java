package net.styleru.ikomarov.data_layer.entities.local.users;

import android.database.Cursor;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.database.utils.CursorIndexCache;
import net.styleru.ikomarov.data_layer.mappings.local.users.UsersLocalEntityMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i_komarov on 30.04.17.
 */

public class UserLocalEntity {

    @NonNull
    private final String id;

    @NonNull
    private final String token;

    @NonNull
    private final String firstName;

    @NonNull
    private final String lastName;

    @NonNull
    private final String imageUrl;

    @NonNull
    private final String phone;

    @NonNull
    private final String role;

    public static UserLocalEntity fromCursor(@NonNull Cursor cursor) {
        return new UserLocalEntity(
                cursor.getString(cursor.getColumnIndex(UsersLocalEntityMapping.COLUMN_ID)),
                null,
                cursor.getString(cursor.getColumnIndex(UsersLocalEntityMapping.COLUMN_FIRST_NAME)),
                cursor.getString(cursor.getColumnIndex(UsersLocalEntityMapping.COLUMN_LAST_NAME)),
                cursor.getString(cursor.getColumnIndex(UsersLocalEntityMapping.COLUMN_IMAGE_URL)),
                null,
                cursor.getString(cursor.getColumnIndex(UsersLocalEntityMapping.COLUMN_ROLE))
        );
    }

    public static List<UserLocalEntity> listFromCursor(@NonNull Cursor cursor) {
        final CursorIndexCache cache = new CursorIndexCache();
        List<UserLocalEntity> entities = new ArrayList<>(cursor.getCount());
        if(cursor.moveToFirst()) {
            do {
                entities.add(
                        new UserLocalEntity(
                                cursor.getString(cache.index(cursor, UsersLocalEntityMapping.COLUMN_ID)),
                                null,
                                cursor.getString(cache.index(cursor, UsersLocalEntityMapping.COLUMN_FIRST_NAME)),
                                cursor.getString(cache.index(cursor, UsersLocalEntityMapping.COLUMN_LAST_NAME)),
                                cursor.getString(cache.index(cursor, UsersLocalEntityMapping.COLUMN_IMAGE_URL)),
                                null,
                                cursor.getString(cache.index(cursor, UsersLocalEntityMapping.COLUMN_ROLE))
                        )
                );
            } while (cursor.moveToNext());
        }

        return entities;
    }

    public UserLocalEntity(@NonNull String id,
                           @NonNull String token,
                           @NonNull String firstName,
                           @NonNull String lastName,
                           @NonNull String imageUrl,
                           @NonNull String phone,
                           @NonNull String role) {
        this.id = id;
        this.token = token;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.phone = phone;
        this.role = role;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getToken() {
        return token;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }

    @NonNull
    public String getPhone() {
        return phone;
    }

    @NonNull
    public String getRole() {
        return role;
    }
}
