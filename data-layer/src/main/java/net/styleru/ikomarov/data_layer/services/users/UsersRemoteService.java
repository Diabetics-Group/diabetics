package net.styleru.ikomarov.data_layer.services.users;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;
import net.styleru.ikomarov.data_layer.services.base.AbstractNetworkService;

import java.util.List;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by i_komarov on 29.04.17.
 */

public class UsersRemoteService extends AbstractNetworkService<IUsersRemoteService> implements IUsersRemoteService {

    private UsersRemoteService(@NonNull OkHttpClient client) {
        super(Endpoints.URL_DIABETICS, client);
    }

    @Override
    public Single<Response<ResponseContainer<UserRemoteEntity>>> get(String userID) {
        return getService().get(userID)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<UserRemoteEntity>>>> list(int offset, int limit) {
        return getService().list(offset, limit)
                .compose(handleError());
    }

    @Override
    protected IUsersRemoteService createService(Retrofit retrofit) {
        return retrofit.create(IUsersRemoteService.class);
    }

    @Override
    protected Gson getGson() {
        return new Gson();
    }

    public static final class Factory {

        @NonNull
        private final OkHttpClient client;

        public Factory(@NonNull OkHttpClient client) {
            this.client = client;
        }

        @NonNull
        public IUsersRemoteService create() {
            return new UsersRemoteService(client);
        }
    }
}
