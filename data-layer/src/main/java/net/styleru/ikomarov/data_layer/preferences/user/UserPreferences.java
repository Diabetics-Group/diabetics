package net.styleru.ikomarov.data_layer.preferences.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.contracts.user.UserPreferencesContract;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;

/**
 * Created by i_komarov on 30.04.17.
 */

public class UserPreferences implements IUserPreferences {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final SharedPreferences preferences;

    @NonNull
    private final UserPreferencesContract contract;

    @Nullable
    private volatile UserLocalEntity cached;

    private UserPreferences(@NonNull SharedPreferences preferences) {
        this.preferences = preferences;
        this.contract = new UserPreferencesContract();
        synchronized (lock) {
            cached = contract.load(preferences);
        }
    }

    @Override
    public void invalidate() {
        synchronized (lock) {
            contract.invalidate(preferences);
        }
    }

    @Override
    public void save(@NonNull UserLocalEntity entity) {
        synchronized (lock) {
            contract.save(preferences, cached = entity);
        }
    }

    @Nullable
    @Override
    public UserLocalEntity load() {
        synchronized (lock) {
            return cached != null ? (cached) : (cached = contract.load(preferences));
        }
    }

    public static final class Factory {

        @NonNull
        private static final String STORAGE_NAME = "user_preferences";

        @NonNull
        private final Context context;

        public Factory(@NonNull Context context) {
            this.context = context;
        }

        public IUserPreferences create() {
            return new UserPreferences(context.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE));
        }
    }
}
