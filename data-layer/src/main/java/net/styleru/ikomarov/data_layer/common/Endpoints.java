package net.styleru.ikomarov.data_layer.common;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 26.04.17.
 */

public class Endpoints {

    @NonNull
    public static final String URL_LAYER = "https://api.layer.com";

    @NonNull
    public static final String URL_DIABETICS = "http://medapi.space/diabetics/api/";

    @NonNull
    public static final String ENDPOINT_USERS = "users";

    @NonNull
    public static final String ENDPOINT_CATEGORIES = "categories";

    @NonNull
    public static final String ENDPOINT_TOPICS = "questions";

    @NonNull
    public static final String ENDPOINT_COMMENTS = "answers";

    @NonNull
    public static final String ENDPOINT_PHONE_CODE = "getcode";

    @NonNull
    public static final String ENDPOINT_AUTHORIZE = "authorise";

    @NonNull
    public static final String ENDPOINT_REGISTER = "registration";

    @NonNull
    public static final String ENDPOINT_TOKEN_APPLICATION = "token";

    @NonNull
    public static final String ENDPOINT_TOKEN_NOTIFICATIONS = "tokenpost";

    @NonNull
    public static final String ENDPOINT_PHOTO_UPLOAD = "image";

    @NonNull
    public static final String ENDPOINT_REPORT = "report";
}
