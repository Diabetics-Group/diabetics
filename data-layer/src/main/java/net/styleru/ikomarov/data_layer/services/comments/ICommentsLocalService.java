package net.styleru.ikomarov.data_layer.services.comments;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.database.results.DeleteResults;
import net.styleru.ikomarov.data_layer.database.results.InsertResults;
import net.styleru.ikomarov.data_layer.entities.local.comments.CommentLocalEntity;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by i_komarov on 26.04.17.
 */

public interface ICommentsLocalService {

    Single<InsertResults> create(List<CommentLocalEntity> entities);

    Single<List<CommentLocalEntity>> list(@NonNull String topicId);

    Single<List<CommentLocalEntity>> list(@NonNull String topicId, int offset, int limit);

    Single<List<CommentLocalEntity>> listNotSynchronized();

    Single<DeleteResults> delete(@NonNull List<String> ids);

    Single<DeleteResults> deleteByTopic(@NonNull String topicId);
}
