package net.styleru.ikomarov.data_layer.exceptions;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.base.IParcelableContainer;
import net.styleru.ikomarov.data_layer.contracts.exception.Reason;

import java.io.Serializable;

/**
 * Created by i_komarov on 26.04.17.
 */

public class ExceptionBundle extends Exception implements IParcelableContainer {

    @NonNull
    private final Reason reason;

    @NonNull
    private final Bundle extras;

    private ExceptionBundle(@NonNull Reason reason, @NonNull Bundle extras) {
        this.reason = reason;
        this.extras = extras;
    }

    public ExceptionBundle(@NonNull Reason reason) {
        this.reason = reason;
        this.extras = new Bundle();
    }

    @NonNull
    public Reason getReason() {
        return this.reason;
    }

    @Override
    public void putString(String key, String value) {
        this.extras.putString(key, value);
    }

    @Override
    public void putInt(String key, int value) {
        this.extras.putInt(key, value);
    }

    @Override
    public void putBoolean(String key, boolean value) {
        this.extras.putBoolean(key, value);
    }

    @Override
    public void putParcelable(String key, Parcelable value) {
        this.extras.putParcelable(key, value);
    }

    @Override
    public void putSerializable(String key, Serializable value) {
        this.extras.putSerializable(key, value);
    }

    @Override
    public String getString(String key) {
        return this.extras.getString(key);
    }

    @Override
    public int getInt(String key) {
        return this.extras.getInt(key);
    }

    @Override
    public boolean getBoolean(String key) {
        return this.extras.getBoolean(key);
    }

    @Override
    public Parcelable getParcelable(String key) {
        return this.extras.getParcelable(key);
    }

    @Override
    public Serializable getSerializable(String key) {
        return this.extras.getSerializable(key);
    }

    @NonNull
    public ExceptionBundle copy() {
        ExceptionBundle copy = new ExceptionBundle(reason, extras != null ? new Bundle(extras) : new Bundle());
        return copy;
    }
}
