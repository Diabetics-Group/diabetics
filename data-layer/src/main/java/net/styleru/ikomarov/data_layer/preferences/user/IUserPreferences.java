package net.styleru.ikomarov.data_layer.preferences.user;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;

/**
 * Created by i_komarov on 30.04.17.
 */

public interface IUserPreferences {

    void invalidate();

    void save(@NonNull UserLocalEntity entity);

    UserLocalEntity load();
}
