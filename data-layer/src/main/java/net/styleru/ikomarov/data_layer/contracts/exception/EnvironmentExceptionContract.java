package net.styleru.ikomarov.data_layer.contracts.exception;

import android.support.annotation.NonNull;
import android.util.SparseArray;

import net.styleru.ikomarov.data_layer.contracts.base.ContractAccessor;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 30.04.17.
 */

public class EnvironmentExceptionContract extends ContractAccessor {

    @NonNull
    private static final String PREFIX = EnvironmentExceptionContract.class.getCanonicalName() + ".";

    @NonNull
    private final String KEY_TYPE = PREFIX + "TYPE";

    public EnvironmentExceptionContract() {

    }

    @NonNull
    public Type extractType(@NonNull ExceptionBundle bundle) {
        return Type.forValue(bundle.getInt(KEY_TYPE));
    }

    public void putType(@NonNull ExceptionBundle bundle, @NonNull Type type) {
        bundle.putInt(KEY_TYPE, type.code);
    }

    public enum Type {
        CACHE_EMPTY         (0x0000000),
        RESOURCE_NOT_EXISTS (0x0000001),
        REMOTE_DB_EMPTY     (0x0000002);

        private static final SparseArray<Type> typesMap = new SparseArray<>(Type.values().length);

        static {
            for(Type type : Type.values()) {
                typesMap.put(type.code, type);
            }
        }

        private final int code;

        public static Type forValue(int code) {
            return typesMap.get(code);
        }

        Type(int code) {
            this.code = code;
        }
    }
}
