package net.styleru.ikomarov.data_layer.services.categories;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.base.ResponseContainer;
import net.styleru.ikomarov.data_layer.entities.remote.categories.CategoryRemoteEntity;
import net.styleru.ikomarov.data_layer.services.base.AbstractNetworkService;

import java.util.List;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by i_komarov on 27.04.17.
 */

public class CategoriesRemoteService extends AbstractNetworkService<ICategoriesRemoteService> implements ICategoriesRemoteService {

    private CategoriesRemoteService(@NonNull OkHttpClient client) {
        super(Endpoints.URL_DIABETICS, client);
    }

    @Override
    public Single<Response<ResponseContainer<List<CategoryRemoteEntity>>>> list() {
        return getService().list()
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<CategoryRemoteEntity>>>> list(String lastModified) {
        return getService().list(lastModified)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<CategoryRemoteEntity>>>> list(int offset, int limit) {
        return getService().list(offset, limit)
                .compose(handleError());
    }

    @Override
    public Single<Response<ResponseContainer<List<CategoryRemoteEntity>>>> list(String lastUpdate, int offset, int limit) {
        return getService().list(lastUpdate, offset, limit)
                .compose(handleError());
    }

    @Override
    protected ICategoriesRemoteService createService(Retrofit retrofit) {
        return retrofit.create(ICategoriesRemoteService.class);
    }

    @Override
    protected Gson getGson() {
        return new Gson();
    }

    public static final class Factory {

        @NonNull
        private final OkHttpClient client;

        public Factory(@NonNull OkHttpClient client) {
            this.client = client;
        }

        @NonNull
        public ICategoriesRemoteService create() {
            return new CategoriesRemoteService(client);
        }
    }
}
