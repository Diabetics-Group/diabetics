package net.styleru.ikomarov.data_layer.mappings.remote_to_local.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 15.05.17.
 */

public class RemoteToLocalUsersEntityMapping implements Function<List<UserRemoteEntity>, List<UserLocalEntity>> {

    @NonNull
    private final Function<UserRemoteEntity, UserLocalEntity> singleMapper;

    public RemoteToLocalUsersEntityMapping(@NonNull Function<UserRemoteEntity, UserLocalEntity> singleMapper) {
        this.singleMapper = singleMapper;
    }

    @Override
    public List<UserLocalEntity> apply(List<UserRemoteEntity> userRemoteEntities) throws Exception {
        return Observable.fromIterable(userRemoteEntities)
                .map(singleMapper)
                .reduce(new ArrayList<UserLocalEntity>(userRemoteEntities.size()), (list, element) -> {
                    list.add(element);
                    return list;
                })
                .blockingGet();
    }
}
