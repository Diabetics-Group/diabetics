package net.styleru.ikomarov.domain_layer_test.services.categories;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.BuildConfig;
import net.styleru.ikomarov.data_layer.database.helper.DBOpenHelper;
import net.styleru.ikomarov.data_layer.database.results.InsertResults;
import net.styleru.ikomarov.data_layer.entities.local.categories.CategoryLocalEntity;
import net.styleru.ikomarov.data_layer.mappings.local.categories.CategoriesLocalEntityMapping;
import net.styleru.ikomarov.data_layer.services.categories.CategoriesLocalService;
import net.styleru.ikomarov.data_layer.services.categories.ICategoriesLocalService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Arrays;
import java.util.List;

import io.reactivex.SingleSource;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.TestObserver;

/**
 * Created by i_komarov on 29.04.17.
 */

@RunWith(RobolectricTestRunner.class)
@Config(minSdk = 17, constants = BuildConfig.class)
public class CategoriesLocalServiceTest {

    @NonNull
    private SQLiteDatabase db;

    @NonNull
    private CategoriesLocalService.Factory serviceFactory;

    @NonNull
    private ICategoriesLocalService service;

    @Before
    public void setUp() {
        db = new DBOpenHelper(RuntimeEnvironment.application).getWritableDatabase();
        serviceFactory = new CategoriesLocalService.Factory(db);
        service = serviceFactory.create();
    }

    @Test
    public void test_insert() {
        TestObserver<InsertResults> observer = new TestObserver<>();

        service.create(Arrays.asList(
                new CategoryLocalEntity.Builder("title 1", "2017-09-15 21:01:00.001")
                        .mutateSynchronized("id_1")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 2", "2017-09-15 21:01:00.003")
                        .mutateSynchronized("id_2")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 3", "2017-09-15 21:01:00.007")
                        .mutateSynchronized("id_3")
                        .withTopicsCount(10L)
                        .create()
        )).subscribe(observer);

        observer.assertSubscribed();
        observer.assertNoErrors();
        observer.assertValueCount(1);
        observer.assertValue(new Predicate<InsertResults>() {
            @Override
            public boolean test(InsertResults results) throws Exception {
                return results.getInsertedRows() == 3 &&
                        results.getStatuses().get("id_1") &&
                        results.getStatuses().get("id_2") &&
                        results.getStatuses().get("id_3") &&
                        results.getTable().equals(CategoriesLocalEntityMapping.TABLE);
            }
        });
    }

    @Test
    public void test_insertWithConflict() {
        TestObserver<InsertResults> observer = new TestObserver<>();

        service.create(Arrays.asList(
                new CategoryLocalEntity.Builder("title 1", "2017-09-15 21:01:00.001")
                        .mutateSynchronized("id_1")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 2", "2017-09-15 21:01:00.003")
                        .mutateSynchronized("id_2")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 2", "2017-09-15 21:01:00.007")
                        .mutateSynchronized("id_2")
                        .withTopicsCount(10L)
                        .create()
        )).subscribe(observer);

        observer.assertSubscribed();
        observer.assertNoErrors();
        observer.assertValueCount(1);
        observer.assertValue(new Predicate<InsertResults>() {
            @Override
            public boolean test(InsertResults results) throws Exception {
                return results.getInsertedRows() == 2 &&
                        results.getUpdatedRows() == 1 &&
                        results.getStatuses().get("id_1") &&
                        results.getStatuses().get("id_2") &&
                        results.getTable().equals(CategoriesLocalEntityMapping.TABLE);
            }
        });
    }

    @Test
    public void test_readOffsetLimit() {
        TestObserver<List<CategoryLocalEntity>> observer = new TestObserver<>();

        service.create(Arrays.asList(
                new CategoryLocalEntity.Builder("title 1", "2017-09-15 21:01:00.001")
                        .mutateSynchronized("id_1")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 2", "2017-09-15 21:01:00.003")
                        .mutateSynchronized("id_2")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 3", "2017-09-15 21:01:00.007")
                        .mutateSynchronized("id_3")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 4", "2017-09-15 21:01:00.007")
                        .mutateSynchronized("id_4")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 5", "2017-09-15 21:01:00.007")
                        .mutateSynchronized("id_5")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 6", "2017-09-15 21:01:00.007")
                        .mutateSynchronized("id_6")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 7", "2017-09-15 21:01:00.007")
                        .mutateSynchronized("id_7")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 8", "2017-09-15 21:01:00.007")
                        .mutateSynchronized("id_8")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 9", "2017-09-15 21:01:00.007")
                        .mutateSynchronized("id_9")
                        .withTopicsCount(10L)
                        .create(),
                new CategoryLocalEntity.Builder("title 10", "2017-09-15 21:01:00.007")
                        .mutateSynchronized("id_10")
                        .withTopicsCount(10L)
                        .create()
        )).flatMap(new Function<InsertResults, SingleSource<List<CategoryLocalEntity>>>() {
            @Override
            public SingleSource<List<CategoryLocalEntity>> apply(InsertResults insertResults) throws Exception {
                return service.list(1, 2);
            }
        }).subscribe(observer);

        observer.assertSubscribed();
        observer.assertNoErrors();
        observer.assertValueCount(1);
        observer.assertValue(new Predicate<List<CategoryLocalEntity>>() {
            @Override
            public boolean test(List<CategoryLocalEntity> entities) throws Exception {
                return entities.size() == 2 &&
                        entities.get(0).getId().equals("id_2") &&
                        entities.get(1).getId().equals("id_3");
            }
        });
    }
}
